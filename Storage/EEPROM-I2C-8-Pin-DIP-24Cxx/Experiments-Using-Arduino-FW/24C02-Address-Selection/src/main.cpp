#include <Arduino.h>
#include <LoopUtil.h>
#include <SourceInfo.h>
#include <Wire.h>

#define TRIGGER_PIN 2
#define TEST_I2C_INDICATOR_PIN 3

boolean debugFlag = true;
uint32_t loopDuration = 1000;
LoopUtil loopUtil(debugFlag, TRIGGER_PIN, loopDuration);

// address selection pins (i.e., the LSBs of the I2C address)
#define PIN_ADDRESS_2 12
#define PIN_ADDRESS_1 11
#define PIN_ADDRESS_0 10

// the current values of the address selection pins
int currentAddress2;
int currentAddress1;
int currentAddress0;

// the 7 and 8 bit I2C address (w/o and w RW flag)
int current7BitAddress;
int current8BitAddress;

// test address range: we start at one address below the allowed range
// of I2C addresses and end at one address above the allowed range.
#define MIN_TEST_ADDRESS (0x50 - 1)
#define MAX_TEST_ADDRESS (0x50 + 7 + 1)

int compute7BitI2CAddress(int iAddress2, int iAddress1, int iAddress0)
{
  int result = 0x50;
  if (iAddress2 != 0)
  {
    result |= 4;
  }
  if (iAddress1 != 0)
  {
    result |= 2;
  }
  if (iAddress0 != 0)
  {
    result |= 1;
  }
  return result;
}

void computeCurrentI2CAddresses()
{
  Serial.print(F("A2="));
  Serial.print(currentAddress2);
  Serial.print(F(" A1="));
  Serial.print(currentAddress1);
  Serial.print(F(" A0="));
  Serial.print(currentAddress0);
  current7BitAddress =
      compute7BitI2CAddress(currentAddress2, currentAddress1, currentAddress0);
  Serial.print(F(" current7BitAddress="));
  Serial.print(current7BitAddress, BIN);
  current8BitAddress = current7BitAddress << 1;
  Serial.print(F(" current8BitAddress="));
  Serial.print(current8BitAddress, BIN);
  Serial.println();
}

void setupSerial()
{
  Serial.begin(115200);
  while (!Serial)
  {
    ;
  }
  SOURCE_INFO_ENTERING("setupSerial")
  SOURCE_INFO_PRINT
  SOURCE_INFO_LEAVING("setupSerial")
}

void setCurrentAddressPins()
{
  digitalWrite(PIN_ADDRESS_0, currentAddress0);
  digitalWrite(PIN_ADDRESS_1, currentAddress1);
  digitalWrite(PIN_ADDRESS_2, currentAddress2);
}

void setupAddressPins()
{
  SOURCE_INFO_ENTERING("setupAddressPins")
  pinMode(PIN_ADDRESS_0, OUTPUT);
  pinMode(PIN_ADDRESS_1, OUTPUT);
  pinMode(PIN_ADDRESS_2, OUTPUT);
  currentAddress2 = 0;
  currentAddress1 = 0;
  currentAddress0 = 0;
  setCurrentAddressPins();
  pinMode(TEST_I2C_INDICATOR_PIN, OUTPUT);
  digitalWrite(TEST_I2C_INDICATOR_PIN, LOW);
  SOURCE_INFO_LEAVING("setupAddressPins")
}

void setup()
{
  setupSerial();
  setupAddressPins();
  loopUtil.setNewLineAfterLoopEnd(true);
  SOURCE_INFO_SNIPPET_START("main")
}

boolean done; // indicates that ALL addresses were checked, so we are done

void incrementAddressPins()
{
  done = (currentAddress0 == 1) && (currentAddress1 == 1) &&
         (currentAddress2 == 1);
  if (done)
  {
    Serial.println(F("Mission completed!"));
  }
  else
  {
    currentAddress0++;
    if (currentAddress0 > 1)
    {
      currentAddress0 = 0;
      currentAddress1++;
      if (currentAddress1 > 1)
      {
        currentAddress1 = 0;
        currentAddress2++;
      }
    }
  }
}

void loop()
{
  loopUtil.loopStart();

  if (done)
  {
    SOURCE_INFO_BAILING_OUT("We are done with all valid I2C addresses.")
  }

  setCurrentAddressPins();
  computeCurrentI2CAddresses();
  int successAddress = 0;
  SOURCE_INFO_NL

  for (int testAddress = MIN_TEST_ADDRESS; testAddress <= MAX_TEST_ADDRESS;
       testAddress++)
  {
    Serial.print(F("test address: "));
    Serial.print(testAddress, BIN);
    delay(2);
    digitalWrite(TEST_I2C_INDICATOR_PIN, HIGH); // raise indicator for analyzers
    Wire.begin();
    Wire.beginTransmission(testAddress);
    int rc = Wire.endTransmission(true);
    if (!rc)
    {
      successAddress = testAddress;
    }
    Wire.end();
    digitalWrite(TEST_I2C_INDICATOR_PIN,
                 LOW); // lower indicator for analyzers   
    Serial.print(F(" => result: "));
    Serial.print(rc);
    Serial.println(rc == 0 ? F(" (success)") : F(" (failure)"));
    delay(100);
  }

  SOURCE_INFO_NL
  Serial.print(F("success address: "));
  Serial.println(successAddress, BIN);
  incrementAddressPins();
  loopUtil.loopEnd();

  if (done)
  {
    SOURCE_INFO_SNIPPET_END("main")
  }
}