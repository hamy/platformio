# import libraries
from sys import argv
from urllib.request import Request, urlopen
from bs4 import BeautifulSoup
 
# specify the url
urlpage = argv[-1] # 'https://bl.ocks.org/mbostock/raw/76456029b13c68accf6e/' 
req = Request(urlpage, headers={'User-Agent': 'Mozilla/5.0'})
page = urlopen(req).read()

# parse the html using beautiful soup and store in variable 'soup'
soup = BeautifulSoup(page, 'html.parser')

# find <script> items
results = soup.find_all('script')
resu = results[-1]
print(resu.getText())