inPath = args[0];
println('inPath: ' + inPath);
outPath = args[1];
println('outPath: ' + outPath);

commonPrefix = '####-DATA-BOUNDARY-LINE-####-';
startSuffix = '@START';
endSuffix = '@END';
startLine = commonPrefix + startSuffix;
endLine = commonPrefix + endSuffix;
xfer = false;

inFile = new File(inPath);
inLines = inFile.readLines();
println('read ' + inLines.size() + ' input lines');
outText = "";

inLines.each() { inLine ->
  work = inLine.trim();
  if (work.equals(startLine)) {
    xfer = true;
  } else if (work.equals(endLine)) {
    xfer = false;
  } else {
    if (xfer) {
      outText += work + '\n';
    }
  }
}

outFile = new File(outPath);
outFile.text = outText;

