import serial

# open the serial connection
port = '/dev/ttyACM0'
baudrate = 115200
ser = serial.Serial(port,baudrate);

# open the output file
path = 'serial.data'
file = open(path,'w')

# copy and flush at line ends
while True:
    while ser.in_waiting>0:
        by = ser.read()
        try:
            file.write(by.decode(encoding="utf-8"))
        except:
            print('Caught an error, continue...')
        if by==0x0a:
            file.flush()
        if by==0x0d:
            file.flush()
