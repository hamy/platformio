#include <Arduino.h>

const int LED_PIN = 3;

const int MAX_DELAY = 500;
const int MIN_DELAY = 50;
const int DELAY_STEP_NUM = 10;
const int DELAY_STEP_DEN = 11;

int currentDelay = MAX_DELAY;

void setup() {
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, LOW);
}

void loop() {
  digitalWrite(LED_PIN, HIGH);
  delay(currentDelay);
  digitalWrite(LED_PIN, LOW);
  delay(currentDelay);
  currentDelay *= DELAY_STEP_NUM;
  currentDelay /= DELAY_STEP_DEN;
  if (currentDelay < MIN_DELAY) {
    currentDelay = MAX_DELAY;
  }
}
