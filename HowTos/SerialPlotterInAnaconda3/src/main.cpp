/*
 * main.cpp: Converts garbage voltages from port 0 through 5 and
 * prints the corresponding values as whitespace separated token line.
 */

#include <Arduino.h>

void setup()
{
  Serial.begin(115200);
}

float adcFactor = 5.0/1023.0;

void loop()
{
  for (int port=0; port<=5; port++) {
    if (port>0) {
      Serial.print(" ");  
    }    
    float value=adcFactor * analogRead(port);
    Serial.print(value);
  }
  Serial.println();
  delay(50);
}
