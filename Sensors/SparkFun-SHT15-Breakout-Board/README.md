# SparkFun SHT15 Breakout Board


Once upon a time, [Sensirion](https://www.sensirion.com/) produced a tiny SMD humidity and temperature sensor
SHT15. Note: The SHT1x family of sensors is now obsolete; so these products are now retired. For new applications,
use the successors of those sensors. I mirrored the old [datasheet](datasheets/SHT1x_datasheet.pdf) locally. 

![sht15.jpg](images/sht15.jpg)

(image cropped from the Sensirion datasheet)

[SparkFun](https://www.sparkfun.com) created 2 breakout boards for the SHT15, this experiment uses the older version.
Like the underlying sensor itself, the breakout board is now retired.

![sht15-breakout.jpg](images/sht15-breakout.jpg)

(image combined from very old [SparkFun](https://www.sparkfun.com/) pages)
