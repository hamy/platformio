/**
 */

#include <Arduino.h>
#include <SHT1x.h>

// Specify data and clock pins
#define SDA 11
#define SCL 12

// initialize sensor object
SHT1x sht1x(SDA, SCL);

// duration for one loop in milliseconds
#define LOOP_DURATION ((unsigned long)100)

// the builtin LED is toggled at the start of each loop
bool toggleLedFlag = false;

void toggleLed() {
  toggleLedFlag = !toggleLedFlag;
  digitalWrite(LED_BUILTIN, toggleLedFlag);
}

// variable for storing the most recent measurement results
float currentTemperatureInCelsius;
float currentTemperatureInFahrenheit;
float currentHumidity;

void printHeader() {
  Serial.println(F("Temperature[°C],Temperature[°F],Humidity[%]"));
}

void measureCurrentTemperatureAndHumidity() {
  currentTemperatureInCelsius = sht1x.readTemperatureC();
  currentTemperatureInFahrenheit = sht1x.readTemperatureF();
  currentHumidity = sht1x.readHumidity();
}

void printCurrentMeasurementValues() {
  Serial.print(currentTemperatureInCelsius, DEC);
  Serial.print(",");
  Serial.print(currentTemperatureInFahrenheit, DEC);
  Serial.print(",");
  Serial.print(currentHumidity, DEC);
  Serial.println();
}

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(9600);
  printHeader();
}

void waitForLoopEnd(unsigned long timeAtLoopStart) {
  unsigned long timeAtLoopEnd = timeAtLoopStart + LOOP_DURATION;
  unsigned long now = millis();
  if (now < timeAtLoopEnd && timeAtLoopStart < timeAtLoopEnd) {
    delay(timeAtLoopEnd - now);
  }
}

void loop() {
  unsigned long timeAtLoopStart = millis();
  toggleLed();
  measureCurrentTemperatureAndHumidity();
  printCurrentMeasurementValues();
  waitForLoopEnd(timeAtLoopStart);
}
