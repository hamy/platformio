#include <Dps310.h>

// Dps310 Opject
Dps310 Dps310PressureSensor = Dps310();

boolean calibrating = true;
int nCalib = 10;
float Rasterisk = 287.05;
float M = 0.02896;
float g = 9.80665;
float p0;
float z1, z1Sum;
int loopCount = 0;

void setup()
{
  Serial.begin(9600);
  while (!Serial);


  //Call begin to initialize Dps310PressureSensor
  //The parameter 0x76 is the bus address. The default address is 0x77 and does not need to be given.
  //Dps310PressureSensor.begin(Wire, 0x76);
  //Use the commented line below instead of the one above to use the default I2C address.
  //if you are using the Pressure 3 click Board, you need 0x76
  Dps310PressureSensor.begin(Wire);
  loopCount = 0;
  z1Sum = 0;
  Serial.println("Init complete!");
}



void loop()
{
  float temperature;
  float pressure;
  uint8_t oversampling = 7;
  int16_t ret;
  Serial.println();
  loopCount++;

  //lets the Dps310 perform a Single temperature measurement with the last (or standard) configuration
  //The result will be written to the paramerter temperature
  //ret = Dps310PressureSensor.measureTempOnce(temperature);
  //the commented line below does exactly the same as the one above, but you can also config the precision
  //oversampling can be a value from 0 to 7
  //the Dps 310 will perform 2^oversampling internal temperature measurements and combine them to one result with higher precision
  //measurements with higher precision take more time, consult datasheet for more information
  ret = Dps310PressureSensor.measureTempOnce(temperature, oversampling);

  if (ret != 0)
  {
    //Something went wrong.
    //Look at the library code for more information about return codes
    Serial.print("FAIL! ret = ");
    Serial.println(ret);
  }
  else
  {
    Serial.print("Temperature: ");
    Serial.print(temperature);
    Serial.println(" degrees of Celsius");
  }

  //Pressure measurement behaves like temperature measurement
  //ret = Dps310PressureSensor.measurePressureOnce(pressure);
  ret = Dps310PressureSensor.measurePressureOnce(pressure, oversampling);
  if (ret != 0)
  {
    //Something went wrong.
    //Look at the library code for more information about return codes
    Serial.print("FAIL! ret = ");
    Serial.println(ret);
  }
  else
  {
    Serial.print("Pressure: ");
    Serial.print(pressure);
    Serial.println(" Pascal");
  }

  calibrating = loopCount <= nCalib;
  if (calibrating) {
    Serial.println("calibrating...");
    p0 += pressure;
    if (loopCount == nCalib) {
      p0 /= loopCount;
      z1 = temperature * Rasterisk / M / g / p0;
      z1 = -100 * z1 ;
      Serial.print("z1: ");
      Serial.println(z1);
    }
  } else {
    float dz = z1 * (pressure - p0);
    Serial.print("dz: ");
    Serial.println(dz);
  }


  //Wait some time
  delay(500);
}