#include <Arduino.h>
#include <IRremote.h>
#include <RingBufCPP.h>

const int IR_RECEIVE_PIN = 11;
IRrecv irrecv(IR_RECEIVE_PIN);
decode_results results;

struct RBItem
{
  uint32_t micros;
  uint32_t opcode;
  uint32_t seqno;
};

void rbiDump(RBItem *p)
{
  Serial.print("#");
  Serial.print(p->seqno);
  Serial.print("@");
  Serial.print(p->micros);
  Serial.print(": 0x");
  Serial.print(p->opcode, HEX);
  Serial.println();
}

#define RB_SIZE 10

RingBufCPP<struct RBItem, RB_SIZE> rb;

void setup()
{
  Serial.begin(115200);
  while (!Serial)
    ;
  Serial.println(F("START " __FILE__ " from " __DATE__));
  irrecv.enableIRIn(); // Start the receiver

  Serial.print(F("Ready to receive IR signals at pin "));
  Serial.println(IR_RECEIVE_PIN);
}

uint32_t seqno = 0;

void loop()
{
  struct RBItem rbi;
  if (irrecv.decode(&results))
  {
    rbi.seqno = ++seqno;
    rbi.micros = micros();
    rbi.opcode = results.value;
    rb.add(rbi);
    rbiDump(&rbi);
    irrecv.resume(); // Receive the next value
  }
}
