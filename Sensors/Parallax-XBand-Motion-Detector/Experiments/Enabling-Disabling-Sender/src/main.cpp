/*
 * main.cpp.
 */

#include <Arduino.h>

#define RADAR_RECTANGLE_PIN 7
#define RADAR_ANALOG_PIN 0 
#define RADAR_ENABLE_PIN 6

/**
 * Increase the ADC conversion rate to its maximum value.
 */

#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif

void setup() {
  pinMode(RADAR_ENABLE_PIN,OUTPUT);
  cbi(ADCSRA, ADPS2);
  cbi(ADCSRA, ADPS1);
  cbi(ADCSRA, ADPS0);
  Serial.begin(115200);
}

void loop() {

  // compute the disabling or enabling of the sender
  unsigned long seconds = millis()/1000;
  int flag = (int) seconds & 1;
  digitalWrite(RADAR_ENABLE_PIN,flag);

  // first CSV column: the ADC output value (0..1023)
  int analogSignal = analogRead(RADAR_ANALOG_PIN);
  Serial.print(analogSignal);
  Serial.print(F(","));
  
  // second CSV column: the motion indicator output value mapped to the interval 850..950
  int rectangleSignal = 850+100*digitalRead(RADAR_RECTANGLE_PIN);
  Serial.print(rectangleSignal);
  Serial.print(F(","));
  
  // third CSV column: the enabling flag mapped to the interval 1050..1150
  Serial.print(1050+100*flag);
  
  // fourth and fifth CSV column: Extreme values 0..1000
  Serial.println(F(",0,1200"));
  
  // wait for next conversion
  delay(15);
}
