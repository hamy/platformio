/*
 * main.cpp.
 */

#include <Arduino.h>

#define RADAR_RECTANGLE_PIN 7
#define RADAR_ANALOG_PIN 0 

/**
 * Increase the ADC conversion rate to its maximum value.
 */

#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif

void setup() {
  cbi(ADCSRA, ADPS2);
  cbi(ADCSRA, ADPS1);
  cbi(ADCSRA, ADPS0);
  Serial.begin(115200);
}

void loop() {
  // first CSV column: the ADC output value (0..1023)
  int analogSignal = analogRead(RADAR_ANALOG_PIN);
  Serial.print(analogSignal);
  Serial.print(F(","));
  
  // second CSV column: the motion indicator output value mapped to the interval 1050..1150
  int rectangleSignal = 1050+100*digitalRead(RADAR_RECTANGLE_PIN);
  Serial.print(rectangleSignal);
  
  // third and fourth CSV column: Extreme values 0..1200
  Serial.println(F(",0,1200"));
  
  // wait for next conversion
  delay(5);
}
