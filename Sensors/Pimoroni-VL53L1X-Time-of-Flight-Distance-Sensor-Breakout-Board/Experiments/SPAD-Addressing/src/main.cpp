#include <Arduino.h>
#include <Wire.h>
#include "vl53l1_api.h"
#include "vl53l1_core.h"

// By default, this example blocks while waiting for sensor data to be ready.
// Comment out this line to poll for data ready in a non-blocking way instead.
#define USE_BLOCKING_LOOP

// Timing budget set through VL53L1_SetMeasurementTimingBudgetMicroSeconds().
#define MEASUREMENT_BUDGET_MS 50

// Interval between measurements, set through
// VL53L1_SetInterMeasurementPeriodMilliSeconds(). According to the API user
// manual (rev 2), "the minimum inter-measurement period must be longer than the
// timing budget + 4 ms." The STM32Cube example from ST uses 500 ms, but we
// reduce this to 55 ms to allow faster readings.
#define INTER_MEASUREMENT_PERIOD_MS 55

VL53L1_Dev_t                   dev;
VL53L1_DEV                     Dev = &dev;

int status;

void setup() {
    uint8_t row,col,spadNumber;
 
    Serial.begin(115200);
    for (row=16; row>0; ) {
        row--;
        for (col=0; col<16; col++) {
            VL53L1_encode_row_col(row,col,&spadNumber);
            Serial.print(" ");
            if (spadNumber<10) Serial.print(" ");
            if (spadNumber<100) Serial.print(" ");
            Serial.print((int) spadNumber);
        }
        Serial.println();
        delay(100);
    }


  uint8_t byteData;
  uint16_t wordData;

  Wire.begin();
  Wire.setClock(400000);
 
  // This is the default 8-bit slave address (including R/W as the least
  // significant bit) as expected by the API. Note that the Arduino Wire library
  // uses a 7-bit address without the R/W bit instead (0x29 or 0b0101001).
  Dev->I2cDevAddr = 0x52;

  VL53L1_software_reset(Dev);

  VL53L1_RdByte(Dev, 0x010F, &byteData);
  Serial.print(F("VL53L1X Model_ID: "));
  Serial.println(byteData, HEX);
  VL53L1_RdByte(Dev, 0x0110, &byteData);
  Serial.print(F("VL53L1X Module_Type: "));
  Serial.println(byteData, HEX);
  VL53L1_RdWord(Dev, 0x010F, &wordData);
  Serial.print(F("VL53L1X: "));
  Serial.println(wordData, HEX);

  Serial.println(F("Autonomous Ranging Test"));
  status = VL53L1_WaitDeviceBooted(Dev);
  status = VL53L1_DataInit(Dev);
  status = VL53L1_StaticInit(Dev);
  status = VL53L1_SetDistanceMode(Dev, VL53L1_DISTANCEMODE_LONG);
  status = VL53L1_SetMeasurementTimingBudgetMicroSeconds(Dev, MEASUREMENT_BUDGET_MS * 1000);
  status = VL53L1_SetInterMeasurementPeriodMilliSeconds(Dev, INTER_MEASUREMENT_PERIOD_MS);
  //VL53L1_SetROI(Dev,4,4);
}
void loop() {
    uint8_t row,col ;
    static VL53L1_RangingMeasurementData_t RangingData;
    static VL53L1_UserRoi_t roi;
  
    for (row=0; row<12; row++ ) {
          for (col=0; col<12; col++) {
            // VL53L1_encode_row_col(row,col,&spadNumber);
            // VL53L2_SetROICenter(Dev,spadNumber);
            roi.TopLeftX = col;
            roi.TopLeftY = row+3;
            roi.BotRightX = col+3;
            roi.BotRightY = row;
            VL53L1_SetUserROI(Dev,&roi);
            status = VL53L1_StartMeasurement(Dev);
            status = VL53L1_WaitMeasurementDataReady(Dev); 
            status = VL53L1_GetRangingMeasurementData(Dev, &RangingData);
//            Serial.print(RangingData.RangeStatus);
//    Serial.print(F(","));
    Serial.print(RangingData.RangeMilliMeter);
    Serial.print(F(","));
//    Serial.print(RangingData.SignalRateRtnMegaCps/65536.0);
//    Serial.print(F(","));
//    Serial.println(RangingData.AmbientRateRtnMegaCps/65336.0);
        status = VL53L1_StopMeasurement(Dev);
            delay(1 );


          }
          Serial.println();
     }
         Serial.println();

 }
