/*****************************/
/* BME280 I2C Sample Program */
/*****************************/

// Source: https://github.com/adafruit/Adafruit_BME280_Library
// with small modifications made by Hamy

#include <Arduino.h>
#include <Wire.h>

#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include <MicroView.h>
#include <SourceInfo.h>

#include <LoopUtil.h>

int loopDuration = 1000;
LoopUtil loopUtil(true, -1, loopDuration);

void setupSerial()
{
    Serial.begin(9600);
    while (!Serial)
    {
        ; // time to get serial running
    }
    SOURCE_INFO_ENTERING("setupSerial")
    Serial.println(F("Starting sketch \"MobileMeasurementUsingMicroView\"..."));
    SOURCE_INFO_PRINT
    SOURCE_INFO_LEAVING("setupSerial")
}

int yText;

void resetView()
{
    yText = 0;
    uView.clear(PAGE);
}

void nextViewLine()
{
    uView.setCursor(0, yText);
    yText += 8;
}

void setupMicroView()
{
    SOURCE_INFO_ENTERING("setupMicroView")
    uView.begin();    // begin of MicroView
    uView.clear(ALL); // erase hardware memory inside the OLED controller
    uView.display();  // display the content in the buffer memory, by default it is the MicroView logo
    delay(1000);
    uView.clear(PAGE);
    uView.display();
    SOURCE_INFO_LEAVING("setupMicroView")
}

Adafruit_BME280 bme;
float temperature;
float pressure;
float humidity;

void setupBME280()
{
    unsigned status;
    SOURCE_INFO_ENTERING("setupBME280")
    status = bme.begin();
    Serial.print(F("status returned by bme.begin(): "));
    Serial.println(status, 16);
    Serial.print(F("bme.sensorID() returned: "));
    Serial.println(bme.sensorID(), 16);

    if (!status)
    {
        SOURCE_INFO_BAILING_OUT("Oops: BME280 initialization error !")
    }
    SOURCE_INFO_LEAVING("setupBME280")
}

void setup()
{
    setupSerial();
    setupMicroView();
    setupBME280();
}

void measureMeteorologicalValues()
{
    SOURCE_INFO_ENTERING("measureMeteorologicalValues")
    temperature = bme.readTemperature();
    Serial.print(F("Temperature:       "));
    Serial.print(temperature);
    Serial.println(F(" °C"));
    pressure = bme.readPressure() / 100.0F;
    Serial.print(F("Pressure:          "));
    Serial.print(pressure);
    Serial.println(F(" hPa"));
    humidity = bme.readHumidity();
    Serial.print(F("Relative Humidity: "));
    Serial.print(humidity);
    Serial.println(F(" %"));
    SOURCE_INFO_LEAVING("measureMeteorologicalValues")
}

void displayMeteorologicalValues()
{
    SOURCE_INFO_ENTERING("displayMeteorologicalValues")
    resetView();
    nextViewLine();
    uView.print(temperature);
    uView.print(F(" C"));
    nextViewLine();
    nextViewLine();
    uView.print(pressure);
    uView.print(F("hPa"));
    nextViewLine();
    nextViewLine();
    uView.print(humidity);
    uView.print(F(" %"));
    uView.display();
    SOURCE_INFO_LEAVING("displayMeteorologicalValues")
}

void loop()
{
    loopUtil.loopStart();
    measureMeteorologicalValues();
    displayMeteorologicalValues();
    loopUtil.loopEnd();
    Serial.println();
}
