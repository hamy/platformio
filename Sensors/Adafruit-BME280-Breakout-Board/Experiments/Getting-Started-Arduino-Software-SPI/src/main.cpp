/*****************************/
/* BME280 I2C Sample Program */
/*****************************/

// Source: https://github.com/adafruit/Adafruit_BME280_Library
// with small modifications made by Hamy

#include <Arduino.h>
#include <Wire.h>

#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

#define LED_PIN 7
#define SEALEVELPRESSURE_HPA (1021)
#define KB (1.380649e-23)
#define EV (1.602177e-19)

#define BME_SCK 13
#define BME_MISO 12
#define BME_MOSI 11
#define BME_CS 10

Adafruit_BME280 bme(BME_CS, BME_MOSI, BME_MISO, BME_SCK); // SW SPI


unsigned long delayTime;

float celsius2fahrenheit(float c) {
    return c*9.0f/5.0f + 32.0f;
}

float celsius2kelvin(float c) {
    return c+273.15f;
}

float celsius2meV(float c) {
    double d = celsius2kelvin(c);
    d = 500*KB*d/EV;
    return (float) d;
}

void setup() {
    pinMode(LED_PIN,OUTPUT);
    Serial.begin(9600);
    while(!Serial);    // time to get serial running
    Serial.println(F("BME280 Getting-Started-Arduino-Software-SPI"));
    Serial.print(F("Sea level pressure: "));
    Serial.print(SEALEVELPRESSURE_HPA);
    Serial.println(F(" hPa"));

    unsigned status;
    
    status = bme.begin();  
	Serial.print(F("status returned by bme.begin(): "));
	Serial.println(status,16);
	Serial.print(F("bme.sensorID() returned: "));
	Serial.println(bme.sensorID(),16);
	
    if (!status) {
        Serial.println(F("Oops: BME280 initialization error !"));
        while (1);
    }
    
    Serial.println("-- Default Test --");
    delayTime = 1000;
    Serial.println();
}

void printValues() {
    digitalWrite(LED_PIN,HIGH);
    float celsius = bme.readTemperature();
    Serial.print(F("Temperature:      "));
    Serial.print(celsius);
    Serial.println(F(" °C"));
    Serial.print(F("                  "));
    Serial.print(celsius2fahrenheit(celsius));
    Serial.println(F(" °F"));
    Serial.print(F("                  "));
    Serial.print(celsius2kelvin(celsius));
    Serial.println(F(" K"));
    Serial.print(F("kT/2:             "));
    Serial.print(celsius2meV(celsius));
    Serial.println(F(" meV"));

    Serial.print(F("Pressure:         "));
    Serial.print(bme.readPressure() / 100.0F);
    Serial.println(F(" hPa"));

    Serial.print(F("Approx. Altitude: "));
    Serial.print(bme.readAltitude(SEALEVELPRESSURE_HPA));
    Serial.println(F(" m"));

    Serial.print(F("Rel. Humidity:    "));
    Serial.print(bme.readHumidity());
    Serial.println(F(" %"));

    Serial.println();
    digitalWrite(LED_PIN,LOW);
}

void loop() { 
    printValues();
    delay(delayTime);
}

