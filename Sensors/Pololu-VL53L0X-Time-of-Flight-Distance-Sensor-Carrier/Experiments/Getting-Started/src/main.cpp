/* This example shows how to use continuous mode to take
range measurements with the VL53L0X. It is based on
vl53l0x_ContinuousRanging_Example.c from the VL53L0X API.

The range readings are in units of mm. */

#include <Wire.h>
#include <VL53L0X.h>

VL53L0X sensor;

void setup()
{
  Serial.begin(115200);
  Wire.begin();

  sensor.init();
  sensor.setTimeout(500);

  // Start continuous back-to-back mode (take readings as
  // fast as possible).  To use continuous timed mode
  // instead, provide a desired inter-measurement period in
  // ms (e.g. sensor.startContinuous(100)).
  sensor.startContinuous(55);
}

char buffer[20];
#define MAX_DOTS 140 
long loopCounter=0;

void loop()
{ 
  loopCounter++;
  int mm = sensor.readRangeContinuousMillimeters();
  memset(buffer,0,sizeof buffer);
  sprintf(buffer,"%12d",loopCounter);
  Serial.print(buffer);
  Serial.print(" ");
  memset(buffer,0,sizeof buffer);
  sprintf(buffer,"%4d",mm);
  Serial.print(buffer);
  Serial.print(" ");
  int dots = mm/10;
  if (dots>MAX_DOTS) dots = MAX_DOTS;
  while (0<dots--) Serial.print("#");
  if (sensor.timeoutOccurred()) { Serial.print(" TIMEOUT"); }

  Serial.println();
  delay(10);
}
