/*
 * main.cpp.
 * WORK IN PROGRESS
 */

#include <Adafruit_MCP4725.h>
#include <Arduino.h>
#include <Wire.h>

Adafruit_MCP4725 dac;

int dacInputValue = 0;       // the input value for the DAC: 0=GND, 4095=VCC
int dacInputValueStep = 273; // step size for incrementing dacInputValue
float expectedVoltage;       // the expected DAC result voltage corresponding to
                             // dacInputValue
float feedBackVoltage; // the test analogRead(..) result created from ADCing the
                       // actual DAC result voltage

int ledFlag = 0;


void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(9600);
  Serial.println(F("DAC/ADC demo"));
  dac.begin(0x62);
}

void toggleLed() {
  ledFlag = !ledFlag; // indicate the next loop
  digitalWrite(LED_BUILTIN, ledFlag);
}

void computeExpectedVoltage() {
  expectedVoltage = (5.0 * dacInputValue) / 4095.0;
}

void convert() {
  dac.setVoltage(dacInputValue, false);
  delay(10);
}

void readFeedBackVoltage() {
  feedBackVoltage = analogRead(0);
  feedBackVoltage *= 5.0 / 1023;
}

void computeNextDacInputValue() {
  dacInputValue += dacInputValueStep;
  if (dacInputValue > 4095) {
    dacInputValue = 0;
  }
}

void report() {
  Serial.print((F("DAC input value: ")));
  Serial.print(dacInputValue);
  Serial.print((F(", expected voltage: ")));
  Serial.print(expectedVoltage);
  Serial.print((F(" V, feed-back voltage: ")));
  Serial.print(feedBackVoltage);
  Serial.println((F(" V.")));
}

void loop() {
  toggleLed();
  computeExpectedVoltage();
  convert();
  readFeedBackVoltage();
  report();
  delay(2000);
  computeNextDacInputValue();
}
