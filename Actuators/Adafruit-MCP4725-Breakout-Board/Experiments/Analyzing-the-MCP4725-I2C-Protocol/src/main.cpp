/*
 * main.cpp.
 * WORK IN PROGRESS
 */

#include <Adafruit_MCP4725.h>
#include <Arduino.h>
#include <Wire.h>

Adafruit_MCP4725 dac;

#define TRIGGER_PIN 13

int dacInputValue = 0x0a5e; // the input value for the DAC: 0=GND, 4095=VCC

void setup() {
  pinMode(TRIGGER_PIN, OUTPUT);
  dac.begin(0x62);
}

void triggerAnalyzer() {
  digitalWrite(TRIGGER_PIN, HIGH);
  delayMicroseconds(2);
  digitalWrite(TRIGGER_PIN, LOW);
}

void convert() {
  dac.setVoltage(dacInputValue, false);
}


void loop() {
  triggerAnalyzer();
  convert();
  delay(2000);
}
