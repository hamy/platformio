/*
 * main.cpp.
 * !!!!!!!!!!!!!! INCOMPLETE !!!!!!!!!!!!!!!!!
 */

#include <Adafruit_MCP4725.h>
#include <Arduino.h>
#include <Wire.h>

Adafruit_MCP4725 dac;

#define MIN_DAC_INPUT_VALUE 0
#define MAX_DAC_INPUT_VALUE 4095
#define CARD_DAC_INPUT_VALUE (MAX_DAC_INPUT_VALUE - MIN_DAC_INPUT_VALUE + 1)

#define TRIGGER_PIN 12

void generateTriggerPulse() {
  digitalWrite(TRIGGER_PIN, true);
  delayMicroseconds(2);
  digitalWrite(TRIGGER_PIN, false);
}

long convertAll(boolean saveInFlash) {
  Serial.print(F("Save in flash: "));
  Serial.println(saveInFlash);
  digitalWrite(LED_BUILTIN, true);
  generateTriggerPulse();
  long ms1 = micros();
  for (int dacInputValue = MIN_DAC_INPUT_VALUE;
       dacInputValue <= MAX_DAC_INPUT_VALUE; dacInputValue++) {
    dac.setVoltage(dacInputValue, saveInFlash);
  }
  long ms2 = micros();
  digitalWrite(LED_BUILTIN, false);
  return (ms2 - ms1) / CARD_DAC_INPUT_VALUE;
}

void setupSerial() {
  Serial.begin(9600);
  Serial.println(F("MCP4725 DAC Speed"));
}

void setupLedBuiltin() {
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, false);
}

void setupTriggerPin() {
  pinMode(TRIGGER_PIN, OUTPUT);
  digitalWrite(TRIGGER_PIN, false);
}

void setupDAC() { dac.begin(0x62); }

void measurement() {
  long usec1 = convertAll(false);
  Serial.println(usec1);
  delay(1000);
  long usec2 = convertAll(true);
  Serial.println(usec2);
}

void setup() {
  setupSerial();
  setupLedBuiltin();
  setupTriggerPin();
  setupDAC();
  measurement();
}

void loop() {}
