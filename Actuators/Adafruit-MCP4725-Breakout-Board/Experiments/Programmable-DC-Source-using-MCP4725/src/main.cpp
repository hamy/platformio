/*
 * main.cpp.
 * WORK IN PROGRESS
 */

#include <Adafruit_MCP4725.h>
#include <Arduino.h>
#include <Wire.h>
#include <stdlib.h>

Adafruit_MCP4725 dac;
#define TRIGGER_PIN 12

#define MIN_DAC_INPUT_VALUE 0
#define MAX_DAC_INPUT_VALUE 4095
int oldDacInputValue = 0; // the input value for the DAC: 0=GND, 4095=VCC
int newDacInputValue = 0; // the input value for the DAC: 0=GND, 4095=VCC
float expectedVoltage;    // the expected DAC result voltage corresponding to
                          // dacInputValue
float feedBackVoltage; // the test analogRead(..) result created from ADCing the
                       // actual DAC result voltage

void convert() {
  digitalWrite(TRIGGER_PIN, HIGH);
  delayMicroseconds(2);
  digitalWrite(TRIGGER_PIN, LOW);
  dac.setVoltage(newDacInputValue, true);
  oldDacInputValue = newDacInputValue;
  delay(10);
}

void readFeedBackVoltage() {
  feedBackVoltage = analogRead(0);
  feedBackVoltage *= 5.0 / 1023;
}

void report() {
  Serial.print(F("DAC input value: "));
  Serial.print(newDacInputValue);
  Serial.print(F("=0x"));
  Serial.print(newDacInputValue,HEX);
  Serial.print(F("=b"));
  Serial.print(newDacInputValue,BIN);
  Serial.print(F(", expected voltage: "));
  Serial.print(expectedVoltage);
  Serial.print(F(" V, feed-back voltage: "));
  Serial.print(feedBackVoltage);
  Serial.println(F(" V."));
}

int ledFlag = 0;

#define TEXT_BUFFER_MAX_LENGTH 16
char textBuffer[TEXT_BUFFER_MAX_LENGTH + 1];
int textBufferLength = 0;

void clearTextBuffer() {
  memset(textBuffer, '\0', TEXT_BUFFER_MAX_LENGTH + 1);
  textBufferLength = 0;
  Serial.println(F("text buffer cleared"));
}

void appendToTextBuffer(int ch) {
  if (textBufferLength < TEXT_BUFFER_MAX_LENGTH) {
    textBuffer[textBufferLength++] = ch;
  }
}

void computeNewDacInputValue() {
  if (textBufferLength > 0) {
    expectedVoltage = atof(textBuffer);
    clearTextBuffer();
    newDacInputValue = (int)(4095 * expectedVoltage / 5.0);
    if (newDacInputValue < MIN_DAC_INPUT_VALUE)
      newDacInputValue = MIN_DAC_INPUT_VALUE;
    if (newDacInputValue > MAX_DAC_INPUT_VALUE)
      newDacInputValue = MAX_DAC_INPUT_VALUE;
    Serial.print(F("new DAC input value: "));
    Serial.println(newDacInputValue);
  }
}

void processSerialInput() {
  digitalWrite(LED_BUILTIN, HIGH);
  while (Serial.available() > 0) {
    int ch = Serial.read();
    switch (ch) {
    case '0':
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
    case '.':
      appendToTextBuffer(ch);
      break;
    case 0x0A:
    case 0x0D:
      computeNewDacInputValue();
      break;
    case ' ':
    case '\t':
      break;
    default:
      clearTextBuffer();
      break;
    }
  }
  digitalWrite(LED_BUILTIN, LOW);
}

void setup() {
  Serial.begin(9600);
  Serial.println(F("DAC/ADC demo"));
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(TRIGGER_PIN, OUTPUT);
  digitalWrite(LED_BUILTIN,LOW);
  digitalWrite(TRIGGER_PIN,LOW);
  clearTextBuffer();
  dac.begin(0x62);
  readFeedBackVoltage();
  Serial.print(F("Initial feed-back voltage: "));
  Serial.print(feedBackVoltage);
  Serial.println(F(" V."));
}

void loop() {
  processSerialInput();
  if (newDacInputValue != oldDacInputValue) {
    digitalWrite(LED_BUILTIN,HIGH);
    convert();
    readFeedBackVoltage();
    report();
    digitalWrite(LED_BUILTIN,LOW);
  }
  delay(2000);
}
