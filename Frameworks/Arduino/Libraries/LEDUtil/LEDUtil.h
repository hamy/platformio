#ifndef _LED_UTIL_H
#define _LED_UTIL_H

/**
 \mainpage LEDUtil - C Library for Controlling Single or RGB LEDs.

 \section lu-purpose Purpose of the LEDUtil Library

 LEDUtil offers an API (provided by the header file LEDUtil.h) for controlling
 LEDs which

 - hides the polarity details of the LED,
 - supports both simple LEDs and RGB LEDs,
 - allows PWM (pulse width modulation) if the microcontroller pin driving the
 LED supports PWM, and
 - provides a rich set of named color methods for RBG LEDs that support PWM.

 \section lu-arduino-library Arduino Library

 The LEDUtil library is available as pre-packed Arduino library
 [LEDUtil-arduino-library.zip](../LEDUtil-arduino-library/LEDUtil-arduino-library.zip). Download
 this zip archive and unpack it in the **libraries** directory of your Arduino
 sketchbook. It will create a branch **LEDUtil** in the **libraries** directory.
 This Arduino library comes with several sketches:

 - **Blink.ino** is the LEDUtil version of the basic Arduino **Blink** sketch
 that comes with the Arduino IDE.

 - **Fading.ino** is a PWM sample sketch that demonstrates fading in and fading
 out of the brightness values of an LED. Caveat: connect the PWM LED to a pin of
 the microcontroller that supports PWM; otherwise, the sketch will not work as
 expected.

 - **PWMNamedColorsForCommonAnode.ino** is a PWM sample sketch that demonstrates 
 the usage of named colors for RGB LEDs with common anodes.

 - **PWMNamedColorsForCommonCathode.ino** is a PWM sample sketch that demonstrates 
 the usage of named colors for RGB LEDs with common cathodes.

 - **HSBDemoForCommonCathode.ino** is a PWM sample sketch that demonstrates 
 the usage of HSB colors for RGB LEDs with common cathodes.

 \section lu-simple-led-polarity LED Polarity for Simple LEDs

 LEDUtil uses an unambiguous convention for driving the LED's brightness:

 - If a LEDUtil client specifies a logical value of true or HIGH as input
 parameter for a LEDUtil method, this means that the LED shows its maximum
 brightness.
 - If a LEDUtil client specifies a logical value of false or LOW as input
 parameter, this means that the LED is dark; i.e., it shows its minimum
 brightness.

 The implementation (the C++ file LEDUtil.cpp) provides the internal mapping
 between the logical client-side voltage levels given above and the actual
 polarity of the LED circuitry. This mapping is specified when a client calls
 the constructor SimpleLED::SimpleLED(). The SimpleLED instance plays the role
 of a man-in-the-middle which decouples the client-side meaning of the voltage
 levels (always: on=true, off=false) from the actual polarity of the circuitry.
 Therefore,

 - the client should never directly call digitalWrite(pin) for the given LED
 pin (although is is possible to do this, but you must keep the LED polarity in
 mind), but
 - should *indirectly* use the SimpleLED instance to affect the LED via its pin.

 Let's demonstrate this by an example for the most simple case: Driving the LED
 that is built-in in the Arduino board. Its pin is identified by the macro variable
 LED_BUILTIN. As said above, clients should not directly invoke
 digitalWrite(LED_BUILTIN), but use indirection via the LEDUtil instance as
 indicated in the following code snippet:

 \code
 #include "Arduino.h"
 #include "LEDUtil.h"

 // define a SimpleLED instance that is connected to pin LED_BUILTIN
 SimpleLED simpleLED(LED_BUILTIN);

 // start with a dark LED
 void setup() {
   simpleLED.off();
 }

 // blink: the brightness value is toggled every second
 void loop() {
   simpleLED.on();
   delay(1000);
   simpleLED.off();
   delay(1000);
 }
 \endcode

 The snippet shown above is the LEDUtil version of
 the **Blink** sketch of the **Basics** section of the built-in examples that
 come with the Arduino IDE.

 Hints:

 - The constructor SimpleLED::SimpleLED(int) automatically sets the pin mode
 of LED_BUILTIN to OUTPUT, so it is not necessary to perform that operation in
 the setup() method.
 - The constructor SimpleLED() is overloaded. The snippet shown above invokes
 the most simple version of the constructor which uses default values for
    - the circuit polarity and
    - the support of PWM.


 The subsequent two
 subsections describe the different circuitry polarities for simple LEDs.

 \subsection lu-simple-led-polarity-on-is-high Simple LED with Polarity ON=HIGH

 The schematics below show the details of the circuitry:

 - The client drives the LED via a connector JP. The LED is dark when a value of
 false or LOW is applied at the connector JP. The LED shines at its maximum
 brightness when a value of true or HIGH is applied at the connector.
 - A 220 Ohm resistor limits the LED current.
 - The current enters the LED via its anode.
 - The LED cathode is connected to GND.

 \image html SimpleLEDOnIsHigh_schem.png

 The following snippet defines a LEDUtil instances that reflects the given
 circuitry polarity in the  SimpleLED::SimpleLED(int,boolean) constructor.
 Henceforth, this specific polarity is hidden by the LEDUtil library.

 \code
 int ledPin=13;
 boolean onIsHigh=true;
 SimpleLED simpleLED(ledPin,onIsHigh);
       :    :
 // switch LED on
 simpleLED.on();
       :    :
 // switch LED off
 simpleLED.off();
 \endcode

 Remarks:

 - The client may retrieve the current on/off state via the methods
 SimpleLED.isOn() or SimpleLED.isOff(), respectively.
 - The client may retrieve the underlying pin number via the method
 SimpleLED.getPin() (that will return 13).

 \subsection lu-simple-led-polarity-on-is-low Simple LED with Polarity ON=LOW

 The schematics below show the details of the circuitry:

 - The client drives the LED via a connector JP. The LED is dark when a value of
 true or HIGH is applied at the connector JP. The LED shines at its maximum
 brightness when a value of false or LOW is applied at the connector.
 - The LED anode is connected to VCC.
 - The current leaves the LED via its cathode.
 - A 220 Ohm resistor limits the LED current.

 \image html SimpleLEDOnIsLow_schem.png

 The following snippet defines a LEDUtil instances that reflects the given
 circuitry polarity in the  SimpleLED::SimpleLED(int,boolean) constructor.
 Henceforth, this specific polarity is hidden by the LEDUtil library.

 \code
 int ledPin=13;
 boolean onIsHigh=false;
 SimpleLED simpleLED(ledPin,onIsHigh);
       :    :
 // switch LED on
 simpleLED.on();
       :    :
 // switch LED off
 simpleLED.off();
 \endcode

 Remarks:

 - The client may retrieve the current on/off state via the methods
 SimpleLED.isOn() or SimpleLED.isOff(), respectively.
 - Tne client may retrieve the underlying pin number via the method
 SimpleLED.getPin() (that will return 13).
 - You **must** invoke the SimpleLED::SimpleLED(int,boolean) constructor with at
 least 2 arguments since LEDUtil would use the default setting onIsHigh=true
 without the second argument resulting in invalid behaviour of methods like
 SimpleLED.on(), SimpleLED.off(), SimpleLED.isOn, etc.

 \section lu-simple-led-pwm-support PWM Support

 Similar to the decoupling of the LED brightness from the circuitry polarity,
 LEDUtil also provides an unambiguous convention for driving the LED by PWM:

 - If the microcontroller pin   driving the LED supports PWM,
     - a PWM value of 0 means minimum brightness (the LED is dark),
     - a PWM value of  255 means maximum brightness, and
     - values between 1 and 254 are mapped appropriately.
 - If the microcontroller pin driving the LED does **not** support PWM,
     - a PWM value of 0 means that the LED is dark,
     - all other values means that the LED shows its maximum brightness.

 Hint: the mapping of non-PWM-supporting pins prevents the microcontroller from
 showing some strange behaviour if a non-PWM-supporting pin is driven by the
 Arduino function analogWrite(). If **missing** PWM-support is indicated at
 LEDUtil construction time, LEDUtil internally **never** applies this function
 to the pin. If not explicitly specified in the 3-argument constructor
 SimpleLED::SimpleLED(int,boolean,boolean), LEDUtil assumes that the
 microcontroller does **not** support PWM on the given pin. This is a safety
 precaution.

 The implementation (the C++ file LEDUtil.cpp) provides the appropriate PWM
 handling between the logical levels given above and the actual polarity of the
 LED circuitry and the support of PWM. This mapping is specified when client
 calls  the most comprehensive version of the constructor
 SimpleLED::SimpleLED(int,boolean,boolean) that uses 3 arguments. For PWM
 support, the third argument of the constructor is important:

 - a value of false indicates that the microcontroller does **not** support PWM
 on the given pin.
 - a value of true indicates that the microcontroller  supports PWM on the given
 pin. This is the default value.
 - Caveat: if the microcontroller does **not** support PWM on the given pin,
 you **MUST** use the 3-argument constructor with false as last argument,
 otherwise, the microcontroller might behave in a very nasty manner.

 \section lu-rgb-led RGB LEDs

 RGB LEDs are an extension to simple LEDs.

 \subsection lu-rgb-led-polarity-on-is-high Polarity ON=HIGH

 In this case, the three R, G, B LEDs use a common cathode.

 \image html RGBLEDOnIsHigh_schem.png

 In this case the corresponding LED is dark when a value of false or LOW is
 applied at the connector JP. The LED shines at its maximum brightness when a
 value of true or HIGH is applied at the connector. Note that for the client,
 this specific polarity is hidden by the LEDUtil library.


 \subsection lu-rgb-led-polarity-on-is-low Polarity ON=LOW

 In this case, the three R, G, B LEDs use a common anode.

 \image html RGBLEDOnIsLow_schem.png

 In this case the LED is dark when a value of true or HIGH is applied at the
 connector JP. The LED shines at its maximum brightness when a value of false or
 LOW is applied at the connector. Note that for the client, this specific
 polarity is hidden by the LEDUtil library.



 */

#include <Arduino.h>

/**
 * The version of this helpers.
 */
#define LED_UTIL_VERSION "1.0.10"

/**
 * The minimum valid LED brightness: The LED is dark.
 */
#define MIN_LED_BRIGHTNESS 0

/**
 * The maximum valid LED brightness: The LED shines at its maximum.
 */
#define MAX_LED_BRIGHTNESS 255

/**
 The minimum hue value in the  system. Note: This is a floating point value.
 */
#define MIN_HSB_HUE_VALUE (0.0)

/**
 The maximum hue value in the HSB system. Note: This is a floating point value.
 */
#define MAX_HSB_HUE_VALUE (359.999)

/**
 The minimum saturation value in the HSB system. Note: This is a floating point
 value.
 */
#define MIN_HSB_SATURATION_VALUE (0.0)

/**
 The maximum saturation value in the HSB system. Note: This is a floating point
 value.
 */
#define MAX_HSB_SATURATION_VALUE (1.0)

/**
 The minimum brightness value in the HSB system. Note: This is a floating point
 value.
 */
#define MIN_HSB__BRIGHTNESS_VALUE (0.0)

/**
 The maximum brightness value in the HSB system. Note: This is a floating point
 value.
 */
#define MAX_HSB__BRIGHTNESS_VALUE (1.0)

/**
 * A simple LED.
 */
class SimpleLED
{
private:
  int _pin;
  boolean _onIsHigh;
  boolean _supportsPWM;
  int _brightness;
  int _limitBrightness(int brightness);

public:
  /**
 Constructor for a simple LED.

 \param pin The pin that drives the LED.

 \param onIsHigh A flag that indicates which voltage level makes the LED
 shine:
        - onIsHigh=true: The LED is bright if a HIGH value is applied. It is
 dark if a LOW value is applied.
        - onIsHigh=false: The LED is bright if a LOW value is applied. It is
 dark if a HIGH value is applied.

 \param supportsPWM A flag that indicates
 that the microcontroller supports PWM on the given pin.

 */
  SimpleLED(int pin, boolean onIsHigh, boolean supportsPWM);

  /**
 Constructor for a simple LED. Note: It is assumed that the microcontroller does
 not support PWM on the given pin.

 \param pin The pin that drives the LED.

 \param onIsHigh A flag
 that indicates which voltage level makes the LED shine:
        - onIsHigh=true: The LED is bright if a HIGH value is applied. It is
 dark if a LOW value is applied.
        - onIsHigh=false: The LED is bright if a LOW value is applied. It is
 dark if a HIGH value is applied.

 */
  SimpleLED(int pin, boolean onIsHigh);

  /**
 Constructor for a simple LED. Note: Note: It is assumed that the
 microcontroller does not support PWM on the given pin.

 \param pin The pin that drives the LED. The LED is bright if a
 HIGH value is applied. It is dark if a LOW value is applied.
 */
  SimpleLED(int pin);

  /**
 Returns the pin that drives the LED.
 \return The pin.
 */
  int getPin();

  /**
 A flag that indicates which voltage level makes the LED shine.
 \return The flag value.
         - true: The LED is bright if a HIGH value is applied. It is dark if a
 LOW value is applied.
         - false: The LED is bright if a LOW value is applied. It is dark if a
 HIGH value is applied.
 */
  boolean onIsHigh();

  /**
 A flag that indicates whether the microcontroller supports PWM on the given
 pin. \return The flag value.
         - true: The microcontroller supports PWM on the given pin.
         - false: The microcontroller does not support PWM on the given pin.
 */
  boolean supportsPWM();

  /**
 A flag that indicates if the LED is bright or dark. If the LED is driven by a
 digitalWrite, the meaning of this flag is obvious. When using PWD (i.e., when
 the LED is driven by analogWrite), all brightness values above
 MIN_LED_BRIGHTNESS are considered as "bright". \return The flag value.
         - true: The LED is bright (at least a little bit in case of using
 PWM).
         - false: The LED is dark.
 */
  boolean isOn();

  /**
 A flag that indicates if the LED is  dark or bright. If the LED is driven by
 a digitalWrite(), the meaning of this flag is obvious. When using PWD (i.e.,
 when the LED is driven by analogWrite()), all brightness values above
 MIN_LED_BRIGHTNESS are considered as "bright". \return The flag value.
         - true: The LED is dark.
         - false: The LED is bright (at least a little bit in case of using
 PWM).
 */
  boolean isOff();

  /**
 Returns the current brightness value. If the LED is driven by digitalWrite(),
 one of the following two values is returned:
 - MIN_LED_BRIGHTNESS if the LED is dark.
 - MAX_LED_BRIGHTNESS if the LED is bright.
 When using PWM (analogWrite()), the the current value between
 MIN_LED_BRIGHTNESS  and MAX_LED_BRIGHTNESS is returned.
 \return The
 brightness value.
 */
  int getBrightness();

  /**
 Switches the LED to maximum brightness  (MAX_LED_BRIGHTNESS).
 */
  void on();

  /**
 Switches the LED to complete darkness  (MIN_LED_BRIGHTNESS).
 */
  void off();

  /**
 Changes the brightness of the LED.
 \param brightness The new brightness value. The actual value is constrained
 to the interval MIN_LED_BRIGHTNESS .. MAX_LED_BRIGHTNESS. The implementation
 uses
        - digitalWrite() for the extremum values  MIN_LED_BRIGHTNESS and
 MAX_LED_BRIGHTNESS, and
        - analogWrite() for all other brightness values (using PWM).
 */
  void setBrightness(int brightness);

  /**
 Toggles the brightness value of the LED. The new brightness is the difference
 between MAX_LED_BRIGHTNESS and the old value. The implementation uses
 - digitalWrite() for the extremum values  MIN_LED_BRIGHTNESS and
 MAX_LED_BRIGHTNESS, and
 - analogWrite() for all other brightness values (using PWM).
 */
  void toggle();
};

/**
 * A combined LED with a red, green, and blue component. Internally, each
 * component is represented by an instance of SimpleLED.
 */
class RGBLED
{
private:
  SimpleLED _r;
  SimpleLED _g;
  SimpleLED _b;

public:
  // RGBLED(SimpleLED redLED, SimpleLED greenLED, SimpleLED blueLED);

  /**
 Constructor for a triple LED.

 \param pinRed The pin that drives the red component  of the LED.

 \param pinGreen The pin that drives the green component  of the LED.

 \param pinBlue The pin that drives the blue  component  of the LED.

 \param onIsHigh A flag that indicates which voltage level makes the LEDs
        shine:
        - onIsHigh=true: The LEDs are bright if a HIGH value is applied. They
 are dark if a LOW value is applied.
        - onIsHigh=false: The LEDs are bright if a LOW value is applied. They
 are dark if a HIGH value is applied.

 \param supportsPWM A flag that indicates
 that the microcontroller supports PWM on the given pins.
 */
  RGBLED(int pinRed, int pinGreen, int pinBlue, boolean onIsHigh,
         boolean supportsPWM);

  /**
 Constructor for a triple LED. Note: It is assumed that the microcontroller does
 not support PWM on the given pins.

 \param pinRed The pin that drives the red component  of the LED.

 \param pinGreen The pin that drives the green component  of the LED.

 \param pinBlue The pin that drives the blue  component  of the LED.

 \param onIsHigh A flag that indicates which voltage level makes the LEDs
        shine:
        - onIsHigh=true: The LEDs are bright if a HIGH value is applied. They
 are dark if a LOW value is applied.
        - onIsHigh=false: The LEDs are bright if a LOW value is applied. They
 are dark if a HIGH value is applied.

 */
  RGBLED(int pinRed, int pinGreen, int pinBlue, boolean onIsHigh);

  /**
 Constructor for a triple LED. Note: It is assumed that the microcontroller does
 not support PWM on the given pins.  The LEDs are bright if a voltage level of
 HIGH is applied.

 \param pinRed The pin that
 drives the red component  of the LED.

 \param pinGreen The pin that drives the
 green component  of the LED.

 \param pinBlue The pin that drives the blue
 component  of the LED.
 */
  RGBLED(int pinRed, int pinGreen, int pinBlue);

  /**
 Returns the red component of the triple LED.
 \return The component LED.
 */
  SimpleLED getRedLED();

  /**
 Returns the green component of the triple LED.
 \return The component LED.
 */
  SimpleLED getGreenLED();

  /**
 Returns the blue component of the triple LED.
 \return The component LED.
 */
  SimpleLED getBlueLED();

  /**
 Indicates whether all three components LEDs are on.
 \return A flag describing the brightness states of all three component LEDs.
         - true: At least one of the LEDs is not completely dark.
         - false: All three LEDs are completely dark.
 */
  boolean isOn();

  /**
 Indicates whether the red component LED is on.
 \return A flag describing the brightness states of the red component LED.
         - true: The LED is not completely dark.
         - false:The LED is completely dark.
 */
  boolean isRedOn();

  /**
 Indicates whether the green component LED is on.
 \return A flag describing the brightness states of the green component LED.
         - true: The LED is not completely dark.
         - false:The LED is completely dark.
 */
  boolean isGreenOn();

  /**
 Indicates whether the blue component LED is on.
 \return A flag describing the brightness states of the blue component LED.
         - true: The LED is not completely dark.
         - false:The LED is completely dark.
 */
  boolean isBlueOn();

  /**
 Indicates whether all three components LEDs are off.
 \return A flag describing the brightness states of all three component LEDs.
         - true: All three LEDs are completely dark.
         - false: At least one of the LEDs is not completely dark.
 */
  boolean isOff();

  /**
 Indicates whether the red component LED is off.
 \return A flag describing the brightness states of the red component LED.
         - true:The LED is completely dark.
         - false: The LED is not completely dark.
 */
  boolean isRedOff();

  /**
 Indicates whether the green component LED is off.
 \return A flag describing the brightness states of the green component LED.
         - true:The LED is completely dark.
         - false: The LED is not completely dark.
 */
  boolean isGreenOff();

  /**
 Indicates whether the blue component LED is off.
 \return A flag describing the brightness states of the blue component LED.
         - true:The LED is completely dark.
         - false: The LED is not completely dark.
 */
  boolean isBlueOff();

  /**
 Returns the brightness value of the red component LED.
 \return The current brightness.
 */
  int getRed();

  /**
 Returns the brightness value of the green component LED.
 \return The current brightness.
 */
  int getGreen();

  /**
 Returns the brightness value of the blue component LED.
 \return The current brightness.
 */
  int getBlue();

  /**
 Switches all three component LEDs on (maximum brightness).
 */
  void on();

  /**
 Switches the red component LED on (maximum brightness).
 */
  void redOn();

  /**
 Switches the green component LED on (maximum brightness).
 */
  void greenOn();

  /**
 Switches the blue component LED on (maximum brightness).
 */
  void blueOn();

  /**
 Switches all three component LEDs off (complete darkness).
 */
  void off();

  /**
 Switches the red component LED off (complete darkness).
 */
  void redOff();

  /**
 Switches the green component LED off (complete darkness).
 */
  void greenOff();

  /**
 Switches the blue component LED off (complete darkness).
 */
  void blueOff();

  /**
 Changes the brightness of the red component LED.
 \param r The new brightness value.
 */
  void setRed(int r);

  /**
 Changes the brightness of the green component LED.
 \param g The new brightness value.
 */
  void setGreen(int g);

  /**
 Changes the brightness of the blue component LED.
 \param b The new brightness value.
 */
  void setBlue(int b);

  /**
 Changes the brightness of all three component LEDs.
 \param r The new brightness value for the red component LED.
 \param g The new brightness value for the green component LED.
 \param b The new brightness value for the blue component LED.
 */
  void setRGB(int r, int g, int b);

  /**
 Changes the brightness of all three component LEDs to a common value resulting
 in gray. \param brightness The new brightness value for all three component
 LEDs.
 */
  void setBrightness(int brightness);

  /**
 Changes the brightness of all three component LEDs to a given HSB value.
 \param hue The hue value (0..360).
 \param saturation The saturation value (0.0 .. 1.0).
 \param brightness The brightness value (0.0 .. 1.0).
 */
  void setHSB(float hue, float saturation, float brightness);

  /**
 Toggles the brightness value of all three component LEDs. Each new brightness
 is the difference between MAX_LED_BRIGHTNESS and the old value. The
 implementation uses
 - digitalWrite() for the extremum values  MIN_LED_BRIGHTNESS and
 MAX_LED_BRIGHTNESS, and
 - analogWrite() for all other brightness values (using PWM).
 */
  void toggle();

  /**
   Convenience method: Indicates that a worker procedure is paused.
   */
  void workerPaused();

  /**
   Convenience method: Indicates that a worker procedure is active.
   */
  void workerRunning();

  /**
   Convenience method: Indicates that a worker procedure has failed.
   */
  void workerFailure();

  /**
   Convenience method: Indicates that a worker procedure was successfully
   terminated.
   */
  void workerSuccess();

  /**
   Changes the color to aliceblue (decimal values: 239/247/255, hex values:
\#EFF7FF)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwUlEQVR4Xu3UMQ0AIBDAwPdvlpERFkTQ5JrT0BlJCrX2AficYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpDxhiVJiS71Dg3JlH9ikgAAAABJRU5ErkJggg=='>
</td>
</tr>
</table>
  */
  void aliceblue();

  /**
   Changes the color to antiquewhite (decimal values: 249/232/210, hex values:
\#F9E8D2)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAw0lEQVR4Xu3UQQ0AIBDAsPMvDxVY4McHESzpUg2bkaRQZy+AzxkWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGW9YkpToApF2eagA50KHAAAAAElFTkSuQmCC'>
</td>
</tr>
</table>
  */
  void antiquewhite();

  /**
   Changes the color to aqua (decimal values: 0/255/255, hex values:
\#00FFFF)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAuklEQVR4Xu3UAQkAAAjAMPuX1hoedpbhM5JUahfgO8MCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgQ5JCHefws21g5bwHAAAAAElFTkSuQmCC'>
</td>
</tr>
</table>
  */
  void aqua();

  /**
   Changes the color to aquamarine (decimal values: 67/183/186, hex values:
\#43B7BA)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwUlEQVR4Xu3UUQkAIBTAwNfN0ILp/DGEgxuXYTOSFGrtA/A5wwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDjDUuSEl1Ueh/y+hDmawAAAABJRU5ErkJggg=='>
</td>
</tr>
</table>
  */
  void aquamarine();

  /**
   Changes the color to azure (decimal values: 239/255/255, hex values:
\#EFFFFF)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwElEQVR4Xu3UQQ0AIAzAwPk3i4TxQQRNrjkNnZGkUGcX4HOGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQMYbliQlujm6YuenvEIMAAAAAElFTkSuQmCC'>
</td>
</tr>
</table>
  */
  void azure();

  /**
   Changes the color to beige (decimal values: 245/243/215, hex values:
\#F5F3D7)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsPNvECGEPx9EsKRLNWxGkkKdvQA+Z1hAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkvGFJUqIL2Jz5VQJligMAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void beige();

  /**
   Changes the color to bisque (decimal values: 253/224/188, hex values:
\#FDE0BC)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UUQkAIBTAwNc/k2EsIvhjCAc3LsNmJCnU2Qvgc4YFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAxhuWJCW6hx3k8ETIO/cAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void bisque();

  /**
   Changes the color to black (decimal values: 0/0/0, hex values: \#000000)<br>
<table border='1'>
<tr valign='top'>
<td>
<img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAUUlEQVR4Xu3BAQ0AAADCoPdPbQ43oAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADeDeqSAAFnUPT8AAAAAElFTkSuQmCC'>
</td>
</tr>
</table>
  */
  void black();

  /**
   Changes the color to blanchedalmond (decimal values: 254/232/198, hex values:
\#FEE8C6)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsPMvDC2oIHwQwZIu1bAZSQp19gL4nGEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQ8YYlSYkuqlJvKptxW8QAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void blanchedalmond();

  /**
   Changes the color to blue (decimal values: 0/0/255, hex values: \#0000FF)<br>
<table border='1'>
<tr valign='top'>
<td>
<img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAuklEQVR4Xu3UAQkAAAjAMPuX1hoedpbhM5KUagHeMywgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyJKnTAbxmWbfS0hGcAAAAAElFTkSuQmCC'>
</td>
</tr>
</table>
  */
  void blue();

  /**
   Changes the color to blueviolet (decimal values: 121/49/223, hex values:
\#7931DF)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsLOEPNRihQ8iWNKlGjYjSaH2OgCfMywgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIy3rAkKdEFuH2WTyfyaQkAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void blueviolet();

  /**
   Changes the color to brown (decimal values: 152/5/22, hex values:
\#980516)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsPtgAe3I5YMIlnSphs1IUqizNsDnDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsICMNyxJSnQB8Z0w7ZkOBvsAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void brown();

  /**
   Changes the color to burlywood (decimal values: 234/190/131, hex values:
\#EABE83)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsPOOLwShgg8iWNKlGjYjSaHOXgCfMywgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIy3rAkKdEFq9vSUGmpEO4AAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void burlywood();

  /**
   Changes the color to cadetblue (decimal values: 87/134/147, hex values:
\#578693)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsHOJIwxgmA8iWNKlGjYjSaHWPgCfMywgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIy3rAkKdEFwP1MRsVdGboAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void cadetblue();

  /**
   Changes the color to chartreuse (decimal values: 138/251/23, hex values:
\#8AFB17)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsHOBZyzz4YMIlnSphs1IUqh9FsDnDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsICMNyxJSnQBwNAgicTSIjoAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void chartreuse();

  /**
   Changes the color to chocolate (decimal values: 200/90/23, hex values:
\#C85A17)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsHOBNxQjiw8iWNKlGjYjSaHOXgCfMywgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIy3rAkKdEFY1xC9koGaWkAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void chocolate();

  /**
   Changes the color to coral (decimal values: 247/101/65, hex values:
\#F76541)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UMQ0AIBDAwLeGGPyPrCyIoMk1p6EzkhTq7AXwOcMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwg4w1LkhJdsmprKZy2r9AAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void coral();

  /**
   Changes the color to cornflowerblue (decimal values: 21/27/141, hex values:
\#151B8D)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsDPBC8145oMIlnSphs1IUqi1D8DnDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsICMNyxJSnQBIu0bWhgIUtYAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void cornflowerblue();

  /**
   Changes the color to cornsilk (decimal values: 255/247/215, hex values:
\#FFF7D7)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsPNvECk84YMIlnSphs1IUqizF8DnDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsICMNyxJSnQBeycOYNf+KlsAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void cornsilk();

  /**
   Changes the color to crimson (decimal values: 228/27/23, hex values:
\#E41B17)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAw0lEQVR4Xu3UQQ0AIBDAsHPBA//OEMIHESzpUg2bkaRQZy+AzxkWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGW9YkpToAkEYDoCzLGF6AAAAAElFTkSuQmCC'>
</td>
</tr>
</table>
  */
  void crimson();

  /**
   Changes the color to cyan (decimal values: 0/255/255, hex values:
\#00FFFF)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAuklEQVR4Xu3UAQkAAAjAMPuX1hoedpbhM5JUahfgO8MCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgQ5JCHefws21g5bwHAAAAAElFTkSuQmCC'>
</td>
</tr>
</table>
  */
  void cyan();

  /**
   Changes the color to darkblue (decimal values: 47/47/79, hex values:
\#2F2F4F)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIAzAwDmaFPy74YMImlxzGjojSaF2D8DnDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsICMNyxJSnQBQipxDwxQS9wAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void darkblue();

  /**
   Changes the color to darkcyan (decimal values: 87/254/255, hex values:
\#57FEFF)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwUlEQVR4Xu3UQQ0AIBDAsHOJfyPABxEs6VINm5GkUGsfgM8ZFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARlvWJKU6AJnl8akVBvUvgAAAABJRU5ErkJggg=='>
</td>
</tr>
</table>
  */
  void darkcyan();

  /**
   Changes the color to darkgoldenrod (decimal values: 175/120/23, hex values:
\#AF7817)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsHOBVVxhkQ8iWNKlGjYjSaHOXgCfMywgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIy3rAkKdEFJDe4JeekceoAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void darkgoldenrod();

  /**
   Changes the color to darkgray (decimal values: 122/119/119, hex values:
\#7A7777)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwElEQVR4Xu3UQQkAMAzAwLouTP0+E7HAhdOQGUkKdXYBPmdYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZLxhSVKiC9rO9xlqIq7xAAAAAElFTkSuQmCC'>
</td>
</tr>
</table>
  */
  void darkgray();

  /**
   Changes the color to darkgreen (decimal values: 37/65/23, hex values:
\#254117)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAw0lEQVR4Xu3UQQ0AIBDAsHOBAoTg3xQfRLCkSzVsRpJC7bMAPmdYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZLxhSVKiCw/wcj19F7uAAAAAAElFTkSuQmCC'>
</td>
</tr>
</table>
  */
  void darkgreen();

  /**
   Changes the color to darkkhaki (decimal values: 183/173/89, hex values:
\#B7AD59)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsLOJSN7Y44MIlnSphs1IUqizF8DnDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsICMNyxJSnQBHHa/sLrzAKAAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void darkkhaki();

  /**
   Changes the color to darkmagenta (decimal values: 244/62/255, hex values:
\#F43EFF)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsFOGf0H84YMIlnSphs1IUqi9DsDnDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsICMNyxJSnQB4h6SLjZbSFQAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void darkmagenta();

  /**
   Changes the color to darkolivegreen (decimal values: 204/251/93, hex values:
\#CCFB5D)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsDOKYxzx4YMIlnSphs1IUqh9FsDnDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsICMNyxJSnQBJPnH0q+dIAIAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void darkolivegreen();

  /**
   Changes the color to darkorange (decimal values: 248/128/23, hex values:
\#F88017)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsHOBXWTz5IMIlnSphs1IUqizF8DnDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsICMNyxJSnQBg+9WLV4dfpgAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void darkorange();

  /**
   Changes the color to darkorchid (decimal values: 125/27/126, hex values:
\#7D1B7E)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwUlEQVR4Xu3UQQ0AIBDAsPOBVtDOBxEs6VINm5GkUHsdgM8ZFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARlvWJKU6AIw1A6AWDbkEgAAAABJRU5ErkJggg=='>
</td>
</tr>
</table>
  */
  void darkorchid();

  /**
   Changes the color to darkred (decimal values: 228/27/23, hex values:
\#E41B17)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAw0lEQVR4Xu3UQQ0AIBDAsHPBA//OEMIHESzpUg2bkaRQZy+AzxkWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGW9YkpToAkEYDoCzLGF6AAAAAElFTkSuQmCC'>
</td>
</tr>
</table>
  */
  void darkred();

  /**
   Changes the color to darksalmon (decimal values: 225/139/107, hex values:
\#E18B6B)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsPOLFFTihA8iWNKlGjYjSaHOXgCfMywgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIy3rAkKdEFuuFUaF1vlFsAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void darksalmon();

  /**
   Changes the color to darkseagreen (decimal values: 139/179/129, hex values:
\#8BB381)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsLPOGzkY5IMIlnSphs1IUqh9FsDnDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsICMNyxJSnQBb6pU//Hht3gAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void darkseagreen();

  /**
   Changes the color to darkslateblue (decimal values: 43/56/86, hex values:
\#2B3856)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAw0lEQVR4Xu3UQQ0AIBDAsPODAZTgXwofRLCkSzVsRpJCrX0APmdYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZLxhSVKiC/QB8LwlY6nOAAAAAElFTkSuQmCC'>
</td>
</tr>
</table>
  */
  void darkslateblue();

  /**
   Changes the color to darkslategray (decimal values: 37/56/86, hex values:
\#253856)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAw0lEQVR4Xu3UQQ0AIBDAsDODB5TgXwofRLCkSzVsRpJCrX0APmdYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZLxhSVKiC8CxMO08YOb1AAAAAElFTkSuQmCC'>
</td>
</tr>
</table>
  */
  void darkslategray();

  /**
   Changes the color to darkturquoise (decimal values: 59/156/156, hex values:
\#3B9C9C)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwElEQVR4Xu3UQQ0AIAzAwPnC6sTyQQRNrjkNnZGkUGcX4HOGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQMYbliQlujEALDVrgeevAAAAAElFTkSuQmCC'>
</td>
</tr>
</table>
  */
  void darkturquoise();

  /**
   Changes the color to darkviolet (decimal values: 132/45/206, hex values:
\#842DCE)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsDOES/ShiQ8iWNKlGjYjSaH2OgCfMywgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIy3rAkKdEFzK2r4racJOwAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void darkviolet();

  /**
   Changes the color to deeppink (decimal values: 245/40/135, hex values:
\#F52887)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsJODUxTz54MIlnSphs1IUqizNsDnDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsICMNyxJSnQBkD51p4RA3X8AAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void deeppink();

  /**
   Changes the color to deepskyblue (decimal values: 59/185/255, hex values:
\#3BB9FF)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwUlEQVR4Xu3UQQ0AIBDAsPOFctTBBxEs6VINm5GkUGsfgM8ZFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARlvWJKU6AKdeX5g2hs2nAAAAABJRU5ErkJggg=='>
</td>
</tr>
</table>
  */
  void deepskyblue();

  /**
   Changes the color to dimgray (decimal values: 70/62/65, hex values:
\#463E41)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UwQkAIBDAsJtM8OP+I/lxCAspmaEzkhTqrA3wOcMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwg4w1LkhJd2cZweDLt7D0AAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void dimgray();

  /**
   Changes the color to dodgerblue (decimal values: 21/137/255, hex values:
\#1589FF)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwUlEQVR4Xu3UQQ0AIBDAsDOBaFTDBxEs6VINm5GkUGsfgM8ZFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARlvWJKU6AJnU2sp5dVAeAAAAABJRU5ErkJggg=='>
</td>
</tr>
</table>
  */
  void dodgerblue();

  /**
   Changes the color to firebrick (decimal values: 128/5/23, hex values:
\#800517)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsPugAbtI54MIlnSphs1IUqizNsDnDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsICMNyxJSnQBQkt8JNt/rDYAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void firebrick();

  /**
   Changes the color to floralwhite (decimal values: 255/249/238, hex values:
\#FFF9EE)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwUlEQVR4Xu3UMQ0AIBDAwPcvlp0JFkTQ5JrT0BlJCnX2AvicYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpDxhiVJiS780lhpfWrTvgAAAABJRU5ErkJggg=='>
</td>
</tr>
</table>
  */
  void floralwhite();

  /**
   Changes the color to forestgreen (decimal values: 78/146/88, hex values:
\#4E9258)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsFPIG10o5oMIlnSphs1IUqh1NsDnDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsICMNyxJSnQBdLX4R/SimXUAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void forestgreen();

  /**
   Changes the color to fuchsia (decimal values: 255/0/255, hex values:
\#FF00FF)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAv0lEQVR4Xu3UMQ0AMAzAsPIn3T0FsUiOjCEzkhRqZwE+Z1hAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVk3LAkKdEDQbWzbZbxQcMAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void fuchsia();

  /**
   Changes the color to gainsboro (decimal values: 216/217/215, hex values:
\#D8D9D7)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwUlEQVR4Xu3UQQ0AIBDAsPNvEHDDBxEs6VINm5GkUPssgM8ZFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARlvWJKU6ALsKfAFTX+LHQAAAABJRU5ErkJggg=='>
</td>
</tr>
</table>
  */
  void gainsboro();

  /**
   Changes the color to ghostwhite (decimal values: 247/247/255, hex values:
\#F7F7FF)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwElEQVR4Xu3UMQ0AMAzAsPIn3HN7BmKRHBlDZiQp1O4B+JxhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkPGGJUmJLo7YYucDU9ocAAAAAElFTkSuQmCC'>
</td>
</tr>
</table>
  */
  void ghostwhite();

  /**
   Changes the color to gold (decimal values: 212/160/23, hex values:
\#D4A017)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsHOBfUQhiA8iWNKlGjYjSaHOXgCfMywgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIy3rAkKdEFYoMrnirJWgsAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void gold();

  /**
   Changes the color to goldenrod (decimal values: 237/218/116, hex values:
\#EDDA74)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsBONWjzggQ8iWNKlGjYjSaHOXgCfMywgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIy3rAkKdEFdK98m+fDLAAAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void goldenrod();

  /**
   Changes the color to gray (decimal values: 128/128/128, hex values:
\#808080)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAtklEQVR4Xu3UQQ0AMAgAMaQjnc9E7JJWRGcAQhbge8ICMoQFZAgLyBAWkCEsIENYQIawgAxhARnCAjKEBWQIC8gQFpAhLCBDWECGsIAMYQEZwgIyhAVkCAvIEBaQISwgQ1hAhrCADGEBGcICMoQFZAgLyBAWkCEsIENYQIawgAxhARnCAjKEBWQIC8gQFpAhLCBDWECGsIAMYQEZwgIyhAVkCAvIEBaQISwgQ1hAhrCAjBcWQMIByMX2guXbB50AAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void gray();

  /**
   Changes the color to green (decimal values: 0/255/0, hex values:
\#00FF00)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAuklEQVR4Xu3UAQkAAAjAMPuX1hoedpbhM5JUagHeMywgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyJCnUARYrWbdhJoXvAAAAAElFTkSuQmCC'>
</td>
</tr>
</table>
  */
  void green();

  /**
   Changes the color to greenyellow (decimal values: 177/251/23, hex values:
\#B1FB17)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAw0lEQVR4Xu3UQQ0AIBDAsHOBfwno48MHESzpUg2bkaRQ+yyAzxkWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGW9YkpToAg52f46NrS3DAAAAAElFTkSuQmCC'>
</td>
</tr>
</table>
  */
  void greenyellow();

  /**
   Changes the color to honeydew (decimal values: 240/254/238, hex values:
\#F0FEEE)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwUlEQVR4Xu3UUQkAIBTAwNc/rJhA/DGEgxuXYTOSFGqfBfA5wwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDjDUuSEl2Hbm38SbHe9gAAAABJRU5ErkJggg=='>
</td>
</tr>
</table>
  */
  void honeydew();

  /**
   Changes the color to indianred (decimal values: 94/34/23, hex values:
\#5E2217)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAw0lEQVR4Xu3UQQ0AIBDAsHPBH2H4l8EHESzpUg2bkaRQZy+AzxkWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGW9YkpToAk2RBvX35XZLAAAAAElFTkSuQmCC'>
</td>
</tr>
</table>
  */
  void indianred();

  /**
   Changes the color to indigo (decimal values: 48/125/126, hex values:
\#307D7E)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwUlEQVR4Xu3UQQ0AIBDAsJOEQdDOBxEs6VINm5GkUGsfgM8ZFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARlvWJKU6AJRiC36MDP+lgAAAABJRU5ErkJggg=='>
</td>
</tr>
</table>
  */
  void indigo();

  /**
   Changes the color to ivory (decimal values: 255/255/238, hex values:
\#FFFFEE)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwElEQVR4Xu3UQQ0AIAzAwPkXi4XxQQRNrjkNnZGkULsH4HOGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQMYbliQlunBTGEe2Uw6VAAAAAElFTkSuQmCC'>
</td>
</tr>
</table>
  */
  void ivory();

  /**
   Changes the color to khaki (decimal values: 173/169/110, hex values:
\#ADA96E)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsFOMLx545IMIlnSphs1IUqizF8DnDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsICMNyxJSnQB7o/KLtW6FNEAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void khaki();

  /**
   Changes the color to lavenderblush (decimal values: 253/238/244, hex values:
\#FDEEF4)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UsQkAIBDAwN9/WCtbwcYhDFy4GTIjSaHO2gCfMywgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIy3rAkKdEFy6VN610Teh8AAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void lavenderblush();

  /**
   Changes the color to lavender (decimal values: 227/228/250, hex values:
\#E3E4FA)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsPNvE1Tw4oMIlnSphs1IUqi1D8DnDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsICMNyxJSnQBNcaOpHb6bqAAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void lavender();

  /**
   Changes the color to lawngreen (decimal values: 135/247/23, hex values:
\#87F717)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsHOBZCTz5YMIlnSphs1IUqh9FsDnDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsICMNyxJSnQBBOEWCzv6jQIAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void lawngreen();

  /**
   Changes the color to lemonchiffon (decimal values: 255/248/198, hex values:
\#FFF8C6)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsPMvDE384IMIlnSphs1IUqizF8DnDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsICMNyxJSnQB5slkFXbnX5sAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void lemonchiffon();

  /**
   Changes the color to lightblue (decimal values: 173/223/255, hex values:
\#ADDFFF)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsPPvBU1IgQ8iWNKlGjYjSaHWPgCfMywgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIy3rAkKdEF8prP9JOMiIAAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void lightblue();

  /**
   Changes the color to lightcoral (decimal values: 231/116/113, hex values:
\#E77471)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwUlEQVR4Xu3UUQkAIBTAwFdZsLM1/DGEgxuXYTOSFOrsBfA5wwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDjDUuSEl08ix9bvrkPnwAAAABJRU5ErkJggg=='>
</td>
</tr>
</table>
  */
  void lightcoral();

  /**
   Changes the color to lightcyan (decimal values: 224/255/255, hex values:
\#E0FFFF)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwElEQVR4Xu3UQQ0AIAzAwPlXiZPxQQRNrjkNnZGkUGcX4HOGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQMYbliQlurljA0sc21qrAAAAAElFTkSuQmCC'>
</td>
</tr>
</table>
  */
  void lightcyan();

  /**
   Changes the color to lightgoldenrodyellow (decimal values: 250/248/204, hex
values: \#FAF8CC)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsPOvDUGEFx9EsKRLNWxGkkKdvQA+Z1hAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkvGFJUqILGlmutdIDEA0AAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void lightgoldenrodyellow();

  /**
   Changes the color to lightgreen (decimal values: 204/255/204, hex values:
\#CCFFCC)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwElEQVR4Xu3UQQ0AIAzAwPnXNk/wQQRNrjkNnZGkUHsW4HOGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQMYbliQlusI1T7Ctb9XPAAAAAElFTkSuQmCC'>
</td>
</tr>
</table>
  */
  void lightgreen();

  /**
   Changes the color to lightpink (decimal values: 250/175/186, hex values:
\#FAAFBA)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UMQ0AIBDAwPdvhh1pTCyIoMk1p6EzkhTqrA3wOcMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwg4w1LkhJdLT8mT4HojUgAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void lightpink();

  /**
   Changes the color to lightsalmon (decimal values: 249/150/107, hex values:
\#F9966B)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsPOLH7zy44MIlnSphs1IUqizF8DnDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsICMNyxJSnQBBpWI3vC7MxUAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void lightsalmon();

  /**
   Changes the color to lightseagreen (decimal values: 62/169/159, hex values:
\#3EA99F)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsFOGVd645IMIlnSphs1IUqh1NsDnDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsICMNyxJSnQB8/O2YNqb2Z8AAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void lightseagreen();

  /**
   Changes the color to lightskyblue (decimal values: 130/202/250, hex values:
\#82CAFA)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsNOOPCTx4oMIlnSphs1IUqi1D8DnDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsICMNyxJSnQBrkuxqKj2LfYAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void lightskyblue();

  /**
   Changes the color to lightslategray (decimal values: 109/123/141, hex values:
\#6D7B8D)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsDOMAjTgmQ8iWNKlGjYjSaHWPgCfMywgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIy3rAkKdEF9dnBdW/6XBsAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void lightslategray();

  /**
   Changes the color to lightsteelblue (decimal values: 114/143/206, hex values:
\#728FCE)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsNOMEZyhiQ8iWNKlGjYjSaHWPgCfMywgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIy3rAkKdEFO3H/Oy8GuhkAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void lightsteelblue();

  /**
   Changes the color to lightyellow (decimal values: 255/254/220, hex values:
\#FFFEDC)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwUlEQVR4Xu3UQQ0AIBDAsPOvES3ABxEs6VINm5GkUGcvgM8ZFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARlvWJKU6ALs3I4NhqCrqwAAAABJRU5ErkJggg=='>
</td>
</tr>
</table>
  */
  void lightyellow();

  /**
   Changes the color to lime (decimal values: 0/255/0, hex values: \#00FF00)<br>
<table border='1'>
<tr valign='top'>
<td>
<img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAuklEQVR4Xu3UAQkAAAjAMPuX1hoedpbhM5JUagHeMywgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyJCnUARYrWbdhJoXvAAAAAElFTkSuQmCC'>
</td>
</tr>
</table>
  */
  void lime();

  /**
   Changes the color to limegreen (decimal values: 65/163/23, hex values:
\#41A317)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsHOBHrRilA8iWNKlGjYjSaH2WQCfMywgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIy3rAkKdEFsskvKDT9p4QAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void limegreen();

  /**
   Changes the color to linen (decimal values: 249/238/226, hex values:
\#F9EEE2)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UMQ0AIBDAwPcvEwHsbCyIoMk1p6EzkhTq7AXwOcMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwg4w1LkhJd4+bjwhLINtIAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void linen();

  /**
   Changes the color to magenta (decimal values: 255/0/255, hex values:
\#FF00FF)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAv0lEQVR4Xu3UMQ0AMAzAsPIn3T0FsUiOjCEzkhRqZwE+Z1hAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVk3LAkKdEDQbWzbZbxQcMAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void magenta();

  /**
   Changes the color to maroon (decimal values: 128/0/0, hex values:
\#800000)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAA20lEQVR4Xu3JsQ0AIADDME7ndJZufaBIjrzlHEn6qAswL/UAWJN6AKxJPQDWpB4Aa1IPgDWpB8Ca1ANgTeoBsCb1AFiTegCsST0A1qQeAGtSD4A1qQfAmtQDYE3qAbAm9QBYk3oArEk9ANakHgBrUg+ANakHwJrUA2BN6gGwJvUAWJN6AKxJPQDWpB4Aa1IPgDWpB8Ca1ANgTeoBsCb1AFiTegCsST0A1qQeAGtSD4A1qQfAmtQDYE3qAbAm9QBYk3oArEk9ANakHgBrUg+ANakHwJrUA2CNJP3UA4bYUiysLFkiAAAAAElFTkSuQmCC'>
</td>
</tr>
</table>
  */
  void maroon();

  /**
   Changes the color to mediumaquamarine (decimal values: 52/135/129, hex
values: \#348781)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsBOFQV7Y5oMIlnSphs1IUqh1NsDnDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsICMNyxJSnQBjf8i5XJFpaAAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void mediumaquamarine();

  /**
   Changes the color to mediumblue (decimal values: 21/45/198, hex values:
\#152DC6)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAw0lEQVR4Xu3UQQ0AIBDAsDOBDPw7QBcfRLCkSzVsRpJCrX0APmdYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZLxhSVKiCye5+XWLrSjPAAAAAElFTkSuQmCC'>
</td>
</tr>
</table>
  */
  void mediumblue();

  /**
   Changes the color to mediumorchid (decimal values: 176/72/181, hex values:
\#B048B5)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsJOHXD7444MIlnSphs1IUqi9DsDnDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsICMNyxJSnQBtZoVdArxn/YAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void mediumorchid();

  /**
   Changes the color to mediumpurple (decimal values: 132/103/215, hex values:
\#8467D7)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsHOLETxihw8iWNKlGjYjSaH2OgCfMywgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIy3rAkKdEFi6407g1exBUAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void mediumpurple();

  /**
   Changes the color to mediumseagreen (decimal values: 48/103/84, hex values:
\#306754)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAw0lEQVR4Xu3UQQ0AIBDAsJOEGDzg3wEfRLCkSzVsRpJCrbMBPmdYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZLxhSVKiC2t2hN0ar6VlAAAAAElFTkSuQmCC'>
</td>
</tr>
</table>
  */
  void mediumseagreen();

  /**
   Changes the color to mediumslateblue (decimal values: 94/90/128, hex values:
\#5E5A80)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsNPJAy1I54MIlnSphs1IUqi9DsDnDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsICMNyxJSnQBdUz4RwPro6YAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void mediumslateblue();

  /**
   Changes the color to mediumspringgreen (decimal values: 52/128/23, hex
values: \#348017)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsHOBEQQinQ8iWNKlGjYjSaH2WQCfMywgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIy3rAkKdEFTLAwVuU6dfoAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void mediumspringgreen();

  /**
   Changes the color to mediumturquoise (decimal values: 72/204/205, hex values:
\#48CCCD)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwUlEQVR4Xu3UQQ0AIBDAsJOHePDEBxEs6VINm5GkUGsfgM8ZFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARlvWJKU6AKhgj7VtehjeAAAAABJRU5ErkJggg=='>
</td>
</tr>
</table>
  */
  void mediumturquoise();

  /**
   Changes the color to mediumvioletred (decimal values: 202/34/107, hex values:
\#CA226B)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsNOCPZyiig8iWNKlGjYjSaHO2gCfMywgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIy3rAkKdEFvAwCPbj7noIAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void mediumvioletred();

  /**
   Changes the color to midnightblue (decimal values: 21/27/84, hex values:
\#151B54)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAw0lEQVR4Xu3UQQ0AIBDAsDPBC1v418IHESzpUg2bkaRQax+AzxkWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGW9YkpToAniGfLsnP4uQAAAAAElFTkSuQmCC'>
</td>
</tr>
</table>
  */
  void midnightblue();

  /**
   Changes the color to mintcream (decimal values: 245/255/249, hex values:
\#F5FFF9)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwUlEQVR4Xu3UMQ0AIBDAwPfvl4EJFkTQ5JrT0BlJCrXOBvicYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpDxhiVJiS65Z2LnCncpaAAAAABJRU5ErkJggg=='>
</td>
</tr>
</table>
  */
  void mintcream();

  /**
   Changes the color to mistyrose (decimal values: 253/225/221, hex values:
\#FDE1DD)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UwQkAIBDAsNt/Rx8OIvhxCAspmaEzkhTq7AXwOcMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwg4w1LkhJdy5XOxki4FdIAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void mistyrose();

  /**
   Changes the color to moccasin (decimal values: 253/224/172, hex values:
\#FDE0AC)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UUQkAIBTAwNc/i6EsIvhjCAc3LsNmJCnU2Qvgc4YFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAxhuWJCW6qB86tEhls9cAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void moccasin();

  /**
   Changes the color to navajowhite (decimal values: 253/218/163, hex values:
\#FDDAA3)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UUQkAIBTAwNe/hq3sIvhjCAc3LsNmJCnU2Qvgc4YFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAxhuWJCW6J0DbCZBSZ3UAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void navajowhite();

  /**
   Changes the color to navy (decimal values: 0/0/128, hex values: \#000080)<br>
<table border='1'>
<tr valign='top'>
<td>
<img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAA20lEQVR4Xu3JsQ0AIADDME7ndJZufaBIjrzlHEn6qgswL/UAWJN6AKxJPQDWpB4Aa1IPgDWpB8Ca1ANgTeoBsCb1AFiTegCsST0A1qQeAGtSD4A1qQfAmtQDYE3qAbAm9QBYk3oArEk9ANakHgBrUg+ANakHwJrUA2BN6gGwJvUAWJN6AKxJPQDWpB4Aa1IPgDWpB8Ca1ANgTeoBsCb1AFiTegCsST0A1qQeAGtSD4A1qQfAmtQDYE3qAbAm9QBYk3oArEk9ANakHgBrUg+ANakHwJrUA2CNJP3TA+JzUixoE6L4AAAAAElFTkSuQmCC'>
</td>
</tr>
</table>
  */
  void navy();

  /**
   Changes the color to oldlace (decimal values: 252/243/226, hex values:
\#FCF3E2)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UMQ0AIBDAwPcvEwusDCyIoMk1p6EzkhTq7AXwOcMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwg4w1LkhJdCIE478rWy5cAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void oldlace();

  /**
   Changes the color to olive (decimal values: 128/128/0, hex values:
\#808000)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAuklEQVR4Xu3UAQkAAAjAMKMb3RoedpbhM5IUahfgO8MCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgQ5JKHdDkpFdNY7xcAAAAAElFTkSuQmCC'>
</td>
</tr>
</table>
  */
  void olive();

  /**
   Changes the color to olivedrab (decimal values: 101/128/23, hex values:
\#658017)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsHOBQnwgnQ8iWNKlGjYjSaH2WQCfMywgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIy3rAkKdEFmnx5yGoCrC0AAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void olivedrab();

  /**
   Changes the color to orange (decimal values: 248/122/23, hex values:
\#F87A17)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsHOBV5Tz5IMIlnSphs1IUqizF8DnDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsICMNyxJSnQBEH2WT98wOiUAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void orange();

  /**
   Changes the color to orangered (decimal values: 246/56/23, hex values:
\#F63817)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAw0lEQVR4Xu3UQQ0AIBDAsHOBFfyL4s0HESzpUg2bkaRQZy+AzxkWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGW9YkpToArPSwqNT8NlRAAAAAElFTkSuQmCC'>
</td>
</tr>
</table>
  */
  void orangered();

  /**
   Changes the color to orchid (decimal values: 229/125/237, hex values:
\#E57DED)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsDOOXJ544IMIlnSphs1IUqi9DsDnDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsICMNyxJSnQBA3RRdbSQnmYAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void orchid();

  /**
   Changes the color to palegoldenrod (decimal values: 237/228/158, hex values:
\#EDE49E)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAw0lEQVR4Xu3UQQ0AIBDAsPMvAnV88cAHESzpUg2bkaRQZy+AzxkWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGW9YkpToAnPOpfywCvAKAAAAAElFTkSuQmCC'>
</td>
</tr>
</table>
  */
  void palegoldenrod();

  /**
   Changes the color to paleturquoise (decimal values: 174/235/236, hex values:
\#AEEBEC)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsPMvBmMggg8iWNKlGjYjSaHWPgCfMywgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIy3rAkKdEFVAIQJQGwKo8AAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void paleturquoise();

  /**
   Changes the color to palevioletred (decimal values: 209/101/135, hex values:
\#D16587)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsDOLFvThiA8iWNKlGjYjSaHO2gCfMywgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIy3rAkKdEFR5y/sI71ST4AAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void palevioletred();

  /**
   Changes the color to papayawhip (decimal values: 254/236/207, hex values:
\#FEECCF)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsPNvDiNoIHwQwZIu1bAZSQp19gL4nGEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQ8YYlSYkuWhU5hnQaHEsAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void papayawhip();

  /**
   Changes the color to peachpuff (decimal values: 252/213/176, hex values:
\#FCD5B0)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAw0lEQVR4Xu3UQQ0AIBDAsPOvBjO44cEHESzpUg2bkaRQZy+AzxkWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGW9YkpToAu5E5YckcigJAAAAAElFTkSuQmCC'>
</td>
</tr>
</table>
  */
  void peachpuff();

  /**
   Changes the color to peru (decimal values: 197/119/38, hex values:
\#C57726)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsFODRRTiiw8iWNKlGjYjSaHOXgCfMywgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIy3rAkKdEF4383So3MYQIAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void peru();

  /**
   Changes the color to pink (decimal values: 250/175/190, hex values:
\#FAAFBE)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAw0lEQVR4Xu3UQQ0AIBDAsPNvBgcI48UHESzpUg2bkaRQZ22AzxkWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGW9YkpToAqT3UN41A2zqAAAAAElFTkSuQmCC'>
</td>
</tr>
</table>
  */
  void pink();

  /**
   Changes the color to plum (decimal values: 185/59/143, hex values:
\#B93B8F)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsPOFSPTgjg8iWNKlGjYjSaHO2gCfMywgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIy3rAkKdEFQYHWcZR+u8AAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void plum();

  /**
   Changes the color to powderblue (decimal values: 173/220/227, hex values:
\#ADDCE3)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAw0lEQVR4Xu3UQQ0AIBDAsPPvBUf8MMIHESzpUg2bkaRQax+AzxkWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGW9YkpToAnKsxg0K655KAAAAAElFTkSuQmCC'>
</td>
</tr>
</table>
  */
  void powderblue();

  /**
   Changes the color to purple (decimal values: 128/0/128, hex values:
\#800080)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAv0lEQVR4Xu3UMQ0AMAzAsEIv9D0FsUiOjCEzkhRqZwE+Z1hAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVk3LAkKdEDfrmkV3OYNCkAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void purple();

  /**
   Changes the color to red (decimal values: 255/0/0, hex values: \#FF0000)<br>
<table border='1'>
<tr valign='top'>
<td>
<img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAuklEQVR4Xu3UAQkAAAjAMPuX1hoedpbhM5IUagHeMywgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyJKnUAW/hWbf+PgkbAAAAAElFTkSuQmCC'>
</td>
</tr>
</table>
  */
  void red();

  /**
   Changes the color to rosybrown (decimal values: 179/132/129, hex values:
\#B38481)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwUlEQVR4Xu3UUQkAIBTAwFddsJEF/TGEgxuXYTOSFOrsBfA5wwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDjDUuSEl09T0qBIMpSPAAAAABJRU5ErkJggg=='>
</td>
</tr>
</table>
  */
  void rosybrown();

  /**
   Changes the color to royalblue (decimal values: 43/96/222, hex values:
\#2B60DE)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsPODKZSjhQ8iWNKlGjYjSaHWPgCfMywgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIy3rAkKdEFMpBByPEn2AUAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void royalblue();

  /**
   Changes the color to saddlebrown (decimal values: 246/53/38, hex values:
\#F63526)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAw0lEQVR4Xu3UQQ0AIBDAsFODA/y74s0HESzpUg2bkaRQZy+AzxkWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGW9YkpToAjspQl9UPu3TAAAAAElFTkSuQmCC'>
</td>
</tr>
</table>
  */
  void saddlebrown();

  /**
   Changes the color to salmon (decimal values: 248/129/88, hex values:
\#F88158)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsJOJHVzz5IMIlnSphs1IUqizF8DnDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsICMNyxJSnQBhmOUiqOBnXQAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void salmon();

  /**
   Changes the color to sandybrown (decimal values: 238/154/77, hex values:
\#EE9A4D)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsDOIQUSigQ8iWNKlGjYjSaHOXgCfMywgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIy3rAkKdEFc1m/GZdyUckAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void sandybrown();

  /**
   Changes the color to seagreen (decimal values: 78/137/117, hex values:
\#4E8975)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsFOIG0Tgmg8iWNKlGjYjSaHW2QCfMywgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIy3rAkKdEFK63NIVb8vjoAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void seagreen();

  /**
   Changes the color to seashell (decimal values: 254/243/235, hex values:
\#FEF3EB)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UsQkAIBDAwN9/VUt7sXEIAxduhsxIUqizF8DnDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsICMNyxJSnQBfFlt/IRWoM4AAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void seashell();

  /**
   Changes the color to sienna (decimal values: 138/65/23, hex values:
\#8A4117)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsHOBHuyhmg8iWNKlGjYjSaHOXgCfMywgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIy3rAkKdEFxTDlEISUjsoAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void sienna();

  /**
   Changes the color to silver (decimal values: 192/192/192, hex values:
\#C0C0C0)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAt0lEQVR4Xu3UQQ0AMAgAMfyrQhqfidglrYjOAIQswPeEBWQIC8gQFpAhLCBDWECGsIAMYQEZwgIyhAVkCAvIEBaQISwgQ1hAhrCADGEBGcICMoQFZAgLyBAWkCEsIENYQIawgAxhARnCAjKEBWQIC8gQFpAhLCBDWECGsIAMYQEZwgIyhAVkCAvIEBaQISwgQ1hAhrCADGEBGcICMoQFZAgLyBAWkCEsIENYQIawgAxhARkvLICEAzfm8co6sUSkAAAAAElFTkSuQmCC'>
</td>
</tr>
</table>
  */
  void silver();

  /**
   Changes the color to skyblue (decimal values: 102/152/255, hex values:
\#6698FF)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwUlEQVR4Xu3UQQ0AIBDAsFOLJtzCBxEs6VINm5GkUGsfgM8ZFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARlvWJKU6AI85GjNpGKUzQAAAABJRU5ErkJggg=='>
</td>
</tr>
</table>
  */
  void skyblue();

  /**
   Changes the color to slateblue (decimal values: 115/124/161, hex values:
\#737CA1)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsPPMFzM45YMIlnSphs1IUqi1D8DnDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsICMNyxJSnQB6HygzdFgsp0AAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void slateblue();

  /**
   Changes the color to slategray (decimal values: 101/115/131, hex values:
\#657383)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsDOLAiRgnA8iWNKlGjYjSaHWPgCfMywgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIy3rAkKdEF7BcszAx7c94AAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void slategray();

  /**
   Changes the color to snow (decimal values: 255/249/250, hex values:
\#FFF9FA)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwElEQVR4Xu3UQQ0AIBDAsPPvGD7wQQRLulTDZiQp1Fkb4HOGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQMYbliQlumQY2BbY+NFAAAAAAElFTkSuQmCC'>
</td>
</tr>
</table>
  */
  void snow();

  /**
   Changes the color to springgreen (decimal values: 74/160/44, hex values:
\#4AA02C)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsBOEElQilQ8iWNKlGjYjSaH2WQCfMywgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIy3rAkKdEFOrsOgJwu0dIAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void springgreen();

  /**
   Changes the color to steelblue (decimal values: 72/99/160, hex values:
\#4863A0)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsJOHD4whlQ8iWNKlGjYjSaHWPgCfMywgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIy3rAkKdEFeZ6CgUOZ2aQAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void steelblue();

  /**
   Changes the color to tan (decimal values: 216/175/121, hex values:
\#D8AF79)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsLONKuwghw8iWNKlGjYjSaHOXgCfMywgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIy3rAkKdEFxdVIvOI36sIAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void tan();

  /**
   Changes the color to teal (decimal values: 0/128/128, hex values:
\#008080)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAuklEQVR4Xu3UAQkAAAjAMKMb3RoedpbhM5JUahfgO8MCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgQ5JCHSyOpFe1wUNeAAAAAElFTkSuQmCC'>
</td>
</tr>
</table>
  */
  void teal();

  /**
   Changes the color to thistle (decimal values: 210/185/211, hex values:
\#D2B9D3)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsPPvCBtgiA8iWNKlGjYjSaH2OgCfMywgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIy3rAkKdEFLp6xEaSinxEAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void thistle();

  /**
   Changes the color to tomato (decimal values: 247/84/49, hex values:
\#F75431)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAw0lEQVR4Xu3UQQ0AIBDAsLOEF/zb4MsHESzpUg2bkaRQZy+AzxkWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGW9YkpToAuGVy/OqUd3BAAAAAElFTkSuQmCC'>
</td>
</tr>
</table>
  */
  void tomato();

  /**
   Changes the color to turquoise (decimal values: 67/198/219, hex values:
\#43C6DB)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsPOGd0xghg8iWNKlGjYjSaHWPgCfMywgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIy3rAkKdEFURAexNfBdMYAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void turquoise();

  /**
   Changes the color to violet (decimal values: 141/56/201, hex values:
\#8D38C9)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsJOFSYThig8iWNKlGjYjSaH2OgCfMywgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIy3rAkKdEFAmQLjTVXZuoAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void violet();

  /**
   Changes the color to wheat (decimal values: 243/218/169, hex values:
\#F3DAA9)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAw0lEQVR4Xu3UQQ0AIBDAsPPvBEvYwAAfRLCkSzVsRpJCnb0APmdYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZBgWkGFYQIZhARmGBWQYFpBhWECGYQEZhgVkGBaQYVhAhmEBGYYFZLxhSVKiC9qfsHqHgnU3AAAAAElFTkSuQmCC'>
</td>
</tr>
</table>
  */
  void wheat();

  /**
   Changes the color to white (decimal values: 255/255/255, hex values:
\#FFFFFF)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAtklEQVR4Xu3UQQ0AMAgAMfybhs9E7JJWRGcAQhbge8ICMoQFZAgLyBAWkCEsIENYQIawgAxhARnCAjKEBWQIC8gQFpAhLCBDWECGsIAMYQEZwgIyhAVkCAvIEBaQISwgQ1hAhrCADGEBGcICMoQFZAgLyBAWkCEsIENYQIawgAxhARnCAjKEBWQIC8gQFpAhLCBDWECGsIAMYQEZwgIyhAVkCAvIEBaQISwgQ1hAhrCAjBcWQMIBbT8NMg7e6dUAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void white();

  /**
   Changes the color to whitesmoke (decimal values: 255/255/255, hex values:
\#FFFFFF)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAtklEQVR4Xu3UQQ0AMAgAMfybhs9E7JJWRGcAQhbge8ICMoQFZAgLyBAWkCEsIENYQIawgAxhARnCAjKEBWQIC8gQFpAhLCBDWECGsIAMYQEZwgIyhAVkCAvIEBaQISwgQ1hAhrCADGEBGcICMoQFZAgLyBAWkCEsIENYQIawgAxhARnCAjKEBWQIC8gQFpAhLCBDWECGsIAMYQEZwgIyhAVkCAvIEBaQISwgQ1hAhrCAjBcWQMIBbT8NMg7e6dUAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void whitesmoke();

  /**
   Changes the color to yellow (decimal values: 255/255/0, hex values:
\#FFFF00)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAuklEQVR4Xu3UAQkAAAjAMPuX1hoedpbhM5IUahfgO8MCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgQ5JKHZtrs23BGOzZAAAAAElFTkSuQmCC'>
</td>
</tr>
</table>
  */
  void yellow();

  /**
   Changes the color to yellowgreen (decimal values: 82/208/23, hex values:
\#52D017)<br> <table border='1'> <tr valign='top'> <td> <img
src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAAyCAIAAAD9fhrKAAAAwklEQVR4Xu3UQQ0AIBDAsHOBL5QjiQ8iWNKlGjYjSaH2WQCfMywgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIy3rAkKdEF+49C9pd8XaAAAAAASUVORK5CYII='>
</td>
</tr>
</table>
  */
  void yellowgreen();
};

#endif
