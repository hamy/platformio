#include "Arduino.h"
#include "LEDUtil.h"

// LED pins
int pwmRed = 9;    // red LED, this pin MUST support PWM
int pwmGreen = 10; // green LED, this pin MUST support PWM
int pwmBlue = 11;  // blue LED, this pin MUST support PWM

// LEDUtil instances (on=HIGH because of the common cathode)
RGBLED rgbLED(pwmRed, pwmGreen, pwmBlue, true, true);

void setup()
{
  Serial.begin(9600);
  Serial.println(F("Starting sketch HSBDemoForCommonCathode..."));
}

float hue = MIN_HSB_HUE_VALUE;
float hueStep = (MAX_HSB_HUE_VALUE - MIN_HSB_HUE_VALUE) / 100;

void loop()
{
  rgbLED.setHSB(hue, MAX_HSB_SATURATION_VALUE, MAX_HSB__BRIGHTNESS_VALUE);
  hue += hueStep;
  if (hue >= MAX_HSB_HUE_VALUE)
  {
    hue = MIN_HSB_HUE_VALUE;
  }
  delay(50);
}
