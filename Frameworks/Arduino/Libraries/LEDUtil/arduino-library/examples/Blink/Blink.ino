#include "Arduino.h"
#include "LEDUtil.h"

SimpleLED simpleLED(LED_BUILTIN);

void setup()
{
  Serial.begin(9600);
  Serial.println(F("Starting sketch Blink..."));
  simpleLED.off();
}

void loop()
{
  Serial.println(F("turning LED on..."));
  simpleLED.on();
  delay(1000);
  Serial.println(F("turning LED off..."));
  simpleLED.off();
  delay(1000);
}