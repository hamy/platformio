#include "Arduino.h"
#include "LEDUtil.h"

// LED pins
int pwmRed = 9;    // red LED, this pin MUST support PWM
int pwmGreen = 10; // green LED, this pin MUST support PWM
int pwmBlue = 11;  // blue LED, this pin MUST support PWM

// LEDUtil instances (on=LOW because of the common anode)
RGBLED rgbLED(pwmRed, pwmGreen, pwmBlue, false, true);

void setup()
{
  Serial.begin(9600);
  Serial.println(F("Starting sketch PWMNamedColorsForCommonAnode..."));
}

#define chooseNamedColor(colorName) \
  Serial.println(F(#colorName));    \
  rgbLED.colorName();               \
  delay(1000);

void loop()
{
#include "chooseNamedColors.h"
}
