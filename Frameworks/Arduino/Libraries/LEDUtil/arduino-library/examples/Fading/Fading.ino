#include "Arduino.h"
#include "LEDUtil.h"

// LED pins
int pwmPin = 11; // this pin MUST support PWM
int waxingIndicatorPin = 12;
int waningIndicatorPin = 13;

// LEDUtil instances (on=HIGH)
SimpleLED pwmLED(pwmPin, true, true);
SimpleLED waxingIndicatorLED(waxingIndicatorPin);
SimpleLED waningIndicatorLED(waningIndicatorPin);

// current PWM brightness value and increment per loop
int currentBrightness = MIN_LED_BRIGHTNESS;
int brightnessStep = 4;

// waxing indicator
boolean waxing = true;

// delay per loop
int stepDuration = 25;

void setup()
{
  Serial.begin(9600);
  Serial.println(F("Starting sketch Fading..."));
}

void loop()
{
  // update LEDs
  pwmLED.setBrightness(currentBrightness);

  if (waxing)
  {
    waxingIndicatorLED.on();
    waningIndicatorLED.off();
  }
  else
  {
    waxingIndicatorLED.off();
    waningIndicatorLED.on();
  }

  // wait
  delay(stepDuration);

  // prepare next loop, toggle waxing flag if necessary
  if (waxing)
  {
    currentBrightness += brightnessStep;
    if (currentBrightness > MAX_LED_BRIGHTNESS)
    {
      currentBrightness = MAX_LED_BRIGHTNESS;
      waxing = false;
      Serial.println(F("waxing: off"));
    }
  }
  else
  {
    currentBrightness -= brightnessStep;
    if (currentBrightness < MIN_LED_BRIGHTNESS)
    {
      currentBrightness = MIN_LED_BRIGHTNESS;
      waxing = true;
      Serial.println(F("waxing: on"));
    }
  }
}