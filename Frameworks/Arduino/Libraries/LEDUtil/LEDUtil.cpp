#include "LEDUtil.h"

SimpleLED::SimpleLED(int pin, boolean onIsHigh, boolean supportsPWM)
{
  _pin = pin;
  _onIsHigh = onIsHigh;
  _supportsPWM = supportsPWM;
  pinMode(_pin, OUTPUT);
  off();
}

SimpleLED::SimpleLED(int pin, boolean onIsHigh)
{
  _pin = pin;
  _onIsHigh = onIsHigh;
  _supportsPWM = false;
  pinMode(_pin, OUTPUT);
  off();
}

SimpleLED::SimpleLED(int pin)
{
  _pin = pin;
  _onIsHigh = true;
  _supportsPWM = false;
  pinMode(_pin, OUTPUT);
  off();
}

int SimpleLED::_limitBrightness(int brightness)
{
  if (brightness < MIN_LED_BRIGHTNESS)
  {
    return MIN_LED_BRIGHTNESS;
  }
  if (brightness > MAX_LED_BRIGHTNESS)
  {
    return MAX_LED_BRIGHTNESS;
  }
  return brightness;
}

int SimpleLED::getPin() { return _pin; }

boolean SimpleLED::onIsHigh() { return _onIsHigh; }

boolean SimpleLED::supportsPWM() { return _supportsPWM; }

boolean SimpleLED::isOn() { return _brightness > MIN_LED_BRIGHTNESS; }

boolean SimpleLED::isOff() { return _brightness == MIN_LED_BRIGHTNESS; }

int SimpleLED::getBrightness() { return _brightness; }

void SimpleLED::on()
{
  _brightness = MAX_LED_BRIGHTNESS;
  digitalWrite(_pin, _onIsHigh ? HIGH : LOW);
}

void SimpleLED::off()
{
  _brightness = MIN_LED_BRIGHTNESS;
  digitalWrite(_pin, _onIsHigh ? LOW : HIGH);
}

void SimpleLED::setBrightness(int brightness)
{
  _brightness = _limitBrightness(brightness);
  switch (_brightness)
  {
  case MIN_LED_BRIGHTNESS:
    off();
    break;
  case MAX_LED_BRIGHTNESS:
    on();
    break;
  default:
    if (_supportsPWM)
    {
      analogWrite(_pin,
                  _onIsHigh ? _brightness : MAX_LED_BRIGHTNESS - _brightness);
    }
    else
    {
      if (_brightness == MIN_LED_BRIGHTNESS)
      {
        digitalWrite(_pin, _onIsHigh ? LOW : HIGH);
      }
      else
      {
        digitalWrite(_pin, _onIsHigh ? HIGH : LOW);
      }
    }
    break;
  }
}

void SimpleLED::toggle() { setBrightness(MAX_LED_BRIGHTNESS - _brightness); }

// RGBLED::RGBLED(SimpleLED redLED, SimpleLED greenLED, SimpleLED blueLED)
//     : _r = redLED,
//       _g = greenLED, _b = blueLED {}
// }

RGBLED::RGBLED(int pinRed, int pinGreen, int pinBlue, boolean onIsHigh,
               boolean supportsPWM)
    : _r(pinRed, onIsHigh, supportsPWM), _g(pinGreen, onIsHigh, supportsPWM),
      _b(pinBlue, onIsHigh, supportsPWM) {}

RGBLED::RGBLED(int pinRed, int pinGreen, int pinBlue, boolean onIsHigh)
    : _r(pinRed, onIsHigh), _g(pinGreen, onIsHigh), _b(pinBlue, onIsHigh) {}

RGBLED::RGBLED(int pinRed, int pinGreen, int pinBlue)
    : _r(pinRed), _g(pinGreen), _b(pinBlue) {}

SimpleLED RGBLED::getRedLED() { return _r; }

SimpleLED RGBLED::getGreenLED() { return _g; }

SimpleLED RGBLED::getBlueLED() { return _b; }

boolean RGBLED::isOn() { return _r.isOn() || _g.isOn() || _b.isOn(); }

boolean RGBLED::isRedOn() { return _r.isOn(); }

boolean RGBLED::isGreenOn() { return _g.isOn(); }

boolean RGBLED::isBlueOn() { return _b.isOn(); }

boolean RGBLED::isOff() { return _r.isOff() && _g.isOff() && _b.isOff(); }

boolean RGBLED::isRedOff() { return _r.isOff(); }

boolean RGBLED::isGreenOff() { return _g.isOff(); }

boolean RGBLED::isBlueOff() { return _b.isOff(); }

int RGBLED::getRed() { return _r.getBrightness(); }

int RGBLED::getGreen() { return _g.getBrightness(); }

int RGBLED::getBlue() { return _b.getBrightness(); }

void RGBLED::on()
{
  _r.on();
  _g.on();
  _b.on();
}

void RGBLED::redOn() { _r.on(); }

void RGBLED::greenOn() { _g.on(); }

void RGBLED::blueOn() { _b.on(); }

void RGBLED::off()
{
  _r.off();
  _g.off();
  _b.off();
}

void RGBLED::redOff() { _r.off(); }

void RGBLED::greenOff() { _g.off(); }

void RGBLED::blueOff() { _b.off(); }

void RGBLED::setRed(int r) { _r.setBrightness(r); }

void RGBLED::setGreen(int g) { _g.setBrightness(g); }

void RGBLED::setBlue(int b) { _b.setBrightness(b); }

void RGBLED::setRGB(int r, int g, int b)
{
  _r.setBrightness(r);
  _g.setBrightness(g);
  _b.setBrightness(b);
}

void RGBLED::setBrightness(int brightness)
{
  _r.setBrightness(brightness);
  _g.setBrightness(brightness);
  _b.setBrightness(brightness);
}

void RGBLED::setHSB(float hue, float saturation, float brightness)
{
  if (hue < MIN_HSB_HUE_VALUE)
  {
    hue = MIN_HSB_HUE_VALUE;
  }
  if (hue > MAX_HSB_HUE_VALUE)
  {
    hue = MAX_HSB_HUE_VALUE;
  }
  if (saturation < MIN_HSB_SATURATION_VALUE)
  {
    saturation = MIN_HSB_SATURATION_VALUE;
  }
  if (saturation > MAX_HSB_SATURATION_VALUE)
  {
    saturation = MAX_HSB_SATURATION_VALUE;
  }
  if (brightness < MIN_HSB__BRIGHTNESS_VALUE)
  {
    brightness = MIN_HSB__BRIGHTNESS_VALUE;
  }
  if (brightness > MAX_HSB__BRIGHTNESS_VALUE)
  {
    brightness = MAX_HSB__BRIGHTNESS_VALUE;
  }
  float hue60 = hue / 60.0;
  int sextant = (int)floor(hue60);
  float huefract = hue60 - sextant;
  float r = 0;
  float g = 0;
  float b = 0;
  if (saturation == MIN_HSB_SATURATION_VALUE)
  {
    r = g = b = brightness;
  }
  else
  {
    float p = brightness * (1 - saturation);
    float q = brightness * (1 - saturation * huefract);
    float t = brightness * (1 - saturation * (1 - huefract));
    switch (sextant)
    {
    case 0:
      r = brightness;
      g = t;
      b = p;
      break;
    case 1:
      r = q;
      g = brightness;
      b = p;
      break;
    case 2:
      r = p;
      g = brightness;
      b = t;
      break;
    case 3:
      r = p;
      g = q;
      b = brightness;
      break;
    case 4:
      r = t;
      g = p;
      b = brightness;
      break;
    default: // case 5:
      r = brightness;
      g = p;
      b = q;
      break;
    }
  }
  setRed((int)floor(256 * r));
  setGreen((int)floor(256 * g));
  setBlue((int)floor(256 * b));
}

void RGBLED::toggle()
{
  _r.toggle();
  _g.toggle();
  _b.toggle();
}

void RGBLED::workerPaused()
{
  off();
}

void RGBLED::workerRunning()
{
  off();
  blueOn();
}

void RGBLED::workerFailure()
{
  off();
  redOn();
}

void RGBLED::workerSuccess()
{
  off();
  greenOn();
}

void RGBLED::aliceblue() { setRGB(239, 247, 255); }
void RGBLED::antiquewhite() { setRGB(249, 232, 210); }
void RGBLED::aqua() { setRGB(0, 255, 255); }
void RGBLED::aquamarine() { setRGB(67, 183, 186); }
void RGBLED::azure() { setRGB(239, 255, 255); }
void RGBLED::beige() { setRGB(245, 243, 215); }
void RGBLED::bisque() { setRGB(253, 224, 188); }
void RGBLED::black() { setRGB(0, 0, 0); }
void RGBLED::blanchedalmond() { setRGB(254, 232, 198); }
void RGBLED::blue() { setRGB(0, 0, 255); }
void RGBLED::blueviolet() { setRGB(121, 49, 223); }
void RGBLED::brown() { setRGB(152, 5, 22); }
void RGBLED::burlywood() { setRGB(234, 190, 131); }
void RGBLED::cadetblue() { setRGB(87, 134, 147); }
void RGBLED::chartreuse() { setRGB(138, 251, 23); }
void RGBLED::chocolate() { setRGB(200, 90, 23); }
void RGBLED::coral() { setRGB(247, 101, 65); }
void RGBLED::cornflowerblue() { setRGB(21, 27, 141); }
void RGBLED::cornsilk() { setRGB(255, 247, 215); }
void RGBLED::crimson() { setRGB(228, 27, 23); }
void RGBLED::cyan() { setRGB(0, 255, 255); }
void RGBLED::darkblue() { setRGB(47, 47, 79); }
void RGBLED::darkcyan() { setRGB(87, 254, 255); }
void RGBLED::darkgoldenrod() { setRGB(175, 120, 23); }
void RGBLED::darkgray() { setRGB(122, 119, 119); }
void RGBLED::darkgreen() { setRGB(37, 65, 23); }
void RGBLED::darkkhaki() { setRGB(183, 173, 89); }
void RGBLED::darkmagenta() { setRGB(244, 62, 255); }
void RGBLED::darkolivegreen() { setRGB(204, 251, 93); }
void RGBLED::darkorange() { setRGB(248, 128, 23); }
void RGBLED::darkorchid() { setRGB(125, 27, 126); }
void RGBLED::darkred() { setRGB(228, 27, 23); }
void RGBLED::darksalmon() { setRGB(225, 139, 107); }
void RGBLED::darkseagreen() { setRGB(139, 179, 129); }
void RGBLED::darkslateblue() { setRGB(43, 56, 86); }
void RGBLED::darkslategray() { setRGB(37, 56, 86); }
void RGBLED::darkturquoise() { setRGB(59, 156, 156); }
void RGBLED::darkviolet() { setRGB(132, 45, 206); }
void RGBLED::deeppink() { setRGB(245, 40, 135); }
void RGBLED::deepskyblue() { setRGB(59, 185, 255); }
void RGBLED::dimgray() { setRGB(70, 62, 65); }
void RGBLED::dodgerblue() { setRGB(21, 137, 255); }
void RGBLED::firebrick() { setRGB(128, 5, 23); }
void RGBLED::floralwhite() { setRGB(255, 249, 238); }
void RGBLED::forestgreen() { setRGB(78, 146, 88); }
void RGBLED::fuchsia() { setRGB(255, 0, 255); }
void RGBLED::gainsboro() { setRGB(216, 217, 215); }
void RGBLED::ghostwhite() { setRGB(247, 247, 255); }
void RGBLED::gold() { setRGB(212, 160, 23); }
void RGBLED::goldenrod() { setRGB(237, 218, 116); }
void RGBLED::gray() { setRGB(128, 128, 128); }
void RGBLED::green() { setRGB(0, 255, 0); }
void RGBLED::greenyellow() { setRGB(177, 251, 23); }
void RGBLED::honeydew() { setRGB(240, 254, 238); }
void RGBLED::indianred() { setRGB(94, 34, 23); }
void RGBLED::indigo() { setRGB(48, 125, 126); }
void RGBLED::ivory() { setRGB(255, 255, 238); }
void RGBLED::khaki() { setRGB(173, 169, 110); }
void RGBLED::lavenderblush() { setRGB(253, 238, 244); }
void RGBLED::lavender() { setRGB(227, 228, 250); }
void RGBLED::lawngreen() { setRGB(135, 247, 23); }
void RGBLED::lemonchiffon() { setRGB(255, 248, 198); }
void RGBLED::lightblue() { setRGB(173, 223, 255); }
void RGBLED::lightcoral() { setRGB(231, 116, 113); }
void RGBLED::lightcyan() { setRGB(224, 255, 255); }
void RGBLED::lightgoldenrodyellow() { setRGB(250, 248, 204); }
void RGBLED::lightgreen() { setRGB(204, 255, 204); }
void RGBLED::lightpink() { setRGB(250, 175, 186); }
void RGBLED::lightsalmon() { setRGB(249, 150, 107); }
void RGBLED::lightseagreen() { setRGB(62, 169, 159); }
void RGBLED::lightskyblue() { setRGB(130, 202, 250); }
void RGBLED::lightslategray() { setRGB(109, 123, 141); }
void RGBLED::lightsteelblue() { setRGB(114, 143, 206); }
void RGBLED::lightyellow() { setRGB(255, 254, 220); }
void RGBLED::lime() { setRGB(0, 255, 0); }
void RGBLED::limegreen() { setRGB(65, 163, 23); }
void RGBLED::linen() { setRGB(249, 238, 226); }
void RGBLED::magenta() { setRGB(255, 0, 255); }
void RGBLED::maroon() { setRGB(128, 0, 0); }
void RGBLED::mediumaquamarine() { setRGB(52, 135, 129); }
void RGBLED::mediumblue() { setRGB(21, 45, 198); }
void RGBLED::mediumorchid() { setRGB(176, 72, 181); }
void RGBLED::mediumpurple() { setRGB(132, 103, 215); }
void RGBLED::mediumseagreen() { setRGB(48, 103, 84); }
void RGBLED::mediumslateblue() { setRGB(94, 90, 128); }
void RGBLED::mediumspringgreen() { setRGB(52, 128, 23); }
void RGBLED::mediumturquoise() { setRGB(72, 204, 205); }
void RGBLED::mediumvioletred() { setRGB(202, 34, 107); }
void RGBLED::midnightblue() { setRGB(21, 27, 84); }
void RGBLED::mintcream() { setRGB(245, 255, 249); }
void RGBLED::mistyrose() { setRGB(253, 225, 221); }
void RGBLED::moccasin() { setRGB(253, 224, 172); }
void RGBLED::navajowhite() { setRGB(253, 218, 163); }
void RGBLED::navy() { setRGB(0, 0, 128); }
void RGBLED::oldlace() { setRGB(252, 243, 226); }
void RGBLED::olive() { setRGB(128, 128, 0); }
void RGBLED::olivedrab() { setRGB(101, 128, 23); }
void RGBLED::orange() { setRGB(248, 122, 23); }
void RGBLED::orangered() { setRGB(246, 56, 23); }
void RGBLED::orchid() { setRGB(229, 125, 237); }
void RGBLED::palegoldenrod() { setRGB(237, 228, 158); }
void RGBLED::paleturquoise() { setRGB(174, 235, 236); }
void RGBLED::palevioletred() { setRGB(209, 101, 135); }
void RGBLED::papayawhip() { setRGB(254, 236, 207); }
void RGBLED::peachpuff() { setRGB(252, 213, 176); }
void RGBLED::peru() { setRGB(197, 119, 38); }
void RGBLED::pink() { setRGB(250, 175, 190); }
void RGBLED::plum() { setRGB(185, 59, 143); }
void RGBLED::powderblue() { setRGB(173, 220, 227); }
void RGBLED::purple() { setRGB(128, 0, 128); }
void RGBLED::red() { setRGB(255, 0, 0); }
void RGBLED::rosybrown() { setRGB(179, 132, 129); }
void RGBLED::royalblue() { setRGB(43, 96, 222); }
void RGBLED::saddlebrown() { setRGB(246, 53, 38); }
void RGBLED::salmon() { setRGB(248, 129, 88); }
void RGBLED::sandybrown() { setRGB(238, 154, 77); }
void RGBLED::seagreen() { setRGB(78, 137, 117); }
void RGBLED::seashell() { setRGB(254, 243, 235); }
void RGBLED::sienna() { setRGB(138, 65, 23); }
void RGBLED::silver() { setRGB(192, 192, 192); }
void RGBLED::skyblue() { setRGB(102, 152, 255); }
void RGBLED::slateblue() { setRGB(115, 124, 161); }
void RGBLED::slategray() { setRGB(101, 115, 131); }
void RGBLED::snow() { setRGB(255, 249, 250); }
void RGBLED::springgreen() { setRGB(74, 160, 44); }
void RGBLED::steelblue() { setRGB(72, 99, 160); }
void RGBLED::tan() { setRGB(216, 175, 121); }
void RGBLED::teal() { setRGB(0, 128, 128); }
void RGBLED::thistle() { setRGB(210, 185, 211); }
void RGBLED::tomato() { setRGB(247, 84, 49); }
void RGBLED::turquoise() { setRGB(67, 198, 219); }
void RGBLED::violet() { setRGB(141, 56, 201); }
void RGBLED::wheat() { setRGB(243, 218, 169); }
void RGBLED::white() { setRGB(255, 255, 255); }
void RGBLED::whitesmoke() { setRGB(255, 255, 255); }
void RGBLED::yellow() { setRGB(255, 255, 0); }
void RGBLED::yellowgreen() { setRGB(82, 208, 23); }
