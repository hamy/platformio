#ifndef _LOOP_UTIL_H
#define _LOOP_UTIL_H

/**
 \mainpage LoopUtil - Helpers for Begin/End of the Arduino loop() Method.

\section lu-purpose Purpose of the LoopUtil Library

 In most applications, Arduino's loop() function is periodically called to
execute domain-specific code. Notwithstanding this domain-specific nature of the
<em>guts</em> within loop(), the very start and end of it  can be considered as
some sort of  <em>generic</em>  milestones where standard operations are
executed:

 - At the start of the loop(), we frequently find standard operations like:

    - creating a trigger pulse on a given pin for analyzing the behaviour of a
sketch by an oscilloscope or a logic analyzer.
    - indicating the loop start by printing some debug messages via
Serial.print(...),
    - incrementing a loop counter that is used for statistical purposes in
domain-specific ways,

 - At the end of the loop(), we frequently find other standard operations like:

    - adding delays so that the duration between loop start and end remains
approximately constant even when the time for the domain-specific operations in
loop() may vary,
    - doing some statistics
    - indicating the loop end by printing some debug messages via
Serial.print(...).

The LoopUtil library supports these standard operation. It provides a
class LoopUtil declared in the header file LoopUtil.h and implemented in the
file LoopUtil.cpp.

\section lu-usage Using the LoopUtil Library

Client programs that want to use this library should include the header file
as shown in the following snippet:

\code
 #include "LoopUtil.h"
\endcode

During the creation of a LoopUtil instance (i.e., within the constructor
LoopUtil::LoopUtil(boolean,int,uint32_t)), you select which of the features
mentioned above are actually used. As an example, let's create a LoopUtil
instance via the following snippet:

\code
boolean debugFlag = true;
int triggerPin = 2;
uint32_t loopDuration = 100;
LoopUtil loopUtil(debugFlag,triggerPin,loopDuration);
\endcode

As you might guess, the LoopUtil instance will make its milestone methods act
in the following manner:
- At the loop start, pin 2 (the trigger pin) will be raised for a short time
(usually a few microseconds), this transition may be used as trigger signal for
oscilloscopes, logic analyzers, etc.
- At the loop start and the loop end, debug messages will be created that
display which milestone (loop start vs. loop end) is passed.
- Before the end of the loop, a delay is inserted so that the actual time
between loop start and the loop end will approximately match the given
loopDuration value. Of course, this will only work as expected if the
domain-specific operations within loop() do not consume too much time.

BTW: you may omit some or all of the constructor arguments shown above since the
constructor is overloaded. LoopUtil will provide an appropriate behaviour in
case of   missing arguments. For example, if you invoke another version of the
constructor LoopUtil::LoopUtil(boolean) that omits the trigger pin and the
loopDuration arguments, LoopUtil will not generate a trigger signal and use a
default loop duration. Sample construction snippet for this case:

\code
boolean debugFlag = true;
LoopUtil loopUtil(debugFlag);
\endcode

The constructor shown above will lead to the following reduced behaviour:
- At the loop start and the loop end, debug messages will be created.
- Before the end of the loop, a delay is inserted as in the previous snippet.
However, the default loop duration value of  ::DEFAULT_LOOP_DURATION is used.


LoopUtil   provides  methods  LoopUtil::loopStart() and LoopUtil::loopEnd()
that should be called immediately at the begin or the end of Arduino's loop()
method as shown in the following example:

 \code
 void loop() {
     loopUtil.loopStart();
        :        :
      // domain specific operations
         :        :
     loopUtil.loopEnd();
}
 \endcode

\section lu-loopStart Operations at the Loop Start

Clients should invoke the method LoopUtil::loopStart()  immediately at the start
of the loop() function as shown in the following snippet:

 \code
 void loop() {
     loopUtil.loopStart();

      // domain specific operations
         :        :
         :        :
}
 \endcode

loopStart() supports the following features:

- Incrementing an internal loop counter.
- Firing a short trigger pulse at a given trigger pin.
- Printing debug messages to Serial.

\subsection lu-loopStart-counter Accessing the Internal Loop Counter

This counter can be accessed by the method LoopUtil::getLoopCount(). The counter
is incremented by 1 during each invocation of loopStart().


Example for accessing the loop counter:

\code
Serial.print(F("foo bar change in loop no. "));
Serial.println(loopUtil.getLoopCount());
\endcode


\subsection lu-loopStart-trigger Trigger Puls Generation

If activated, a short trigger pulse (normally with a duration of a few
microseconds) is raised at the trigger pin. To use this feature, specify a
non-negative pin number in the appropriate argument of the LoopUtil constructor.
Specifying a negative pin number deactivates the creation of a trigger pulse.
The width of the trigger pulse may be varied by the method
LoopUtil::setTriggerPulseWidth(uint32_t). If no trigger pulse width is specified,
the default value ::DEFAULT_TRIGGER_PULSE_WIDTH is used.

Example for setting the pulse width to a value of 100 microseconds:

\code
loopUtil.setTriggerPulseWidth(100);
\endcode

\subsection lu-loopStart-print Printing Debug Messages at the Loop Start

The internal debug flag controls the generation of such messages. The library
provides methods:

- LoopUtil::getDebugFlag() returns the current value of the debug flag.
- LoopUtil::setDebugFlag(boolean) modifies the current value of the debug flag.

Example for switching off debug messages:

\code
loopUtil.setDebugFlag(false);
\endcode


\section lu-loopEnd Operations at the Loop End

Clients should invoke the method LoopUtil::loopEnd()  immediately before the end
of the loop() function as shown in the following snippet:

 \code
 void loop() {

      // domain specific operations
         :        :
         :        :
      loopUtil.loopEnd();
}
 \endcode

loopEnd() comes in four overloaded versions. They support  the following
features:

- Burning CPU cycles so that the loop duration will approximately match the
value that was given in the constructor.
- Printing debug messages to Serial.
- Evaluating the result of the current loop by a boolean value that is counted
for statistical purposes.
- Weighing the current loop by a floating point amount that is aggregated for
statistical purposes.

The following snippet shows a LoopUtil::loopEnd(boolean,float) invocation with a
flag value of false and a weight of 0.5:

 \code
 loopUtil.loopEnd(false,0.5);
 \endcode

 */

#include <Arduino.h>

/**
 * The version of this helpers.
 */
#define LOOP_UTIL_VERSION "1.0.5"

/**
 * The default value for the loop duration in milliseconds.
 */
#define DEFAULT_LOOP_DURATION (100)

/**
 * The default value for the trigger pulse width in microseconds.
 */
#define DEFAULT_TRIGGER_PULSE_WIDTH (10)

/**
 * Denotes an invalid trigger pin value.
 */
#define INVALID_TRIGGER_PIN (-1)

extern "C" {
/**
 The callback function for intermittent functions that are infrequently called
 from the loopStart() or loopEnd() method.
 */
typedef void (*LoopUtilIntermittentCallback)();
}

/**
 * A set of simple helper functions for the loop method.
 */
class LoopUtil {
private:
  boolean _debugFlag;
  boolean _debugFlag2;
  int _triggerPin;
  uint32_t _triggerPulseWidth;
  boolean _usesTriggerPin;
  void _initializeTriggering(int triggerPin);
  uint32_t _loopDuration;
  uint32_t _loopCount;
  uint32_t _loopCountFalse;
  uint32_t _loopCountTrue;
  float _loopSumFalse;
  float _loopSumTrue;
  uint32_t _loopStartTime;
  uint32_t _loopEndTime;
  boolean _timeRolloverInLoop;
  void _computeLoopEndTime();
  void _waitForLoopEnd();
  boolean _skipWait;
  boolean _newLineBeforeLoopStart;
  boolean _newLineAfterLoopEnd;
  LoopUtilIntermittentCallback _loopStartIntermittentCallback;
  uint32_t _loopStartIntermittentInterval;
  uint32_t _loopStartIntermittentCounter;
  LoopUtilIntermittentCallback _loopEndIntermittentCallback;
  uint32_t _loopEndIntermittentInterval;
  uint32_t _loopEndIntermittentCounter;

public:
  /**
Constructor for the helper instance.

\param debugFlag A flag that controls the generation of debug message at the
start and the end of the loop.

\param triggerPin The pin number of the trigger pin. A negative value means that
no trigger pin is used.

 \param loopDuration The duration of one loop in milliseconds.
*/
  LoopUtil(boolean debugFlag, int triggerPin, uint32_t loopDuration);

  /**
Constructor for the helper instance. It uses a default value for the loop
duration. You may change the duration by the method setLoopDuration(uint32_t).

\param debugFlag A flag that controls the generation of debug message at the
start and the end of the loop.

\param triggerPin The pin number of the trigger
pin. A negative value means that no trigger pin is used.
*/
  LoopUtil(boolean debugFlag, int triggerPin);

  /**
Constructor for the helper instance. No trigger pin will be used and the loop
duration will be set to a default value.

\param debugFlag A flag that controls the generation of debug message at the
start and the end of the loop.
*/
  LoopUtil(boolean debugFlag);

  /**
Constructor for the helper instance. Debug messages are switched off, no trigger
pin is used.
*/
  LoopUtil();

  /**
Print a self-description to the Serial output.
*/
  void printInfo();

  /**
Print statistics to the Serial output.
*/
  void printStatistics();

  /**
A flag that indicates whether debug output is created or not.
\return The flag value.
     - true: At each loop start and loop end, debug messages are created.
     - false: No debug output is created.
*/
  boolean getDebugFlag();

  /**
Changes the  debug output  creation flag.
\param debugFlag The new flag value.
     - true: At each loop start and loop end, debug messages are created.
     - false: No debug output is created.
*/
  void setDebugFlag(boolean debugFlag);

  /**
Returns the pin number of the trigger pin.
\return The pin number.
*/
  int getTriggerPin();

  /**
Indicates whether a trigger pin is used or not.
\return An indicator flag:
       - true: A trigger pin is used.
       - false: No trigger pin is used.
*/
  boolean usesTriggerPin();

  /**
Returns the trigger pulse width.
\return The pulse width in microseconds.
*/
  uint32_t getTriggerPulseWidth();

  /**
Changes the trigger pulse width.
\param triggerPulseWidth The new pulse width in microseconds.
*/
  void setTriggerPulseWidth(uint32_t triggerPulseWidth);

  /**
Returns the loop duration.
\return The duration in milliseconds.
*/
  uint32_t getLoopDuration();

  /**
Changes the loop duration.
\param loopDuration The new duration in milliseconds.
*/
  void setLoopDuration(uint32_t loopDuration);

  /**
   * Resets the loop statistics.
   */
  void resetLoopStatistics();

  /**
* Returns the loop count of the current loop.
\return The count.
*/
  uint32_t getLoopCount();

  /**
* Returns the loop count of all loops with result false.
\return The count.
*/
  uint32_t getLoopCountFalse();

  /**
* Returns the loop count of all loops with  result true.
\return The count.
*/
  uint32_t getLoopCountTrue();

  /**
* Returns the loop sum of the current loop.
\return The sum.
*/
  float getLoopSum();

  /**
* Returns the loop sum of all loops with result false.
\return The sum.
*/
  float getLoopSumFalse();

  /**
* Returns the loop sum of all loops with result true.
\return The sum.
*/
  float getLoopSumTrue();

  /**
* Returns the start time of the current loop.
\return The start time in milliseconds measured by the internal clock.
*/
  uint32_t getLoopStartTime();

  /**
* Returns the estimated end time of the current loop.
\return The end time in milliseconds with respect to the the internal clock.
*/
  uint32_t getLoopEndTime();

  /**
   * This method should be called immediately at the beginning of the method
   * loop().
   */
  void loopStart();

  /**
   * This method aborts the final delay in loopEnd() for the current loop
   * instance. It must be called before loopEnd() is entered.
   */
  void skipWait();

  /**
   * This method controls the insertion of a empty line before the loop start
   * debug message block in debug mode.
   *
   * \param flag The flag that controls the insertion.
   */
  void setNewLineBeforeLoopStart(boolean flag);

  /**
   * This method controls the insertion of a empty line after the loop end debug
   * message block in debug mode.
   *
   * \param flag The flag that controls the insertion.
   */
  void setNewLineAfterLoopEnd(boolean flag);

  /**
   * This method should be called immediately before the end of the method
loop().

  \param flag A flag that is reflected in the loop statistics.
  \param amount An amount that is reflected in the loop statistics.
  */
  void loopEnd(boolean flag, float amount);

  /**
* This method should be called immediately before the end of the method loop().
\param flag A flag that is reflected in the loop statistics.
*/
  void loopEnd(boolean flag);

  /**
* This method should be called immediately before the end of the method loop().
\param amount An amount that is reflected in the loop statistics.
*/
  void loopEnd(float amount);

  /**
   * This method should be called immediately before the end of the method
   * loop().
   */
  void loopEnd();

  /**
   * This method registers a loop start intermittent callback.
   *
   * \param callback The intermittent callback or NULL.
   * \param interval The interval between two intermittent callback invocations.
   */
  void
  registerLoopStartIntermittentCallback(LoopUtilIntermittentCallback callback,
                                        uint32_t interval);

  /**
   * This method registers a loop end intermittent callback.
   *
   * \param callback The intermittent callback or NULL.
   * \param interval The interval between two intermittent callback invocations.
   */
  void
  registerLoopEndIntermittentCallback(LoopUtilIntermittentCallback callback,
                                      uint32_t interval);
};

#endif
