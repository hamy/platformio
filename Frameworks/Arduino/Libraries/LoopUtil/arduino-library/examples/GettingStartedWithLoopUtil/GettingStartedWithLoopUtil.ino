/**
 * GettingStartedWithLoopUtil.ino
 *
 * Example for the LoopUtil library
 *
 * Source: https://gitlab.com/hamy/platformio
 *
 * This very simple sketch demonstrates the basic usage of the LoopUtil helper
 * library.
 */

#include "Arduino.h"
#include "LoopUtil.h"

/**
 * Create a LoopUtil instance.
 */
boolean debugFlag = true; // make LoopUtil print debug messages from the methods
                          // loopStart() and loopEnd()
int triggerPin = 2; // create a short trigger pulse on pin 2 for logic analyzers
uint32_t loopDuration = 1000; // each loop should last for 1000 milliseconds
LoopUtil loopUtil(debugFlag, triggerPin,
                  loopDuration); // create the instance via the constructor

/**
 * Initializes the sketch.
 * - Open a serial connection to the desktop with a baud rate of 9600.
 * - Print a start message.
 */
void setup() {
  Serial.begin(9600);
  Serial.println(F("Starting sketch GettingStartedWithLoopUtil..."));
}

/**
 * Simulate work by wasting CPU cycles by doing nothing.
 * - The burning of CPU cycles will take a random duration between one quarter and
 * three quarters of the expected loop duration.
 * - Print trace messages.
 */
void burnCpuCycles() {
  Serial.println();
  uint32_t minimumAmountOfMillisecondsToBeBurnt = loopDuration / 4;
  uint32_t maximumAmountOfMillisecondsToBeBurnt = (3 * loopDuration) / 4;
  uint32_t currentAmountOfMillisecondsToBeBurnt =
      random(minimumAmountOfMillisecondsToBeBurnt,
             maximumAmountOfMillisecondsToBeBurnt);
  Serial.print(F("In this loop, "));
  Serial.print(currentAmountOfMillisecondsToBeBurnt);
  Serial.println(
      F(" milliseconds of CPU cycles will be burnt by fictitious operations."));
  uint32_t burnStartTime = millis();
  Serial.print(F("Burn start time: "));
  Serial.print(burnStartTime);
  Serial.println(F(" milliseconds"));
  uint32_t burnEndTime = burnStartTime + currentAmountOfMillisecondsToBeBurnt;
  Serial.print(F("Burn end time: "));
  Serial.print(burnEndTime);
  Serial.println(F(" milliseconds."));
  while (millis() < burnEndTime) {
    // do nothing but burning cycles...
  }
  uint32_t actualEndTime = millis();
  Serial.print(F("CPU cycle burning actually ended at "));
  Serial.print(actualEndTime);
  Serial.println(F(" milliseconds."));
  Serial.println();
}

/**
 * The Arduino main loop.
 * - Perform loopStart() operations:
 *     - Firing a trigger pulse on pin 2.
 *     - Print debug messages.
 * - Simulate work by burning CPU cycles.
 * - Perform loopEnd() operations:
 *     - Sleep for a certain amount of time so that the complete loop duration
 * approximately equals the expected value.
 *     - Print debug messages.
 */
void loop() {
  loopUtil.loopStart();

  burnCpuCycles();

  loopUtil.loopEnd();
}