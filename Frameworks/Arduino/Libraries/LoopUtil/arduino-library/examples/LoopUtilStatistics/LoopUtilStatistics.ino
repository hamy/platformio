/**
 * LoopUtilStatistics.ino
 *
 * Example for the LoopUtil library
 *
 * Source: https://gitlab.com/hamy/platformio
 *
 * This sketch demonstrates the creation of simple loop statistics using the
 * LoopUtil helper library. The sketch may be run in two modes:
 * - Text output mode: The sketch produces human-readable debug messages that
 * describe the statistics produced so far.
 * - Graphical output mode: The sketch produces machine-readable statistics
 * records (whitespace separated values. Use the Arduino serial plotter to
 * visualize the generated time-series.
 */

#include "Arduino.h"
#include "LoopUtil.h"

// uncomment one of the following two lines to select the output mode:
boolean debugFlag = true; // selects text output mode
// boolean debugFlag = false; // selects graphical output mode

int triggerPin = -1; // we don't use any trigger pin
uint32_t loopDuration =
    debugFlag ? 1000 : 100; // the loop duration depends on the output mode

LoopUtil loopUtil(debugFlag, triggerPin,
                  loopDuration); // LoopUtil instance creation

/**
 * Sketch initialization:
 * - Open a serial connection to the desktop host at 9600 baud.
 * - In text output mode, print an initial debug method.
 */
void setup() {
  Serial.begin(9600);
  if (debugFlag) {
    Serial.println(F("Starting sketch LoopUtilStatistics..."));
  }
}

/**
 * Show the aggregation statistics of the most recently completed loop.
 * - In text output mode, print each statistics item on a line of its own
 * including a short description.
 * - In graphical output mode, create the first part of the whitespace separated
 * values.
 */
void showAggregationResults() {
  if (debugFlag) {
    Serial.print(F("loopCount@False: "));
    Serial.println(loopUtil.getLoopCountFalse());
    Serial.print(F("loopSum@False:   "));
    Serial.println(loopUtil.getLoopSumFalse());
    Serial.print(F("loopCount@True:  "));
    Serial.println(loopUtil.getLoopCountTrue());
    Serial.print(F("loopSum@True:    "));
    Serial.println(loopUtil.getLoopSumTrue());
    Serial.println();
  } else {
    Serial.print(loopUtil.getLoopCountFalse());
    Serial.print(F(" "));
    Serial.print(loopUtil.getLoopSumFalse());
    Serial.print(F(" "));
    Serial.print(loopUtil.getLoopCountTrue());
    Serial.print(F(" "));
    Serial.print(loopUtil.getLoopSumTrue());
  }
}

/**
 * Show the new statistics values for the current loop:
 * - In text output mode, print each statistics item on a line of its own
 * including a short description.
 * - In graphical output mode, create the last part of the whitespace separated
 * values and add an EOL.
 */
void showNewValues(int value, boolean flag, float amount) {
  if (debugFlag) {
    Serial.print(F("new analog input value: "));
    Serial.print(value);
    Serial.print(F(", flag="));
    Serial.print(flag);
    Serial.print(F(", amount="));
    Serial.println(amount);
  } else {
    Serial.print(F(" "));
    Serial.print(flag);
    Serial.print(F(" "));
    Serial.println(amount);
  }
}

/**
 * The main Arduino loop:
 * - In text output mode, print loopStart() statistics
 * - Show the aggregation statistics of the most recently completed loop.
 * - Create the new statistics values from reading noisy signals of analog input
 * port #0.
 * - Show these new statistics values.
 * - Via loopEnd(), wait until the expected loop duration has elapsed and print
 * statistics (in text output mode).
 */
void loop() {
  loopUtil.loopStart();
  showAggregationResults();

  int value = analogRead(0); // noisy value from open analog input pin
  boolean flag = value & 1;  // flag is true for odd values, otherwise false
  float amount =
      value / 1023.0 - 0.5; // normalize amount to interval -0.5 ... + 0.5

  showNewValues(value, flag, amount);
  loopUtil.loopEnd(flag, amount);
}