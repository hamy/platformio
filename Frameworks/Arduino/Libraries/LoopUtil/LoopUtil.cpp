#include "LoopUtil.h"

/**
 * The implementation.
 * \sa LoopUtil.h
 */
LoopUtil::LoopUtil(boolean debugFlag, int triggerPin, uint32_t loopDuration) {
  setDebugFlag(debugFlag);
  _initializeTriggering(triggerPin);
  setLoopDuration(loopDuration);
  resetLoopStatistics();
}

LoopUtil::LoopUtil(boolean debugFlag, int triggerPin) {
  setDebugFlag(debugFlag);
  _initializeTriggering(triggerPin);
  setLoopDuration(DEFAULT_LOOP_DURATION);
  resetLoopStatistics();
}

LoopUtil::LoopUtil(boolean debugFlag) {
  setDebugFlag(debugFlag);
  _initializeTriggering(INVALID_TRIGGER_PIN);
  setLoopDuration(DEFAULT_LOOP_DURATION);
  resetLoopStatistics();
}

LoopUtil::LoopUtil() {
  setDebugFlag(false);
  _initializeTriggering(INVALID_TRIGGER_PIN);
  setLoopDuration(DEFAULT_LOOP_DURATION);
  resetLoopStatistics();
}

void LoopUtil::printInfo() {
  Serial.println(F("LoopUtil Info:"));
  Serial.print(F("- Version: "));
  Serial.println(F(LOOP_UTIL_VERSION));
  Serial.print(F("- Source file: "));
  Serial.println(F(__FILE__));
  Serial.print(F("- Last modification time: "));
  Serial.println(F(__TIMESTAMP__));
  Serial.print(F("- Compilation time: "));
  Serial.print(F(__DATE__));
  Serial.print(F(" "));
  Serial.println(F(__TIME__));
  Serial.print(F("- Debug flag: "));
  Serial.println(_debugFlag);
  Serial.print(F("- Trigger pin: "));
  Serial.print(_triggerPin);
  Serial.print(F(", pulse width: "));
  Serial.print(_triggerPulseWidth);
  Serial.println(F(" usec"));
  Serial.print(F("- Loop duration: "));
  Serial.print(_loopDuration);
  Serial.println(F(" msec"));
}

void LoopUtil::printStatistics() {
  Serial.println(F("LoopUtil Statistics:"));
  Serial.print(F("- Current loop count: "));
  Serial.println(_loopCount);
  Serial.print(F("- Number of loops ending with flag=false: "));
  Serial.println(_loopCountFalse);
  Serial.print(F("- Number of loops ending with flag=true:  "));
  Serial.println(_loopCountTrue);
  Serial.print(F("- Aggregated amount for all loops: "));
  Serial.println(_loopSumFalse + _loopSumTrue);
  Serial.print(F("- Aggregated amount for loops ending with flag=false: "));
  Serial.println(_loopSumFalse);
  Serial.print(F("- Aggregated amount for loops ending with flag=true:  "));
  Serial.println(_loopSumTrue);
}

boolean LoopUtil::getDebugFlag() { return _debugFlag; }

void LoopUtil::setDebugFlag(boolean debugFlag) {
  _debugFlag = debugFlag;
  _debugFlag2 = _debugFlag && _loopCount > 0;
  if (_debugFlag2) {
    Serial.println(F("debug flag is raised."));
  }
}

int LoopUtil::getTriggerPin() { return _triggerPin; }

boolean LoopUtil::usesTriggerPin() { return _triggerPin >= 0; }

uint32_t LoopUtil::getTriggerPulseWidth() { return _triggerPulseWidth; }

void LoopUtil::setTriggerPulseWidth(uint32_t triggerPulseWidth) {
  _triggerPulseWidth = triggerPulseWidth;
  if (_debugFlag2) {
    Serial.print(F("trigger pin: "));
    Serial.print(_triggerPin);
    Serial.print(F(", pulse width: "));
    Serial.print(_triggerPulseWidth);
    Serial.println(F(" usec"));
  }
}

void LoopUtil::_initializeTriggering(int triggerPin) {
  _usesTriggerPin = triggerPin >= 0;
  _triggerPin = _usesTriggerPin ? triggerPin : INVALID_TRIGGER_PIN;
  if (_usesTriggerPin) {
    pinMode(_triggerPin, OUTPUT);
    digitalWrite(_triggerPin, LOW);
  }
  setTriggerPulseWidth(DEFAULT_TRIGGER_PULSE_WIDTH);
}

uint32_t LoopUtil::getLoopDuration() { return _loopDuration; }

void LoopUtil::setLoopDuration(uint32_t loopDuration) {
  _loopDuration = loopDuration;
  if (_debugFlag2) {
    Serial.print(F("loop duration: "));
    Serial.print(_loopDuration);
    Serial.println(F(" msec"));
  }
}

uint32_t LoopUtil::getLoopCount() { return _loopCount; }

uint32_t LoopUtil::getLoopCountFalse() { return _loopCountFalse; }

uint32_t LoopUtil::getLoopCountTrue() { return _loopCountTrue; }

float LoopUtil::getLoopSum() { return _loopSumFalse + _loopSumTrue; }

float LoopUtil::getLoopSumFalse() { return _loopSumFalse; }

float LoopUtil::getLoopSumTrue() { return _loopSumTrue; }

void LoopUtil::resetLoopStatistics() {
  if (_debugFlag2) {
    Serial.println(F("resetting the loop statistics."));
  }
  _loopCount = _loopCountFalse = _loopSumTrue = 0;
  _loopSumTrue = _loopSumFalse = 0;
}

uint32_t LoopUtil::getLoopStartTime() { return _loopStartTime; }

uint32_t LoopUtil::getLoopEndTime() { return _loopEndTime; }

void LoopUtil::loopStart() {
  _skipWait = false;
  _loopCount++;
  _debugFlag2 = _debugFlag;
  _loopStartTime = millis();
  if (_usesTriggerPin) {
    digitalWrite(_triggerPin, HIGH);
    delayMicroseconds(_triggerPulseWidth);
    digitalWrite(_triggerPin, LOW);
  }
  _computeLoopEndTime();
  if (_debugFlag2) {
    if (_newLineBeforeLoopStart) {
      Serial.println();
    }
    Serial.print(F("loop #"));
    Serial.print(_loopCount);
    Serial.print(F(" start, internal clock: "));
    Serial.print(millis());
    Serial.print(F(" msec"));
    if (_usesTriggerPin) {
      Serial.print(F(", raised trigger pin "));
      Serial.print(_triggerPin);
      Serial.print(F(" for "));
      Serial.print(_triggerPulseWidth);
      Serial.print(F(" usec"));
    }
    Serial.println();
    if (_loopStartIntermittentCallback) {
      _loopStartIntermittentCounter++;
      if (_loopStartIntermittentCounter >= _loopStartIntermittentInterval) {
        _loopStartIntermittentCallback();
        _loopStartIntermittentCounter = 0;
      }
    }
  }
}

void LoopUtil::loopEnd(boolean flag, float amount) {
  if (flag) {
    _loopCountTrue++;
    _loopSumTrue += amount;
  } else {
    _loopCountFalse++;
    _loopSumFalse += amount;
  }
  _waitForLoopEnd();
  if (_debugFlag2) {
    Serial.print(F("loop #"));
    Serial.print(_loopCount);
    Serial.println(F(" end"));
    if (_loopEndIntermittentCallback) {
      _loopEndIntermittentCounter++;
      if (_loopEndIntermittentCounter >= _loopEndIntermittentInterval) {
        _loopEndIntermittentCallback();
        _loopEndIntermittentCounter = 0;
      }
    }
    if (_newLineAfterLoopEnd) {
      Serial.println();
    }
  }
}

void LoopUtil::loopEnd(boolean flag) { loopEnd(flag, 0.0); }

void LoopUtil::loopEnd(float amount) { loopEnd(true, amount); }

void LoopUtil::loopEnd() { loopEnd(true); }

void LoopUtil::skipWait() { _skipWait = true; }

void LoopUtil::setNewLineBeforeLoopStart(boolean flag) {
  _newLineBeforeLoopStart = flag;
}

void LoopUtil::setNewLineAfterLoopEnd(boolean flag) {
  _newLineAfterLoopEnd = flag;
}

void LoopUtil::_computeLoopEndTime() {
  _loopEndTime = _loopStartTime + _loopDuration;
  _timeRolloverInLoop = _loopEndTime < _loopStartTime;
}

void LoopUtil::_waitForLoopEnd() {
  if (_timeRolloverInLoop || _skipWait) {
    // nope
  } else {
    uint32_t now = millis();
    if (now < _loopEndTime) {
      uint32_t waitInterval = _loopEndTime - now;
      delay(waitInterval);
    }
  }
}

void LoopUtil::registerLoopStartIntermittentCallback(LoopUtilIntermittentCallback callback,
                                               uint32_t interval) {
  _loopStartIntermittentCallback = callback;
  if (callback) {
    _loopStartIntermittentInterval = interval;
  } else {
    _loopStartIntermittentInterval = 0;
  }
  _loopStartIntermittentCounter = 0;
}

void LoopUtil::registerLoopEndIntermittentCallback(LoopUtilIntermittentCallback callback,
                                             uint32_t interval) {
  _loopEndIntermittentCallback = callback;
  if (callback) {
    _loopEndIntermittentInterval = interval;
  } else {
    _loopEndIntermittentInterval = 0;
  }
  _loopEndIntermittentCounter = 0;
}
