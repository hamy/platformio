#ifndef _RETRY_UTIL_H
#define _RETRY_UTIL_H

/** \mainpage RetryUtil - C Helpers to Achieve a Goal via Multiple Attempts

  <table border='0'><tr><td>
  <em>Success consists of going from failure to failure without loss of
  enthusiasm. </em>
  </td></tr><tr><td align='right'>
  <em>Winston Churchill (1874-1965)</em>
  </td></tr></table>

  \section manual-approach A Manual Approach

  Consider the following pseudo-code:

  \code
  boolean tryAgainAndAgain(int maxAttempts) {}
    for (int attempt=1; attempt <= maxAttempts; attempt++) {
       boolean success = trySomething(attempt);
       if (success) {
          handlerForSuccess(attempt);
          return true;
       }
       waitSomeGracePeriodBeforeTheNextAttempt();
    }
    handlerForFailure(maxAttempts);
    return false;
  }
  \endcode

  - The function tryAgainAndAgain() is some sort of umbrella that encapsulates
  multiple attempts to achieve the goal.
  - The domain specific code in function trySomething() encapsulates a single attempt  to achieve the
  goal. The function returns
     - **true** if the attempt ended successfully, and
     - **false** if the attempt failed.
  - If trySomething() was successful, the domain-specific handler
  handlerForSuccess() is called with the number of needed attempts as argument.
  Then, the umbrella function is left with the result **true** as success indicator.
  - If trySometing() failed, it is called again after a certain grace period.
  The attempt is repeated until trySomething() returns successfully or the limit
  maxAttempts is reached.
  - If the final call to trySomething() failed, the domain-specific handler
  handlerForFailure() is called (again with the number of attemps as argument).
  Then, the umbrella function is left with the result **false** as failure
  indicator.

  \section ru-approach The RetryUtil Approach

  Construct a RetryUtil instance with the following arguments:
     - A flag controlling the generation of debug output.
     - The maximum number of attempts.
     - The grace period between two attempts.

  The following example uses an RetryUtil instance with activated debugging, a
  maximum attempt number of 50, and a grace period of 10 milliseconds.

  \code
  RetryUtil retryUtil(true,50,10);
  \endcode

  Register a request callback function with a single integer argument (the
  number of the current attempt) that returns a success indicator flag. The type
  definition  RetryUtilRequestCallback denotes this sort of callback:

  \code
  boolean trySomething(int attempt) { ... }
            :          :
  retryUtil.setRequestCallback(trySomething);
  \endcode



  Optionally, register callback functions with a single argument (the final
  number of attempts) for success or failure. The type definition
  RetryUtilReplyCallback denotes this sort of callback. Note: You can omit the
  registration of one or both handler callbacks if you don't need them.

  \code
  void handlerForSucccess(int attempt) { ... }
            :          :
  retryUtil.setSuccessCallback(handlerForSuccess);
            :          :
  void handlerForFailure(int attempt) { ... }
            :          :
  retryUtil.setSuccessCallback(handlerForSuccess);
  \endcode



  Finally, let the function retry() of class RetryUtil do all the heavy-lifting including attempt loop control and
  invoking the appropriate callback functions:

  \code
  int result = retryUtil.retry();
  \endcode

  - If the goal could be achieved, a positive value is returned, that denotes
  the number of attempts.
  - If the goal could not be achieved, a negative value is returned. Its
  magnitude denotes the number of attempts.
 */

#include <Arduino.h>

extern "C" {
/**
 The callback function for the request part of the retry procedure.
 The RetryUtil instance calls this callback and expects that the callback
 indicates success or failure in the result value. \param attempt The sequence
 number of the current attempt (starting with 1). \return A flag that indicates
 success or failure.
         - true: The request was handled successfully.
         - false: The request ended with a failure. The RetryUtil will wait for
 the given delay between attempts; afterwards, it will retry this request until
 the maximum number of attempts is reached.
 */
typedef boolean (*RetryUtilRequestCallback)(int attempt);

/**
 The callback function for the reply part of the retry procedure.
 Clients may register both
 - a success callback, and
 - a failure callback functions.
 Both implement this callback.
 \param The number of attempts that occured until the callback was fired.
  */
typedef void (*RetryUtilReplyCallback)(int attempts);
}

/**
 * The version of this helpers.
 */
#define RETRY_UTIL_VERSION "1.0.7"

/**
 * The default value for the maximum number of attempts.
 */
#define DEFAULT_MAX_ATTEMPTS (100)

/**
 * The default value for the delay between two attempts in milliseconds.
 */
#define DEFAULT_DELAY_BETWEEN_ATTEMPTS (10)

/**
 * A set of simple helper functions.
 */
class RetryUtil {
private:
  boolean _debugFlag;
  int _attempts;
  int _maxAttempts;
  int _delayBetweenAttempts;
  uint32_t _retryCount;
  uint32_t _successCount;
  uint32_t _failureCount;
  uint32_t _successAttemptCount;
  uint32_t _failureAttemptCount;
  RetryUtilRequestCallback _requestCallback;
  RetryUtilReplyCallback _successCallback;
  RetryUtilReplyCallback _failureCallback;
  boolean _abortFlag;
  boolean _abortCheck(int attempt);

public:
  /**
  Constructor for the helper instance.
  \param debugFlag A flag that controls the generation of debug message.
  \param maxAttempts The maximum number of attempts.
  \param delayBetweenAttempts  The delay between two attempts in milliseconds.
  */
  RetryUtil(boolean debugFlag, int maxAttempts, uint32_t delayBetweenAttempts);

  /**
  Constructor for the helper instance. For the delay between two attemps, the
  default value DEFAULT_DELAY_BETWEEN_ATTEMPTS is used.

  \param debugFlag A flag  that controls the generation of debug message.

  \param maxAttempts The maximum  number of attempts.

  */
  RetryUtil(boolean debugFlag, int maxAttempts);

  /**
  Constructor for the helper instance.  For the maximum number of  attemps, the
  default value DEFAULT_MAX_ATTEMPTS is used.  For the delay between two
  attemps, the  default value DEFAULT_DELAY_BETWEEN_ATTEMPTS is used.

  \param debugFlag A flag that controls the generation of debug message.
    */
  RetryUtil(boolean debugFlag);

  /**
  Constructor for the helper instance. No debug output is generated. For the
  maximum number of  attemps, the default value DEFAULT_MAX_ATTEMPTS is used.
  For the delay between two attemps, the  default value
  DEFAULT_DELAY_BETWEEN_ATTEMPTS is used.
    */
  RetryUtil();

  /**
Print a self-description to the Serial output.
*/
  void printInfo();

  /**
  A flag that indicates whether debug output is created or not.
  \return The flag value.
        - true: At each loop start and loop end, debug messages are created.
        - false: No debug output is created.
  */
  boolean getDebugFlag();

  /**
  Changes the  debug output  creation flag.
  \param debugFlag The new flag value.
        - true: At each loop start and loop end, debug messages are created.
        - false: No debug output is created.
  */
  void setDebugFlag(boolean debugFlag);

  /**
   Returns the maximum number of attempts.
   \return The maximum number.
   */
  int getMaxAttempts();

  /**
   Redefines the maximum number of attempts.
   \param maxAttempts The new maximum number.
   */
  void setMaxAttempts(int maxAttempts);

  /**
   Returns the delay between two attempts.
   \return The delay in milliseconds.
   */
  uint32_t getDelayBetweenAttempts();

  /**
   Redefines the delay between two attempts.
   \param delayBetweenAttempts The new delay in milliseconds.
   */
  void setDelayBetweenAttempts(uint32_t delayBetweenAttempts);

  /**
   Returns the request callback.
   \return The callback.
   */
  RetryUtilRequestCallback getRequestCallback();

  /**
   Redefines the request callback.
   \param requestCallback The callback.
   */
  void setRequestCallback(RetryUtilRequestCallback requestCallback);

  /**
   Returns the reply callback for successful attempts.
   \return The callback.
   */
  RetryUtilReplyCallback getSuccessCallback();

  /**
   Redefines the reply callback for successful attempts.
   \param successCallback The callback.
   */
  void setSuccessCallback(RetryUtilReplyCallback successCallback);

  /**
   Returns the reply callback for failed attempts.
   \return The callback.
   */
  RetryUtilReplyCallback getFailureCallback();

  /**
   Redefines the reply callback for failed attempts.
   \param failureCallback The callback.
   */
  void setFailureCallback(RetryUtilReplyCallback failureCallback);

  /**
   Retries an operation until a successful attempt is performed or the maximum
   number of attempts is reached. \return The final number of attempts. The sign
   indicates whether the attempts finallyi were successful or failed.
           - positive values indicate final success.
           - negative values indicate final failure.
   */
  int retry();

  /**
   Aborts the current retry procedure even if the maximum number of attempts is not reached.
   */
  void abort();

  /**
   Returns the number of calls to retry().
   \return The number of calls.
   */
  uint32_t getRetryCount();

  /**
   Returns the number of calls to retry() that ended in final success.
   \return The number of calls.
   */
  uint32_t getSuccessCount();

  /**
   Returns the number of calls to retry() that ended in final failure.
   \return The number of calls.
   */
  uint32_t getFailureCount();

  /**
   Returns the total number of attempts during calls to retry().
   \return The number of attempts.
   */
  uint32_t getAttemptCount();

  /**
   Returns the total number of attempts during calls to retry() that ended in
   final success. \return The number of attempts.
   */
  uint32_t getSuccessAttemptCount();

  /**
   Returns the total number of attempts during calls to retry() that ended in
   final failure. \return The number of attempts.
   */
  uint32_t getFailureAttemptCount();

  /**
   Resets the statistics.
   */
  void resetStatistics();

  /**
Print statistics to the Serial output.
 */
  void printStatistics();
};

#endif
