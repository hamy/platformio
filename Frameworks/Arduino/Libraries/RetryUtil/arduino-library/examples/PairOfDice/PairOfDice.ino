/**
 * RetryUtilStatistics.ino
 *
 * Example for the RetryUtil library
 *
 * Source: https://gitlab.com/hamy/platformio
 *
 * This sketch simulates a game that uses a pair of dice.
 * The dice are repeatedly rolled until they show a double six.
 * These repeated attempts are controlled by an instance of RetryUtil.
 */

#include "Arduino.h"
#include "RetryUtil.h"

// The debugFlag controls the generating of debug output messages.
// Specify true or false in order to switch messages on or off, respectively.
boolean debugFlag = true;

int maxAttempts = 100;
int delayBetweenAttempts = 1; // one millisecond delay between two attempts
RetryUtil retryUtil(debugFlag, maxAttempts, delayBetweenAttempts);

/**
 * Rolls a single dice. Returns a value between 1 and 6.
 */
int rollDice() {
  return random(1, 7); // lower bound is inclusive, upper bound is exclusive
}

/**
 * The request callback function. Rolls a pair of dice:
 * - Return true for a double six.
 * - Otherwise, return false.
 * - Note: the argument attempt is ignored.
 */
boolean rollPairOfDice(int attempt) {
  int value1 = rollDice();
  int value2 = rollDice();
  boolean result = (value1 == 6) && (value2 == 6);
  if (debugFlag) {
    Serial.print(F("Rolling a pair of dice: ("));
    Serial.print(value1);
    Serial.print(F(","));
    Serial.print(value2);
    Serial.print(F(") => "));
    Serial.println(result);
  }
  return result;
}

/**
 * The callback handler for success.
 */
void handlerForSuccess(int finalNumberOfAttempts) {}

/**
 * The callback handler for failure.
 */
void handlerForFailure(int finalNumberOfAttempts) {}

/**
 * Serial connection initialization:
 * - Open a serial connection using a baud rate of 9600.
 * - Create debug output messages if debugFlag is true.
 */
void setupSerial() {
  Serial.begin(9600);
  if (debugFlag) {
    Serial.println(F("Starting sketch PairOfDice..."));
  }
}

/**
 * Game initialization:
 * - Register callbacks.
 * - Create debug output messages if debugFlag is true.
 */
void setupGame() {
  retryUtil.setRequestCallback(rollPairOfDice);
  retryUtil.setSuccessCallback(handlerForSuccess);
  retryUtil.setFailureCallback(handlerForFailure);
  if (debugFlag) {
    retryUtil.printInfo();
  }
}

/**
 * Sketch initialization:
 * - Initialize serial connection,
 * - Initializes the game.
 */
void setup() {
  setupSerial();
  setupGame();
}

/**
 * The main Arduino loop:
 */
void loop() {
  int attempts = retryUtil.retry();
  if (debugFlag) {
    Serial.println();
    if (attempts > 0) {
      Serial.print(F("Final success after "));
      Serial.print(attempts);
    } else {
      Serial.print(F("Final failure after "));
      Serial.print(-attempts);
    }
    Serial.println(F(" attempts"));
    retryUtil.printStatistics();
    Serial.println();
    delay(1000);
  }
}