/**
 * RandomWalk.ino
 *
 * Example for the RetryUtil library
 *
 * Source: https://gitlab.com/hamy/platformio
 *
 * This sketch simulates a one-dimensional random walk.
 */

#include "Arduino.h"
#include "RetryUtil.h"

// The debugFlag controls the generating of debug output messages.
// Specify true or false in order to switch messages on or off, respectively.
boolean debugFlag = true;

int maxAttempts = 100;
int delayBetweenAttempts = 1; // one millisecond delay between two attempts
RetryUtil retryUtil(debugFlag, maxAttempts, delayBetweenAttempts);

int currentPosition;

void resetCurrentPosition() { currentPosition = 0; }

/**
 * Performs a single step of the random walk.
 */
int walk() { return random(2) ? 1 : -1; }

/**
 * The request callback function. Rolls a pair of dice:
 * - Return true if the initial position 0 is reached again.
 * - Otherwise, return false.
 * - Note: the argument attempt is ignored.
 */
boolean performWalks(int attempt) {
  int step = walk();
  currentPosition += step;
  boolean result = currentPosition == 0;
  if (debugFlag) {
    Serial.print(F("Attempt #"));
    Serial.print(attempt);
    Serial.print(F(", walk step: "));
    Serial.print(step);
    Serial.print(F(", current Position: "));
    Serial.print(currentPosition);
    Serial.print(F(" => "));
    Serial.println(result);
  }
  return result;
}

/**
 * The callback handler for success.
 */
void handlerForSuccess(int finalNumberOfAttempts) {}

/**
 * The callback handler for failure.
 */
void handlerForFailure(int finalNumberOfAttempts) {}

/**
 * Serial connection initialization:
 * - Open a serial connection using a baud rate of 9600.
 * - Create debug output messages if debugFlag is true.
 */
void setupSerial() {
  Serial.begin(9600);
  if (debugFlag) {
    Serial.println(F("Starting sketch RandomWalk..."));
  }
}

/**
 * Game initialization:
 * - Register callbacks.
 * - Create debug output messages if debugFlag is true.
 */
void setupGame() {
  retryUtil.setRequestCallback(performWalks);
  retryUtil.setSuccessCallback(handlerForSuccess);
  retryUtil.setFailureCallback(handlerForFailure);
  if (debugFlag) {
    retryUtil.printInfo();
  }
}

/**
 * Sketch initialization:
 * - Initialize serial connection,
 * - Initializes the game.
 */
void setup() {
  setupSerial();
  setupGame();
}

/**
 * The main Arduino loop:
 */
void loop() {
  resetCurrentPosition();
  int attempts = retryUtil.retry();
  if (debugFlag) {
    Serial.println();
    if (attempts > 0) {
      Serial.print(F("Final success after "));
      Serial.print(attempts);
    } else {
      Serial.print(F("Final failure after "));
      Serial.print(-attempts);
    }
    Serial.println(F(" attempts"));
    retryUtil.printStatistics();
    Serial.println();
    delay(1000);
  }
}
