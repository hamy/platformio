#include "RetryUtil.h"

RetryUtil::RetryUtil(boolean debugFlag, int maxAttempts,
                     uint32_t delayBetweenAttempts) {
  setDebugFlag(debugFlag);
  setMaxAttempts(maxAttempts);
  setDelayBetweenAttempts(delayBetweenAttempts);
  resetStatistics();
}

RetryUtil::RetryUtil(boolean debugFlag, int maxAttempts) {
  setDebugFlag(debugFlag);
  setMaxAttempts(maxAttempts);
  setDelayBetweenAttempts(DEFAULT_DELAY_BETWEEN_ATTEMPTS);
  resetStatistics();
}

RetryUtil::RetryUtil(boolean debugFlag) {
  setDebugFlag(debugFlag);
  setMaxAttempts(DEFAULT_MAX_ATTEMPTS);
  setDelayBetweenAttempts(DEFAULT_DELAY_BETWEEN_ATTEMPTS);
  resetStatistics();
}

RetryUtil::RetryUtil() {
  setDebugFlag(false);
  setMaxAttempts(DEFAULT_MAX_ATTEMPTS);
  setDelayBetweenAttempts(DEFAULT_DELAY_BETWEEN_ATTEMPTS);
  resetStatistics();
}

void RetryUtil::printInfo() {
  Serial.println(F("RetryUtil Info:"));
  Serial.print(F("- Version: "));
  Serial.println(F(RETRY_UTIL_VERSION));
  Serial.print(F("- Source file: "));
  Serial.println(F(__FILE__));
  Serial.print(F("- Last modification time: "));
  Serial.println(F(__TIMESTAMP__));
  Serial.print(F("- Compilation time: "));
  Serial.print(F(__DATE__));
  Serial.print(F(" "));
  Serial.println(F(__TIME__));
  Serial.print(F("- Debug flag: "));
  Serial.println(_debugFlag);
  Serial.print(F("- Max attempts: "));
  Serial.println(_maxAttempts);
  Serial.print(F("- Delay between attempts: "));
  Serial.print(_delayBetweenAttempts);
  Serial.println(F(" msec"));
  Serial.print(F("- Request callback: 0x"));
  Serial.println((uint32_t) _requestCallback,HEX);
  Serial.print(F("- Success callback: 0x"));
  Serial.println((uint32_t) _successCallback,HEX);
  Serial.print(F("- Failure callback: 0x"));
  Serial.println((uint32_t) _failureCallback,HEX);
}

boolean RetryUtil::getDebugFlag() { return _debugFlag; }

void RetryUtil::setDebugFlag(boolean debugFlag) { _debugFlag = debugFlag; }

int RetryUtil::getMaxAttempts() { return _maxAttempts; }

void RetryUtil::setMaxAttempts(int maxAttempts) { _maxAttempts = maxAttempts; }

uint32_t RetryUtil::getDelayBetweenAttempts() { return _delayBetweenAttempts; }

void RetryUtil::setDelayBetweenAttempts(uint32_t delayBetweenAttempts) {
  _delayBetweenAttempts = ((int)delayBetweenAttempts);
}

RetryUtilRequestCallback RetryUtil::getRequestCallback() {
  return _requestCallback;
}

void RetryUtil::setRequestCallback(RetryUtilRequestCallback requestCallback) {
  _requestCallback = requestCallback;
}

RetryUtilReplyCallback RetryUtil::getSuccessCallback() {
  return _successCallback;
}

void RetryUtil::setSuccessCallback(RetryUtilReplyCallback successCallback) {
  _successCallback = successCallback;
}

RetryUtilReplyCallback RetryUtil::getFailureCallback() {
  return _failureCallback;
}

void RetryUtil::setFailureCallback(RetryUtilReplyCallback failureCallback) {
  _failureCallback = failureCallback;
}

int RetryUtil::retry() {
  int attempt;
  _abortFlag = false;
  _retryCount++;
  for (attempt = 1; attempt <= _maxAttempts; attempt++) {
    if (_abortCheck(attempt)) {
      return -attempt;
    }
    if ((attempt > 1) && (_delayBetweenAttempts > 0)) {
      delay(_delayBetweenAttempts);
    }
    boolean result = _requestCallback(attempt);
    if (_abortCheck(attempt)) {
      return -attempt;
    }
    if (result) {
      _successCount++;
      _successAttemptCount += attempt;
      if (_successCallback) {
        _successCallback(attempt);
      }
      return attempt;
    }
  }
  _failureCount++;
  _failureAttemptCount += _maxAttempts;
  if (_failureCallback) {
    _failureCallback(_maxAttempts);
  }
  return -_maxAttempts;
}

boolean RetryUtil::_abortCheck(int attempt) {
  if (_abortFlag) {
    _failureCount++;
    _failureAttemptCount += attempt;
    return true;
  } else {
    return false;
  }
}

void RetryUtil::abort() { _abortFlag = true; }

uint32_t RetryUtil::getRetryCount() { return _retryCount; }

uint32_t RetryUtil::getSuccessCount() { return _successCount; }

uint32_t RetryUtil::getFailureCount() { return _failureCount; }

uint32_t RetryUtil::getAttemptCount() {
  return _successAttemptCount + _failureAttemptCount;
}

uint32_t RetryUtil::getSuccessAttemptCount() { return _successAttemptCount; }

uint32_t RetryUtil::getFailureAttemptCount() { return _failureAttemptCount; }

void RetryUtil::resetStatistics() {
  _retryCount = _successCount = _failureCount = _successAttemptCount =
      _failureAttemptCount = 0;
}

void RetryUtil::printStatistics() {
  Serial.println(F("RetryUtil Statistics:"));
  Serial.print(F("- retry count: "));
  Serial.println(_retryCount);
  Serial.print(F("- success count: "));
  Serial.println(_successCount);
  Serial.print(F("- failure count: "));
  Serial.println(_failureCount);
  Serial.print(F("- attempt count: "));
  Serial.println(getAttemptCount());
  Serial.print(F("- success attempt count: "));
  Serial.println(_successAttemptCount);
  Serial.print(F("- failure attempt count: "));
  Serial.println(_failureAttemptCount);
}
