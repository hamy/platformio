#ifndef _ESP8266_UTIL_H
#define _ESP8266_UTIL_H

/**
 \mainpage ESP8266Util - C Library Providing Helpers for the 10 Platform.

 \section lu-purpose Purpose of the ESP8266Util Library

 ESP8266Util offers....

 \section lu-arduino-library Arduino Library

 The ESP8266Util library is available as pre-packed Arduino library
 [ESP8266Util-arduino-library.zip](../ESP8266Util-arduino-library/ESP8266Util-arduino-library.zip). Download
 this zip archive and unpack it in the **libraries** directory of your Arduino
 sketchbook. It will create a branch **ESP8266Util** in the **libraries** directory.
 This Arduino library comes with several sketches:

 - **Blink.ino** is the ESP8266Util version of the basic Arduino **Blink** sketch
 that comes with the Arduino IDE.
 */
   
#include <Arduino.h>

/**
 * The version of this helpers.
 */
#define ESP8266_UTIL_VERSION "1.0.0"

 #ifndef MAC_ADDR_MY_NODEMCU_1_0_1 
/**
 The MAC address of NodeMCU 1.0 #1
 */
#define MAC_ADDR_MY_NODEMCU_1_0_1  ({ 0x5c,0xcf,0x7f,0x00,0xd7,0xe9 })
#endif
 
  
 
/**
 * A simple 10 helper class.
 */
class ESP8266Util
{
private:
 
 
public:
 };

/**
 The singleton instance of this class.
 */
ESP8266Util E8266U();

#endif
