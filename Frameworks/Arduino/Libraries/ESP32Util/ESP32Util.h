#ifndef _ESP32_UTIL_H
#define _ESP32_UTIL_H

/**
 \mainpage ESP32Util - C Library Providing Helpers for the ESP32 Platform.

 \section lu-purpose Purpose of the ESP32Util Library

 ESP32Util offers....

 \section lu-arduino-library Arduino Library

 The ESP32Util library is available as pre-packed Arduino library
 [ESP32Util-arduino-library.zip](../ESP32Util-arduino-library/ESP32Util-arduino-library.zip). Download
 this zip archive and unpack it in the **libraries** directory of your Arduino
 sketchbook. It will create a branch **ESP32Util** in the **libraries** directory.
 This Arduino library comes with several sketches:

 - **Blink.ino** is the ESP32Util version of the basic Arduino **Blink** sketch
 that comes with the Arduino IDE.
 */
   
#include <Arduino.h>

/**
 * The version of this helpers.
 */
#define ESP32_UTIL_VERSION "1.0.0"

#ifndef MAC_ADDR_MY_HUZZAH32_ESP32_FEATHER
/**
 The MAC address of my Adafruit Huzzah32 ESP32 Feather
 */
#define MAC_ADDR_MY_HUZZAH32_ESP32_FEATHER ({ 0xa4,0xcf,0x12,0x31,0xa5,0xf4 })
#endif

#ifndef MAC_ADDR_MY_HUZZAH32_NODEMCU_ESP32_1 
/**
 The MAC address of my Joy-IT NodeMCU ESP32 #1
 */
#define MAC_ADDR_MY_HUZZAH32_NODEMCU_ESP32_1 ({ 0x24,0x6f,0x28,0x21,0xfa,0x1c })
#endif
 
#ifndef MAC_ADDR_MY_HUZZAH32_NODEMCU_ESP32_2 
/**
 The MAC address of my Joy-IT NodeMCU ESP32 #2
 */
#define MAC_ADDR_MY_HUZZAH32_NODEMCU_ESP32_2 ({ 0x24,0x62,0xab,0xb0,0x72,0xb8 })
#endif
 
 
/**
 * A simple ESP32 helper class.
 */
class ESP32Util
{
private:
 
 
public:
  /**
   Prints an explanation of an error code on the Serial output.
   @param err The error code.
   */
  void printErrorCode(uint32_t err);
};

/**
 The singleton instance of this class.
 */
ESP32Util E32U();

#endif
