#include "ESP32Util.h"
 
 
void ESP32Util::printErrorCode(uint32_t err) { 
  Serial.print(F("0x"));
  Serial.print(err,HEX);
  Serial.print(F("="));
  Serial.print(err);
  Serial.print(F(": "));
  Serial.println(esp_err_to_name(err));
}

 