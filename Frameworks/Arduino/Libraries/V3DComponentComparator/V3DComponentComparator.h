#ifndef _V3D_COMPONENT_COMPARATOR_H
#define _V3D_COMPONENT_COMPARATOR_H

/**
 * \mainpage V3DComponentComparator- A Set of C Helpers for Comparing the
 * Componentes of 3D Vectors
 */

#include <Arduino.h>

/**
 * The version of this helpers.
 */
#define V3D_VERSION "1.0.3"

/**
 * Indicates an invalid order of the x,y,z components.
 */
#define V3D_INVALID ((uint8_t)0)

/**
 * Indicates x > y > z.
 */
#define V3D_XYZ ((uint8_t)1)

/**
 * Indicates x > z > y.
 */
#define V3D_XZY ((uint8_t)2)

/**
 * Indicates y > x > z.
 */
#define V3D_YXZ ((uint8_t)3)

/**
 * Indicates y > z > x.
 */
#define V3D_YZX ((uint8_t)4)

/**
 * Indicates z > x > y.
 */
#define V3D_ZXY ((uint8_t)5)

/**
 * Indicates z > y > x.
 */
#define V3D_ZYX ((uint8_t)6)

/**
 * The cardinality of the V3D_... values.
 */
#define V3D_CARDINALITY ((uint8_t)7)

/**
 Identifies an invalid threshold value.
 */
#define V3D_INVALID_THRESHOLD (-1.0)

/**
 * A comparator instance.
 */
class V3DComponentComparator {
private:
  float _threshold;
  float _thresholdSquare;
  boolean _usesThreshold;

public:
  /**
 Constructor for a comparator.

 \param threshold The threshold for detecting neglectable  vectors.
        - threshold >= 0: Use a threshold for identifying neglectable vectors.
        - threshold <0: Do not use any threshold, no vectors are neglected.
  */
  V3DComponentComparator(float threshold);

  /**
 Constructor for a comparator. No vectors are neglected.
  */
  V3DComponentComparator();

  /**
   Returs a flag that indicates whether a threshold value is used.

   \return The flag value.
         - true: The comparator uses a threshold value.
         - false: The comparator does note use a threshold value.
   */
  boolean usesThreshold();

  /**
   Returns the threshold value.

   \return The threshold value.
   */
  float getThreshold();

  /**
   Returns the square of the threshold value.

   \return The squared threshold value.
   */
  float getThresholdSquare();

  /**
   Performs a simple comparation of the x,y,z components directly using the
   x,y,z values.

   \param x The x value.

   \param y The y value.

   \param z The z
   value.
   */
  uint8_t compare(float x, float y, float z);

  /**
   Performs a comparation of the x,y,z components using the magnitudes of the
   x,y,z values.

   \param x The x value.

   \param y The y value.

   \param z The z
   value.
   */
  uint8_t compareByMagnitude(float x, float y, float z);
};

#endif
