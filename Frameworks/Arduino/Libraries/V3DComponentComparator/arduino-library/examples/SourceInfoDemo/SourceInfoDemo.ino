/**
 * SourceInfoDemo.ino
 *
 * Example for the SourceInfo library
 *
 * Source: https://gitlab.com/hamy/platformio
 *
 * This sketch simulates a one-dimensional random walk.
 */

#include "Arduino.h"
#include "SourceInfo.h"

 
/**
 * Serial connection initialization:
 * - Open a serial connection using a baud rate of 9600.
   */
void setup () {
  Serial.begin(9600);
 }
 
/**
 * The main Arduino loop:
 */
void loop() {
     delay(1000);
}
