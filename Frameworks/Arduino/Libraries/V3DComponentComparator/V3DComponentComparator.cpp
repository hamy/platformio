#include "V3DComponentComparator.h"

V3DComponentComparator::V3DComponentComparator(float threshold)
{
  _usesThreshold = threshold >= 0.0;
  _threshold = _usesThreshold ? threshold : V3D_INVALID_THRESHOLD;
  _thresholdSquare = _usesThreshold ? threshold : V3D_INVALID_THRESHOLD;
  _thresholdSquare = _usesThreshold ? threshold * threshold : V3D_INVALID_THRESHOLD;
}

V3DComponentComparator::V3DComponentComparator()
{
  _threshold = _thresholdSquare = V3D_INVALID_THRESHOLD;
  _usesThreshold = false;
}

boolean V3DComponentComparator::usesThreshold()
{
  return _usesThreshold;
}

float V3DComponentComparator::getThreshold()
{
  return _usesThreshold ? _threshold : V3D_INVALID_THRESHOLD;
}

float V3DComponentComparator::getThresholdSquare()
{
  return _usesThreshold ? _thresholdSquare : V3D_INVALID_THRESHOLD;
}

uint8_t V3DComponentComparator::compare(float x, float y, float z)
{
  if (_usesThreshold)
  {
    float mag2 = x * x + y * y + z * z;
    if (mag2 < _thresholdSquare)
    {
      return V3D_INVALID;
    }
  }
  if (x >= y && y >= z)
  {
    return V3D_XYZ;
  }
  if (x >= z && z >= y)
  {
    return V3D_XZY;
  }
  if (y >= x && x >= z)
  {
    return V3D_YXZ;
  }
  if (y >= z && z >= x)
  {
    return V3D_YZX;
  }
  if (z >= x && x >= y)
  {
    return V3D_ZXY;
  }
  return V3D_ZYX;
}

uint8_t V3DComponentComparator::compareByMagnitude(float x, float y, float z)
{
  float xx = abs(x);
  float yy = abs(y);
  float zz = abs(z);
  return compare(xx, yy, zz);
}
