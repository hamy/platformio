/**
 * SourceInfoDemo.ino
 *
 * Example for the SourceInfo library
 *
 * Source: https://gitlab.com/hamy/platformio
 *
 * This sketch demonstrates the usage of the SourceInfo macros.
 */

#include "Arduino.h"
#include "SourceInfo.h"

/**
 * Serial connection initialization:
 * - Open a serial connection using a baud rate of 9600.
 * - Macro SOURCE_INFO_PRINT demo: Print several meta-data for this sketch.
 */
void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
  Serial.begin(9600);
  while (!Serial) {
      ;
  }
  SOURCE_INFO_ENTERING("setup")
  SOURCE_INFO_PRINT
  digitalWrite(LED_BUILTIN, LOW);
}

int loopCount = 0;
int maxLoopCount = 5;

void atLoopStart() {
  SOURCE_INFO_ENTERING("atLoopStart")
  digitalWrite(LED_BUILTIN, HIGH);
  Serial.print(F("loopCount: "));
  Serial.println(loopCount);
  delay(500);
  digitalWrite(LED_BUILTIN, LOW);
  SOURCE_INFO_LEAVING("atLoopStart")
}

void atLoopEnd() {
  SOURCE_INFO_ENTERING("atLoopEnd")
  digitalWrite(LED_BUILTIN, HIGH);
  Serial.print(F("loopCount: "));
  Serial.println(loopCount);
  delay(500);
  digitalWrite(LED_BUILTIN, LOW);
  SOURCE_INFO_LEAVING("atLoopEnd")
}

void createSnippetWithAnalogInputValues() {
  SOURCE_INFO_ENTERING("createSnippetWithAnalogInputValues")
  SOURCE_INFO_SNIPPET_START("analogInputValues")
  Serial.println("[");
    for (int analogInputPort = 0; analogInputPort < 6; analogInputPort++) {
      Serial.print(F("  { analogInputPort=\""));
      Serial.print(analogInputPort);
      Serial.print(F("\", analogValue=\""));
      Serial.print(analogRead(analogInputPort));
      Serial.print(F("\" }"));
      if (analogInputPort<5) {
        Serial.print(F(","));
      }
      Serial.println();
  }
  Serial.println("]");
  SOURCE_INFO_SNIPPET_END("analogInputValues")
  SOURCE_INFO_LEAVING("createSnippetWithAnalogInputValues")
}

/**
 * The main Arduino loop:
 */
void loop() {
  loopCount++;
  atLoopStart();
  if (loopCount==1) {
    createSnippetWithAnalogInputValues();
  }
  delay(2000);
  if (loopCount > maxLoopCount) {
    digitalWrite(LED_BUILTIN, HIGH);
    SOURCE_INFO_BAILING_OUT("maximum loopCount exceeded !")
  }
  atLoopEnd();
}
