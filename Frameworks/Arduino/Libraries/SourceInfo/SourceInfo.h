#ifndef _SOURCE_INFO_H
#define _SOURCE_INFO_H

/**
 \mainpage SourceInfo - Macros that Provide Debug Print Output.

 This header file contains a set of macros that generate debug output via the
 serial connection between the microcontroller and the desktop host.

 \section si-purpose Purpose of the SourceInfo Header File

 TBD

 \section si-progmem Storing Strings in the PROGMEM Area

 Since on most Arduinos, RAM space is an **extremly** limited
 resource, all String constants within the SourceInfo macros are stored in the PROGMEM
 area; i.e., they do not waste precious RAM space but reside within the flash
 space that normally holds the compiled and linked instructions. The macros
 frequently use the **F(...)** macro that moves string constants to the PROGMEM
 area.

 \section si-macro-constants Macro Constants

 - ::SOURCE_INFO_VERSION: the version number of this header file.
 - ::SOURCE_INFO_FILE: the name of the source file that contains the macro
 invocation.
 - ::SOURCE_INFO_COMPILED_AT: the date and time of the compilation of the source
 file that contains the macro invocation.
 - ::SOURCE_INFO_MODIFIED_AT: the last modification date of the source file that
 contains the macro invocation.

 \section si-macro-output Macros that Produce Debug Output

 - ::SOURCE_INFO_NL: Prints an EOL sequence.
 - ::SOURCE_INFO_PRINT: Prints the name of the source file and its compilation
 and last modification dates.
 - ::SOURCE_INFO_ENTERING: Generates print output at the beginning of a
 function.
 - ::SOURCE_INFO_LEAVING: Generates print output at the end of a function.
 - ::SOURCE_INFO_BAILING_OUT: Generates print output before entering an infinite
 loop when an unrecoverable error occurs.
 - ::SOURCE_INFO_SNIPPET_START: Prints a distinguished debug output snippet
 header line.
 - ::SOURCE_INFO_SNIPPET_END: Prints a distinguished debug output snippet footer
 line.
 
 
\section si-usage Using the SourceInfo Library

Client programs that want to use this library should include the header file
as shown in the following snippet:

\code
 #include "SourceInfo.h"
\endcode

 
 \section si-arduino-library Arduino Library

 The SourceInfo library is available as pre-packed Arduino library
 [SourceInfo-arduino-library.zip](../SourceInfo-arduino-library/SourceInfo-arduino-library.zip). Download
 this zip archive and unpack it in the **libraries** directory of your Arduino
 sketchbook. It will create a branch **SourceInfo** in the **libraries** directory.
 This Arduino library comes with one sketch:

 - **SourceInfoDemo.ino** is a demo the uses most of the macros defined in the header file SourceInfo.h.  
 
 */

#include <Arduino.h>

/**
 The version of this file.
 */
#define SOURCE_INFO_VERSION (F("1.0.8"))

/**
 The name of the current source file in the PROGMEM area.
 */
#define SOURCE_INFO_FILE (F(__FILE__))

/**
 The compilation time in the PROGMEM area.
 */
#define SOURCE_INFO_COMPILED_AT (F(__DATE__ " " __TIME__))

/**
 The last modification time of the embedding source file in the PROGMEM area.
 */
#define SOURCE_INFO_MODIFIED_AT (F(__TIMESTAMP__))

/**
 Prints the following source meta data to Serial:
 - The source file name.
 - The last modification time of the source file.
 - The compilation time of the sketch.
 */
#define SOURCE_INFO_PRINT                                                      \
  Serial.print(F("Source file: "));                                            \
  Serial.println(SOURCE_INFO_FILE);                                            \
  Serial.print(F("Modified at: "));                                            \
  Serial.println(SOURCE_INFO_MODIFIED_AT);                                     \
  Serial.print(F("Compiled at: "));                                            \
  Serial.println(SOURCE_INFO_COMPILED_AT);

/**
 Generates debug output at the beginning of a function. The output contains:
 - The name of the function that is specified in the macro argument.
 - The number of the source line that contains the macro call.

 The following snippet shows an example for calling SOURCE_INFO_ENTERING at the
 beginning of a function **foo()**:

 \code
 void foo() {
    SOURCE_INFO_ENTERING("foo")
        :    :    :
 }
 \endcode

 Hint: There is no need for a terminating semicolon, the macro expansion
 provides those statement separators.

 \param fn The name of the function.
 */
#define SOURCE_INFO_ENTERING(fn)                                               \
  Serial.print(F(fn));                                                         \
  Serial.print(F(": entering in line "));                                      \
  Serial.print(__LINE__);                                                      \
  Serial.println(F("..."));

/**
 Generates debug output at the end of a function. The output contains:
 - The name of the function that is specified in the macro argument.
 - The number of the source line that contains the macro call.

 The following snippet shows an example for calling SOURCE_INFO_LEAVING at the
 end of a function **foo()**:

 \code
 void foo() {
         :    :    :
   SOURCE_INFO_LEAVING("foo")
 }
 \endcode

 Hint: There is no need for a terminating semicolon, the macro expansion
 provides those statement separators.

 \param fn The name of the function.
 */
#define SOURCE_INFO_LEAVING(fn)                                                \
  Serial.print(F(fn));                                                         \
  Serial.print(F(": leaving in line "));                                       \
  Serial.print(__LINE__);                                                      \
  Serial.println(F("..."));

 
 /**
 The auxiliary macro SOURCE_INFO_BAILING_OUT_OPS defines code that is run
 in an infinite loop after bailing out (see below). With standard Arduino boards,
 this macro is expanded to an empty statement. For  ESP8266 and  ESP32 based
 boards however, this macro is expanded to a **yield()** statement since with those boards,
 a single core must not claim all CPU cycles.
 */ 
#if defined(ESP8266)
#define SOURCE_INFO_BAILING_OUT_OPS yield();
#elif defined(ESP32)
#define SOURCE_INFO_BAILING_OUT_OPS yield();
#else 
#define SOURCE_INFO_BAILING_OUT_OPS ;
#endif

/**
 Generates debug output before entering an infinite loop. The output contains:
 - The number of the source line that contains the macro call.
 - The reason for the bailing-out procedure that is specified in the macro
 argument.

 The following snippet shows an example for calling SOURCE_INFO_BAILING_OUT
 after a failure to initialize a sensor correctly:

 \code
 if (!sensorWasInitializedSuccessfully()) {
     SOURCE_INFO_BAILING_OUT("Oops: sensor initialization failed !")
 }
 \endcode

 Hint: There is no need for a terminating semicolon, the macro expansion
 provides those statement separators.


 \param reason The reason for bailing out.
 
 */

#define SOURCE_INFO_BAILING_OUT(reason)                                        \
  Serial.print(F("Bailing out at line "));                                     \
  Serial.print(__LINE__);                                                      \
  Serial.println(F(", reason:"));                                              \
  Serial.println(F(reason));                                                   \
  Serial.println(F("Now entering an infinite loop..."));                       \
  while (true) {                                                               \
    SOURCE_INFO_BAILING_OUT_OPS                                                \
  }

/**
 Prints an empty new line character.
 */
#define SOURCE_INFO_NL Serial.println();

/**
 Prefix for SOURCE_INFO_SNIPPET generated lines
 */
#define SOURCE_INFO_SNIPPET_PREFIX "@#@-SOURCE-INFO-SNIPPET-"

/**
 Creates a SOURCE_INFO_SNIPPET start line. The output contains a whitespace
 separated line of:
 - The SOURCE_INFO_SNIPPET_PREFIX and and immediately following literal "START"
 - The name of the snippet that is specified in the macro argument.
 - The name of the source file that contains the macro call.
 - The number of the source line that contains the macro call.

 \param name The name of the snippet.
 */
#define SOURCE_INFO_SNIPPET_START(name)                                        \
  Serial.print(F(SOURCE_INFO_SNIPPET_PREFIX));                                 \
  Serial.print(F("START "));                                                   \
  Serial.print(F(name));                                                       \
  Serial.print(F(" " __FILE__ " "));                                           \
  Serial.print(__LINE__);                                                      \
  Serial.println();

/**
 Creates a SOURCE_INFO_SNIPPET end line. The output contains a whitespace
 separated line of:
 - The SOURCE_INFO_SNIPPET_PREFIX and and immediately following literal "END"
 - The name of the snippet that is specified in the macro argument.
 - The name of the source file that contains the macro call.
 - The number of the source line that contains the macro call.

 \param name The name of the snippet.
 */
#define SOURCE_INFO_SNIPPET_END(name)                                          \
  Serial.print(F(SOURCE_INFO_SNIPPET_PREFIX));                                 \
  Serial.print(F("END "));                                                     \
  Serial.print(F(name));                                                       \
  Serial.print(F(" " __FILE__ " "));                                           \
  Serial.print(__LINE__);                                                      \
  Serial.println();

#endif
