file = new File('../work/grabber.log');
inPbm = false;
lines = file.readLines();
println(lines.size() + ' lines read from file ' + file);
pbmFile = null;
pbmName = '';
pbmText = '';
script = '';

lines.each() { line ->
    if (line.startsWith('@PBM-GRABBER-PROLOGUE')) {
        inPbm = true;
        pbmName = line.tokenize()[1];
        pbmName = pbmName.tokenize('/')[-1];
        pbmName = pbmName.tokenize('.')[0] + ".pbm";
        pbmFile = new File("../work/" + pbmName);
        pbmText = '';
        println('found PBM snippet start, file name: ' + pbmName);
    } else if (line.startsWith('@PBM-GRABBER-EPILOGUE')) {
        inPbm = false;
        pbmFile.text = pbmText;
        println('found PBM snippet end');
        pngName = pbmName.replace('pbm','png');
        script += "pnminvert " + pbmName  + " | pnmscale 1 | pnmtopng > " + pngName + '\n' ; 
     } else {
        if (inPbm) {
            pbmText += line.trim() + '\n';
        }
    }
}

shFile = new File("../work/conv.sh");
shFile.text = script;