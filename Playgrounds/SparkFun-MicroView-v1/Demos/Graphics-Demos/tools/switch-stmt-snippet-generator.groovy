rawHeaderFile = new File("../lib/demos/src/DemoKey.h");
println("raw header file : " + rawHeaderFile);
rawHeaderLines = rawHeaderFile.readLines();
println("read " + rawHeaderLines.size() + " lines");

tokens = [];
headerLines = [];
rawHeaderLines.forEach() { line ->
  trimmedLine = line.trim();
  if (trimmedLine.startsWith("DEMO_")) {
     headerLines.add(trimmedLine.tokenize(" =,")[0]);
  }
}
headerLineCount = headerLines.size();
println(headerLineCount + " header lines: " + headerLines);

generatedTextHeader = "// BEGIN OF GENERATED SNIPPET\n" +
                      "// Caution: This snippet was written by a generator.\n" +
                      "// Do not edit manually; instead, re-run the generator.\n" +
                      "// Generation timestamp: " + new Date() + "\n";
generatedTextFooter = "// END OF GENERATED SNIPPET\n";
 
// generate mapping to function names
functionNames = [:];
for (i=0; i<headerLineCount; i++) {
  key = headerLines[i];
  switch (key) {
  case "DEMO_INVALID":
  case "DEMO_DEFAULT":
  case "DEMO_SPECIAL":
    break;
  default:
    tokens = key.toLowerCase().tokenize("_");
    tokenCount = tokens.size();
    functionName = tokens[0];
    for (k=1; k<tokenCount; k++) {
      functionName += tokens[k].capitalize();
    }
    functionNames.put(key,functionName);
    break;
  }
}
println("function name mapping: " + functionNames);

// generate char2DemoKey switch statement
generatedText = generatedTextHeader;
generatedText += "switch (key) {\n";
for (i=0; i<headerLineCount; i++) {
  key = headerLines[i];
  switch (key) {
  case "DEMO_INVALID":
  case "DEMO_DEFAULT":
    break;
  default:
    generatedText += "case " + key + ":\n";
    generatedText += "  return " + key + ";\n";
  }
}
generatedText += "default:\n";
generatedText += "  return DEMO_INVALID;\n";
generatedText += "}\n";
generatedText += generatedTextFooter;
file = new File("../lib/generated/src/char2DemoKey.h");
file.text = generatedText;

 
// generate demoRouting switch statement
generatedText = generatedTextHeader;
generatedText += "switch (key) {\n";
for (i=0; i<headerLineCount; i++) {
  key = headerLines[i];
  switch (key) {
  case "DEMO_INVALID":
  case "DEMO_DEFAULT":
  case "DEMO_SPECIAL":
    break;
  default:
    generatedText += "case " + key + ":\n";
    generatedText += "  ddp = " + functionNames[key];
    generatedText += "();\n";
    generatedText += "  break;\n";
    break;
  }
}
generatedText += "default:\n";
generatedText += "  ddp = NULL;\n";
generatedText += "  break;\n";
generatedText += "}\n";
generatedText += generatedTextFooter;
file = new File("../lib/generated/src/demoRouting.h");
file.text = generatedText;
//println(generatedText);

 
// generate demo functions declaration switch statement
generatedText = generatedTextHeader;
for (i=0; i<headerLineCount; i++) {
  key = headerLines[i];
  switch (key) {
  case "DEMO_INVALID":
  case "DEMO_DEFAULT":
  case "DEMO_SPECIAL":
    break;
  default:
    generatedText += "extern DemoDetails *" + functionNames[key] + "(void);\n";
    break;
  }
}
generatedText += generatedTextFooter;
file = new File("../lib/generated/src/externDemoFunctions.h");
file.text = generatedText;
//println(generatedText);

// generate nextDemoKey switch statement
generatedText = generatedTextHeader;
generatedText += "switch (key) {\n";
for (i=0; i<headerLineCount; i++) {
  key = headerLines[i];
  switch (key) {
  case "DEMO_INVALID":
  case "DEMO_DEFAULT":
  case "DEMO_SPECIAL":
  case "DEMO_LISSAJOUS":
    break;
  default:
    generatedText += "case " + key + ":\n";
    generatedText += "  return " + headerLines[i+1] + ";\n";
  }
}
generatedText += "default:\n";
generatedText += "  return DEMO_INVALID;\n";
generatedText += "}\n";
generatedText += generatedTextFooter;
file = new File("../lib/generated/src/nextDemoKey.h");
file.text = generatedText;
 
// generate previousDemoKey switch statement
generatedText = generatedTextHeader;
generatedText += "switch (key) {\n";
for (i=0; i<headerLineCount; i++) {
  key = headerLines[i];
  switch (key) {
  case "DEMO_INVALID":
  case "DEMO_DEFAULT":
  case "DEMO_SPECIAL":
  case "DEMO_ALL_PIXELS_0":
    break;
  default:
    generatedText += "case " + key + ":\n";
    generatedText += "  return " + headerLines[i-1] + ";\n";
  }
}
generatedText += "default:\n";
generatedText += "  return DEMO_INVALID;\n";
generatedText += "}\n";
generatedText += generatedTextFooter;
file = new File("../lib/generated/src/previousDemoKey.h");
file.text = generatedText;
 
// generate image display source code for a notebook
generatedText = "";
for (i=0; i<headerLineCount; i++) {
  key = headerLines[i];
  switch (key) {
  case "DEMO_INVALID":
  case "DEMO_DEFAULT":
  case "DEMO_SPECIAL":
    break;
  default:
    generatedText += "\n";
    generatedText += "[" + functionNames[key] + ".png](images/" + functionNames[key] + ".png)\n";
    generatedText += "\n";
    generatedText += "![" + functionNames[key] + ".png](images/" + functionNames[key] + ".png)\n";
    generatedText += "\n";
  }
}
file = new File("../work/images.txt");
file.text = generatedText;
 
 
 
                    