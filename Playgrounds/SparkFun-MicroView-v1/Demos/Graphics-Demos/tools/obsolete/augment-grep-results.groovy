
inputFile = args[0];
inputFile = new File(inputFile);
println("input file: " + inputFile);
 
outputFile = args[1];
outputFile = new File(outputFile);
println("output file: " + outputFile);
outputText = "";
 
inputLines = inputFile.readLines();
println("read " + inputLines.size + " input lines.");

inputLines.forEach(){ inputLine ->
  //println(inputLine);
  tokens = inputLine.tokenize();
  srcFile = tokens[0] - ':';
  tokens = srcFile.tokenize('/');
  functionName = tokens[-1] - '.cpp';
  println("function name: " + functionName);
  println("source file: " + srcFile);
  srcFile = new File(srcFile);
  srcLines = srcFile.readLines();
  println("read " + srcLines.size() + " source lines.");
  xfer = false;
  nxfer = 0;
  srcLines.forEach(){ srcLine ->
    trimmedLine = srcLine.trim();
    if (trimmedLine.startsWith('make_demo_details(')) {
      xfer = true;
      outputText += "__FILE__ = '" + functionName + "';\n";
    }
    if (xfer) {
      outputText += srcLine + "\n";
      nxfer++;
    }
    if (xfer && trimmedLine.contains(');')) {
      xfer = false;
    }
  }
  println("transferred " + nxfer + " source lines.");
}

outputFile.text = outputText;