
routingFile = args[0];
routingFile = new File(routingFile);
println("routing file: " + routingFile);
routingText = "// BEGIN OF GENERATED SNIPPET\n" +
              "// Caution: This snippet was written by a generator.\n" +
              "// Do not edit manually; instead, re-run the generator.\n" +
              "// Generation timestamp: " + new Date() + "\n";

checkingFile = args[1];
checkingFile = new File(checkingFile);
println("checking file: " + checkingFile);
checkingText = routingText;

__LINE__ = -1;
def make_demo_details(String fname,int lno, String icon, String  descr,int delay, int nskip, String demoKey) {
    routingText += "case '" + demoKey + "':\n";
    routingText += "  ddp = " + fname + "();\n";
    routingText += "  break;\n";
    checkingText += "case '" + demoKey + "':\n";
}

