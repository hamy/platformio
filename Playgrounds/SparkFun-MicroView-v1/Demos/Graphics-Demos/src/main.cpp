
#include "Demos.h"
#include "Grabber.h"
#include "Screen.h"
#include <Arduino.h>
#include <MicroView.h>
#include <ctype.h>

const uint32_t BAUD_RATE = 115200;

void setupSerial() {
  Serial.begin(BAUD_RATE);
  Serial.print(F("Baud rate: "));
  Serial.println(BAUD_RATE);
}

DemoKey oldDemoKey = DEMO_DEFAULT;
DemoKey newDemoKey;
boolean demoKeyChanged = false;
int framesToSkip = 0;
boolean needsGrab = false;

void setup() {
  setupSerial();
  setupScreen();
}

void processSerialInput() {
  newDemoKey = oldDemoKey;
  while (Serial.available()) {
    char ch = Serial.read();
    if (isValidDemoKey(ch)) {
      newDemoKey = char2DemoKey(ch);
    }
  }
  demoKeyChanged = newDemoKey != oldDemoKey;
  oldDemoKey = newDemoKey;
}

void doSpecial() {
  newDemoKey = DEMO_ALL_PIXELS_0;
  while (newDemoKey != DEMO_INVALID) {
    demoIsStarting = true;
    DemoDetails *ddp = demoRouting(newDemoKey);
    delay(1000);
    int nskip = ddp->numberOfSkippedFrames;
    if (nskip > 0) {
      while (0 < nskip--) {
        ddp = demoRouting(newDemoKey);
        delay(10);
      }
    }
    grabScreenBufferAsAsciiPBM(ddp);
    delay(1000);
    newDemoKey = nextDemoKey(newDemoKey);
  }
  resetScreenAndPaintingAttributes();
  uView.setCursor(0, 0);
  uView.println("DONE !");
  uView.println("PLEASE");
  uView.println("REBOOT");
  uView.display();
  while (true) {
  }
}

void loop() {
  processSerialInput();
  if (newDemoKey == DEMO_SPECIAL) {
    doSpecial();
  }
  if (demoKeyChanged) {
    Serial.print(F("Demo key changed, new key: ")); Serial.println(newDemoKey);
    resetScreenAndPaintingAttributes();
    demoIsStarting = true;
    needsGrab = true;
  }
  DemoDetails *ddp = demoRouting(newDemoKey);
  if (demoKeyChanged) {
    demoKeyChanged = false;
    framesToSkip = ddp->numberOfSkippedFrames;
  }
  if (needsGrab) {
    if (framesToSkip == 0) {
      grabScreenBufferAsAsciiPBM(ddp);
      needsGrab = false;
    } else if (framesToSkip > 0) {
      framesToSkip--;
    }
  }
}
