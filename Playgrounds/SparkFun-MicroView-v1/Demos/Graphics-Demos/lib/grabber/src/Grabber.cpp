#include "Grabber.h"

static void printPGM_P(PGM_P ptr) {
  while (true) {
    char ch = pgm_read_byte(ptr++);
    if (ch) {
      Serial.print(ch);
    } else {
      return;
    }
  }
}

static void printPrologue(DemoDetails *ddp) {
  Serial.println();
  Serial.print(F("@PBM-GRABBER-PROLOGUE "));
  printPGM_P(ddp->fileName);
  Serial.println();
}

static void printPBMHeader(DemoDetails *ddp) {
  Serial.println(F("P1"));
  Serial.println(F("# Playground: SparkFun-MicroView-v1"));
  Serial.println(F("# Sketch: Graphics-Demos"));
  Serial.print(F("# File: "));
  printPGM_P(ddp->fileName);
  Serial.println();
  Serial.print(F("# Icon text: "));
  printPGM_P(ddp->iconText);
  Serial.println();
  Serial.print(F("# Purpose: "));
  printPGM_P(ddp->description);
  Serial.println();
  Serial.print(F("# Demo key: "));
  Serial.print(((char)ddp->demoKey));
  Serial.println();
  Serial.print(F("# Frames skipped before grabbing: "));
  Serial.print(ddp->numberOfSkippedFrames);
  Serial.println();
  Serial.println(F("# Author: Hamy (harald.meyer@kit.edu)"));
  Serial.print(screenWidth);
  Serial.print(F(" "));
  Serial.println(screenHeight);
}

static void printPBMBody() {
  int byteRowCount = screenHeight / 8;

  for (int byteRow = 0; byteRow < byteRowCount; byteRow++) {
    uint8_t *byteRowBuffer = screenBuffer + byteRow * screenWidth;
    for (int bitRow = 0; bitRow < 8; bitRow++) {
      uint8_t mask = 1 << bitRow;
      for (int column = 0; column < screenWidth; column++) {
        if (byteRowBuffer[column] & mask) {
          Serial.print("1");
        } else {
          Serial.print("0");
        }
      }
      Serial.println();
    }
  }
}

static void printEpilogue(DemoDetails *ddp) {
  Serial.print(F("@PBM-GRABBER-EPILOGUE "));
  printPGM_P(ddp->fileName);
  Serial.println();
  Serial.println();
}

void grabScreenBufferAsAsciiPBM(DemoDetails *ddp) {
  printPrologue(ddp);
  printPBMHeader(ddp);
  printPBMBody();
  printEpilogue(ddp);
}
