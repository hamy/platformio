#ifndef __GRABBER_H_INCLUDED__
#define __GRABBER_H_INCLUDED__

#include "Demos.h"
#include "Screen.h"

extern void grabScreenBufferAsAsciiPBM(DemoDetails *ddp);

#endif