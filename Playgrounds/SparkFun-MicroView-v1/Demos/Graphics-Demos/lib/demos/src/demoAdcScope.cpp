
#include "Demos.h"

static DemoDetails demoDetails;

DemoDetails *demoAdcScope()
{
    makeDemoDetails(__FILE__, __LINE__, "ADC",
                    "ADC oscilloscope.", 0, 70, DEMO_ADC_SCOPE);
    resetScreenAndPaintingAttributes();
    if (clearWhenDemoIsStarting())
    {
        memset(demoWorkBuffer, 0, DEMO_WORK_BUFFER_SIZE);
    }
    memcpy(demoWorkBuffer + 1, demoWorkBuffer, screenWidth - 1);
    uint32_t adc = analogRead(0);
    adc *= screenHeight - 2;
    adc /= 1024;
    demoWorkBuffer[0] = adc;
    for (int h = 0; h < screenWidth; h++)
    {
        uView.pixel(h, screenHeight - 1 - demoWorkBuffer[h]);
    }
    return displayAndReturnDemoDetails(&demoDetails);
}
