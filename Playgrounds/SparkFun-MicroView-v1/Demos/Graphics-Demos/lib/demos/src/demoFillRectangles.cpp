
#include "Demos.h"

static DemoDetails demoDetails;

DemoDetails *demoFillRectangles() {
  makeDemoDetails(__FILE__, __LINE__, "Fill Rect.",
                  "Draws a set of filled rectangles.", 500, 0,
                  DEMO_FILL_RECTANGLES);
  resetScreenAndPaintingAttributes();
  int nrect = 20;
  int minSize = 4;
  int maxSize = 10;
  for (int i = 0; i < nrect; i++) {
    int width = random(minSize, maxSize);
    int height = random(minSize, maxSize);
    int x = random(screenWidth - width);
    int y = random(screenHeight - height);
    uView.rectFill(x, y, width, height);
  }
  return displayAndReturnDemoDetails(&demoDetails);
}
