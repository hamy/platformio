
#include "Demos.h"

static DemoDetails demoDetails;

DemoDetails *demoFont8x16() {
  makeDemoDetails(__FILE__, __LINE__, "Font 8x16",
                  "Displays the font #0 (5x7).", 1000, 0, DEMO_FONT_8X16);
  resetScreenAndPaintingAttributes();
  uView.setFontType(0);
  uView.setCursor(0, 0);
  uView.print(F("ft.#1:8x16"));
  uView.setFontType(1);
  char text[8];

  for (int y = 12; y < 40; y += 16) {
    uView.setCursor(0, y);
    memset(text, 0, sizeof text);
    for (int x = 0; x < 7; x++) {
      text[x] = randomChar();
    }
    uView.print(text);
  }
  uView.display();
  return displayAndReturnDemoDetails(&demoDetails);
}