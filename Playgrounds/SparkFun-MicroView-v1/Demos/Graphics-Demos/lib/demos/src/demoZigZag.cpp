
#include "Demos.h"

static DemoDetails demoDetails;
static int x = 0;
static int y = 0;
static boolean horizontal = true;
const int dist = 4;

DemoDetails *demoZigZag() {
  makeDemoDetails(__FILE__, __LINE__, "Zig-Zag", "Creates a Zig-Zag pattern.",
                  30, 80, DEMO_ZIG_ZAG);
  resetPaintingAttributes();
  if (clearWhenDemoIsStarting()) {
    x = 0;
    y = 0;
    horizontal = true;
  }
  if (x == 0 && y == 0) {
    resetScreenAndPaintingAttributes();
  }
  if (horizontal) {
    int xx = x + dist;
    if (xx >= screenWidth) {
      uView.line(x, y, screenWidth - 1, y);
      x = 0;
    } else {
      uView.line(x, y, xx, y);
      x = xx;
    }
  } else {
    int yy = y + dist;
    if (yy >= screenHeight) {
      uView.line(x, y, x, screenHeight - 1);
      y = 0;
    } else {
      uView.line(x, y, x, yy);
      y = yy;
    }
  }
  horizontal = !horizontal;
  return displayAndReturnDemoDetails(&demoDetails);
}
