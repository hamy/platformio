#include "Demos.h"

boolean demoIsStarting = true;

DemoDetails *displayAndReturnDemoDetails(DemoDetails *ddp)
{
  uView.display();
  if (ddp->delayInMillis > 0)
  {
    delay(ddp->delayInMillis);
  }
  demoIsStarting = false;
  return ddp;
}

boolean clearWhenDemoIsStarting()
{
  if (demoIsStarting)
  {
    resetScreenAndPaintingAttributes();
  }
  return demoIsStarting;
}

boolean isValidDemoKey(char key)
{
  return (char2DemoKey(key) == DEMO_INVALID) ? false : true;
}

DemoKey nextDemoKey(DemoKey key){
#include "nextDemoKey.h"
}

DemoKey previousDemoKey(DemoKey key){
#include "previousDemoKey.h"
}

DemoKey char2DemoKey(char key)
{
  if (key >= 'a' && key <= 'z')
  {
    key -= 'a' - 'A';
  }
#include "char2DemoKey.h"
}

DemoDetails *demoRouting(DemoKey key)
{
  //Serial.print(F("demoRouting: key="));
  //Serial.println((char)key);
  DemoDetails *ddp;
#include "demoRouting.h"
  return ddp;
}

uint8_t demoWorkBuffer[DEMO_WORK_BUFFER_SIZE];
