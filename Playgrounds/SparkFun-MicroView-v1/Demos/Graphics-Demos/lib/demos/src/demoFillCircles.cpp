
#include "Demos.h"

static DemoDetails demoDetails;

DemoDetails *demoFillCircles() {
  makeDemoDetails(__FILE__, __LINE__, "Circles", "Draws a set of circles.", 500,
                  0, DEMO_FILL_CIRCLES);
  resetScreenAndPaintingAttributes();
  int ncirc = 4;
  int minSize = 4;
  int maxSize = 10;
  for (int i = 0; i < ncirc; i++) {
    int radius = random(minSize, maxSize);
    int x = radius + random(screenWidth - 2 * radius);
    int y = radius + random(screenHeight - 2 * radius);
    uView.circleFill(x, y, radius);
  }
  return displayAndReturnDemoDetails(&demoDetails);
}
