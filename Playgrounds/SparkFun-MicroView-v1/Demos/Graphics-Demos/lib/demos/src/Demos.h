#ifndef __DEMOS_H_INCLUDED__
#define __DEMOS_H_INCLUDED__

#include "DemoKey.h"
#include "util.h"
#include "Screen.h"
#include <Arduino.h>
#include <MicroView.h>

extern DemoKey nextDemoKey(DemoKey key);
extern DemoKey previousDemoKey(DemoKey key);
extern boolean demoIsStarting;
extern boolean clearWhenDemoIsStarting();

typedef struct
{
  PGM_P fileName;
  int lineNumber;
  PGM_P iconText;
  PGM_P description;
  int delayInMillis;
  int numberOfSkippedFrames;
  DemoKey demoKey;
} DemoDetails;

#define makeDemoDetails(fname, lineno, icon, descr, msec, nskip, key) \
  demoDetails.fileName = PSTR(fname);                                 \
  demoDetails.lineNumber = lineno;                                    \
  demoDetails.iconText = PSTR(icon);                                  \
  demoDetails.description = PSTR(descr);                              \
  demoDetails.delayInMillis = msec;                                   \
  demoDetails.numberOfSkippedFrames = nskip;                          \
  demoDetails.demoKey = key;

extern DemoDetails *displayAndReturnDemoDetails(DemoDetails *ddp);

extern boolean isValidDemoKey(char key);
extern DemoKey char2DemoKey(char key);
extern DemoDetails *demoRouting(DemoKey key);

#define DEMO_WORK_BUFFER_SIZE 100
extern uint8_t  demoWorkBuffer[];

#include "externDemoFunctions.h"

#endif
