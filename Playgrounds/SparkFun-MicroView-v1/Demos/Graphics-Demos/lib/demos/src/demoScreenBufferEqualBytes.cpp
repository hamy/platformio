
#include "Demos.h"

static DemoDetails demoDetails;

static uint8_t value = 100;

DemoDetails *demoScreenBufferEqualBytes() {
  makeDemoDetails(__FILE__, __LINE__, "Buffer=", "Equal buffer byte values.",
                    200, 0, DEMO_SCREEN_BUFFER_EQUAL_BYTES);
  uView.clear(PAGE, value++);
  return displayAndReturnDemoDetails(&demoDetails);
}
