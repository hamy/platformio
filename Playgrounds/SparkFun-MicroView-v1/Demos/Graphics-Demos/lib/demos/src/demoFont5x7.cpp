
#include "Demos.h"

static DemoDetails demoDetails;

extern DemoDetails *demoFont5x7() {
  makeDemoDetails(__FILE__, __LINE__, "Font 5x7", "Displays the font #0 (5x7).",
                  1000, 0, DEMO_FONT_5X7);
  resetScreenAndPaintingAttributes();
  uView.setFontType(0);
  uView.setCursor(0, 0);
  uView.print(F("font#0:5x7"));
  char text[11];

  for (int y = 9; y < 40; y += 9) {
    uView.setCursor(0, y);
    memset(text, 0, sizeof text);
    for (int x = 0; x < 10; x++) {
      text[x] = randomChar();
    }
    uView.print(text);
  }
  uView.display();
  return displayAndReturnDemoDetails(&demoDetails);
}