
#include "Demos.h"

static DemoDetails demoDetails;

DemoDetails *demoScreenBufferRandomBytes() {
  makeDemoDetails(__FILE__, __LINE__, "Buffer rnd.", "Creates a random bitmap.",
                  0, 0, DEMO_SCREEN_BUFFER_RANDOM_BYTES);
  resetScreenAndPaintingAttributes();
  for (int i = 0; i < screenBufferSize; i++) {
    screenBuffer[i] = random(0, 255);
  }
  return displayAndReturnDemoDetails(&demoDetails);
}
