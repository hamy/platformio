
#include "Demos.h"

static DemoDetails demoDetails;
static int oldx = 0;
static int oldy = 0;

DemoDetails *demoRandomWalk() {
  makeDemoDetails(__FILE__, __LINE__, "Rnd. walk", "Simulates a random walk.",
                  0, 40, DEMO_RANDOM_WALK);
  resetPaintingAttributes();
  if (clearWhenDemoIsStarting()) {
    oldx = screenWidth / 2;
    oldy = screenHeight / 2;
  }
  int dx = random(7) - 3;
  int dy = random(7) - 3;
  int x = oldx + dx;
  int y = oldy + dy;
  if (x < 0 || y < 0 || x >= screenWidth || y >= screenHeight) {
    resetScreenAndPaintingAttributes();
    oldx = screenWidth / 2;
    oldy = screenHeight / 2;
  } else {
    uView.line(oldx, oldy, x, y);
    oldx = x;
    oldy = y;
  }
  uView.display();
  return displayAndReturnDemoDetails(&demoDetails);
}
