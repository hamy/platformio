
#include "Demos.h"

static DemoDetails demoDetails;

DemoDetails *demoFont7segment() {
  makeDemoDetails(__FILE__, __LINE__, "Font 8x16",
                  "Displays the font #0 (5x7).", 1000, 0, DEMO_FONT_7SEGMENT);
  resetScreenAndPaintingAttributes();
  uView.setFontType(0);
  uView.setCursor(0, 0);
  uView.print(F("ft.#2:7seg"));
  uView.setFontType(2);
  char text[6];

  for (int y = 12; y < 40; y += 18) {
    uView.setCursor(0, y);
    memset(text, 0, sizeof text);
    for (int x = 0; x < 5; x++) {
      text[x] = '0' + random(10);
    }
    uView.print(text);
  }
  uView.display();
  return displayAndReturnDemoDetails(&demoDetails);
}
