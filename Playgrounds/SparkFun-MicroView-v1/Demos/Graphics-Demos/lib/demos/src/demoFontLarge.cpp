
#include "Demos.h"

static DemoDetails demoDetails;

DemoDetails *demoFontLarge() {
  makeDemoDetails(__FILE__, __LINE__, "Large Font",
                    "Displays the font #3 (large font).", 1500, 0,
                    DEMO_FONT_LARGE);
  resetScreenAndPaintingAttributes();
  uView.setFontType(0);
  uView.setCursor(0, 0);
  uView.print(F("font#3:big"));
  uView.display();
  delay(500);
  resetScreenAndPaintingAttributes();
  uView.setFontType(3);
  char text[6];
  uView.setCursor(0, 0);
  memset(text, 0, sizeof text);
  for (int x = 0; x < 5; x++) {
    text[x] = '0' + random(10);
  }
  uView.print(text);
  return displayAndReturnDemoDetails(&demoDetails);
}
