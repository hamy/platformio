
#include "Demos.h"

static DemoDetails demoDetails;
static int fx = 2;
static int fy = 3;
static float px = 0.0;
static float py = 0.0;
static int count = 0;

DemoDetails *demoLissajous() {
  makeDemoDetails(__FILE__, __LINE__, "Lissajous",
                    "Creates a Lissajous pattern.", 0, 10, DEMO_LISSAJOUS);
  resetScreenAndPaintingAttributes();
  if (clearWhenDemoIsStarting() || count > 50) {
    fx = random(3, 6);
    fy = random(7, 9);
    px = 0.002 * PI * random(1000);
    py = 0.002 * PI * random(1000);
    count = 0;
  }
  int ndots = 500;
  float stepSize = 2.0 * PI / ndots;
  int oldx = 0;
  int oldy = 0;
  float t = 0;
  for (int i = 0; i <= ndots; i++) {
    int x = 32 + int(30.0 * sin(t * fx + px));
    int y = 24 + int(22.0 * sin(t * fy + py));
    t += stepSize;
    if (i > 0) {
      uView.line(oldx, oldy, x, y);
    }
    oldx = x;
    oldy = y;
  }
  px += 0.222 * PI;
  py += 0.267 * PI;
  count++;
  return displayAndReturnDemoDetails(&demoDetails);
}
