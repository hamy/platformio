
#include "Demos.h"

static DemoDetails demoDetails;

DemoDetails *demoAllPixels0() {
  makeDemoDetails(__FILE__, __LINE__, "All bits 0",
                  "Clears all bits in the page buffer.", 100, 0,
                  DEMO_ALL_PIXELS_0);
  resetScreenAndPaintingAttributes();
  return displayAndReturnDemoDetails(&demoDetails);
}
