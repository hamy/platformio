
#include "Demos.h"

static DemoDetails demoDetails;
static uint8_t start = 1;
static uint8_t stop = 1;
static uint8_t dx = 0;

DemoDetails *demoScrollH() {
  makeDemoDetails(__FILE__, __LINE__, "Scroll H",
                  "Scrolls in horizontal direction.", 20, 40, DEMO_SCROLL_H);
  resetScreenAndPaintingAttributes();
  for (int y = 0; y < 6; y++) {
    if (y >= start && y <= stop) {
      uView.setCursor(dx, y * 8);
    } else {
      uView.setCursor(0, y * 8);
    }
    uView.print(F("#"));
    uView.print(y);
  }
  dx++;
  if (dx > 55) {
    dx = 0;
    stop++;
  }
  if (stop > 4) {
    stop = 1;
  }
  return displayAndReturnDemoDetails(&demoDetails);
}
