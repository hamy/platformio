
#include "Demos.h"

static DemoDetails demoDetails;

DemoDetails *demoScreenBufferIncrBytes() {
  makeDemoDetails(__FILE__, __LINE__, "Buffer incr.",
                  "Fills the buffer with ascending bytes.", 0, 0,
                  DEMO_SCREEN_BUFFER_INCR_BYTES);
  resetScreenAndPaintingAttributes();
  uint8_t byteValue = 0;
  for (int i = 0; i < screenBufferSize; i++) {
    screenBuffer[i] = byteValue++;
  }
  return displayAndReturnDemoDetails(&demoDetails);
}
