
#include "Demos.h"

static DemoDetails demoDetails;
static int angle = 0;

DemoDetails *demoLines() {
  makeDemoDetails(__FILE__, __LINE__, "Lines", "Paints lines.", 50, 30,
                  DEMO_LINES);
  resetPaintingAttributes();
  if (clearWhenDemoIsStarting()) {
    angle = 0;
  }
  if (angle > 360) {
    angle = 0;
    resetScreenAndPaintingAttributes();
  }
  float radian = angle * PI / 180.0;
  int x = int(screenWidth / 2 + 22.0 * cos(radian));
  int y = int(screenHeight / 2 - 22.0 * sin(radian));
  uView.line(screenWidth / 2, screenHeight / 2, x, y);
  angle += 15;
  return displayAndReturnDemoDetails(&demoDetails);
}
