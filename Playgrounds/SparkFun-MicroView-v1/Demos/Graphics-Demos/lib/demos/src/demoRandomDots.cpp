
#include "Demos.h"

static DemoDetails demoDetails;
static int maxDots = 1200;
static int nDots = 0;

DemoDetails *demoRandomDots() {
  makeDemoDetails(__FILE__, __LINE__, "Rnd. dots", "Generates random dots.", 3,
                  900, DEMO_RANDOM_DOTS);
  resetPaintingAttributes();
  if (clearWhenDemoIsStarting()) {
    nDots = 0;
  }
  if (++nDots > maxDots) {
    nDots = 0;
    resetScreenAndPaintingAttributes();
  }
  int x = random(screenWidth);
  int y = random(screenHeight);
  uView.pixel(x, y);
  return displayAndReturnDemoDetails(&demoDetails);
}
