
#include "Demos.h"

static DemoDetails demoDetails;

DemoDetails *demoAllPixels1() {
  makeDemoDetails(__FILE__, __LINE__, "All bits 1",
                  "Raises all bits in the page buffer.", 100, 0,
                  DEMO_ALL_PIXELS_1);
  resetScreenAndPaintingAttributes();
  uView.clear(PAGE, 255);
  return displayAndReturnDemoDetails(&demoDetails);
}
