
#include "Demos.h"

static DemoDetails demoDetails;

DemoDetails *demoChessboardUltraFine() {
  makeDemoDetails(__FILE__, __LINE__, "Chessb. ultra", "Ultra-fine chessboard.",
                  0, 0, DEMO_CHESSBOARD_ULTRA_FINE);
  resetScreenAndPaintingAttributes();
  for (int i = 0; i < screenBufferSize; i++) {
    uint8_t value = (i & 1) ? 0b01010101 : 0b10101010;
    screenBuffer[i] = value;
  }
  return displayAndReturnDemoDetails(&demoDetails);
}
