
#include "Demos.h"

static DemoDetails demoDetails;
static float sigma = 0.2f;

DemoDetails *demoGaussian() {
  makeDemoDetails(__FILE__, __LINE__, "Gaussian",
                    "Simulates a decaying gaussian.", 0, 10, DEMO_GAUSSIAN);
  resetScreenAndPaintingAttributes();
  if (clearWhenDemoIsStarting()) {
    sigma = 0.44f;
  }
  for (int h = 0; h < screenWidth; h++) {
    float x = (h - screenWidth / 2) / 5.0f;
    float twoSigma2 = 2.0f * sigma * sigma;
    float arg = -x * x / twoSigma2;
    float y = 1.0f / sqrt(PI * twoSigma2) * exp(arg);
    int v = int(y * 47.0f);
    uView.lineV(h, screenHeight - 1 - v, v + 1);
  }
  uView.display();
  sigma += 0.03f;
  if (sigma > 5.0f) {
    sigma = 0.44f;
  }
  return displayAndReturnDemoDetails(&demoDetails);
}
