
#include "Demos.h"

static DemoDetails demoDetails;

DemoDetails *demoDrawRectangles() {
  makeDemoDetails(__FILE__, __LINE__, "Rectangles",
                  "Draws a set of rectangles.", 500, 0, DEMO_DRAW_RECTANGLES);
  resetScreenAndPaintingAttributes();
  int nrect = 40;
  int minSize = 4;
  int maxSize = 10;
  for (int i = 0; i < nrect; i++) {
    int width = random(minSize, maxSize);
    int height = random(minSize, maxSize);
    int x = random(screenWidth - width);
    int y = random(screenHeight - height);
    uView.rect(x, y, width, height);
  }
  return displayAndReturnDemoDetails(&demoDetails);
}
