
#include "Demos.h"

static DemoDetails demoDetails;

DemoDetails *demoDrawCircles() {
  makeDemoDetails(__FILE__, __LINE__, "Circles", "Draws a set of circles.", 500,
                  0, DEMO_DRAW_CIRCLES);
  resetScreenAndPaintingAttributes();
  int ncirc = 25;
  int minSize = 4;
  int maxSize = 10;
  for (int i = 0; i < ncirc; i++) {
    int radius = random(minSize, maxSize);
    int x = radius + random(screenWidth - 2 * radius);
    int y = radius + random(screenHeight - 2 * radius);
    uView.circle(x, y, radius);
  }
  return displayAndReturnDemoDetails(&demoDetails);
}
