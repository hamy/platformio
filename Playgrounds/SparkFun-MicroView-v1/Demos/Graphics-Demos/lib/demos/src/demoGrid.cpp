
#include "Demos.h"

static DemoDetails demoDetails;
static int meshSize = 2;

DemoDetails *demoGrid() {
  makeDemoDetails(__FILE__, __LINE__, "Grid",
                  "Paints grids with changing mesh sizes.", 500, 2, DEMO_GRID);
  resetScreenAndPaintingAttributes();
  if (clearWhenDemoIsStarting()) {
    meshSize = 2;
  }
  for (int x = 0; x < screenWidth; x += meshSize) {
    uView.line(x, 0, x, screenHeight - 1);
  }
  for (int y = 0; y < screenHeight; y += meshSize) {
    uView.line(0, y, screenWidth - 1, y);
  }
  meshSize++;
  if (meshSize > screenHeight / 3) {
    meshSize = 2;
  }
  return displayAndReturnDemoDetails(&demoDetails);
}
