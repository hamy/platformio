
#include "Demos.h"

static DemoDetails demoDetails;

DemoDetails *demoChessboardFine() {
  makeDemoDetails(__FILE__, __LINE__, "Chessb. fine", "Fine chessboard.", 0, 0,
                  DEMO_CHESSBOARD_FINE);
  resetScreenAndPaintingAttributes();
  for (int i = 0; i < screenBufferSize; i++) {
    uint8_t value = (i & 2) ? 0b00110011 : 0b11001100;
    screenBuffer[i] = value;
  }
  return displayAndReturnDemoDetails(&demoDetails);
}
