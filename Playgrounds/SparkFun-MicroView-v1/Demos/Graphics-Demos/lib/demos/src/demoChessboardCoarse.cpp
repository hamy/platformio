
#include "Demos.h"

static DemoDetails demoDetails;

DemoDetails *demoChessboardCoarse() {
  makeDemoDetails(__FILE__, __LINE__, "Chessb. coarse", "Coarse chessboard.", 0,
                  0, DEMO_CHESSBOARD_COARSE);
  resetScreenAndPaintingAttributes();
  for (int i = 0; i < screenBufferSize; i++) {
    uint8_t value = (i & 4) ? 0b00001111 : 0b11110000;
    screenBuffer[i] = value;
  }
  return displayAndReturnDemoDetails(&demoDetails);
}
