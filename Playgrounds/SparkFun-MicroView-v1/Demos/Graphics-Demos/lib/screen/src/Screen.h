#ifndef __SCREEN_H_INCLUDED__
#define __SCREEN_H_INCLUDED__

#include <Arduino.h>
#include <MicroView.h>

extern int screenWidth;
extern int screenHeight;
extern uint8_t *screenBuffer;
extern int screenBufferSize;
extern void setupScreen(void);

extern void resetPaintingAttributes(void);
extern void resetScreenAndPaintingAttributes(void);

#endif