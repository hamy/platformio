
#include "Screen.h"

int screenWidth;
int screenHeight;
uint8_t *screenBuffer;
int screenBufferSize;
void setupScreen(void);

void setupScreen() {
  uView.begin();
  uView.clear(PAGE);
  uView.display();
  Serial.println(F(__FILE__));
  screenWidth = uView.getLCDWidth();
  Serial.print(F("Screen width: "));
  Serial.println(screenWidth);
  screenHeight = uView.getLCDHeight();
  Serial.print(F("Screen height: "));
  Serial.println(screenHeight);
  screenBuffer = uView.getScreenBuffer();
  Serial.print(F("Screen buffer: 0x"));
  Serial.println((int)screenBuffer, HEX);
  screenBufferSize = (screenWidth * screenHeight) / 8;
  Serial.print(F("Screen buffer size: "));
  Serial.println(screenBufferSize);
}

void resetPaintingAttributes() {
  uView.setColor(WHITE);
  uView.setDrawMode(NORM);
  uView.setFontType(0);
}

void resetScreenAndPaintingAttributes() {
  uView.clear(PAGE);
  resetPaintingAttributes();
}
