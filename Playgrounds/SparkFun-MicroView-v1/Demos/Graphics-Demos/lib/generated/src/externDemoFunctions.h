// BEGIN OF GENERATED SNIPPET
// Caution: This snippet was written by a generator.
// Do not edit manually; instead, re-run the generator.
// Generation timestamp: Thu Jul 16 08:49:52 CEST 2020
extern DemoDetails *demoAllPixels0(void);
extern DemoDetails *demoAllPixels1(void);
extern DemoDetails *demoScreenBufferEqualBytes(void);
extern DemoDetails *demoScreenBufferIncrBytes(void);
extern DemoDetails *demoScreenBufferRandomBytes(void);
extern DemoDetails *demoChessboardUltraFine(void);
extern DemoDetails *demoChessboardFine(void);
extern DemoDetails *demoChessboardCoarse(void);
extern DemoDetails *demoGrid(void);
extern DemoDetails *demoLines(void);
extern DemoDetails *demoDrawRectangles(void);
extern DemoDetails *demoFillRectangles(void);
extern DemoDetails *demoDrawCircles(void);
extern DemoDetails *demoFillCircles(void);
extern DemoDetails *demoFont5x7(void);
extern DemoDetails *demoFont8x16(void);
extern DemoDetails *demoFont7segment(void);
extern DemoDetails *demoFontLarge(void);
extern DemoDetails *demoScrollH(void);
extern DemoDetails *demoRandomDots(void);
extern DemoDetails *demoRandomWalk(void);
extern DemoDetails *demoZigZag(void);
extern DemoDetails *demoGaussian(void);
extern DemoDetails *demoAdcScope(void);
extern DemoDetails *demoLissajous(void);
// END OF GENERATED SNIPPET
