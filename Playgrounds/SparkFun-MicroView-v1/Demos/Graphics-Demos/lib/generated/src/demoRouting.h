// BEGIN OF GENERATED SNIPPET
// Caution: This snippet was written by a generator.
// Do not edit manually; instead, re-run the generator.
// Generation timestamp: Thu Jul 16 08:49:52 CEST 2020
switch (key) {
case DEMO_ALL_PIXELS_0:
  ddp = demoAllPixels0();
  break;
case DEMO_ALL_PIXELS_1:
  ddp = demoAllPixels1();
  break;
case DEMO_SCREEN_BUFFER_EQUAL_BYTES:
  ddp = demoScreenBufferEqualBytes();
  break;
case DEMO_SCREEN_BUFFER_INCR_BYTES:
  ddp = demoScreenBufferIncrBytes();
  break;
case DEMO_SCREEN_BUFFER_RANDOM_BYTES:
  ddp = demoScreenBufferRandomBytes();
  break;
case DEMO_CHESSBOARD_ULTRA_FINE:
  ddp = demoChessboardUltraFine();
  break;
case DEMO_CHESSBOARD_FINE:
  ddp = demoChessboardFine();
  break;
case DEMO_CHESSBOARD_COARSE:
  ddp = demoChessboardCoarse();
  break;
case DEMO_GRID:
  ddp = demoGrid();
  break;
case DEMO_LINES:
  ddp = demoLines();
  break;
case DEMO_DRAW_RECTANGLES:
  ddp = demoDrawRectangles();
  break;
case DEMO_FILL_RECTANGLES:
  ddp = demoFillRectangles();
  break;
case DEMO_DRAW_CIRCLES:
  ddp = demoDrawCircles();
  break;
case DEMO_FILL_CIRCLES:
  ddp = demoFillCircles();
  break;
case DEMO_FONT_5X7:
  ddp = demoFont5x7();
  break;
case DEMO_FONT_8X16:
  ddp = demoFont8x16();
  break;
case DEMO_FONT_7SEGMENT:
  ddp = demoFont7segment();
  break;
case DEMO_FONT_LARGE:
  ddp = demoFontLarge();
  break;
case DEMO_SCROLL_H:
  ddp = demoScrollH();
  break;
case DEMO_RANDOM_DOTS:
  ddp = demoRandomDots();
  break;
case DEMO_RANDOM_WALK:
  ddp = demoRandomWalk();
  break;
case DEMO_ZIG_ZAG:
  ddp = demoZigZag();
  break;
case DEMO_GAUSSIAN:
  ddp = demoGaussian();
  break;
case DEMO_ADC_SCOPE:
  ddp = demoAdcScope();
  break;
case DEMO_LISSAJOUS:
  ddp = demoLissajous();
  break;
default:
  ddp = NULL;
  break;
}
// END OF GENERATED SNIPPET
