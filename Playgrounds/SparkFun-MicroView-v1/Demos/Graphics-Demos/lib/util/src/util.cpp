#include "util.h"
#include <Arduino.h>

char randomChar() {
  int max = 10 + 26 + 26 + 2;
  int i = random(max);
  if (i < 10) {
    return '0' + i;
  } else if (i < 36) {
    return 'A' + i - 10;
  } else if (i < 62) {
    return 'a' + i - 36;
  } else {
    return ' ';
  }
}
