#include <Arduino.h>

void setup()
{
    pinMode(LED_BUILTIN, OUTPUT);
    Serial.begin(9600);
    Serial.println(F("Sketch \"Getting-Started\" starts..."));
    Serial.print(F("Builtin LED: "));
    Serial.println(LED_BUILTIN);
    uint64_t chipid = ESP.getEfuseMac();
    Serial.printf("ESP32 Chip ID = %04X", (uint16_t)(chipid >> 32)); //print High 2 bytes
    Serial.printf("%08X\n", (uint32_t)chipid);                       //print Low 4bytes.
}

void loop()
{
    digitalWrite(LED_BUILTIN, LOW);
    delay(900);
    digitalWrite(LED_BUILTIN, HIGH);
    delay(100);
}
