
#include <Arduino.h>
#include <SPI.h>
#include <TFT_eSPI.h>

TFT_eSPI tft = TFT_eSPI();

const int TOUCH_PIN = 27;

void setup(void) {
  Serial.begin(115200);
  tft.init();
  tft.setRotation(0);
  tft.fillScreen(TFT_BLACK);
}

int timeSeries[TFT_WIDTH];

void shiftTimeSeries() {
  for (int x = 0; x < TFT_WIDTH - 1; x++) {
    timeSeries[x] = timeSeries[x + 1];
  }
}

void convertTouchValue() {
  long scaled = touchRead(TOUCH_PIN);
  Serial.println(scaled);
  scaled *= TFT_HEIGHT;
  scaled /= 255;
  timeSeries[TFT_WIDTH - 1] = scaled;
}

void plotTimeSeries() {
  for (int x = 0; x < TFT_WIDTH; x++) {
    int y = timeSeries[x];
    tft.drawFastVLine(x, 0, TFT_HEIGHT - y, TFT_BLACK);
    tft.drawFastVLine(x, TFT_HEIGHT - y - 1, TFT_HEIGHT - 1, TFT_YELLOW);
  }
}

void waitForNextLoop() { delay(5); }

void loop() {
  shiftTimeSeries();
  convertTouchValue();
  plotTimeSeries();
  waitForNextLoop();
}
