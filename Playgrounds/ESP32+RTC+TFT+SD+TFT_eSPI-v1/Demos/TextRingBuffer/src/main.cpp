
#include <Arduino.h>
#include <SPI.h>
#include <TFT_eSPI.h>

TFT_eSPI tft = TFT_eSPI();

const int TOUCH_PIN = 27;
const int FONT_ID = 1;
const int CELL_NX = 36;
const int CELL_NY = 20;
const int CELL_HEIGHT = 8;

char textArray[CELL_NY][CELL_NX + 1];
uint16_t colorArray[CELL_NY];
int yTop = 0;

void clearBuffer() {
  memset(textArray, 0, CELL_NY * (CELL_NX + 1));
  for (int i = 0; i < CELL_NY; i++) {
    colorArray[i] = TFT_BLACK;
  }
  yTop = 0;
}

void appendText(char *text, uint16_t color) {
  yTop++;
  if (yTop >= CELL_NY) {
    yTop = 0;
  }
  memset(textArray[yTop], 0, CELL_NX + 1);
  int len = strlen(text);
  if (len > CELL_NX) {
    len = CELL_NX;
  }
  memcpy(textArray[yTop], text, len);
  colorArray[yTop] = color;
}

void drawText() {
  tft.fillScreen(TFT_BLACK);
  int yText = yTop;
  int yDraw = 0;
  for (int row = 0; row < CELL_NY; row++) {
    tft.setTextColor(colorArray[yText], TFT_BLACK);
    tft.drawString(textArray[yText], 0, yDraw, FONT_ID);
    yDraw += CELL_HEIGHT;
    yText--;
    if (yText < 0) {
      yText = CELL_NY - 1;
    }
  }
}

void setup(void) {
  Serial.begin(115200);
  tft.init();
  tft.setRotation(0);
  tft.fillScreen(TFT_BLACK);
  clearBuffer();
}

int loopCount = 0;

void loop() {
  char text[80];
  loopCount++;
  int rnd = random(1000);
  int ms = millis();
  sprintf(text, "%d %d %d", loopCount, rnd, ms);
  int color = TFT_WHITE;
  switch (random(6)) {
  case 0:
    color = TFT_RED;
    break;
  case 1:
    color = TFT_GREEN;
    break;
  case 2:
    color = TFT_BLUE;
    break;
  case 3:
    color = TFT_CYAN;
    break;
  case 4:
    color = TFT_MAGENTA;
    break;
  case 5:
    color = TFT_YELLOW;
    break;
  }
  appendText(text, color);
  drawText();
  delay(1000);
}
