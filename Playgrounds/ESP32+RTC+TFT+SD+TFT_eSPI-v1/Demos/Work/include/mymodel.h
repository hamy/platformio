#ifndef __MY_MODEL_H_INCLUDED__
#define __MY_MODEL_H_INCLUDED__

class MyModel
{
public:
    MyModel(int num);
    int hello();

private:
    int _num;
};

extern MyModel myModel;

#endif