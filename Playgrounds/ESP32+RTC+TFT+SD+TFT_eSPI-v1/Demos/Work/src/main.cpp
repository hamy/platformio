
#include <Arduino.h>
#include <SPI.h>
#include <SparkFun_APDS9960.h>
#include <TFT_eSPI.h>
#include <mymodel.h>

TFT_eSPI tft = TFT_eSPI();
MyModel myModel(42);

#define BAUD_RATE 115200

#define SENSOR_MODE_LIGHT 0
#define SENSOR_MODE_PROXIMITY 1
#define SENSOR_MODE_GESTURE 2
#define SENSOR_MODE_MIN SENSOR_MODE_LIGHT
#define SENSOR_MODE_MAX SENSOR_MODE_GESTURE
int sensorMode = SENSOR_MODE_MIN;

SparkFun_APDS9960 apds = SparkFun_APDS9960();
boolean allModesDisabled = true;

void disableAllModes() { allModesDisabled = true; }

void enableOneMode()
{
  if (allModesDisabled)
  {
    apds.disableGestureSensor();
    apds.disableLightSensor();
    apds.disableProximitySensor();
    yield();
    delay(100);
    allModesDisabled = false;
    switch (sensorMode)
    {
    case SENSOR_MODE_LIGHT:
      apds.enableLightSensor(false);
      break;
    case SENSOR_MODE_PROXIMITY:
      apds.enableProximitySensor(false);
      break;
    case SENSOR_MODE_GESTURE:
      apds.setGestureGain(GGAIN_1X);
      apds.enableGestureSensor();
      break;
    }
  }
}

void lightSensor()
{
  uint16_t ambient, red, green, blue;
  uint16_t divisor = 10;
  // SOURCE_INFO_ENTERING("lightSensor")
  tft.fillScreen(TFT_BLACK);
  digitalWrite(LED_BUILTIN, HIGH);
  boolean ambientOK = apds.readAmbientLight(ambient);
  boolean redOK = apds.readRedLight(red);
  boolean greenOK = apds.readGreenLight(green);
  boolean blueOK = apds.readBlueLight(blue);
  digitalWrite(LED_BUILTIN, LOW);
  if (ambientOK && redOK && greenOK && blueOK)
  {
    Serial.print(F("RGB: "));
    Serial.print(red);
    Serial.print(F("/"));
    Serial.print(green);
    Serial.print(F("/"));
    Serial.print(blue);
    Serial.print(F(", ambient: "));
    Serial.println(ambient);
    int h = red / divisor;
    tft.fillRect(8, 150 - h, 16, h, TFT_RED);
    h = green / divisor;
    tft.fillRect(40, 150 - h, 16, h, TFT_GREEN);
    h = blue / divisor;
    tft.fillRect(72, 150 - h, 16, h, TFT_BLUE);
    h = ambient / divisor;
    tft.fillRect(104, 150 - h, 16, h, TFT_LIGHTGREY);
  }
  else
  {
    Serial.println("no ambi li");
  }
  // SOURCE_INFO_LEAVING("lightSensor")
}

void proximitySensor()
{
  uint8_t proximity;
  //  SOURCE_INFO_ENTERING("proximitySensor")
  tft.fillScreen(TFT_BLACK);
  digitalWrite(LED_BUILTIN, HIGH);
  boolean proximityOK = apds.readProximity(proximity);
  digitalWrite(LED_BUILTIN, LOW);
  if (proximityOK)
  {
    Serial.print(F("proximity value: "));
    Serial.println(proximity);
    int h = proximity / 2;
    tft.fillRect(32, 150 - h, 64, h, TFT_YELLOW);
  }
  else
  {
    Serial.println(F("no proximity data available"));
  }
  // SOURCE_INFO_LEAVING("proximitySensor")
}

int retryGestureReading()
{
  int maxAttempts = 100;
  int delayBetweenAttempts = 1;
  digitalWrite(LED_BUILTIN, HIGH);
  for (int attempt = 0; attempt < maxAttempts; attempt++)
  {
    if (apds.isGestureAvailable())
    {
      digitalWrite(LED_BUILTIN, LOW);
      return apds.readGesture();
    }
    delay(delayBetweenAttempts);
  }
  digitalWrite(LED_BUILTIN, LOW);
  return 0;
}

void gestureSensor()
{
  // SOURCE_INFO_ENTERING("gestureSensor")
  tft.fillScreen(TFT_BLACK);
  if (apds.isGestureAvailable())
  {
    digitalWrite(LED_BUILTIN, HIGH);
    switch (apds.readGesture())
    {
    case DIR_RIGHT:
      Serial.println("RIGHT");
      tft.fillRect(5, 60, 91, 40, TFT_YELLOW);
      tft.fillTriangle(96, 40, 96, 120, 123, 80, TFT_YELLOW);
      break;
    case DIR_DOWN:
      Serial.println("DOWN");
      tft.fillRect(44, 21, 40, 91, TFT_YELLOW);
      tft.fillTriangle(104, 112, 24, 112, 64, 139, TFT_YELLOW);
      break;
    case DIR_LEFT:
      Serial.println("LEFT");
      tft.fillRect(32, 60, 91, 40, TFT_YELLOW);
      tft.fillTriangle(32, 120, 32, 40, 5, 80, TFT_YELLOW);
      break;
    case DIR_UP:
      Serial.println("UP");
      tft.fillRect(44, 48, 40, 91, TFT_YELLOW);
      tft.fillTriangle(24, 48, 104, 48, 64, 21, TFT_YELLOW);
      break;
    case DIR_NEAR:
      Serial.println("NEAR");
      break;
    case DIR_FAR:
      Serial.println("FAR");
      break;
    default:
      Serial.println("NONE");
    }
    digitalWrite(LED_BUILTIN, LOW);
    // SOURCE_INFO_LEAVING("gestureSensor")
  }
  else
  {
    // Serial.println(F("No gesture data"));
  }
}

const int LEFT_BUTTON = 18;
const int RIGHT_BUTTON = 19;
const uint32_t BUTTON_TIMEOUT = 200;

uint32_t leftButtonTime = 0;
uint32_t rightButtonTime = 0;

void IRAM_ATTR detectLeftButtonFired()
{
  uint32_t now = millis();
  if (now > leftButtonTime + BUTTON_TIMEOUT)
  {
    if (sensorMode == SENSOR_MODE_MIN)
    {
      sensorMode = SENSOR_MODE_MAX;
    }
    else
    {
      sensorMode--;
    }
    disableAllModes();
    leftButtonTime = now;
  }
}

void IRAM_ATTR detectRightButtonFired()
{
  uint32_t now = millis();
  if (now > rightButtonTime + BUTTON_TIMEOUT)
  {
    if (sensorMode == SENSOR_MODE_MAX)
    {
      sensorMode = SENSOR_MODE_MIN;
    }
    else
    {
      sensorMode++;
    }
    disableAllModes();
    rightButtonTime = now;
  }
}

// The top-level setup method that invokes the specific setup methods

void setup(void)
{
  attachInterrupt(digitalPinToInterrupt(LEFT_BUTTON), detectLeftButtonFired,
                  FALLING);
  attachInterrupt(digitalPinToInterrupt(RIGHT_BUTTON), detectRightButtonFired,
                  FALLING);

  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);

  Serial.begin(BAUD_RATE);
  while (!Serial)
  {
    ;
  }
  delay(500);
  tft.init();
  tft.setRotation(0);
  tft.fillScreen(TFT_BLACK);
  if (apds.init())
  {
    Serial.println(F("APDS-9960 initialization complete"));
  }
  else
  {
    Serial.println(F("APDS-9960 initialization failed"));
  }
  disableAllModes();
  Serial.print(F("myModel -> "));
  Serial.println(myModel.hello());
}

void loop()
{
  yield();
  enableOneMode();
  switch (sensorMode)
  {
  case SENSOR_MODE_LIGHT:
    lightSensor();
    break;
  case SENSOR_MODE_PROXIMITY:
    proximitySensor();
    break;
  case SENSOR_MODE_GESTURE:
    gestureSensor();
    break;
  }
  yield();
  delay(100);
  yield();
}