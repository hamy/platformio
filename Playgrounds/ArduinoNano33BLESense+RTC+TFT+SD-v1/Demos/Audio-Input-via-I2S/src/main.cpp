#include <Arduino.h>
#include <PDM.h>
#include <LED.h>

void setupLED(void) {
  ledsOff();
  ledBuiltin.on();
  delay(1000);
  ledsOff();
  ledRed.on();
  delay(1000);
  ledsOff();
  ledGreen.on();
  delay(1000);
  ledsOff();
  ledBlue.on();
  delay(1000);
  ledsOff();
}

const int BAUD_RATE = 115200;

void setupSerial(void) {
  Serial.begin(BAUD_RATE);
  while (!Serial) {
    ;
  }
  Serial.print(F("Serial baud rate: "));
  Serial.println(BAUD_RATE);
}

const long BUFFER_SIZE = 200000;
long bufferPos;
const long MIN_BUFFER_POS = 512;
uint8_t buffer[BUFFER_SIZE];

void word2buffer(uint16_t word) {
  bufferPos -= 2;
  memcpy(buffer + bufferPos, &word, 2);
}

uint16_t buffer2word() {
  uint16_t word = 0;
  bufferPos -= 2;
  memcpy(&word, buffer + bufferPos, 2);
  return word;
}

int16_t buffer2sword() {
  int16_t word = 0;
  bufferPos -= 2;
  memcpy(&word, buffer + bufferPos, 2);
  return word;
}

void setupBuffer(void) {
  bufferPos = BUFFER_SIZE;
  memset(buffer, 0, BUFFER_SIZE);
}

typedef enum _AudioMode {
  AM_INIT,
  AM_RECORDING,
  AM_RECORDED,
  AM_REPLAYING,
  AM_DONE
} AudioMode;
volatile AudioMode audioMode = AM_INIT;
const int SAMPLE_RATE = 16000;
const int CHANNEL_COUNT = 1;
uint32_t previousMicros;
volatile uint32_t audioChunksRead;
volatile uint32_t audioBytesRead;

void audioCB() {
  switch (audioMode) {
  case AM_RECORDING:
    uint32_t currentMicros = micros();
    int bytesAvailable = PDM.available();
    PDM.read(buffer, bytesAvailable);
    audioChunksRead++;
    audioBytesRead += bytesAvailable;
    if (bytesAvailable & 1) {
      Serial.print(F("Error: Odd number of bytes="));
      Serial.println(bytesAvailable);
    }
    if (bytesAvailable == 0) {
      Serial.println(F("Error: No data"));
    }
    uint16_t wordsAvailable = bytesAvailable >> 1;
    uint16_t timestamp = currentMicros - previousMicros;
    previousMicros = currentMicros;
    word2buffer(timestamp);
    word2buffer(wordsAvailable);
    bufferPos -= bytesAvailable;
    memcpy(buffer + bufferPos, buffer, bytesAvailable);
    if (bufferPos <= MIN_BUFFER_POS) {
      audioMode = AM_RECORDED;
    }
    break;
  }
}

void setupAudio(void) {
  audioMode = AM_INIT;
  PDM.onReceive(audioCB);
  PDM.setGain(20);
}

void recordAudio() {
  Serial.println(F("recording audio..."));
  digitalWrite(LED_BUILTIN, HIGH);
  previousMicros = micros();
  audioMode = AM_RECORDING;
  if (!PDM.begin(CHANNEL_COUNT, SAMPLE_RATE)) {
    Serial.println(F("Failed to start recording !"));
  }
  while (audioMode == AM_RECORDING) {
    Serial.print(".");
    delay(200);
  }
  Serial.println();
  Serial.print(F("audio recording completed, bufferPos="));
  Serial.println(bufferPos);
  Serial.print(F("audio size: chunks read="));
  Serial.print(audioChunksRead);
  Serial.print(F(", bytes read="));
  Serial.print(audioBytesRead);
  Serial.print(F(", avg chunk size="));
  Serial.println(((float)audioBytesRead) / audioChunksRead);
  digitalWrite(LED_BUILTIN, LOW);
  audioMode = AM_RECORDED;
  PDM.end();
}

void replayAudio() {
  Serial.println(F("replaying audio..."));
  digitalWrite(LED_BUILTIN, HIGH);
  audioMode = AM_REPLAYING;
  long minBufferPos = bufferPos;
  bufferPos = BUFFER_SIZE;
  while (bufferPos > minBufferPos) {
    Serial.print(buffer2word());
    Serial.print(",");
    int chunkSize = buffer2word();
    Serial.print(chunkSize);
    bufferPos -= chunkSize * 2;
    for (int i = 0; i < chunkSize; i++) {
      Serial.print(",");
      Serial.print(buffer2sword());
    }
    Serial.println();
  }

  digitalWrite(LED_BUILTIN, LOW);
  audioMode = AM_DONE;
}

void setup(void) {
  setupLED();
  setupSerial();
  setupBuffer();
  setupAudio();
}

void loop() {
  switch (audioMode) {
  case AM_INIT:
    Serial.println(F("Preparing recording, sleeping"));
    delay(1500);
    recordAudio();
    break;
  case AM_RECORDING:
    delay(1000);
    break;
  case AM_RECORDED:
    delay(1000);
    replayAudio();
    break;
  default:
    Serial.println(F("Done"));
    delay(5000);
    break;
  }
}
