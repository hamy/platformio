#ifndef _BUFFERS_H_INCLUDED_
#define _BUFFERS_H_INCLUDED_

#include <Arduino.h>

#define SMALL_BUFFER_SIZE_IN_WORDS 100000
#define SMALL_BUFFER_SIZE_IN_BYTES (2*SMALL_BUFFER_SIZE_IN_WORDS) 
#define LARGE_BUFFER_SIZE_IN_WORDS 256
#define LARGE_BUFFER_SIZE_IN_BYTES (2*LARGE_BUFFER_SIZE_IN_WORDS) 



#endif