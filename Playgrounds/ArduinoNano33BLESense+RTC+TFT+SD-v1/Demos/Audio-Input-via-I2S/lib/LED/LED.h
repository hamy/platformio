#ifndef _LED_H_INCLUDED_
#define _LED_H_INCLUDED_

#include <Arduino.h>

class LED {
public:
    LED(  int pin, boolean brightnessPolarity);
    int getPin();
    boolean getBrightnessPolarity();
    void on();
    void off();
    void toggle();
    boolean isOn();
private:
    int _pin;
    boolean _brightnessPolarity;
    boolean _flag;
};

static LED ledRed(22,false);
static LED ledGreen(23,false);
static LED ledBlue(24,false);
static LED ledBuiltin(LED_BUILTIN,true);

static void ledsOff() {
    ledRed.off();
    ledGreen.off();
    ledBlue.off();
    ledBuiltin.off();
}

#endif