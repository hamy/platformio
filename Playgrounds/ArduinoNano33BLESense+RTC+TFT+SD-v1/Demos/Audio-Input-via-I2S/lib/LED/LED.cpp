#include <LED.h>

LED::LED(int pin, boolean brightnessPolarity) {
    _pin = pin;
    _brightnessPolarity = brightnessPolarity ;
    pinMode(_pin,OUTPUT);
    off();
}

int LED::getPin() { return _pin; }

boolean LED::getBrightnessPolarity() { return _brightnessPolarity; } 

void LED::on() { 
    _flag = true;
    digitalWrite(_pin, _brightnessPolarity ? HIGH : LOW);
     } 

void LED::off() { 
    _flag = false;
    digitalWrite(_pin, _brightnessPolarity ? LOW : HIGH);
     } 

void LED::toggle() { 
    if (_flag) {
        LED::off();
    } else {
        LED::on();
    }
} 
     
boolean LED::isOn() { return _flag; }