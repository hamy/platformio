#include <Arduino.h>

//#define SPI_FREQUENCY 20000000
#include <SPI.h>
#include <Wire.h>

#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_ST7789.h> // Hardware-specific library for ST7789

#define TFT_CS 10
#define TFT_RST 9 // Or set to -1 and connect to Arduino RESET pin
#define TFT_DC 8

Adafruit_ST7789 tft = Adafruit_ST7789(TFT_CS, TFT_DC, TFT_RST);

void setup(void) {
  Serial.begin(115200);
  while (!Serial) {
    ;
  }
//  tft.init(240, 240); // Init ST7789 240x240
 //tft.fillScreen(ST77XX_BLUE);

  Serial.println(F("Initialized"));
}

void loop() {
  digitalWrite(LED_BUILTIN, LOW);
  delay(900);
  digitalWrite(LED_BUILTIN, HIGH);
  delay(100);
}
