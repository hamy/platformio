
#include "RTClib.h"
#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_ST7735.h> // Hardware-specific library for ST7735
#include <Arduino.h>
#include <Arduino_HTS221.h>
#include <Arduino_LPS22HB.h>
#include <SPI.h>
#include <Wire.h>

#define BAUD_RATE 115200

void setupSerial() {
  Serial.begin(BAUD_RATE);
  while (!Serial) {
    ;
  }
  delay(1000);
  Serial.print(F("setupSerial: Baud rate = "));
  Serial.println(BAUD_RATE);
  Serial.println();
}

void bailOut() {
  Serial.println(F("Bailing out..."));
  while (true) {
    ;
  }
}

RTC_DS3231 rtc;
DateTime now;

void setupRTC() {
  Serial.println(F("setupRTC: entering"));
  if (!rtc.begin()) {
    Serial.println(F("Couldn't find RTC"));
    bailOut();
  }

  if (rtc.lostPower()) {
    Serial.println(F("RTC lost power, time is invalid"));
    bailOut();
  }
  now = rtc.now();
  Serial.print(F("Current time: "));
  Serial.println(now.timestamp());
  Serial.println(F("setupRTC: leaving"));
  Serial.println();
}

// For the breakout board, you can use any 2 or 3 pins.
// These pins will also work for the 1.8" TFT shield.
#define TFT_CS 10
#define TFT_RST 9 // Or set to -1 and connect to Arduino RESET pin
#define TFT_DC 8

Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS, TFT_DC, TFT_RST);

void setupTFT() {
  Serial.println(F("setupTFT: entering"));
  tft.initR(INITR_GREENTAB);
  tft.fillScreen(ST77XX_BLACK);
  Serial.println(F("setupTFT: leaving"));
  Serial.println();
}

float pressure;
float humidity;
float tempByHTS;
float tempByRTC;

void setupSensors(void) {
  Serial.println(F("setupSensors: entering"));
  if (BARO.begin()) {
    Serial.println(F("barometer initialized successfully"));
  } else {
    Serial.println(F("barometer initialization failed !"));
    bailOut();
  }
  if (HTS.begin()) {
    Serial.println(F("hygrometer initialized successfully"));
  } else {
    Serial.println(F("hygrometer initialization failed !"));
    bailOut();
  }

  Serial.println(F("setupSensors: leaving"));
  Serial.println();
}

void setupLEDs(void) {
  Serial.println(F("setupLEDs: entering"));
  Serial.print(F("PIN_LED: "));
  Serial.println(PIN_LED);
  Serial.print(F("LED_BUILTIN: "));
  Serial.println(LED_BUILTIN);
  pinMode(LED_BUILTIN, OUTPUT);digitalWrite(LED_BUILTIN,1);
  Serial.print(F("LEDR: "));
  Serial.println(LEDR);
  Serial.print(F("LEDR: "));
  pinMode(LEDR, OUTPUT);
  Serial.println(LEDR);
  Serial.print(F("LEDG: "));
  pinMode(LEDG, OUTPUT);
  Serial.println(LEDG);
  Serial.print(F("LEDB: "));
  pinMode(LEDB, OUTPUT);
  Serial.println(LEDB);
  Serial.println(F("setupLEDs: leaving"));
  Serial.println();
}

void setup(void) {
  setupSerial();
  setupRTC();
  setupTFT();
  setupSensors();
  setupLEDs();
}

void doMeasurements() {
  // now = rtc.now();
  Serial.print(F("Now is: "));
  Serial.println(now.timestamp());
  pressure = BARO.readPressure(MILLIBAR);
  Serial.print(F("Pressure: "));
  Serial.print(pressure);
  Serial.println(F(" hPa"));
  humidity = HTS.readHumidity();
  tempByHTS = HTS.readTemperature(CELSIUS);
  Serial.print(F("Relative humidity: "));
  Serial.print(humidity);
  Serial.println(F(" %"));
  tempByRTC = rtc.getTemperature();
  Serial.println(F("Temperature measurements:"));
  Serial.print(F("by HTS: "));
  Serial.print(tempByHTS);
  Serial.println(F(" Celsius"));
  Serial.print(F("by RTC: "));
  Serial.print(tempByRTC);
  Serial.println(F(" Celsius"));
  Serial.println();
}

void reportViaTFT() {
  // tft.fillScreen(ST77XX_BLACK);
  tft.setRotation(2);
  tft.setTextColor(ST77XX_GREEN, ST77XX_BLACK);
  tft.setCursor(0, 0);
  tft.println(now.timestamp(DateTime::TIMESTAMP_TIME));
  tft.print(F("Pressure: "));
  tft.print(pressure);
  tft.println(" hPa");
  tft.print(F("Humidity: "));
  tft.print(humidity);
  tft.println(" %");
  tft.println("Temperature:");
  tft.print(F("by HTS: "));
  tft.print(tempByHTS);
  tft.println(" C");
  tft.print(F("by RTC: "));
  tft.print(tempByRTC);
  tft.println(" C");

  tft.flush();
}

uint32_t oldSecond = 0;

void loop() {
  now = rtc.now();
  uint32_t newSecond = now.unixtime();
  if (newSecond == oldSecond) {
    return;
  }
  oldSecond = newSecond;
  doMeasurements();
  reportViaTFT();
   digitalWrite(LEDR, (newSecond & 2) != 0);
  digitalWrite(LEDG, (newSecond & 4) != 0);
  digitalWrite(LEDB, (newSecond & 8) != 0);
  digitalWrite(LED_BUILTIN, (newSecond & 2) != 0);
}
