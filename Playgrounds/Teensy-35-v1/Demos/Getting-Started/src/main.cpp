
#include <Arduino.h>

const int BAUDRATE = 115200;

void setup(void)
{
  Serial.begin(BAUDRATE);
  while (!Serial)
  {
    ;
  }
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);
}

void loop()
{
  Serial.println(F("LOW"));
  digitalWrite(LED_BUILTIN, LOW);
  delay(800);
  Serial.println(F("HIGH"));
  digitalWrite(LED_BUILTIN, HIGH);
  delay(200);
}
