import serial
import sys
import time
import concurrentseriallogger as csl

nargs = len(sys.argv)
print('CLI argument count:',nargs)
portName = "/dev/ttyACM0"     
if nargs > 1:     
    portName = sys.argv[1]  
print('using serial port',portName)
baudRate = 115200
wordSize = serial.EIGHTBITS
parity = serial.PARITY_NONE
stopBits = serial.STOPBITS_ONE
ser = serial.Serial(portName,baudRate,wordSize,parity,stopBits)
print("Serial connection opened: ",ser)
time.sleep(1)

print('Creating logger...')
# Note: the nominal delay is 1 msec. However, the actual delay may vary by a far more larger amount
# since the serial reader thread may be paused for a larger interval.
logger = csl.ConcurrentSerialLogger(ser,idleDelay=0.01) # logPath='reports/synchronization-batch-log.txt')
print('logger running ? ',logger.isRunning())
logger.start()
time.sleep(0.01)
print('logger running ? ',logger.isRunning())
 

now = time.time()
localtime = time.localtime(now)
nowAsctime = time.asctime(localtime)
nowAsText = '\n@rtc-sync ' + str(int(now)) + '\n'
print(nowAsctime," Sending the following record via serial: ",nowAsText)
print(nowAsctime,"The firmware will echo the received bytes in HEX.")
ser.write(nowAsText.encode(encoding="utf-8"))  
ser.flush()  
time.sleep(2)

now = time.time()
localtime = time.localtime(now)
nowAsctime = time.asctime(localtime)
nowAsText = '\n@rtc-sync ' + str(int(now)) + '\n'
print(nowAsctime," Sending the following record via serial: ",nowAsText)
print(nowAsctime,"The firmware will echo the received bytes in HEX.")
ser.write(nowAsText.encode(encoding="utf-8"))  
ser.flush()  
time.sleep(2)

logger.stop()
ser.close()
print("Serial connection closed.")
 
