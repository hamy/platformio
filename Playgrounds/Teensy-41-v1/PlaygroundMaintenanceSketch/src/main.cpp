#include <RTClib.h>
#include <SPI.h>
#include <Wire.h>
#include <stdlib.h>
#include <string.h>

RTC_DS3231 rtc;

const int BAUD_RATE = 115200;

void setupSerial() {
  Serial.begin(BAUD_RATE);
  while (!Serial) {
    ;
  }
  Serial.print(F("Serial connection initialized, baud rate: "));
  Serial.println(BAUD_RATE);
}

const int CLI_BUFFER_SIZE = 80;
char cliBuffer[CLI_BUFFER_SIZE + 1];
int cliBufferLength = 0;
char *cliOpcode;
const char *CLI_BUFFER_DELIM = " \t";

void cliReset() {
  memset(cliBuffer, 0, CLI_BUFFER_SIZE + 1);
  cliBufferLength = 0;
}

void cliBufferHandlerForRTCSync() {
  char *arg = strtok(NULL, CLI_BUFFER_DELIM);
  Serial.print(F("cliBufferHandlerForRTCSync - arg: "));
  Serial.println(arg);
  long secondsSinceEpoch = atol(arg);
  Serial.println(secondsSinceEpoch);
  DateTime sync = DateTime(secondsSinceEpoch);
  rtc.adjust(sync);
  Serial.print(F("Adjusted to "));
  Serial.println(sync.timestamp());
  Serial.flush();
}

void cliBufferRouter() {
  Serial.print(F("buffer router - text: "));
  Serial.println(cliBuffer);
  Serial.print(F("buffer router - size: "));
  Serial.println(cliBufferLength);
  cliOpcode = strtok(cliBuffer, CLI_BUFFER_DELIM);
  Serial.print(F("opcode: "));
  Serial.println(cliOpcode);
  if (!strcmp(cliOpcode, "@rtc-sync")) {
    cliBufferHandlerForRTCSync();
  }
}

void cliReadSerialInput() {
  while (Serial.available() > 0) {
    int ch = Serial.read();
    switch (ch) {
    case 0xa:
    case 0xd:
      if (cliBufferLength > 0) {
        cliBufferRouter();
      }
      cliReset();
      return;
    default:
      if (cliBufferLength < CLI_BUFFER_SIZE) {
        cliBuffer[cliBufferLength++] = ch;
      }
    }
  }
}

void setupCLI() {
  cliReset();
  Serial.print(F("CLI initialized, max. line length: "));
  Serial.println(CLI_BUFFER_SIZE);
}

void setupRTC(void) {
  if (!rtc.begin()) {
    Serial.println(F("Couldn't find RTC"));
    while (1)
      ;
  }
  if (rtc.lostPower()) {
    Serial.println(F(
        "RTC lost power, time is copied from the compilation timestamp."));
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  }
  Serial.print(F("RTC initialized: current time (UTC): "));
  DateTime now = rtc.now();
  Serial.println(now.timestamp());
}

void setup(void) {
  setupSerial();
  setupCLI();
  setupRTC();
}

void loop() {
  cliReadSerialInput();
  DateTime now = rtc.now();
  float temp = rtc.getTemperature();
  Serial.print(now.timestamp());
  Serial.print(F("  "));
  Serial.print(temp);
  Serial.println(F(" C"));
  delay(1000);
}