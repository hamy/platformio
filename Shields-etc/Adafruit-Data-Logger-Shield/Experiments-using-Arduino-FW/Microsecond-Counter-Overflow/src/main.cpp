#include <Arduino.h>
#include <LoopUtil.h>
#include <RTClib.h>
#include <SD.h>
#include <SPI.h>
#include <SourceInfo.h>

#define SECOND_LED 3
#define MINUTE_LED 4

void setupLED() {
  pinMode(SECOND_LED, OUTPUT);
  pinMode(MINUTE_LED, OUTPUT);
  digitalWrite(SECOND_LED, LOW);
  digitalWrite(MINUTE_LED, LOW);
}

// Variables and methods for the serial communication.
#define BAUD_RATE 9600

void setupSerial() {
  Serial.begin(BAUD_RATE);
  while (!Serial) {
    ;
  }
  delay(500);
  SOURCE_INFO_ENTERING("setupSerial")
  Serial.print(F("Baud rate: "));
  Serial.println(BAUD_RATE);
  SOURCE_INFO_PRINT
  SOURCE_INFO_LEAVING("setupSerial")
  SOURCE_INFO_NL
}

// Variables and methods used for the RTC
RTC_PCF8523 rtc;
DateTime startDateTime;
uint32_t startSeconds;
uint32_t previousMicros;

void setupRTC() {
  SOURCE_INFO_ENTERING("setupRTC")
  Serial.println(F("remember: all RTC timestamps are based on UTC"));
  int rc = rtc.begin();
  Serial.print(F("begin() returned rc="));
  Serial.println(rc);
  if (!rc) {
    SOURCE_INFO_BAILING_OUT(
        "Catastrophic error: begin() failed ! Bailing out...")
  }
  rc = rtc.initialized();
  Serial.print(F("initialized() returned rc="));
  Serial.println(rc);
  if (!rc) {
    SOURCE_INFO_BAILING_OUT("Oops: RTC was not initialized so far !")
  }
  startDateTime = rtc.now();
  startSeconds = startDateTime.unixtime();
  Serial.print(F("Initial DateTime value: "));
  Serial.println(startDateTime.timestamp());
  previousMicros = micros();
  SOURCE_INFO_LEAVING("setupRTC")
  SOURCE_INFO_NL
}

#define CSV_PATH_FORMAT "/DATA/USECOV%02d.CSV"
const int chipSelect = 10;
File csvFile;
Sd2Card card;
char csvPath[20];

void findUnusedCsvPath() {
  for (int i = 0; i < 100; i++) {
    sprintf(csvPath, CSV_PATH_FORMAT, i);
    if (!SD.exists(csvPath)) {
      return;
    }
  }
  SOURCE_INFO_BAILING_OUT("No unused CSV path found.")
}

void setupSD() {
  SOURCE_INFO_ENTERING("setupSD")
  SD.begin(chipSelect);
  // we'll use the initialization code from the utility libraries
  // since we're just testing if the card is working!
  if (!card.init(SPI_HALF_SPEED, chipSelect)) {
    SOURCE_INFO_BAILING_OUT("card initialization failed.")
  }
  findUnusedCsvPath();
  Serial.print(F("CVS path: "));
  Serial.println(csvPath);
  csvFile = SD.open(csvPath, FILE_WRITE);
  csvFile.println(F("H,TIME_SINCE_EPOCH,TIME_UTC,DUR_HOURS,DUR_SECONDS,COUNTER_"
                    "USEC,OVFL_DETECTED"));
  csvFile.flush();
  SOURCE_INFO_LEAVING("setupSD")
  SOURCE_INFO_NL
}

// The top-level setup method that invokes the specific setup methods
void setup() {
  digitalWrite(SECOND_LED, HIGH);
  digitalWrite(MINUTE_LED, HIGH);
  setupLED();
  setupSerial();
  setupRTC();
  setupSD();
  digitalWrite(SECOND_LED, LOW);
  digitalWrite(MINUTE_LED, LOW);
}

LoopUtil loopUtil(false, -1, 1000);

#define APPEND_CSV_ITEM(item)                                                  \
  csvFile.print(item);                                                         \
  Serial.print(item);

void loop() {
  char fmtBuffer[10];
  loopUtil.loopStart();
  DateTime now = rtc.now();
  int nowSecond = now.second();
  digitalWrite(SECOND_LED, nowSecond & 1);
  digitalWrite(MINUTE_LED, LOW);
  if (!(nowSecond % 5)) {
    digitalWrite(MINUTE_LED, HIGH);
    uint32_t nowSeconds = now.unixtime();
    uint32_t durSeconds = nowSeconds - startSeconds;
    uint32_t usec = micros();
    double durHours = durSeconds / 3600.0;
    APPEND_CSV_ITEM(F("D,"))
    APPEND_CSV_ITEM(nowSeconds)
    APPEND_CSV_ITEM(F(","))
    APPEND_CSV_ITEM(now.timestamp())
    APPEND_CSV_ITEM(F(","))
    // sprintf(fmtBuffer, "%7.04e", durHours);
    dtostrf(durHours, 6, 4, fmtBuffer);
    APPEND_CSV_ITEM(fmtBuffer)
    APPEND_CSV_ITEM(F(","))
    APPEND_CSV_ITEM(durSeconds)
    APPEND_CSV_ITEM(F(","))
    APPEND_CSV_ITEM(usec)
    APPEND_CSV_ITEM(F(","))
    APPEND_CSV_ITEM(usec < previousMicros)
    previousMicros = usec;
    Serial.println();
    csvFile.println();
    csvFile.flush();
  }
  loopUtil.loopEnd();
}
