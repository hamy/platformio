#include <Arduino.h>
//#include <LEDUtil.h>
#include <DallasTemperature.h>
#include <LoopUtil.h>
#include <OneWire.h>
#include <RTClib.h>
#include <SD.h>
#include <SPI.h>
#include <SourceInfo.h>
#include <Wire.h>

// green LED indicates temperature measurement, red LED indicates writing to SD
#define GREEN_LED 3
#define RED_LED 4

void setupLEDs()
{
  pinMode(GREEN_LED, OUTPUT);
  pinMode(RED_LED, OUTPUT);
  digitalWrite(GREEN_LED, HIGH);
  digitalWrite(RED_LED, HIGH);
}

void setupSerial()
{
  Serial.begin(9600);
  while (!Serial)
  {
    ;
  }
  SOURCE_INFO_ENTERING("setupSerial")
  SOURCE_INFO_PRINT
  SOURCE_INFO_LEAVING("setupSerial")
  SOURCE_INFO_NL
}

// Variables and methods used for the RTC
RTC_PCF8523 rtc;
DateTime startDateTime;
uint32_t startSeconds;

void setupRTC()
{
  SOURCE_INFO_ENTERING("setupRTC")
  Serial.println(F("remember: all RTC timestamps are based on UTC"));
  int rc = rtc.begin();
  Serial.print(F("begin() returned rc="));
  Serial.println(rc);
  if (!rc)
  {
    SOURCE_INFO_BAILING_OUT(
        "Catastrophic error: begin() failed ! Bailing out...")
  }
  rc = rtc.initialized();
  Serial.print(F("initialized() returned rc="));
  Serial.println(rc);
  if (!rc)
  {
    SOURCE_INFO_BAILING_OUT("Oops: RTC was not initialized so far !")
  }
  startDateTime = rtc.now();
  startSeconds = startDateTime.unixtime();
  Serial.print(F("Initial DateTime value: "));
  Serial.println(startDateTime.timestamp());
  SOURCE_INFO_LEAVING("setupRTC")
  SOURCE_INFO_NL
}

#define CSV_PATH_FORMAT "/DATA/TEMPER%02d.CSV"
const int chipSelect = 10;
File csvFile;
Sd2Card card;
char csvPath[20];

void findUnusedCsvPath()
{
  for (int i = 0; i < 100; i++)
  {
    sprintf(csvPath, CSV_PATH_FORMAT, i);
    if (!SD.exists(csvPath))
    {
      return;
    }
  }
  SOURCE_INFO_BAILING_OUT("No unused CSV path found.")
}

void setupSD()
{
  SOURCE_INFO_ENTERING("setupSD")
  SD.begin(chipSelect);
  // we'll use the initialization code from the utility libraries
  // since we're just testing if the card is working!
  if (!card.init(SPI_HALF_SPEED, chipSelect))
  {
    SOURCE_INFO_BAILING_OUT("card initialization failed.")
  }
  findUnusedCsvPath();
  Serial.print(F("CVS path: "));
  Serial.println(csvPath);
  csvFile = SD.open(csvPath, FILE_WRITE);
  csvFile.println(
      F("H,TIME_SINCE_EPOCH,TIME_UTC,DUR_HOURS,DUR_SECONDS,TEMPERATURE"));
  csvFile.flush();
  SOURCE_INFO_LEAVING("setupSD")
  SOURCE_INFO_NL
}

// Setup a oneWire instance to communicate with any OneWire devices
#define ONE_WIRE_BUS 2

OneWire oneWire(ONE_WIRE_BUS);

// Pass our oneWire reference to Dallas Temperature.
DallasTemperature sensors(&oneWire);

// variable to hold device addresses
DeviceAddress Thermometer;

int deviceCount = 0;

void printAddress(DeviceAddress deviceAddress)
{
  for (uint8_t i = 0; i < 8; i++)
  {
    Serial.print("0x");
    if (deviceAddress[i] < 0x10)
      Serial.print("0");
    Serial.print(deviceAddress[i], HEX);
    if (i < 7)
      Serial.print(", ");
  }
  Serial.println("");
}

void setupSensors()
{
  // Start up the library
  sensors.begin();

  // locate devices on the bus
  Serial.println("Locating devices...");
  Serial.print("Found ");
  deviceCount = sensors.getDeviceCount();
  Serial.print(deviceCount, DEC);
  Serial.println(" devices.");
  Serial.println("");

  Serial.println("Printing addresses...");
  for (int i = 0; i < deviceCount; i++)
  {
    Serial.print("Sensor ");
    Serial.print(i + 1);
    Serial.print(" : ");
    sensors.getAddress(Thermometer, i);
    printAddress(Thermometer);
  }
}

void setup()
{
  setupLEDs();
  setupSerial();
  setupRTC();
  setupSD();
  setupSensors();
  digitalWrite(GREEN_LED, LOW);
  digitalWrite(RED_LED, LOW);
}

LoopUtil loopUtil(true, -1, 1000);

#define APPEND_CSV_ITEM(item) \
  csvFile.print(item);        \
  Serial.print(item);

void loop(void)
{
  char fmtBuffer[10];
  loopUtil.loopStart();
  digitalWrite(GREEN_LED, HIGH);
  digitalWrite(RED_LED, LOW);
  Serial.println(F("requesting temperature value..."));
  sensors.requestTemperatures();
  Serial.println(F("temperature value was returned"));
  float celsius = sensors.getTempCByIndex(0);
  digitalWrite(GREEN_LED, LOW);
  digitalWrite(RED_LED, HIGH);
  DateTime now = rtc.now();
  uint32_t nowSeconds = now.unixtime();
  uint32_t durSeconds = nowSeconds - startSeconds;
  double durHours = durSeconds / 3600.0;
  APPEND_CSV_ITEM(F("D,"))
  APPEND_CSV_ITEM(nowSeconds)
  APPEND_CSV_ITEM(F(","))
  APPEND_CSV_ITEM(now.timestamp())
  APPEND_CSV_ITEM(F(","))
  dtostrf(durHours, 6, 4, fmtBuffer);
  APPEND_CSV_ITEM(fmtBuffer)
  APPEND_CSV_ITEM(F(","))
  APPEND_CSV_ITEM(durSeconds)
  APPEND_CSV_ITEM(F(","))
  dtostrf(celsius, 4, 2, fmtBuffer);
  APPEND_CSV_ITEM(fmtBuffer)
  Serial.println();
  csvFile.println();
  csvFile.flush();
  digitalWrite(GREEN_LED, LOW);
  digitalWrite(RED_LED, LOW);
  loopUtil.loopEnd();
}