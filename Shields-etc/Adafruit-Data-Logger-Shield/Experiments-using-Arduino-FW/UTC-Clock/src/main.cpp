#include <Arduino.h>

#include <RTClib.h>
#include <SourceInfo.h>
#include <LoopUtil.h>

// Variables and methods for the LEDs:
// SECOND_LED changes it state every second.
// MINUTE_LED changes it state every minute.
#define SECOND_LED 2
#define MINUTE_LED 3

void setupLED()
{
  pinMode(SECOND_LED, OUTPUT);
  pinMode(MINUTE_LED, OUTPUT);
}

// Variables and methods for the serial communication b
#define BAUD_RATE 115200

void setupSerial()
{
  Serial.begin(BAUD_RATE);
  while (!Serial)
  {
    ;
  }
  delay(500);
  Serial.println(F("setupSerial: entering..."));
  Serial.print(F("Baud rate: "));
  Serial.println(BAUD_RATE);
  SOURCE_INFO_PRINT
  Serial.println(F("setupSerial: leaving..."));
  Serial.println();
}

// Variables and methods used for the RTC
RTC_PCF8523 rtc;

void setupRTC()
{
  Serial.println(F("setupRTC: entering..."));
  Serial.println(F("remember: all RTC timestamps are based on UTC"));
  int rc = rtc.begin();
  Serial.print(F("begin() returned rc="));
  Serial.println(rc);
  if (!rc)
  {
    Serial.println(F("Catastrophic error: begin() failed ! Bailing out..."));
    while (1)
      ;
  }
  rc = rtc.initialized();
  Serial.print(F("initialized() returned rc="));
  Serial.println(rc);
  if (!rc)
  {
    Serial.println(F("Oops: RTC was not initialized so far !"));
    Serial.println(F("Adjusting RTC from compilation time stamp..."));
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    Serial.println(F("That's not very nice, but a kludge..."));
  }
  DateTime now = rtc.now();
  Serial.print(F("Initial DateTime value: "));
  Serial.println(now.timestamp());
  Serial.println(F("setupRTC: leaving..."));
  Serial.println();
}

// The top-level setup method that invokes the specific setup methods
void setup()
{
  setupLED();
  setupSerial();
  setupRTC();
}

LoopUtil loopUtil(true, -1, 1000);

void loop()
{
  loopUtil.loopStart();
  DateTime now = rtc.now();
  int second = now.second();
  digitalWrite(SECOND_LED, second & 1);
  int minute = now.minute();
  digitalWrite(MINUTE_LED, minute & 1);
  Serial.print(F("Now is "));
  Serial.println(now.timestamp());
  loopUtil.loopEnd();
}
