#include <Arduino.h>

#include <RTClib.h>
#include <SD.h>
#include <SPI.h>
#include <SourceInfo.h>
#include <Wire.h>

#define GREEN_LED 3
#define RED_LED 4
#define NEXTLINE_LED 5

void redLedOff() { digitalWrite(RED_LED, LOW); }
void redLedOn() { digitalWrite(RED_LED, HIGH); }
void greenLedOff() { digitalWrite(GREEN_LED, LOW); }
void greenLedOn() { digitalWrite(GREEN_LED, HIGH); }
void allLedsOff() {
  redLedOff();
  greenLedOff();
  digitalWrite(NEXTLINE_LED, LOW);
}

void setupLED() {
  pinMode(RED_LED, OUTPUT);
  pinMode(GREEN_LED, OUTPUT);
  pinMode(NEXTLINE_LED, OUTPUT);
  allLedsOff();
}

// Variables and methods for the serial communication b
#define BAUD_RATE 115200

void setupSerial() {
  Serial.begin(BAUD_RATE);
  while (!Serial) {
    ;
  }
  delay(500);
  SOURCE_INFO_ENTERING("setupSerial")
  Serial.print(F("Baud rate: "));
  Serial.println(BAUD_RATE);
  SOURCE_INFO_PRINT
  SOURCE_INFO_LEAVING("setupSerial")
  SOURCE_INFO_NL
}

// Variables and methods used for the RTC
RTC_PCF8523 rtc;

void setupRTC() {
  SOURCE_INFO_ENTERING("setupRTC")
  Serial.println(F("remember: all RTC timestamps are based on UTC"));
  int rc = rtc.begin();
  Serial.print(F("begin() returned rc="));
  Serial.println(rc);
  if (!rc) {
    SOURCE_INFO_BAILING_OUT(
        "Catastrophic error: begin() failed ! Bailing out...")
  }
  rc = rtc.initialized();
  Serial.print(F("initialized() returned rc="));
  Serial.println(rc);
  if (!rc) {
    Serial.println(F("Oops: RTC was not initialized so far !"));
    Serial.println(F("Adjusting RTC from compilation time stamp..."));
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    Serial.println(F("That's not very nice, but a kludge..."));
  }
  DateTime now = rtc.now();
  Serial.print(F("Initial DateTime value: "));
  Serial.println(now.timestamp());
  SOURCE_INFO_LEAVING("setupRTC")
  SOURCE_INFO_NL
}

const int chipSelect = 10;
#define SYNC_HISTORY_PATH "/VAR/LOG/RTC-SYNC.CSV"

// set up variables using the SD utility library functions:
Sd2Card card;
SdVolume volume;
SdFile root;

void setupSD() {
  SOURCE_INFO_ENTERING("setupSD")
  SD.begin(chipSelect);
  // we'll use the initialization code from the utility libraries
  // since we're just testing if the card is working!
  if (!card.init(SPI_HALF_SPEED, chipSelect)) {
    SOURCE_INFO_BAILING_OUT("SD initialization failed.")
  }
  Serial.println(F("Wiring is correct and a card is present."));

  // print the type of card
  Serial.print(F("Card type:         "));
  switch (card.type()) {
  case SD_CARD_TYPE_SD1:
    Serial.println(F("SD1"));
    break;
  case SD_CARD_TYPE_SD2:
    Serial.println(F("SD2"));
    break;
  case SD_CARD_TYPE_SDHC:
    Serial.println(F("SDHC"));
    break;
  default:
    Serial.println(F("Unknown"));
  }

  // Now we will try to open the 'volume'/'partition' - it should be FAT16 or
  // FAT32
  if (!volume.init(card)) {
    SOURCE_INFO_BAILING_OUT("Could not find FAT16/FAT32 partition.")
  }
  SOURCE_INFO_LEAVING("setupSD")
  SOURCE_INFO_NL
}

// Variables and methods for serial input
// THIS SECTION NEEDS IMPROVEMENT !!!
#define SI_LINE_SEPARATOR 0
#define SI_LINE_HEADER 1
#define SI_ADJUSTMENT_DATA 2
#define SI_REPORT_MARK 3
#define SI_LINE_TRAILER 4
uint8_t siPos = SI_LINE_SEPARATOR;
uint8_t siNextByte;
uint32_t siAdjustmentData = 0;
boolean siErrorFlag = false;
boolean siAdjustmentRequested = false;
boolean siReportRequested = false;
#define HASH_MARK ('#')
#define REPORT_MARK ('R')

void siReset() {
  siPos = SI_LINE_SEPARATOR;
  siErrorFlag = false;
  siAdjustmentData = 0;
  siAdjustmentRequested = false;
  siReportRequested = false;
}

void siRaiseErrorFlag() {
  siErrorFlag = true;
  siAdjustmentData = 0;
  siAdjustmentRequested = false;
  siReportRequested = false;
}

void setupSI() {
  SOURCE_INFO_ENTERING("setupSI")
  siReset();
  SOURCE_INFO_LEAVING("setupSI")
  SOURCE_INFO_NL
}

void siPrintDataBoundaryLine() {
  Serial.print(F("####-DATA-BOUNDARY-LINE-####-"));
}

void siPrintDataBoundaryLineAtStart() {
  siPrintDataBoundaryLine();
  Serial.println(F("@START"));
}

void siPrintDataBoundaryLineAtEnd() {
  siPrintDataBoundaryLine();
  Serial.println(F("@END"));
}

void siActionForError() {
  switch (siNextByte) {
  case 0x0a:
  case 0x0d:
    siReset();
    break;
  }
}

void siActionForLineSeparator() {
  switch (siNextByte) {
  case 0x0a:
  case 0x0d:
    break;
  case HASH_MARK:
    siPos = SI_LINE_HEADER;
    break;
  default:
    siRaiseErrorFlag();
    break;
  }
}

void siActionForLineHeader() {
  if (isDigit(siNextByte)) {
    siPos = SI_ADJUSTMENT_DATA;
    siAdjustmentData = siNextByte - '0';
    siAdjustmentRequested = true;
  } else {
    switch (siNextByte) {
    case HASH_MARK:
      break;
    case REPORT_MARK:
      siPos = SI_REPORT_MARK;
      siReportRequested = true;
      break;
    default:
      siRaiseErrorFlag();
      break;
    }
  }
}

void siActionForAdjustmentData() {
  if (isDigit(siNextByte)) {
    siAdjustmentData *= 10;
    siAdjustmentData += siNextByte - '0';
  } else {
    switch (siNextByte) {
    case HASH_MARK:
      siPos = SI_LINE_TRAILER;
      break;
    default:
      siRaiseErrorFlag();
      break;
    }
  }
}

void siActionForReportMark() {
  switch (siNextByte) {
  case HASH_MARK:
    siPos = SI_LINE_TRAILER;
    break;
  default:
    siRaiseErrorFlag();
    break;
  }
}

boolean siActionForLineTrailer() {
  switch (siNextByte) {
  case HASH_MARK:
    break;
  case 0x0a:
  case 0x0d:
    siPos = SI_LINE_SEPARATOR;
    return true;
  default:
    siRaiseErrorFlag();
    break;
  }
  return false;
}

void siNextLine() {
  while (Serial.available() > 0) {
    digitalWrite(NEXTLINE_LED, HIGH);
    siNextByte = Serial.read();
    // Hint: we will echo the received bytes, so that the host-side reader will
    // wake up early. Otherwise, the receiver lost data.
    Serial.print(siNextByte, HEX);
    Serial.print(" ");
    if (siErrorFlag) {
      siActionForError();
    } else {
      switch (siPos) {
      case SI_LINE_SEPARATOR:
        siActionForLineSeparator();
        break;
      case SI_LINE_HEADER:
        siActionForLineHeader();
        break;
      case SI_ADJUSTMENT_DATA:
        siActionForAdjustmentData();
        break;
      case SI_REPORT_MARK:
        siActionForReportMark();
        break;
      case SI_LINE_TRAILER:
        if (siActionForLineTrailer()) {
          digitalWrite(NEXTLINE_LED, LOW);
          return;
        }
        break;
      }
    }
    digitalWrite(NEXTLINE_LED, LOW);
  }
}

// The top-level setup method that invokes the specific setup methods
void setup() {
  setupLED();
  redLedOn();
  setupSerial();
  setupRTC();
  setupSD();
  setupSI();
  redLedOff();
}

#define APPEND_CSV_ITEM(item)                                                  \
  logFile.print(item);                                                         \
  Serial.print(item);

void doAdjustment() {
  greenLedOn();
  SOURCE_INFO_NL
  SOURCE_INFO_ENTERING("doAdjustment")
  Serial.println();
  Serial.print(F("adjustment data: "));
  Serial.println(siAdjustmentData);
  DateTime rtcDateTime = rtc.now();
  DateTime ntpDateTime = DateTime(siAdjustmentData);
  uint32_t rtcSeconds = rtcDateTime.unixtime();
  uint32_t ntpSeconds = siAdjustmentData;
  rtc.adjust(ntpDateTime);
  int dow = ntpDateTime.dayOfTheWeek();
  int adjSeconds = 0;
  if (ntpSeconds > rtcSeconds) {
    adjSeconds = ntpSeconds - rtcSeconds;
  } else {
    adjSeconds = rtcSeconds - ntpSeconds;
    adjSeconds = -adjSeconds;
  }
  boolean exists = SD.exists(SYNC_HISTORY_PATH);
  if (!exists) {
    SOURCE_INFO_BAILING_OUT("File " SYNC_HISTORY_PATH " does not exist !")
  }

  File logFile = SD.open(SYNC_HISTORY_PATH, FILE_WRITE);
  uint32_t oldLogSize = logFile.size();
  Serial.print(F("log file opened: "));
  Serial.println(SYNC_HISTORY_PATH);
  Serial.print(F("old log file size: "));
  Serial.println(oldLogSize);
  siPrintDataBoundaryLineAtStart();
  Serial.println(F("appended CSV line:"));
  // H,DOW,NTP_SECONDS,NTP_TEXT,RTC_SECONDS,RTC_TEXT,ADJ_SECONDS,OLD_LOG_SIZE
  APPEND_CSV_ITEM(F("D,"))
  switch (dow) {
  case 0:
    APPEND_CSV_ITEM(F("Sun,"))
    break;
  case 1:
    APPEND_CSV_ITEM(F("Mon,"))
    break;
  case 2:
    APPEND_CSV_ITEM(F("Tue,"))
    break;
  case 3:
    APPEND_CSV_ITEM(F("Wed,"))
    break;
  case 4:
    APPEND_CSV_ITEM(F("Thu,"))
    break;
  case 5:
    APPEND_CSV_ITEM(F("Fri,"))
    break;
  case 6:
    APPEND_CSV_ITEM(F("Sat,"))
    break;
  }
  APPEND_CSV_ITEM(ntpSeconds)
  APPEND_CSV_ITEM(F(","))
  APPEND_CSV_ITEM(ntpDateTime.timestamp())
  APPEND_CSV_ITEM(F(","))
  APPEND_CSV_ITEM(rtcSeconds)
  APPEND_CSV_ITEM(F(","))
  APPEND_CSV_ITEM(rtcDateTime.timestamp())
  APPEND_CSV_ITEM(F(","))
  APPEND_CSV_ITEM(adjSeconds)
  APPEND_CSV_ITEM(F(","))
  APPEND_CSV_ITEM(oldLogSize)
  logFile.println();
  Serial.println();
  logFile.flush();
  uint32_t newLogSize = logFile.size();
  logFile.close();
  Serial.print(F("new record appended to log file, new size: "));
  Serial.println(newLogSize);
  Serial.print(F("log file grew by "));
  Serial.print(newLogSize - oldLogSize);
  Serial.println(F(" bytes"));
  Serial.print(F("NTP data: "));
  Serial.print(ntpDateTime.timestamp());
  Serial.print(F(" ("));
  Serial.print(ntpSeconds);
  Serial.println(F(" seconds since the epoch)"));
  Serial.print(F("RTC data: "));
  Serial.print(rtcDateTime.timestamp());
  Serial.print(F(" ("));
  Serial.print(rtcSeconds);
  Serial.println(F(" seconds since the epoch)"));
  Serial.print(F("adjustment: "));
  Serial.print(adjSeconds);
  Serial.println(F(" seconds"));
  siPrintDataBoundaryLineAtEnd();
  SOURCE_INFO_LEAVING("doAdjustment")
  SOURCE_INFO_NL
  greenLedOff();
}

void doReport() {
  greenLedOn();
  SOURCE_INFO_NL
  SOURCE_INFO_ENTERING("doReport")
  Serial.println();
  siPrintDataBoundaryLineAtStart();
  File logFile = SD.open(SYNC_HISTORY_PATH, FILE_READ);
  while (true) {
    int ch = logFile.read();
    if (ch < 0) {
      break;
    }
    Serial.write(ch);
  }
  logFile.close();
  siPrintDataBoundaryLineAtEnd();
  SOURCE_INFO_LEAVING("doReport")
  SOURCE_INFO_NL
  greenLedOff();
}

void loop() {
  siNextLine();
  if (siAdjustmentRequested) {
    doAdjustment();
  }
  if (siReportRequested) {
    doReport();
  }
  siReset();
}
