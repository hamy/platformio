import serial
import sys
import time
import concurrentseriallogger as csl

nargs = len(sys.argv)
print('CLI argument count:',nargs)
portName = "/dev/ttyACM0"     
if nargs > 1:     
    portName = sys.argv[1]  
print('using serial port',portName)
baudRate = 115200
wordSize = serial.EIGHTBITS
parity = serial.PARITY_NONE
stopBits = serial.STOPBITS_ONE
ser = serial.Serial(portName,baudRate,wordSize,parity,stopBits)
print("Serial connection opened: ",ser)
time.sleep(1)

print('Creating logger...')
# Note: the nominal delay is 1 msec. However, the actual delay may vary by a far more larger amount
# since the serial reader thread may be paused for a larger interval.
logger = csl.ConcurrentSerialLogger(ser,idleDelay=0.001,logPath='reports/report-batch-log.txt')
print('logger running ? ',logger.isRunning())
logger.start()
print('logger running ? ',logger.isRunning())
time.sleep(5)

rec = '####R####\n'
print("Sending the following record via serial: ",rec)
ser.write(rec.encode(encoding="utf-8"))    
print("Record sent.")
time.sleep(10)

logger.stop()
ser.close()
print("Serial connection closed.")