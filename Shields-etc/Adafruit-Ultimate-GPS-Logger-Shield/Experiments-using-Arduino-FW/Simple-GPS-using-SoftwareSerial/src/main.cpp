#include <Arduino.h>
#include <Adafruit_GPS.h>
#include <LEDUtil.h>
#include <LoopUtil.h>
#include <SoftwareSerial.h>
#include <SourceInfo.h>

/**
 \mainpage A Simple GPS Demo using the Software Serial
 
 This sketch is a slightly modified version of the
 [GPS_SoftwareSerial_EchoTest](https://github.com/adafruit/
 Adafruit_GPS/blob/master/examples/GPS_SoftwareSerial_EchoTest/GPS_SoftwareSerial_EchoTest.ino)
 sketch that comes with the 
 [Adafruit Ultimate GPS Library](https://github.com/adafruit/Adafruit_GPS).
 
 The sketch uses the following libraries:
 - [Adafruit Ultimate GPS Library](https://github.com/adafruit/Adafruit_GPS)
 - [Arduino Software Serial Library](https://www.arduino.cc/en/Reference/SoftwareSerial)
 - My [LEDUtil Library](../../lib/LEDUtil/api-docs/html/index.html)
 - My [LoopUtil Library](../../lib/LoopUtil/api-docs/html/index.html)
 - My [SourceInfo Macros](../../lib/SourceInfo/api-docs/html/index.html)



 [Files](files.html)
 */
 
 
/**
 Baud rate for the communication between microcontroller and desktop.
 Caveat: The software serial used for the communication between the microcontroller and the GPS module will
 use another baud rate. 
*/
#define BAUD_RATE 115200

/**
 Initializes the serial connection to the desktop computer. After initialization,
 some meta data describing the current build are printed.
 */
void setupSerial()
{
  Serial.begin(BAUD_RATE);
  while (!Serial)
  {
    ;
  }
  delay(500);
  SOURCE_INFO_ENTERING("setupSerial")
  SOURCE_INFO_PRINT
  Serial.print(F("Baud rate: "));
  Serial.println(BAUD_RATE);
  SOURCE_INFO_LEAVING("setupSerial")
}

/**
 The built-in LED as LEDUtil::SimpleLED instance.
 */
SimpleLED led(LED_BUILTIN, true, true);

/**
 Initializes the built-in LED. Switches it off.
 */
void setupLED()
{
  SOURCE_INFO_ENTERING("setupLED")
  led.off();
  SOURCE_INFO_LEAVING("setupLED")
}

/**
 The pin that is used for the software serial RX connection.
 */
#define SOFTWARE_SERIAL_RX_PIN (8)

/**
 The pin that is used for the software serial TX connection.
 */
#define SOFTWARE_SERIAL_TX_PIN (7)

/**
 The software serial that is used for the connection between the microcontroller and the GPS module.
 */
SoftwareSerial softwareSerial(SOFTWARE_SERIAL_RX_PIN, SOFTWARE_SERIAL_TX_PIN);

#define PMTK_SET_NMEA_UPDATE_1HZ "$PMTK220,1000*1F"
#define PMTK_SET_NMEA_UPDATE_5HZ "$PMTK220,200*2C"
#define PMTK_SET_NMEA_UPDATE_10HZ "$PMTK220,100*2F"

// turn on only the second sentence (GPRMC)
#define PMTK_SET_NMEA_OUTPUT_RMCONLY \
  "$PMTK314,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*29"
// turn on GPRMC and GGA
#define PMTK_SET_NMEA_OUTPUT_RMCGGA \
  "$PMTK314,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*28"
// turn on ALL THE DATA
#define PMTK_SET_NMEA_OUTPUT_ALLDATA \
  "$PMTK314,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0*28"
// turn off output
#define PMTK_SET_NMEA_OUTPUT_OFF \
  "$PMTK314,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*28"

#define PMTK_Q_RELEASE "$PMTK605*31"

/**
 The length of the text buffer that is used for line aggregation. The buffer
 must hold a complete NMEA text line from the GPS module including the terminal NUL character. 
 */
#define TEXT_BUFFER_LENGTH (100)

/**
 The text buffer that is used for line aggregation. 
 */
char textBuffer[TEXT_BUFFER_LENGTH + 1];

/**
 The current number of characters in the text buffer that is used for line aggregation.
 This value also defines the next write position for appending future characters.
 */
int textBufferPos;

/**
 Inundates the text buffer with NUL characters. Also resets the text buffer write position.
 */
void clearTextBuffer()
{
  textBufferPos = 0;
  memset(textBuffer, 0, TEXT_BUFFER_LENGTH + 1);
  led.off();
}

/**
 Flushes the text buffer.
 - If it is not empty, the contents of the buffer is written to Serial.
 - In any case, the buffer is cleared.
 */
void flushTextBuffer()
{
  if (textBufferPos > 0)
  {
    Serial.println(textBuffer);
  };
  clearTextBuffer();
}

/**
 Processes an incoming character.
 - If the character is an EOL character, the text buffer is flushed and cleared.
 - Otherwise, the character is appended to the text buffer if there is enough
 space left.

 \param ch The incoming character.
 */
void appendTextBuffer(int ch)
{
  switch (ch)
  {
  case 0xd:
  case 0xa:
    flushTextBuffer();
    break;
  default:
    if (textBufferPos < TEXT_BUFFER_LENGTH)
    {
      textBuffer[textBufferPos++] = ch;
    }
    break;
  }
  if (textBufferPos > 0)
  {
    led.on();
  }
}

/**
 A pump for copying ingoing characters from the SoftwareSerial to the text buffer.
 \param msec The number of milliseconds that will be used for pumping data.
 */
void pumpSerial(uint32_t msec)
{
  uint32_t t1 = millis();
  uint32_t t2 = t1 + msec;
  if (t2 > t1)
  {
    while (millis() < t2)
    {
      while (softwareSerial.available())
      {
        int ch = softwareSerial.read();
        appendTextBuffer(ch);
      }
    }
  }
}

/**
 Setting up the software serial.
 */
void setupSoftwareSerial()
{
  SOURCE_INFO_ENTERING("setupSoftwareSerial")
  softwareSerial.begin(9600);
  delay(2000);
  softwareSerial.println(F(PMTK_SET_NMEA_OUTPUT_OFF));
  pumpSerial(1000);
  Serial.println(F("Get version!"));
  softwareSerial.println(F(PMTK_Q_RELEASE));
  pumpSerial(1000);
  softwareSerial.println(F(PMTK_SET_NMEA_OUTPUT_OFF));
  pumpSerial(1000);
  // you can send various commands to get it started
  // sotwareSerial.println(PMTK_SET_NMEA_OUTPUT_RMCGGA);
  softwareSerial.println(F(PMTK_SET_NMEA_OUTPUT_ALLDATA));
  softwareSerial.println(F(PMTK_SET_NMEA_UPDATE_1HZ));
  pumpSerial(1000);
  SOURCE_INFO_LEAVING("setupSoftwareSerial")
}

// The top-level setup method that invokes the specific setup methods
void setup()
{
  setupSerial();
  setupLED();
  setupSoftwareSerial();
}

void loop() { pumpSerial(100); }
