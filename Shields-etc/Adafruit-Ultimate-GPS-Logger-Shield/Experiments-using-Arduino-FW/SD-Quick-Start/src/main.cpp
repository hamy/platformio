#include <Arduino.h>
#include <SdFat.h>
#include <sdios.h>
#include <LEDUtil.h>
#include <LoopUtil.h>
#include <SourceInfo.h>

/**
 \mainpage A Quick Start Sample for Accessing the SD Card of the Ultimate GPS Logger Shield
 
 This sketch is a slightly modified version of the
 [GPS_SoftwareSerial_EchoTest](https://github.com/adafruit/
 Adafruit_GPS/blob/master/examples/GPS_SoftwareSerial_EchoTest/GPS_SoftwareSerial_EchoTest.ino)
 sketch that comes with the 
 [Adafruit Ultimate GPS Library](https://github.com/adafruit/Adafruit_GPS).
 
 The sketch uses the following libraries:
 - [Adafruit Ultimate GPS Library](https://github.com/adafruit/Adafruit_GPS)
 - [Arduino Software Serial Library](https://www.arduino.cc/en/Reference/SoftwareSerial)
 - My [LEDUtil Library](../../lib/LEDUtil/api-docs/html/index.html)
 - My [LoopUtil Library](../../lib/LoopUtil/api-docs/html/index.html)
 - My [SourceInfo Macros](../../lib/SourceInfo/api-docs/html/index.html)
 - [SdFat Library](../../lib/SdFat/api-docs/index.html)



 [Files](files.html)
 */

/**
 Baud rate for the communication between microcontroller and desktop.
 Caveat: The software serial used for the communication between the microcontroller and the GPS module will
 use another baud rate. 
*/
#define BAUD_RATE 115200

/**
 The pin used for SPI Chip Select.
*/
#define CHIP_SELECT_PIN (SS)

/**
 Initializes the serial connection to the desktop computer. After initialization,
 some meta data describing the current build are printed.
 */
void setupSerial()
{
  Serial.begin(BAUD_RATE);
  while (!Serial)
  {
    ;
  }
  delay(500);
  SOURCE_INFO_ENTERING("setupSerial")
  SOURCE_INFO_PRINT
  Serial.print(F("Baud rate: "));
  Serial.println(BAUD_RATE);
  SOURCE_INFO_LEAVING("setupSerial")
}

/**
 The built-in LED as LEDUtil::SimpleLED instance.
 */
SimpleLED led(LED_BUILTIN, true, true);

/**
 Initializes the built-in LED. Switches it off.
 */
void setupLED()
{
  SOURCE_INFO_ENTERING("setupLED")
  led.off();
  SOURCE_INFO_LEAVING("setupLED")
}

/**
 The file system object.
 */
SdFat sd;

/** 
 The root directory of the SD card.
 */
SdFile root;

/**
 Work file.
 */
SdFile file;

/**
 Initializes the SD card.
 */
void setupSD()
{
  SOURCE_INFO_ENTERING("setupSD")
  Serial.print(F("Chip select pin: "));
  Serial.println(CHIP_SELECT_PIN);
  int rc = sd.begin(CHIP_SELECT_PIN);
  Serial.print(F("sd.begin returned: "));
  Serial.println(rc);
  if (rc)
  {
    Serial.println(F("OK."));
  }
  else
  {
    SOURCE_INFO_BAILING_OUT("SD initialization failed !");
  }
  SOURCE_INFO_LEAVING("setupSD")
}

/**
 The top-level setup method that invokes the specific setup methods.
 */
void setup()
{
  setupSerial();
  setupLED();
  setupSD();
}

void loop() {}
