#include <Arduino.h>

#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <LoopUtil.h>
#include <RTClib.h>
#include <SourceInfo.h>
#include <Wire.h>

void setupLED() { pinMode(LED_BUILTIN, OUTPUT); }

// Variables and methods for the serial communication b
#define BAUD_RATE 115200

void setupSerial() {
  Serial.begin(BAUD_RATE);
  while (!Serial) {
    ;
  }
  delay(500);
  Serial.println(F("setupSerial: entering..."));
  Serial.print(F("Baud rate: "));
  Serial.println(BAUD_RATE);
  SOURCE_INFO_PRINT
  Serial.println(F("setupSerial: leaving..."));
  Serial.println();
}

// Variables and methods used for the RTC
RTC_PCF8523 rtc;

void setupRTC() {
  Serial.println(F("setupRTC: entering..."));
  Serial.println(F("remember: all RTC timestamps are based on UTC"));
  int rc = rtc.begin();
  Serial.print(F("begin() returned rc="));
  Serial.println(rc);
  if (!rc) {
    Serial.println(F("Catastrophic error: begin() failed ! Bailing out..."));
    while (1)
      ;
  }
  rc = rtc.initialized();
  Serial.print(F("initialized() returned rc="));
  Serial.println(rc);
  if (!rc) {
    Serial.println(F("Oops: RTC was not initialized so far !"));
    Serial.println(F("Adjusting RTC from compilation time stamp..."));
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    Serial.println(F("That's not very nice, but a kludge..."));
  }
  DateTime now = rtc.now();
  Serial.print(F("Initial DateTime value: "));
  Serial.println(now.timestamp());
  Serial.println(F("setupRTC: leaving..."));
  Serial.println();
}

Adafruit_SSD1306 display = Adafruit_SSD1306(128, 32, &Wire);

void setupDisplay() {
  Serial.println(F("setupDisplay: entering..."));
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C); // Address 0x3C for 128x32
  display.display();
  delay(1500);
  display.clearDisplay();
  display.display();
  display.setTextSize(1);
  display.setTextColor(SSD1306_WHITE);
  display.setCursor(0, 0);
  Serial.println(F("setupDisplay: leaving..."));
  Serial.println();
}

// The top-level setup method that invokes the specific setup methods
void setup() {
  setupLED();
  setupSerial();
  setupRTC();
  setupDisplay();
}

LoopUtil loopUtil(true, -1, 1000);

void loop() {
  loopUtil.loopStart();
  DateTime now = rtc.now();
  int second = now.second();
  digitalWrite(LED_BUILTIN, second & 1);
  Serial.print(F("Now is "));
  Serial.println(now.timestamp());
  display.clearDisplay();
  display.setTextSize(2);
  display.setTextColor(SSD1306_WHITE);
  display.setCursor(0, 0);
  display.println(now.timestamp(DateTime::TIMESTAMP_DATE));
  display.setCursor(0, 18);
  display.println(now.timestamp(DateTime::TIMESTAMP_TIME));
  display.display();
  loopUtil.loopEnd();
}
