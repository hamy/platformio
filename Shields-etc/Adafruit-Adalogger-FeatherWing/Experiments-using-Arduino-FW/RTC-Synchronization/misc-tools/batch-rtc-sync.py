import serial
import time

portName = "/dev/ttyUSB0"
baudRate = 115200
wordSize = serial.EIGHTBITS
parity = serial.PARITY_NONE
stopBits = serial.STOPBITS_ONE
ser = serial.Serial(portName,baudRate,wordSize,parity,stopBits)
print("Serial connection opened: ",ser)

def displayInputFromSerial():
    while ser.in_waiting>0:
        by = ser.read()
        print(by.decode(encoding="utf-8"),end='')

displayInputFromSerial()
now = time.time()
localtime = time.localtime(now)
nowAsctime = time.asctime(localtime)
nowAsText = '####' + str(int(now)) + '####\n'
print(nowAsctime," Sending the following record via serial: ",nowAsText)
ser.write(nowAsText.encode(encoding="utf-8"))    
print("Record sent.")
displayInputFromSerial()
time.sleep(1)
displayInputFromSerial()
ser.close()
print("Serial connection closed.")
 