import serial
import time
portName = "/dev/ttyUSB0"
baudRate = 115200
wordSize = serial.EIGHTBITS
parity = serial.PARITY_NONE
stopBits = serial.STOPBITS_ONE
ser = serial.Serial(portName, baudRate, wordSize, parity, stopBits)
maxDisplayLoops = 250
displayLoopDelay = 0.01
loopsWithDataTransfer = 0
loopsWithoutDataTransfer = 0
logFile = open("reports/rtc-adjustments.csv", "w")


def displayInputFromSerial():
    dataTransfer = False
    while ser.in_waiting > 0:
        by = ser.read()
        print(by.decode(encoding="utf-8"), end='')
        logFile.write(by.decode(encoding="utf-8"))
        dataTransfer = True
    return dataTransfer

displayInputFromSerial()
requestLine = '####R####\n'
print("request line: ", requestLine)
ser.write(requestLine.encode(encoding="utf-8"))
print("report from Huzzah32 / Adalogger FeatherWing:")
print()

for i in range(maxDisplayLoops):
    if displayInputFromSerial():
        loopsWithDataTransfer += 1
    else:
        loopsWithoutDataTransfer += 1
    time.sleep(displayLoopDelay)
  
ser.close()
logFile.close()
print()
print("remember: all timestamps use UTC (Zulu time)")
print()
print("display loops w/ transfer: ", loopsWithDataTransfer,
      ", w/o transfer: ", loopsWithoutDataTransfer)
