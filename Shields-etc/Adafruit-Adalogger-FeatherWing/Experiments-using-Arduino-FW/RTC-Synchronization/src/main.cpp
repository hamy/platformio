#include <Arduino.h>

#include "FS.h"
#include "RTClib.h"
#include "SD.h"
#include "SourceInfo.h"
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Wire.h>

// Variables and methods for the built-in LED
void switchOffLED() { digitalWrite(LED_BUILTIN, LOW); }

void switchOnLED() { digitalWrite(LED_BUILTIN, HIGH); }

void setupLED() {
  pinMode(LED_BUILTIN, OUTPUT);
  switchOffLED();
}

// Variables and methods for the serial communication between Huzzah32 and
// desktop.
#define BAUD_RATE 115200

void setupSerial() {
  Serial.begin(BAUD_RATE);
  while (!Serial) {
    ;
  }
  delay(500);
  Serial.println(F("setupSerial: entering..."));
  Serial.print(F("Baud rate: "));
  Serial.println(BAUD_RATE);
  SOURCE_INFO_PRINT
  Serial.println(F("setupSerial: leaving..."));
  Serial.println();
}

// Variables and methods used for the RTC
RTC_PCF8523 rtc;

void setupRTC() {
  Serial.println(F("setupRTC: entering..."));
  Serial.println(F("remember: all RTC timestamps are based on UTC"));
  int rc = rtc.begin();
  Serial.print(F("begin() returned rc="));
  Serial.println(rc);
  if (!rc) {
    Serial.println(F("Catastrophic error: begin() failed ! Bailing out..."));
    while (1)
      ;
  }
  rc = rtc.initialized();
  Serial.print(F("initialized() returned rc="));
  Serial.println(rc);
  if (!rc) {
    Serial.println(F("Oops: RTC was not initialized so far !"));
    Serial.println(F("Adjusting RTC from compilation time stamp..."));
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    Serial.println(F("That's not very nice, but a kludge..."));
  }
  DateTime now = rtc.now();
  Serial.print(F("Initial DateTime value: "));
  Serial.println(now.timestamp());
  Serial.println(F("setupRTC: leaving..."));
  Serial.println();
}

// Variables and methods used for the micro SD card
#define SD_SPI_CHIP_SELECT_PIN 33

int sdSizeInMB(uint64_t sizeInBytes) {
  return (int)(sizeInBytes / 1024 / 1024);
}

void setupSD() {
  Serial.println("setupSD: entering...");
  Serial.print(F("Using pin "));
  Serial.print(SD_SPI_CHIP_SELECT_PIN);
  Serial.println(F(" for SPI chip selection"));
  pinMode(SD_SPI_CHIP_SELECT_PIN, OUTPUT);
  if (!SD.begin(SD_SPI_CHIP_SELECT_PIN)) {
    Serial.println(F("Oops: begin()) failed !"));
    return;
  }
  uint8_t cardType = SD.cardType();
  Serial.print(F("cardType: "));
  Serial.print(cardType);
  Serial.print(F(" ("));
  switch (cardType) {
  case CARD_NONE:
    Serial.print(CARD_NONE);
    Serial.println(F(")"));
    return;
  case CARD_MMC:
    Serial.print(F("CARD_MMC"));
    break;
  case CARD_SD:
    Serial.print(F("CARD_SD"));
    break;
  case CARD_SDHC:
    Serial.print(F("CARD_SDHC"));
    break;
  default:
    Serial.print(F("UNKNOWN"));
    break;
  }
  Serial.println(F(")"));
  Serial.print(F("card size: "));
  Serial.print(sdSizeInMB(SD.cardSize()));
  Serial.println(F(" MB"));
  Serial.print(F("total size: "));
  Serial.print(sdSizeInMB(SD.totalBytes()));
  Serial.println(F(" MB"));
  Serial.print(F("used size: "));
  Serial.print(sdSizeInMB(SD.usedBytes()));
  Serial.println(F(" MB"));
  Serial.println(F("setupSD: leaving..."));
  Serial.println();
}

// Variables and methods used for the file system
#define FS_LOG_DIRECTORY_0 "/var"
#define FS_LOG_DIRECTORY_1 "/var/log"
#define FS_LOG_LEAF_NAME "rtc-adjustments.csv"
String fullyQualifiedLeafName;
File logFile;

void setUpFSmkdir(String name) {
  Serial.print(F("checking directory "));
  Serial.print(name);
  Serial.print(F(", exists ? "));
  boolean exists = SD.exists(name);
  Serial.println(exists ? F("yes") : F("no"));
  if (!exists) {
    boolean success = SD.mkdir(name);
    Serial.print(F("attempt to create directory "));
    Serial.print(name);
    Serial.print(F(" success ? "));
    Serial.println(success ? F("yes") : F("no"));
  }
}
void setupFS() {
  Serial.println(F("setupFS: entering..."));
  setUpFSmkdir(FS_LOG_DIRECTORY_0);
  setUpFSmkdir(FS_LOG_DIRECTORY_1);
  Serial.print(F("log leaf name: "));
  Serial.println(F(FS_LOG_LEAF_NAME));
  fullyQualifiedLeafName = FS_LOG_DIRECTORY_1 "/" FS_LOG_LEAF_NAME;
  Serial.print(F("fully qualified leaf name: "));
  Serial.print(fullyQualifiedLeafName);
  boolean exists = SD.exists(fullyQualifiedLeafName);
  Serial.print(F(", exists ? "));
  Serial.println(exists ? F("yes") : F("no"));
  if (!exists) {
    Serial.println(F("creating header record"));
    logFile = SD.open(fullyQualifiedLeafName, FILE_WRITE);
    logFile.println(F("H,DOW,NTP_SECONDS,NTP_TEXT,RTC_SECONDS,RTC_TEXT,ADJ_"
                      "SECONDS,OLD_LOG_SIZE"));
    logFile.flush();
    logFile.close();
  }
  logFile = SD.open(fullyQualifiedLeafName);
  Serial.print(F("intial log file size: "));
  Serial.println(logFile.size());
  logFile.close();
  Serial.println(F("setupFS: leaving..."));
  Serial.println();
}

// Variables and methods for serial input
// THIS SECTION NEEDS IMPROVEMENT !!!
#define SI_LINE_SEPARATOR 0
#define SI_LINE_HEADER 1
#define SI_ADJUSTMENT_DATA 2
#define SI_REPORT_MARK 3
#define SI_LINE_TRAILER 4
uint8_t siPos = SI_LINE_SEPARATOR;
uint8_t siNextByte;
uint32_t siAdjustmentData = 0;
boolean siErrorFlag = false;
boolean siAdjustmentRequested = false;
boolean siReportRequested = false;
#define HASH_MARK ('#')
#define REPORT_MARK ('R')

void siReset() {
  siPos = SI_LINE_SEPARATOR;
  siErrorFlag = false;
  siAdjustmentData = 0;
  siAdjustmentRequested = false;
  siReportRequested = false;
}

void siRaiseErrorFlag() {
  siErrorFlag = true;
  siAdjustmentData = 0;
  siAdjustmentRequested = false;
  siReportRequested = false;
}

void setupSI() {
  Serial.println(F("setupSI: entering..."));
  siReset();
  Serial.println(F("setupSI: leaving..."));
}

void siActionForError() {
  switch (siNextByte) {
  case 0x0a:
  case 0x0d:
    siReset();
    break;
  }
}

void siActionForLineSeparator() {
  switch (siNextByte) {
  case 0x0a:
  case 0x0d:
    break;
  case HASH_MARK:
    siPos = SI_LINE_HEADER;
    break;
  default:
    siRaiseErrorFlag();
    break;
  }
}

void siActionForLineHeader() {
  if (isDigit(siNextByte)) {
    siPos = SI_ADJUSTMENT_DATA;
    siAdjustmentData = siNextByte - '0';
    siAdjustmentRequested = true;
  } else {
    switch (siNextByte) {
    case HASH_MARK:
      break;
    case REPORT_MARK:
      siPos = SI_REPORT_MARK;
      siReportRequested = true;
      break;
    default:
      siRaiseErrorFlag();
      break;
    }
  }
}

void siActionForAdjustmentData() {
  if (isDigit(siNextByte)) {
    siAdjustmentData *= 10;
    siAdjustmentData += siNextByte - '0';
  } else {
    switch (siNextByte) {
    case HASH_MARK:
      siPos = SI_LINE_TRAILER;
      break;
    default:
      siRaiseErrorFlag();
      break;
    }
  }
}

void siActionForReportMark() {
  switch (siNextByte) {
  case HASH_MARK:
    siPos = SI_LINE_TRAILER;
    break;
  default:
    siRaiseErrorFlag();
    break;
  }
}

boolean siActionForLineTrailer() {
  switch (siNextByte) {
  case HASH_MARK:
    break;
  case 0x0a:
  case 0x0d:
    siPos = SI_LINE_SEPARATOR;
    return true;
  default:
    siRaiseErrorFlag();
    break;
  }
  return false;
}

void siNextLine() {
  while (Serial.available() > 0) {
    siNextByte = Serial.read();
    if (siErrorFlag) {
      siActionForError();
    } else {
      switch (siPos) {
      case SI_LINE_SEPARATOR:
        siActionForLineSeparator();
        break;
      case SI_LINE_HEADER:
        siActionForLineHeader();
        break;
      case SI_ADJUSTMENT_DATA:
        siActionForAdjustmentData();
        break;
      case SI_REPORT_MARK:
        siActionForReportMark();
        break;
      case SI_LINE_TRAILER:
        if (siActionForLineTrailer()) {
          return;
        }
        break;
      }
    }
  }
}

Adafruit_SSD1306 display = Adafruit_SSD1306(128, 32, &Wire);

void setupDisplay() {
  Serial.println(F("setupDisplay: entering..."));
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C); // Address 0x3C for 128x32
  display.display();
  delay(1500);
  display.clearDisplay();
  display.display();
  display.setTextSize(1);
  display.setTextColor(SSD1306_WHITE);
  display.setCursor(0, 0);
  Serial.println(F("setupDisplay: leaving..."));
  Serial.println();
}

// The top-level setup method that invokes the specific setup methods
void setup() {
  setupLED();
  switchOnLED();
  setupSerial();
  setupRTC();
  setupSD();
  setupFS();
  setupSI();
  setupDisplay();
  switchOffLED();
}

void doAdjustment() {
  switchOnLED();
  Serial.print(F("doAdjustment: "));
  Serial.println(siAdjustmentData);
  DateTime rtcDateTime = rtc.now();
  DateTime ntpDateTime = DateTime(siAdjustmentData);
  uint32_t rtcSeconds = rtcDateTime.unixtime();
  uint32_t ntpSeconds = siAdjustmentData;
  rtc.adjust(ntpDateTime);
  int dow = ntpDateTime.dayOfTheWeek();
  // H,DOW,NTP_SECONDS,NTP_TEXT,RTC_SECONDS,RTC_TEXT,ADJ_SECONDS,OLD_LOG_SIZE
  int adjSeconds = 0;
  if (ntpSeconds > rtcSeconds) {
    adjSeconds = ntpSeconds - rtcSeconds;
  } else {
    adjSeconds = rtcSeconds - ntpSeconds;
    adjSeconds = -adjSeconds;
  }
  logFile = SD.open(fullyQualifiedLeafName, FILE_APPEND);
  uint32_t oldLogSize = logFile.size();
  Serial.print(F("log file opened: "));
  Serial.println(fullyQualifiedLeafName);
  logFile.print(F("D,"));
  switch (dow) {
  case 0:
    logFile.print(F("Sun,"));
    break;
  case 1:
    logFile.print(F("Mon,"));
    break;
  case 2:
    logFile.print(F("Tue,"));
    break;
  case 3:
    logFile.print(F("Wed,"));
    break;
  case 4:
    logFile.print(F("Thu,"));
    break;
  case 5:
    logFile.print(F("Fri,"));
    break;
  case 6:
    logFile.print(F("Sat,"));
    break;
  }
  logFile.print(ntpSeconds);
  logFile.print(F(","));
  logFile.print(ntpDateTime.timestamp());
  logFile.print(F(","));
  logFile.print(rtcSeconds);
  logFile.print(F(","));
  logFile.print(rtcDateTime.timestamp());
  logFile.print(F(","));
  logFile.print(adjSeconds);
  logFile.print(F(","));
  logFile.println(oldLogSize);
  logFile.flush();
  uint32_t newLogSize = logFile.size();
  logFile.close();
  Serial.print(F("new record appended to log file, new size: "));
  Serial.println(newLogSize);
  Serial.print(F("NTP data: "));
  Serial.print(ntpDateTime.timestamp());
  Serial.print(F(" ("));
  Serial.print(ntpSeconds);
  Serial.println(F(" seconds since the epoch)"));
  Serial.print(F("RTC data: "));
  Serial.print(rtcDateTime.timestamp());
  Serial.print(F(" ("));
  Serial.print(rtcSeconds);
  Serial.println(F(" seconds since the epoch)"));
  Serial.print(F("adjustment: "));
  display.println(F("Caveat: using UTC !"));
  display.println(ntpDateTime.timestamp());
  display.print(F("Adjustment: "));
  Serial.print(adjSeconds);
  display.print(adjSeconds);
  Serial.println(F(" seconds"));
  display.println(F(" sec."));
  display.print(F("Log file: "));
  display.print(newLogSize);
  display.print(F(" bytes"));
  display.display();
  switchOffLED();
}

void doReport() {
  switchOnLED();
  logFile = SD.open(fullyQualifiedLeafName, FILE_READ);
  while (true) {
    int ch = logFile.read();
    if (ch < 0) {
      break;
    }
    Serial.write(ch);
  }
  logFile.close();
  switchOffLED();
}

void loop() {
  siNextLine();
  if (siAdjustmentRequested) {
    doAdjustment();
  }
  if (siReportRequested) {
    doReport();
  }
  siReset();
}
