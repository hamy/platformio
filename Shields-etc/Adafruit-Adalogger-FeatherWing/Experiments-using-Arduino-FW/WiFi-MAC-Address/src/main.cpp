#include <Arduino.h>
#include <SourceInfo.h>
#include <WiFi.h>

// Variables and methods for the serial communication between Huzzah32 and
// desktop.
#define BAUD_RATE 115200

void setupSerial() {
  Serial.begin(BAUD_RATE);
  while (!Serial) {
    ;
  }
  delay(500);
  SOURCE_INFO_ENTERING("setupSerial");
  Serial.print(F("Baud rate: "));
  Serial.println(BAUD_RATE);
  SOURCE_INFO_PRINT
  SOURCE_INFO_LEAVING("setupSerial");
  SOURCE_INFO_NL
}

void setupWiFi() {
  SOURCE_INFO_ENTERING("setupWiFi");
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
  WiFi.mode(WIFI_MODE_STA);
  Serial.println(WiFi.macAddress());
  digitalWrite(LED_BUILTIN, LOW);
  SOURCE_INFO_LEAVING("setupWiFi");
}

// The top-level setup method that invokes the specific setup methods
void setup() {
  setupSerial();
  setupWiFi();
}

void loop() {}
