#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Arduino.h>
#include <SourceInfo.h>
#include <WiFi.h>
#include <Wire.h>

#ifndef AP_SSID
#define AP_SSID "Hamy-Adafruit-Huzzah32"
#endif

#ifndef AP_PASS
#define AP_PASS "top-secret"
#endif

const char *ssid = AP_SSID;
const char *pass = AP_PASS;
IPAddress myIP = IPAddress(10, 0, 32, 1);
IPAddress myMask = IPAddress(255, 255, 255, 0);

// Variables and methods for the serial communication between Huzzah32 and
// desktop.
#define BAUD_RATE 115200

void setupSerial() {
  Serial.begin(BAUD_RATE);
  while (!Serial) {
    ;
  }
  delay(500);
  SOURCE_INFO_ENTERING("setupSerial");
  Serial.print(F("Baud rate: "));
  Serial.println(BAUD_RATE);
  SOURCE_INFO_PRINT
  SOURCE_INFO_LEAVING("setupSerial");
  SOURCE_INFO_NL
}

WiFiServer server(80);

void setupWiFi() {
  SOURCE_INFO_ENTERING("setupWiFi")
  Serial.print(F("SSID: "));
  Serial.println(ssid);
  Serial.print(F("PASS: "));
  Serial.print(strlen(pass));
  Serial.println(F(" characters"));
  WiFi.mode(WIFI_AP);
  Serial.print(F("mode set to WIFI_AP="));
  Serial.println(WIFI_AP);
  WiFi.softAP(ssid, pass);
  Serial.println(F("started soft AP, waiting..."));
  delay(1000);
  WiFi.softAPConfig(myIP, myIP, myMask);
  Serial.println(F("soft AP was configured:"));
  Serial.print(F("IP address: "));
  Serial.print(myIP);
  Serial.print(F(", mask: "));
  Serial.println(myMask);
  IPAddress myIP2 = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(myIP2);
  server.begin();
  SOURCE_INFO_LEAVING("setupWiFi")
  Serial.println();
}

Adafruit_SSD1306 display = Adafruit_SSD1306(128, 32, &Wire);

void setupDisplay() {
  Serial.println(F("setupDisplay: entering..."));
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C); // Address 0x3C for 128x32
  display.display();
  delay(1500);
  display.clearDisplay();
  display.display();
  display.setTextSize(1);
  display.setTextColor(SSD1306_WHITE);
  display.setCursor(0, 0);
  Serial.println(F("setupDisplay: leaving..."));
  Serial.println();
}

// The top-level setup method that invokes the specific setup methods
void setup() {
  setupSerial();
  setupWiFi();
  setupDisplay();
}

int answerCount = 0;

void loop() {
  yield();
  WiFiClient client = server.available(); // Listen for incoming clients

  if (client) { // If a new client connects,
    Serial.println();
    Serial.println(
        F("#### New Client ####")); // print a message out in the serial port
    while (client.connected()) {    // loop while the client's connected
      while (client.available()) {  // if there's bytes to read from the client,
        char c = client.read();     // read a byte, then
        Serial.write(c);            // print it out the serial monitor
        delay(1);
        yield();
      }
      Serial.println(F("Received header, now sending answer..."));
      answerCount++;
      display.clearDisplay();
      display.setCursor(0, 0);
      display.print(F("answer count: "));
      display.println(answerCount);
      display.print(F("R: "));
      display.print(client.remotePort());
      display.print(F("/"));
      display.println(client.remoteIP());
      display.print(F("L: "));
      display.print(client.localPort());
      display.print(F("/"));
      display.println(client.localIP());
      display.print(F("mac:"));
      display.println(WiFi.macAddress());
      display.display();
      client.print(F("HTTP/1.1 200 OK\r\n"));
      client.print(F("Content-type:text/plain\r\n"));
      client.print(F("Connection: close\r\n"));
      client.print("\r\n");
      client.print(F("Hello #"));
      client.print(answerCount);
      client.print(F(" from "));
      client.print(myIP);
      client.print(F(" via "));
      client.println(ssid);
      client.println();
      client.flush();
      Serial.println(F("all sent."));
      yield();
      // Close the connection
      client.stop();
      // elay(400);
      yield();
    }
    Serial.println(F("Client disconnected."));
    Serial.println();
  } else {
    Serial.print(F("."));
    for (int i = 0; i < 500; i++) {
      yield();
      delay(1);
    }
  }
}
