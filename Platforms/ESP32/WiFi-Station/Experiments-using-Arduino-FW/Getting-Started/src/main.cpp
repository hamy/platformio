#include <Arduino.h>
#include <LoopUtil.h>
#include <SourceInfo.h>
#include <WiFi.h>

LoopUtil loopUtil(true, -1, 1000);

// Variables and methods for the serial communication
#define BAUD_RATE 115200

void setupSerial()
{
  Serial.begin(BAUD_RATE);
  while (!Serial)
  {
    ;
  }
  delay(500);
  SOURCE_INFO_ENTERING("setupSerial")
  Serial.print(F("Baud rate: "));
  Serial.println(BAUD_RATE);
  SOURCE_INFO_PRINT
  SOURCE_INFO_LEAVING("setupSerial")
  SOURCE_INFO_NL
}

void setupLED()
{
  SOURCE_INFO_ENTERING("setupLED")
  Serial.print(F("Baud rate: "));
  pinMode(LED_BUILTIN, OUTPUT);
  SOURCE_INFO_LEAVING("setupLED")
  SOURCE_INFO_NL
}

#ifndef AP_SSID
#define AP_SSID "iot-mpsk-test"
#endif

#ifndef AP_PASS
#define AP_PASS "top-secret"
#endif

const char *ssid = AP_SSID;
const char *pass = AP_PASS;
//IPAddress myIP = IPAddress(10, 0, 32, 1);
//IPAddress myMask = IPAddress(255, 255, 255, 0);

WiFiServer server(80);

void setupWiFi()
{
  SOURCE_INFO_ENTERING("setupWiFi")
  digitalWrite(LED_BUILTIN, HIGH);
  Serial.print(F("SSID: "));
  Serial.println(ssid);
  Serial.print(F("PASS: "));
  Serial.print(strlen(pass));
  Serial.println(F(" characters"));
  WiFi.mode(WIFI_STA);
  Serial.print(F("mode set to WIFI_STA="));
  Serial.println(WIFI_STA);
  WiFi.begin(ssid, pass);
  Serial.println(F("started soft AP, waiting..."));
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println();
  Serial.println(F("connected."));
  Serial.print(F("IP address: "));
  Serial.println(WiFi.localIP());
  Serial.print(F(", mask: "));
  server.begin();
  digitalWrite(LED_BUILTIN, LOW);
  WiFi.printDiag(Serial);
  SOURCE_INFO_LEAVING("setupWiFi")
  SOURCE_INFO_NL
}

// The top-level setup method that invokes the specific setup methods
void setup()
{
  setupSerial();
  setupLED();
  setupWiFi();
}

int answerCount = 0;

#define cprt(arg) client.print(arg)
#define cprtF(arg) cprt(F(arg))
#define cprtFln(arg) cprtF(arg "\r\n")
#define cprtRow(lhs, rhs)                                    \
  cprtF("<tr><td> &nbsp; " lhs " &nbsp; </td><td> &nbsp; "); \
  cprt(rhs);                                                 \
  cprtFln(" &nbsp; </td></tr>")

void loop()
{
  yield();
  WiFiClient client = server.available(); // Listen for incoming clients

  if (client)
  { // If a new client connects,
    long msec1 = millis();
    digitalWrite(LED_BUILTIN, HIGH);
    Serial.println(F("New Client.")); // print a message out in the serial port
    while (client.connected())
    { // loop while the client's connected
      while (client.available())
      {                         // if there's bytes to read from the client,
        char c = client.read(); // read a byte, then
        Serial.write(c);        // print it out the serial monitor
        delay(1);
        yield();
      }
      Serial.println(F("Received header, now sending answer..."));
      WiFi.printDiag(Serial);

      answerCount++;
      cprtFln("HTTP/1.1 200 OK");
      cprtFln("Content-type:text/html");
      cprtFln("Connection: close");
      cprtFln("");
      cprtFln("<html>");
      cprtFln("<head>");
      cprtFln("<title>ESP32 - Getting Started</title>");
      cprtFln("</head>");
      cprtFln("<body>");
      cprtFln("<h2>ESP32 - Getting Started</h2>");
      cprtFln("</body>");
      cprtFln("<p/>");
      cprtFln("<table border='1'>");
      cprtFln("<tr bgcolor='#ffff00'><td> &nbsp; Variable &nbsp; </td><td> &nbsp; Value &nbsp; </td></tr>");
      cprtRow("Source file", SOURCE_INFO_FILE);
      cprtRow("Last modification", SOURCE_INFO_MODIFIED_AT);
      cprtRow("Compilation date", SOURCE_INFO_COMPILED_AT);
      cprtRow("millis()", millis());
      cprtRow("Answer count", answerCount);
      cprtRow("MAC address", WiFi.macAddress());
      cprtRow("SSID", ssid);
      cprtRow("Local port", client.localPort());
      cprtRow("Local IP address", client.localIP());
      cprtRow("Remote port", client.remotePort());
      cprtRow("Remote IP address", client.remoteIP());
      long msec2 = millis();
      cprtRow("duration [msec]", msec2 - msec1);
      cprtFln("</table>");
      cprtFln("<p/>");
      cprtFln("</html>");
      yield();
      // Close the connection
      client.stop();
      digitalWrite(LED_BUILTIN, LOW);
      //delay(400);
      yield();
    }
    Serial.println(F("Client disconnected."));
    Serial.println();
  }
  else
  {
    Serial.println(F("No client available"));
    for (int i = 0; i < 500; i++)
    {
      yield();
      delay(1);
    }
  }
}
