#include <Arduino.h>
#include <LoopUtil.h>
#include <SourceInfo.h>
#include <OneWire.h>
#include <DallasTemperature.h>

LoopUtil loopUtil(true, -1, 1000);

// Variables and methods for the serial communication
#define BAUD_RATE 115200

void setupSerial()
{
  Serial.begin(BAUD_RATE);
  while (!Serial)
  {
    ;
  }
  delay(500);
  SOURCE_INFO_ENTERING("setupSerial")
  Serial.print(F("Baud rate: "));
  Serial.println(BAUD_RATE);
  SOURCE_INFO_PRINT
  SOURCE_INFO_LEAVING("setupSerial")
  SOURCE_INFO_NL
}

void setupLED()
{
  SOURCE_INFO_ENTERING("setupLED")
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);
  SOURCE_INFO_LEAVING("setupLED")
  SOURCE_INFO_NL
}

// GPIO where the DS18B20 is connected to
const int oneWireBus = 4;

// Setup a oneWire instance to communicate with any OneWire devices
OneWire oneWire(oneWireBus);

// Pass our oneWire reference to Dallas Temperature sensor
DallasTemperature sensors(&oneWire);

void setupSensors()
{
  SOURCE_INFO_ENTERING("setupSensors")
  byte addr[8];
  //oneWire.reset();
  delay(250);
  oneWire.reset_search();
  delay(250);
  int devCount = 0;
  Serial.println(F("device query"));
  while (oneWire.search(addr))
  {
    devCount++;
    Serial.print(F("device #"));
    Serial.print(devCount);
    Serial.print(F(" found:"));
    for (int i = 0; i < 8; i++)
    {
      Serial.print(" ");
      Serial.print(addr[i], HEX);
    }
    Serial.println();
  }
  delay(250);
  oneWire.reset();
  delay(250);
  oneWire.reset_search();
  delay(250);
  sensors.begin();
  SOURCE_INFO_LEAVING("setupSensors")
  SOURCE_INFO_NL
}

// The top-level setup method that invokes the specific setup methods
void setup()
{
  setupSerial();
  setupLED();
  setupSensors();
}

void loop()
{
  loopUtil.loopStart();
  sensors.requestTemperatures();
  float temperatureC = sensors.getTempCByIndex(0);
  float temperatureF = sensors.getTempFByIndex(0);
  Serial.print(temperatureC);
  Serial.println("ºC");
  Serial.print(temperatureF);
  Serial.println("ºF");

  loopUtil.loopEnd();
}