#include <Arduino.h>
#include <LoopUtil.h>
#include <SourceInfo.h>
#include <SparkFun_APDS9960.h>

LoopUtil loopUtil(false, -1, 10);

// Variables and methods for the serial communication
#define BAUD_RATE 115200

void setupSerial()
{
  Serial.begin(BAUD_RATE);
  while (!Serial)
  {
    ;
  }
  delay(500);
  SOURCE_INFO_ENTERING("setupSerial")
  Serial.print(F("Baud rate: "));
  Serial.println(BAUD_RATE);
  SOURCE_INFO_PRINT
  SOURCE_INFO_LEAVING("setupSerial")
  SOURCE_INFO_NL
}

void setupLED()
{
  SOURCE_INFO_ENTERING("setupLED")
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);
  SOURCE_INFO_LEAVING("setupLED")
  SOURCE_INFO_NL
}

#define SENSOR_MODE_LIGHT 0
#define SENSOR_MODE_PROXIMITY 1
#define SENSOR_MODE_GESTURE 2
#define SENSOR_MODE_MIN SENSOR_MODE_LIGHT
#define SENSOR_MODE_MAX SENSOR_MODE_GESTURE
int sensorMode = SENSOR_MODE_MIN;

SparkFun_APDS9960 apds = SparkFun_APDS9960();
boolean allModesDisabled = true;

void disableAllModes()
{
  allModesDisabled = true;
}

void enableOneMode()
{
  if (allModesDisabled)
  {
    apds.disableGestureSensor();
    apds.disableLightSensor();
    apds.disableProximitySensor();
    yield();
    delay(100);
    allModesDisabled = false;
    switch (sensorMode)
    {
    case SENSOR_MODE_LIGHT:
      apds.enableLightSensor(false);
      break;
    case SENSOR_MODE_PROXIMITY:
      apds.enableProximitySensor(false);
      break;
    case SENSOR_MODE_GESTURE:
      apds.setGestureGain(GGAIN_1X);
      apds.enableGestureSensor();
      break;
    }
  }
}

void setupSensors()
{
  SOURCE_INFO_ENTERING("setupSensors")
  if (apds.init())
  {
    Serial.println(F("APDS-9960 initialization complete"));
  }
  else
  {
    SOURCE_INFO_BAILING_OUT("APDS-9960 initialization failed")
  }
  disableAllModes();
  SOURCE_INFO_LEAVING("setupSensors")
  SOURCE_INFO_NL
}

void lightSensor()
{
  uint16_t ambient, red, green, blue;
  //SOURCE_INFO_ENTERING("lightSensor")
  boolean ambientOK = apds.readAmbientLight(ambient);
  boolean redOK = apds.readRedLight(red);
  boolean greenOK = apds.readGreenLight(green);
  boolean blueOK = apds.readBlueLight(blue);
  if (ambientOK && redOK && greenOK && blueOK)
  {
    Serial.print(F("RGB: "));
    Serial.print(red);
    Serial.print(F("/"));
    Serial.print(green);
    Serial.print(F("/"));
    Serial.print(blue);
    Serial.print(F(", ambient: "));
    Serial.println(ambient);
  }
  else
  {
    Serial.println("no ambi li");
  }
  //SOURCE_INFO_LEAVING("lightSensor")
}

void proximitySensor()
{
  uint8_t proximity;
  //  SOURCE_INFO_ENTERING("proximitySensor")
  boolean proximityOK = apds.readProximity(proximity);
  if (proximityOK)
  {
    Serial.print(F("proximity value: "));
    Serial.println(proximity);
  }
  else
  {
    Serial.println(F("no proximity data available"));
  }
  //SOURCE_INFO_LEAVING("proximitySensor")
}

int retryGestureReading() {
  int maxAttempts = 100;
  int delayBetweenAttempts = 1;
  for (int attempt=0; attempt<maxAttempts; attempt++) {
    if (apds.isGestureAvailable()) {
      return apds.readGesture();
    }
    delay(delayBetweenAttempts);
  }
  return 0;
}

void gestureSensor()
{
  //SOURCE_INFO_ENTERING("gestureSensor")
  if (apds.isGestureAvailable())
  {
    switch (apds.readGesture())
    {
    case DIR_UP:
      Serial.println("UP");
      break;
    case DIR_DOWN:
      Serial.println("DOWN");
      break;
    case DIR_LEFT:
      Serial.println("LEFT");
      break;
    case DIR_RIGHT:
      Serial.println("RIGHT");
      break;
    case DIR_NEAR:
      Serial.println("NEAR");
      break;
    case DIR_FAR:
      Serial.println("FAR");
      break;
    default:
      Serial.println("NONE");
    }

    //SOURCE_INFO_LEAVING("gestureSensor")
  } else {
    //Serial.println(F("No gesture data"));
  }
}

const int LEFT_BUTTON = 18;
const int RIGHT_BUTTON = 19;
const uint32_t BUTTON_TIMEOUT = 200;

uint32_t leftButtonTime = 0;
uint32_t rightButtonTime = 0;

void IRAM_ATTR detectLeftButtonFired()
{
  uint32_t now = millis();
  if (now > leftButtonTime + BUTTON_TIMEOUT)
  {
    if (sensorMode == SENSOR_MODE_MIN)
    {
      sensorMode = SENSOR_MODE_MAX;
    }
    else
    {
      sensorMode--;
    }
    disableAllModes();
    leftButtonTime = now;
  }
}

void IRAM_ATTR detectRightButtonFired()
{
  uint32_t now = millis();
  if (now > rightButtonTime + BUTTON_TIMEOUT)
  {
    if (sensorMode == SENSOR_MODE_MAX)
    {
      sensorMode = SENSOR_MODE_MIN;
    }
    else
    {
      sensorMode++;
    }
    disableAllModes();
    rightButtonTime = now;
  }
}

void setupButtons()
{
  SOURCE_INFO_ENTERING("setupButtons")
  attachInterrupt(digitalPinToInterrupt(LEFT_BUTTON), detectLeftButtonFired, FALLING);
  attachInterrupt(digitalPinToInterrupt(RIGHT_BUTTON), detectRightButtonFired, FALLING);
  SOURCE_INFO_LEAVING("setupButtons")
  SOURCE_INFO_NL
}

// The top-level setup method that invokes the specific setup methods
void setup()
{
  setupSerial();
  setupLED();
  setupButtons();
  setupSensors();
}

void loop()
{
  yield();
  loopUtil.loopStart();
  enableOneMode();
  switch (sensorMode)
  {
  case SENSOR_MODE_LIGHT:
    lightSensor();
    break;
  case SENSOR_MODE_PROXIMITY:
    proximitySensor();
    break;
  case SENSOR_MODE_GESTURE:
    gestureSensor();
    break;
  }
  yield();
  loopUtil.loopEnd();
  yield();
}