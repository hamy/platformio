#include <Arduino.h>
#include <LEDUtil.h>
#include <LoopUtil.h>
#include <SourceInfo.h>
#include <WiFi.h>
#include <esp_now.h>

// Variables and methods for the serial communication
// between the microcontroller and the desktop.
#define BAUD_RATE 115200

SimpleLED simpleLED(LED_BUILTIN, true, false);
LoopUtil loopUtil(true, -1, 1000);

void setupSerial()
{
  Serial.begin(BAUD_RATE);
  while (!Serial)
  {
    ;
  }
  delay(500);
  SOURCE_INFO_ENTERING("setupSerial");
  Serial.print(F("Baud rate: "));
  Serial.println(BAUD_RATE);
  SOURCE_INFO_PRINT
  SOURCE_INFO_LEAVING("setupSerial");
  SOURCE_INFO_NL
}

void setupWireless()
{
  SOURCE_INFO_ENTERING("setupWireless");
  simpleLED.on();
  WiFi.mode(WIFI_MODE_STA);
  Serial.print(F("my own MAC address: "));
  Serial.println(WiFi.macAddress());
  uint32_t version;
  int rc = esp_now_get_version(&version);
  Serial.print(F("getting ESP-NOW version: rc="));
  Serial.print(rc);
  Serial.print(F(", version="));
  Serial.println(version);
  rc = esp_now_init();
  Serial.print(F("initializing ESP-NOW: rc="));
  Serial.println(rc);
  esp_now_peer_num_t peer_num;
  rc = esp_now_get_peer_num(&peer_num);
  Serial.print(F("getting initial number of peers: rc="));
  Serial.print(rc);
  Serial.print(F(", total_num="));
  Serial.print(peer_num.total_num);
  Serial.print(F(", encrypt_num="));
  Serial.println(peer_num.encrypt_num);
  simpleLED.off();
  SOURCE_INFO_LEAVING("setupWireless");
}

// The top-level setup method that invokes the specific setup methods
void setup()
{
  setupSerial();
  setupWireless();
}

void loop()
{
  loopUtil.loopStart();
  SOURCE_INFO_BAILING_OUT("not yet implemented")
  loopUtil.loopEnd();
}
