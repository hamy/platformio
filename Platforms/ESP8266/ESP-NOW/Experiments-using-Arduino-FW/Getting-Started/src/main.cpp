#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <SourceInfo.h>
#include <LoopUtil.h>
#include <espnow.h>

LoopUtil loopUtil(true, -1, 1000);

// Variables and methods for the serial communication
// between the microcontroller and the desktop.
#define BAUD_RATE 115200

void setupSerial()
{
  Serial.begin(BAUD_RATE);
  while (!Serial)
  {
    ;
  }
  delay(500);
  SOURCE_INFO_ENTERING("setupSerial");
  Serial.print(F("Baud rate: "));
  Serial.println(BAUD_RATE);
  SOURCE_INFO_PRINT
  SOURCE_INFO_LEAVING("setupSerial");
  SOURCE_INFO_NL
}

void setupWireless()
{
  SOURCE_INFO_ENTERING("setupWireless");
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
  WiFi.mode(WIFI_STA);
  Serial.print(F("my own MAC address: "));
  Serial.println(WiFi.macAddress());
  int rc = esp_now_init();
  Serial.print(F("initializing ESP-NOW: rc="));
  Serial.println(rc);
  uint8_t all_cnt, encrypt_cnt;
  rc = esp_now_get_cnt_info(&all_cnt, &encrypt_cnt);
  ;
  Serial.print(F("getting initial number of peers: rc="));
  Serial.print(rc);
  Serial.print(F(", all_cnt="));
  Serial.print(all_cnt);
  Serial.print(F(", encrypt_cnt="));
  Serial.println(encrypt_cnt);
  digitalWrite(LED_BUILTIN, LOW);
  SOURCE_INFO_LEAVING("setupWireless");
}

// The top-level setup method that invokes the specific setup methods
void setup()
{
  setupSerial();
  setupWireless();
}

void loop()
{
  loopUtil.loopStart();
  SOURCE_INFO_BAILING_OUT("no loop code so far !")
  loopUtil.loopEnd();
}
