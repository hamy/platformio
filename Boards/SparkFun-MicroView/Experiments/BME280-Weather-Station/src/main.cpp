/*****************************/
/* BME280 I2C Sample Program */
/*****************************/

// Source: https://github.com/adafruit/Adafruit_BME280_Library
// with small modifications made by Hamy

#include <MicroView.h>
#include <Arduino.h>
#include <Wire.h>

#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

#define SEALEVELPRESSURE_HPA (1015)

Adafruit_BME280 bme; // I2C

unsigned long delayTime;

float celsius2fahrenheit(float c) {
    return c*9.0f/5.0f + 32.0f;
}

float celsius2kelvin(float c) {
    return c+273.15f;
}

void setup() {
    Serial.begin(9600);
    while(!Serial);    // time to get serial running
    uView.begin();
    Serial.println(F("BME280 Weather Station"));
    Serial.print(F("Sea level pressure: "));
    Serial.print(SEALEVELPRESSURE_HPA);
    Serial.println(F(" hPa"));

    unsigned status;
    
    status = bme.begin();  
	Serial.print(F("status returned by bme.begin(): "));
	Serial.println(status,16);
	Serial.print(F("bme.sensorID() returned: "));
	Serial.println(bme.sensorID(),16);
	
    if (!status) {
        Serial.println(F("Oops: BME280 initialization error !"));
        while (1);
    }
    delayTime = 1000;
}

int y=0;

void nextLine() {
     uView.setCursor(0,y);
     y += 8;
 }

void showValues() {
    uView.clear(PAGE);
    uView.setColor(1);
    y = 0;
    
    nextLine();
    float celsius = bme.readTemperature();
    uView.print( celsius );
    uView.print( F(" C" ));
    
    nextLine();
    uView.print(celsius2fahrenheit(celsius));
    uView.print(F(" F"));
    
    nextLine();
    uView.print(celsius2kelvin(celsius));
    uView.print(F(" K"));
 
    nextLine();
    uView.print(bme.readPressure() / 100.0F);
    uView.print(F("hPa"));

    nextLine();
    uView.print(bme.readAltitude(SEALEVELPRESSURE_HPA));
    uView.print(F(" m"));

    nextLine();
    uView.print(bme.readHumidity());
    uView.print(F(" %"));

    uView.display();
}

void loop() { 
    showValues();
    delay(delayTime);
}

