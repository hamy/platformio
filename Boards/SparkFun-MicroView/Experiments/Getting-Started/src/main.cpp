#include <MicroView.h>

int screenWidth;
int screenHeight;

void setup() {
    Serial.begin(9600);
    uView.begin();
    uView.clear(PAGE);
    uView.display();
    screenWidth = uView.getLCDWidth();
    screenHeight = uView.getLCDHeight();
    Serial.print(F("screen geometry: "));
    Serial.print(screenWidth);
    Serial.print(F("x"));
    Serial.println(screenHeight);



}

void loop() {
    uView.clear(PAGE);
    uView.setColor(1);
    uView.setDrawMode(XOR);
    for (int i=0; i<12500; i++) {
        uint8_t x = random(screenWidth);
        uint8_t y = random(screenHeight);
        uint8_t color = 0xff;
        uint8_t mode = NORM;
        uView.pixel(x,y);
        uView.display();
    }
}

