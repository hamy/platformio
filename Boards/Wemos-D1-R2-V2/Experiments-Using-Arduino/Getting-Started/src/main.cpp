#include <Arduino.h>
#include <SourceInfo.h>
#include <LEDUtil.h>
#include <LoopUtil.h>

#define BAUDRATE 115200

SimpleLED simpleLED(LED_BUILTIN, false, false);
LoopUtil loopUtil(true, -1, 2000);

void setup()
{
    simpleLED.on();
    Serial.begin(BAUDRATE);
    while (!Serial)
    {
        ;
    }
    delay(1000);
    SOURCE_INFO_ENTERING("setup")
    loopUtil.setNewLineAfterLoopEnd(true);
    Serial.println(F("Sketch \"Getting-Started\" starts..."));
    SOURCE_INFO_PRINT
    pinMode(LED_BUILTIN, OUTPUT);
    Serial.print(F("LED_BUILTIN: "));
    Serial.println(LED_BUILTIN);
    simpleLED.off();
    SOURCE_INFO_LEAVING("setup")
    SOURCE_INFO_NL
    simpleLED.off();
}

boolean ledFlag = false;

void toggleLed()
{
    ledFlag = !ledFlag;
    Serial.print(F("toggleLed: ledFlag: "));
    Serial.println(ledFlag);
    if (ledFlag)
    {
        simpleLED.on();
    }
    else
    {
        simpleLED.off();
    }
}

void measureRateOfADC()
{
    // Caveat: n is limited because of the ESP8266's  yield() problem
    int n = 5000;
    long usec1 = micros();
    for (int i = 0; i < n; i++)
    {
        analogRead(A0);
    }
    long usec2 = micros();
    long usec = usec2 - usec1;
    usec /= n;
    Serial.print(F("measureRateOfADC: time for 1 ADC: "));
    Serial.print(usec);
    Serial.println(F(" microseconds"));
    Serial.print(F("measureRateOfADC: rate: "));
    long rate = 1000000 / usec;
    Serial.print(rate);
    Serial.println(F(" per second"));
}

void sampleADC()
{
    int n = 10;
    for (int i = 0; i < n; i++)
    {
        simpleLED.on();
        int value = analogRead(A0);
        float volts = 3.3 / 1023 * value;
        Serial.print(F("sampleADC: #"));
        Serial.print(i);
        Serial.print(F(": "));
        Serial.print(value);
        Serial.print(F(" -> "));
        Serial.print(volts);
        Serial.println(F(" V"));
        simpleLED.off();
        delay(100);
    }
}

void loop()
{
    loopUtil.loopStart();
    toggleLed();
    measureRateOfADC();
    sampleADC();
    loopUtil.loopEnd();
}
