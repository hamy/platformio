#include <Arduino.h>
#include <SourceInfo.h>
#include <LEDUtil.h>
#include <LoopUtil.h>
#include <ESP8266Util.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>

#define BAUDRATE 115200

SimpleLED simpleLED(LED_BUILTIN, false, false);
LoopUtil loopUtil(true, -1, 2000);

void setupSerial()
{
    Serial.begin(BAUDRATE);
    while (!Serial)
    {
        ;
    }
    delay(1000);
    SOURCE_INFO_ENTERING("setupSerial")
    Serial.println(F("Sketch \"WiFi-Access-Point\" starts..."));
    SOURCE_INFO_PRINT
    SOURCE_INFO_LEAVING("setupSerial")
    SOURCE_INFO_NL
}

void setupLED()
{
    SOURCE_INFO_ENTERING("setupLED")
    Serial.print(F("LED_BUILTIN: "));
    Serial.println(LED_BUILTIN);
    SOURCE_INFO_LEAVING("setupLED")
    SOURCE_INFO_NL
}

#ifndef AP_SSID
#define AP_SSID "Hamy-Wemos-D1-R2"
#endif

#ifndef AP_PASS
#define AP_PASS "top-secret"
#endif

const char *ssid = AP_SSID;
const char *password = AP_PASS;
IPAddress myIP = IPAddress(10, 0, 82, 66);
IPAddress myMask = IPAddress(255, 255, 255, 0);

ESP8266WebServer webServer(80);

void simpleHandler()
{
    SOURCE_INFO_ENTERING("simpleHandler")
    webServer.send(200, "text/plain", "This is the answer from the simpleHandler: URI=" + webServer.uri() + ", loopCount=" + loopUtil.getLoopCount());
    SOURCE_INFO_LEAVING("simpleHandler")
}

void handler404()
{
    SOURCE_INFO_ENTERING("handler404")
    webServer.send(404, "text/plain", "No handler was given for URI " + webServer.uri());
    SOURCE_INFO_LEAVING("handler404")
}

void setupWiFi()
{
    SOURCE_INFO_ENTERING("setupWiFi")
    Serial.print(F("SSID: "));
    Serial.println(ssid);
    Serial.print(F("Password: "));
    Serial.print(strlen(password));
    Serial.println(F(" bytes"));
    WiFi.mode(WIFI_AP);
    Serial.print(F("set WiFi mode to WIFI_AP="));
    Serial.println(WIFI_AP);
    WiFi.softAP(ssid, password);
    Serial.println(F("Started soft AP..."));
    delay(1000);
    WiFi.softAPConfig(myIP, myIP, myMask);
    Serial.println(F("AP configuration was set"));
    WiFi.printDiag(Serial);
    webServer.on("/", simpleHandler);
    webServer.onNotFound(handler404);
    webServer.begin();
    SOURCE_INFO_LEAVING("setupWiFi")
    SOURCE_INFO_NL
}

void setup()
{
    setupSerial();
    setupLED();
    setupWiFi();
    loopUtil.setNewLineAfterLoopEnd(true);
}

void loop()
{
    loopUtil.loopStart();
    webServer.handleClient();
    loopUtil.loopEnd();
}
