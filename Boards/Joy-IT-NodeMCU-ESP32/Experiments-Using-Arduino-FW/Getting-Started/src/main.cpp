#include <Arduino.h>
#include <Esp.h>
 
 void newline() {
 Serial.println();
 }
 
void setup()
{
    pinMode(LED_BUILTIN, OUTPUT);
     Serial.begin(115200);
    Serial.println(F("Sketch \"Getting-Started\" starts..."));
}

boolean ledFlag = false;

void toggleLed()
{
    newline();
    ledFlag = !ledFlag;
    Serial.print(F("toggleLed: ledFlag: "));
    Serial.println(ledFlag);
    Serial.print(F("toggleLed: ledPin: "));
    Serial.println(LED_BUILTIN);
    digitalWrite(LED_BUILTIN, ledFlag);
 }

void measureRateOfADC()
{
    newline();
    // Caveat: n is limited because of the ESP8266's  yield() problem
    int n = 5000;
    long usec1 = micros();
    for (int i = 0; i < n; i++)
    {
        analogRead(A0);
    }
    long usec2 = micros();
    long usec = usec2 - usec1;
    usec /= n;
    Serial.print(F("measureRateOfADC: time for 1 ADC: "));
    Serial.print(usec);
    Serial.println(F(" microseconds"));
    Serial.print(F("measureRateOfADC: rate: "));
    long rate = 1000000 / usec;
    Serial.print(rate);
    Serial.println(F(" per second"));
}

void sampleADC()
{
    newline();
    int n = 5;
    for (int i = 0; i < n; i++)
    {
          int value = analogRead(A0);
        float volts = 3.3 / 1023 * value;
        Serial.print(F("sampleADC: #"));
        Serial.print(i);
        Serial.print(F(": "));
        Serial.print(value);
        Serial.print(F(" -> "));
        Serial.print(volts);
        Serial.println(F(" V"));
         delay(1000);
    }
} 

void loop()
{
    toggleLed();
    measureRateOfADC();
    sampleADC();
     delay(500);
}
