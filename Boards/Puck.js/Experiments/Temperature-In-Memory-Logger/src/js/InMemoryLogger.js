var temperatureArray=[];
var temperatureIndex = 0;
var secondCounter = 0;

console.log("");
console.log("#####################");
console.log("# InMemoryLogger.js #");
console.log("#####################");
console.log("");

function showBattery() {
  console.log("# battery: ", Puck.getBatteryPercentage(),"% / ", NRF.getBattery(), " V");
}

function flushTemperatureArray() {
    var n = temperatureArray.length;
    showBattery();
    console.log("# data points: ",n);
    while (0<n--) {
        console.log(n + "," + temperatureArray[n]);
    }
    temperatureArray = [];
    temperatureIndex = 0;
    showBattery();
}

setInterval(function() {
    secondCounter += 1;
    if ((secondCounter % 60)==0) {
        var temp = E.getTemperature();
        temperatureArray[temperatureIndex] = temp;
        temperatureIndex += 1;
    }
    if (BTN.read()) {
        flushTemperatureArray();
    }
}, 1000);