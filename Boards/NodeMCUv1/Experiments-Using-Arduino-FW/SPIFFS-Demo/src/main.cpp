#include <Arduino.h>
#include <FS.h>

 void setup()
{
    pinMode(LED_BUILTIN, OUTPUT);
    Serial.begin(9600);
    Serial.println(F("Sketch \"SPIFFS-Demo\" starts..."));
    SPIFFS.format();
}

char buffer[256];
int fico=0;

void create()
{
  for (int n=0;n<4;n++) {
    fico++;
    sprintf(buffer,"%s%d%s","/",fico,".txt");
    File f = SPIFFS.open(buffer,"w");
    Serial.println(buffer);
    for (int i=0;i<=fico;i++) {
      f.println(buffer);
    }
    f.close();
  }
}

void list()
{
   Dir dir = SPIFFS.openDir("/");
   Serial.println(F("Directory / opened."));
   int count=0;
   while (dir.next()) {
     count++;
     Serial.print(F("Entry #"));
     Serial.print(count);
     Serial.print(F(": "));
     Serial.print(dir.fileName());
     Serial.print(F(": "));
     Serial.print(dir.fileSize());
     Serial.println(F(" bytes"));
   }
 }

void loop()
{
   SPIFFS.begin();
   create();
   list();
   SPIFFS.end();
   delay(1000);
}
 