#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <WebSocketsServer.h>
#include <ESP8266mDNS.h>
#include <FS.h>

void ledOff()
{
  digitalWrite(LED_BUILTIN, HIGH);
}

void ledOn()
{
  digitalWrite(LED_BUILTIN, LOW);
}

#define SERIAL_BAUD_RATE 115200

void setupSerial()
{
  // while (!Serial);
  Serial.begin(SERIAL_BAUD_RATE);
  Serial.println(F("Serial started."));
}

void setupFS()
{
  SPIFFS.begin();
  Serial.println(F("SPIFFS started."));
}

void setupLED()
{
  Serial.println(F("setting up LED..."));
  pinMode(LED_BUILTIN, OUTPUT);
  ledOn();
  delay(100);
  ledOff();
}

#ifndef AP_SSID
#define AP_SSID "Hamy-NodeMCU-via-Arduino-FW"
#endif

#ifndef AP_PASS
#define AP_PASS "credential"
#endif

const char *ssid = AP_SSID;
const char *password = AP_PASS;
IPAddress myIP;

void setupWiFi()
{
  Serial.println(F("setting up WiFi..."));
  WiFi.softAP(ssid, password);
  myIP = WiFi.softAPIP();
  Serial.print(F("my IP: "));
  Serial.println(myIP);
}

ESP8266WebServer webServer(80);

void setupWebServer()
{
  Serial.println(F("setting up WebServer..."));
  webServer.serveStatic("/", SPIFFS, "/index.html");
  webServer.serveStatic("/index.html", SPIFFS, "/index.html");
  webServer.serveStatic("/index.js", SPIFFS, "/index.js");
  webServer.serveStatic("/favicon.ico", SPIFFS, "/favicon.ico");
  webServer.begin();
}

WebSocketsServer webSocketsServer(81);

void webSocketsEventHandler(uint8_t num, WStype_t type, uint8_t *payload, size_t length)
{
  Serial.print("handler: type: ");
  Serial.println(type);
  switch (type)
  {
  case WStype_DISCONNECTED:
    Serial.printf("[%u] Disconnected!\n", num);
    break;
  case WStype_CONNECTED:
  {
    IPAddress ip = webSocketsServer.remoteIP(num);
    Serial.printf("[%u] Connected from %d.%d.%d.%d url: %s\n", num, ip[0], ip[1], ip[2], ip[3], payload);

    // send message to client
    webSocketsServer.sendTXT(num, "Connected");
  }
  break;
  case WStype_TEXT:
    Serial.printf("[%u] get Text: %s\n", num, payload);

    if (payload[0] == '#')
    {
      // we get RGB data

      // decode rgb data
      uint32_t rgb = (uint32_t)strtol((const char *)&payload[1], NULL, 16);
    }

    break;
  }
}

void setupWebSocketsServer()
{
  Serial.println(F("setting up WebSocketsServer..."));
  webSocketsServer.begin();
  webSocketsServer.onEvent(webSocketsEventHandler);
}

void setup()
{
  delay(1000);
  setupSerial();
  setupFS();
  setupLED();
  setupWiFi();
  setupWebSocketsServer();

  if (MDNS.begin("esp8266"))
  {
    Serial.println("MDNS responder started");
  }

  setupWebServer();

  MDNS.addService("http", "tcp", 80);
  MDNS.addService("ws", "tcp", 81);

  Serial.println(F("setup done."));
}

void loop()
{
  webSocketsServer.loop();
  webServer.handleClient();
}
