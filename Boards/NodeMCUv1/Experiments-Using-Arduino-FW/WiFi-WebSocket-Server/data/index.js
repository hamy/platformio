var connection = new WebSocket('ws://192.168.4.1:81/', ['arduino']);
//var connection = new WebSocket('ws://' + location.hostname + ':81/', ['arduino']);
console.log("con: " + connection);
console.log('in index.js: ' + new Date());
connection.onopen = function () { connection.send('Connect ' + new Date()); };
connection.onerror = function (error) { console.log('WebSocket Error ', error); };
connection.onmessage = function (e) { console.log('Server: ', e.data); };
function sendRGB() {
    var r = parseInt(document.getElementById('r').value).toString(16);
    var g = parseInt(document.getElementById('g').value).toString(16);
    var b = parseInt(document.getElementById('b').value).toString(16);
    if (r.length < 2) { r = '0' + r; }
    if (g.length < 2) { g = '0' + g; }
    if (b.length < 2) { b = '0' + b; }
    var rgb = '#' + r + g + b;
    console.log('sending RGB: ' + rgb);
    connection.send(rgb);
}

