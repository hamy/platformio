var can;
var ctx;
function init() {
	can = document.getElementById("MyCanvasArea");
	ctx = can.getContext("2d");
	y = can.height / 2;
	x = can.width - 20;
	mid = can.width / 2;
	// rainbow - vibgyor
	drawQuadraticCurve(20, y, mid, 0, x, y, "violet", 7);
	drawQuadraticCurve(20, y - 10, mid, -10, x, y - 10, "indigo", 7);
	drawQuadraticCurve(20, y - 20, mid, -20, x, y - 20, "blue", 7);
	drawQuadraticCurve(20, y - 30, mid, -30, x, y - 30, "green", 7);
	drawQuadraticCurve(20, y - 40, mid, -40, x, y - 40, "yellow", 7);
	drawQuadraticCurve(20, y - 50, mid, -50, x, y - 50, "orange", 7);
	drawQuadraticCurve(20, y - 60, mid, -60, x, y - 60, "red", 7);

}
function drawQuadraticCurve(xStart, yStart, xControl, yControl, xEnd, yEnd,
		color, width) {
	ctx.beginPath();
	ctx.strokeStyle = color;
	ctx.lineJoin = "round";
	ctx.lineWidth = width;
	ctx.moveTo(xStart, yStart);
	ctx.quadraticCurveTo(xControl, yControl, xEnd, yEnd);
	ctx.stroke();
	ctx.closePath();
}
