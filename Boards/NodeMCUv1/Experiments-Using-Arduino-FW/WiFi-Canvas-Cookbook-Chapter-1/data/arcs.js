function init() {
	can = document.getElementById("MyCanvasArea");
	ctx = can.getContext("2d");

	drawArc(60, 80, 40, 180, 270, false, "aqua", "yellow", 8);
	drawArc(120, 80, 40, 270, 360, false, "aqua", "yellow", 8);
	drawArc(220, 80, 40, 180, 360, false, "aqua", "red", 8);

	drawArc(60, 150, 40, 90, 180, false, "aqua", "yellow", 8);
	drawArc(120, 150, 40, 0, 90, false, "aqua", "yellow", 8);
	drawArc(220, 150, 40, 0, 180, false, "aqua", "red", 8);

	drawArc(100, 250, 40, 0, 360, false, "aqua", "yellow", 8);
	drawArc(200, 250, 40, 360, 0, false, "aqua", "red", 8);

	// function to draw curve
	function drawArc(xPos, yPos, radius, startAngle, endAngle, anticlockwise,
			lineColor, fillColor, width) {
		var startAngle = startAngle * (Math.PI / 180);
		var endAngle = endAngle * (Math.PI / 180);

		var radius = radius;

		ctx.strokeStyle = lineColor;
		ctx.fillStyle = fillColor;
		ctx.lineWidth = width;

		ctx.beginPath();

		ctx.arc(xPos, yPos, radius, startAngle, endAngle, anticlockwise);

		ctx.fill();
		ctx.stroke();
	}
}
