var can;
var ctx;
function init() {

	can = document.getElementById("MyCanvasArea");
	ctx = can.getContext("2d");
	drawJoinedLines(50, 150, 50, 50, 20, "blue", "round");
	drawJoinedLines(50, 300, 50, 200, 20, "red", "miter");
	drawJoinedLines(50, 450, 50, 350, 20, "green", "bevel");
}

function drawJoinedLines(xstart, ystart, xnext, ynext, width, color, jointype) {
	ctx.beginPath();
	ctx.lineJoin = jointype;
	ctx.lineCap = "square";
	ctx.strokeStyle = color;
	ctx.lineWidth = width;
	x1 = xstart;
	y1 = ystart;
	x2 = xnext;
	y2 = ynext;

	ctx.moveTo(x1, y1);
	for (i = 1; i <= 20; i += 1) {
		ctx.lineTo(x2, y2);
		if (i % 2 == 1) {
			x2 = x2 + 50;
		} else {
			if (y2 > ynext)
				y2 = ynext;
			else
				y2 = y2 + 100;
		}
	}
	ctx.stroke();
	ctx.closePath();
}
