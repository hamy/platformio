var can;
var ctx;
function init() {
	can = document.getElementById("MyCanvasArea");
	ctx = can.getContext("2d");
	drawLine(30, 30, 300, 30, 20, "orange", "butt"); // default cap style
	drawLine(30, 80, 300, 80, 20, "crimson", "round");
	drawLine(30, 130, 300, 130, 20, "teal", "square");
}
function drawLine(xstart, ystart, xend, yend, width, color, cap) {
	ctx.beginPath();
	ctx.strokeStyle = color;
	ctx.lineWidth = width;
	ctx.lineCap = cap;
	ctx.moveTo(xstart, ystart);
	ctx.lineTo(xend, yend);
	ctx.stroke();
	ctx.closePath();
}
