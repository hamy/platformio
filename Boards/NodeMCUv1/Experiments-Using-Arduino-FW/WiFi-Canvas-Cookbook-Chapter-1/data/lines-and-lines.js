var can;
var ctx;
function init() {
	can = document.getElementById("MyCanvasArea");
	ctx = can.getContext("2d");
	drawHorizontalLines();
	drawVerticalLines();
	drawSkewedLines();
}
function drawLine(xstart, ystart, xend, yend, width, color) {
	ctx.beginPath();
	ctx.strokeStyle = color;
	ctx.lineWidth = width;
	ctx.moveTo(xstart, ystart);
	ctx.lineTo(xend, yend);
	ctx.stroke();
	ctx.closePath();
}
function drawHorizontalLines() {
	xs = 10;
	ys = 10;
	xe = 100;
	ye = 10;
	c = "teal";
	w = 2;
	// draw 10 lines
	for (i = 1; i <= 10; i++) {
		// x coordinates remain same and y coordinates change
		drawLine(xs, ys, xe, ye, w++, c);
		ys += 15;
		ye += 15;
	}
}
function drawVerticalLines() {
	xs = 130;
	ys = 10;
	xe = 130;
	ye = 160;
	c = "crimson";
	w = 2;
	// draw 10 lines
	for (i = 1; i <= 10; i++) {
		// y coordinates remain same and x coordinates change
		drawLine(xs, ys, xe, ye, w++, c);
		xs += 15;
		xe += 15;
	}
}
function drawSkewedLines() {
	// center point
	xcenter = 400;
	ycenter = 125;
	xe = xcenter - 100;
	ye = ycenter;
	c = "orange";
	w = 2;
	// first quadrant
	for (xe = xcenter - 100; xe <= xcenter; xe += 5, ye -= 5)
		drawLine(xcenter, ycenter, xe, ye, w, c);
	// second quadrant
	for (ye = ycenter - 100; ye <= ycenter; xe += 5, ye += 5)
		drawLine(xcenter, ycenter, xe, ye, w, c);
	// third quadrant
	for (xe = xcenter + 100; xe >= xcenter; xe -= 5, ye += 5)
		drawLine(xcenter, ycenter, xe, ye, w, c);
	// fourth quadrant
	for (ye = ycenter + 100; ye >= ycenter; xe -= 5, ye -= 5)
		drawLine(xcenter, ycenter, xe, ye, w, c);
}
