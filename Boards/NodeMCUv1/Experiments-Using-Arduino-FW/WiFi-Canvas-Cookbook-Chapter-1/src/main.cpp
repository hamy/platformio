#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <FS.h>

void ledOff()
{
  digitalWrite(LED_BUILTIN, HIGH);
}

void ledOn()
{
  digitalWrite(LED_BUILTIN, LOW);
}

#define SERIAL_BAUD_RATE 115200

void setupSerial()
{
  // while (!Serial);
  Serial.begin(SERIAL_BAUD_RATE);
  Serial.println(F("Serial started."));
}

void setupFS()
{
  SPIFFS.begin();
  Serial.println(F("SPIFFS started."));
}

void setupLED()
{
  Serial.println(F("setting up LED..."));
  pinMode(LED_BUILTIN, OUTPUT);
  ledOn();
  delay(100);
  ledOff();
}

#ifndef AP_SSID
#define AP_SSID "Hamy-NodeMCU-via-Arduino-FW"
#endif

#ifndef AP_PASS
#define AP_PASS "credential"
#endif

const char *ssid = AP_SSID;
const char *password = AP_PASS;
IPAddress myIP;

void setupWiFi()
{
  Serial.println(F("setting up WiFi..."));
  WiFi.softAP(ssid, password);
  myIP = WiFi.softAPIP();
  Serial.print(F("my IP: "));
  Serial.println(myIP);
}

ESP8266WebServer server(80);

void routeSample(char *prefix)
{
  int len = strlen(prefix);
  char *htmlPath = (char *)malloc(len + 7);
  strcpy(htmlPath, "/");
  strcat(htmlPath, prefix);
  strcat(htmlPath, ".html");
  server.serveStatic(htmlPath, SPIFFS, htmlPath);
  char *jsPath = (char *)malloc(len + 5);
  strcpy(jsPath, "/");
  strcat(jsPath, prefix);
  strcat(jsPath, ".js");
  server.serveStatic(jsPath, SPIFFS, jsPath);
}

void setupWebServer()
{
  Serial.println(F("setting up WebServer..."));
  server.serveStatic("/", SPIFFS, "/index.html");
  server.serveStatic("/index.html", SPIFFS, "/index.html");
  server.serveStatic("/booktitle.jpg", SPIFFS, "/booktitle.jpg");
  server.serveStatic("/favicon.ico", SPIFFS, "/favicon.ico");
  routeSample("simple-lines");
  routeSample("lines-and-lines");
  routeSample("joined-lines");
  routeSample("arcs");
  routeSample("rainbow");
  server.begin();
}

void setup()
{
  delay(1000);
  setupSerial();
  setupFS();
  setupLED();
  setupWiFi();
  setupWebServer();
  Serial.println(F("setup done."));
}

void loop()
{
  server.handleClient();
}
