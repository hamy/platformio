
console.log('location.hostnamn: ', location.hostname);
 
console.log("init");
var wsConnection = new WebSocket('ws://' + location.hostname + ':81/', ['arduino']);
wsConnection.binaryType = "arraybuffer";
console.log("wsConnection: ", wsConnection);

wsConnection.onopen = function () {
  console.log('onopen: ', wsConnection);
  wsConnection.send('Connect ' + new Date());
};

wsConnection.onerror = function (error) {
  console.log('onerror ',
    error);
};

wsConnection.onmessage = function (e) {
  var payload = new Uint8Array(e.data);
  console.log('onmessage: ', payload);
  document.getElementById('slider1').value = payload[0];
  document.getElementById('slider2').value = payload[1];
};
console.log("init done");

function slider1CB() {
  var brightness =
    parseInt(document.getElementById('slider1').value);
  console.log("slider1CB: ", brightness);
  var payload = new Uint8Array(2);
  payload[0] = 1;
  payload[1] = brightness;
  console.log("slider1CB: ", payload);
  wsConnection.send(payload.buffer);
}

function slider2CB() {
  var brightness =
    parseInt(document.getElementById('slider2').value);
  console.log("slider2CB: ", brightness);
  var payload = new Uint8Array(2);
  payload[0] = 2;
  payload[1] = brightness;
  console.log("slider2CB: ", payload);
  wsConnection.send(payload.buffer);
}
