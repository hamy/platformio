/*
 * WebSocketServerAllFunctionsDemo.ino
 *
 *  Created on: 10.05.2018
 *
 */

#include <Arduino.h>

#include <ESP8266WebServer.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266mDNS.h>
#include <FS.h>
#include <Hash.h>
#include <WebSocketsServer.h>

#define LED_1 LED_BUILTIN
#define LED_2 2

uint8_t brightness1 = 128;
uint8_t brightness2 = 128;

ESP8266WebServer webServer(80);
WebSocketsServer webSocketsServer = WebSocketsServer(81);

void broadcastBrightnessValues()
{
  unsigned char ui8a[2];
  ui8a[0] = brightness1;
  ui8a[1] = brightness2;
  webSocketsServer.broadcastBIN(ui8a, 2);
}

const char *type2name(WStype_t type)
{
  switch (type)
  {
  default:
  case WStype_ERROR:
    return "ERROR";
  case WStype_DISCONNECTED:
    return "DISCONNECTED";
  case WStype_CONNECTED:
    return "CONNECTED";
  case WStype_TEXT:
    return "TEXT";
  case WStype_BIN:
    return "BIN";
  case WStype_FRAGMENT_TEXT_START:
    return "FRAGMENT_TEXT_START";
  case WStype_FRAGMENT_BIN_START:
    return "FRAGMENT_BIN_START";
  case WStype_FRAGMENT:
    return "FRAGMENT";
  case WStype_FRAGMENT_FIN:
    return "FRAGMENT_FIN";
  case WStype_PING:
    return "PING";
  case WStype_PONG:
    return "PONG";
  }
}

void webSocketEvent(uint8_t num, WStype_t type, uint8_t *payload,
                    size_t length)
{
  Serial.printf("webSocketEvent: num=%d type=%d=%s len=%d\n", num, type,
                type2name(type), length);
  IPAddress ip ;
  switch (type)
  {
  default:
    break;
  case WStype_DISCONNECTED:
    Serial.printf("[%u] Disconnected!\n", num);
    break;
  case WStype_CONNECTED:
    ip = webSocketsServer.remoteIP(num);
    Serial.printf("[%u] Connected from %d.%d.%d.%d url: %s\n", num, ip[0],
                  ip[1], ip[2], ip[3], payload);

    // send message to client

    // webSocketsServer.sendBIN(num, bubu,10);
    break;
  case WStype_TEXT:
    Serial.printf("[%u] get Text: %s\n", num, payload);

    break;
  case WStype_BIN:
    switch (payload[0])
    {
    case 1:
      brightness1 = payload[1];
      analogWrite(LED_1, 1023 - 4 * brightness1);
      break;
    case 2:
      brightness2 = payload[1];
      analogWrite(LED_2, 1023 - 4 * brightness2);
      break;
    }
    broadcastBrightnessValues();
    break;
  }
}

#ifndef AP_SSID
#define AP_SSID "Hamy-NodeMCU-via-Arduino-FW"
#endif

#ifndef AP_PASS
#define AP_PASS "kartoffelbrei"
#endif

const char *ssid = AP_SSID;
const char *password = AP_PASS;
IPAddress myIP;

void setup()
{
  Serial.begin(115200);

  Serial.println();
  Serial.println();
  Serial.println();
  SPIFFS.begin();
  Serial.println(F("SPIFFS started."));

  for (uint8_t t = 4; t > 0; t--)
  {
    Serial.printf("[SETUP] BOOT WAIT %d...\n", t);
    Serial.flush();
    delay(1000);
  }

  pinMode(LED_1, OUTPUT);
  pinMode(LED_2, OUTPUT);

  Serial.println(F("setting up WiFi..."));
  WiFi.softAP(ssid, password);
  myIP = WiFi.softAPIP();
  Serial.print(F("my IP: "));
  Serial.println(myIP);
  Serial.print(F("SSID: "));
  Serial.print(ssid);
  Serial.print(F(" / "));
  Serial.println(password);

  // start webSocket server
  webSocketsServer.begin();
  webSocketsServer.onEvent(webSocketEvent);

  if (MDNS.begin("esp8266"))
  {
    Serial.println("MDNS responder started");
  }

  Serial.println(F("routing..."));
  webServer.serveStatic("/", SPIFFS, "/index.html");
  webServer.serveStatic("/index.html", SPIFFS, "/index.html");
  webServer.serveStatic("/index.js", SPIFFS, "/index.js");
  webServer.serveStatic("/favicon.ico", SPIFFS, "/favicon.ico");
  Serial.println(F("routing defined."));

  webServer.begin();
  Serial.println(F("web server runs"));

  // Add service to MDNS
  MDNS.addService("http", "tcp", 80);
  MDNS.addService("ws", "tcp", 81);
  Serial.println(F("setup done"));
}

unsigned long last_10sec = 0;
unsigned int counter = 0;

void loop()
{
  unsigned long t = millis();
  webSocketsServer.loop();
  webServer.handleClient();

  if ((t - last_10sec) > 10 * 1000)
  {
    counter++;
    bool ping = (counter % 2);
    int i = webSocketsServer.connectedClients(ping);
    Serial.printf("%d Connected websocket clients ping: %d\n", i, ping);
    last_10sec = millis();
  }
}
