#include <Arduino.h>
#include <Wire.h>

#include <SparkFun_APDS9960.h>
#include <LEDUtil.h>
#include <LoopUtil.h>
#include <RetryUtil.h>
#include <SourceInfo.h>

#define BAUD_RATE 115200
#define TRIGGER_PIN 2

LoopUtil loopUtil(true, TRIGGER_PIN, 2000);
SimpleLED simpleLED(LED_BUILTIN, true, false);
RetryUtil retryUtil(true, 600, 1);

/**
 * Initialize the serial connection to the host.
 */
void setupSerial()
{
  Serial.begin(BAUD_RATE);
  while (!Serial)
  {
    ;
  }
  delay(1000);
  SOURCE_INFO_NL
  SOURCE_INFO_ENTERING("setupSerial")
  SOURCE_INFO_PRINT
  SOURCE_INFO_LEAVING("setupSerial")
  SOURCE_INFO_NL
}

/**
 * Initialize the color LED.
 */
void setupLED()
{
  SOURCE_INFO_ENTERING("setupLED")
  simpleLED.off();
  SOURCE_INFO_LEAVING("setupLED")
  SOURCE_INFO_NL
}

/**
 * Initialize the gesture sensor.
 */
void setupSensor()
{
  SOURCE_INFO_ENTERING("setupSensor")
  if (APDS.begin())
  {
    Serial.println(F("Gesture sensor initialized successfully"));
  }
  else
  {
    simpleLED.on(); // indicate failure
    SOURCE_INFO_BAILING_OUT("failed to initialize gesture sensor")
  }
  SOURCE_INFO_LEAVING("setupSensor")
  SOURCE_INFO_NL
}

/**
 * The request callback function for RetryUtil.
 */
boolean requestCB(int attempt) { return APDS.gestureAvailable(); }

/**
 * The success indicator callback function for RetryUtil.
 */
void successCB(int attempts)
{
  Serial.print(F("Gesture value is available after "));
  Serial.print(attempts);
  Serial.println(F(" attempts."));
  //simpleLED.workerSuccess();
  simpleLED.on();
  delay(100);
  simpleLED.off();
}

/**
 * The failure indicator callback function for RetryUtil.
 */
void failureCB(int attempts)
{
  Serial.print(F("No gesture value is available after "));
  Serial.print(attempts);
  Serial.println(F(" attempts."));
  //simpleLED.workerFailure();
}

/**
 * Initialize the RetryUtil callback set.
 */
void setupCallbacks()
{
  SOURCE_INFO_ENTERING("setupCallbacks")
  retryUtil.setRequestCallback(requestCB);
  retryUtil.setSuccessCallback(successCB);
  retryUtil.setFailureCallback(failureCB);
  SOURCE_INFO_LEAVING("setupCallbacks")
  SOURCE_INFO_NL
}

void reportCB()
{
  retryUtil.printStatistics();
}

/**
 * The top-level initialization function.
 */
void setup()
{
  setupSerial();
  setupLED();
  setupSensor();
  setupCallbacks();
  loopUtil.registerLoopEndIntermittentCallback(reportCB, 20);
}

/**
 * The Arduino main loop.
 */
void loop()
{
  loopUtil.loopStart();
  //simpleLED.workerRunning();

  if (retryUtil.retry() > 0)
  {
    //simpleLED.workerSuccess();
    loopUtil.skipWait();
    int gesture = APDS.readGesture();
    Serial.print(F("Gesture value:"));
    Serial.print(gesture);
    Serial.print(F(" = "));
    switch (gesture)
    {
    case GESTURE_DOWN:
      Serial.println(F("GESTURE_DOWN"));
      break;
    case GESTURE_UP:
      Serial.println(F("GESTURE_UP"));
      break;
    case GESTURE_LEFT:
      Serial.println(F("GESTURE_LEFT"));
      break;
    case GESTURE_RIGHT:
      Serial.println(F("GESTURE_RIGHT"));
      break;
    case GESTURE_NONE:
      Serial.println(F("GESTURE_NONE"));
      break;
    default:
      Serial.println(F("*** unknown ***"));
      break;
    }
  }
  else
  {
    loopUtil.skipWait();
    //simpleLED.workerFailure();
  }

  loopUtil.loopEnd();
}
