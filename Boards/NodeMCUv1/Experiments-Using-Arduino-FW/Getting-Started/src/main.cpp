#include <Arduino.h>
#include <Esp.h>
#include <sntp.h>

#define LED_2 2

void newline()
{
    Serial.println();
}

void setup()
{
    pinMode(LED_BUILTIN, OUTPUT);
    pinMode(LED_2, OUTPUT);
    Serial.begin(115200);
    Serial.println(F("Sketch \"Getting-Started\" starts..."));
}

boolean ledFlag = false;

void toggleLed()
{
    newline();
    ledFlag = !ledFlag;
    Serial.print(F("toggleLed: ledFlag: "));
    Serial.println(ledFlag);
    digitalWrite(LED_BUILTIN, ledFlag);
    digitalWrite(LED_2, !ledFlag);
}

void measureRateOfADC()
{
    newline();
    // Caveat: n is limited because of the ESP8266's  yield() problem
    int n = 5000;
    long usec1 = micros();
    for (int i = 0; i < n; i++)
    {
        analogRead(A0);
    }
    long usec2 = micros();
    long usec = usec2 - usec1;
    usec /= n;
    Serial.print(F("measureRateOfADC: time for 1 ADC: "));
    Serial.print(usec);
    Serial.println(F(" microseconds"));
    Serial.print(F("measureRateOfADC: rate: "));
    long rate = 1000000 / usec;
    Serial.print(rate);
    Serial.println(F(" per second"));
}

void sampleADC()
{
    newline();
    int n = 5;
    for (int i = 0; i < n; i++)
    {
        digitalWrite(LED_2, LOW);
        int value = analogRead(A0);
        float volts = 3.3 / 1023 * value;
        Serial.print(F("sampleADC: #"));
        Serial.print(i);
        Serial.print(F(": "));
        Serial.print(value);
        Serial.print(F(" -> "));
        Serial.print(volts);
        Serial.println(F(" V"));
        digitalWrite(LED_2, HIGH);
        delay(1000);
    }
}

#define callEspSpecificFunction(func)     \
    Serial.print(F("ESP." #func "(): ")); \
    Serial.println(ESP.func())

void espSpecificFunctions()
{
    newline();
    Serial.println(F("ESP specific functions:"));
    callEspSpecificFunction(getBootMode);
    callEspSpecificFunction(getBootVersion);
    callEspSpecificFunction(getChipId);
    callEspSpecificFunction(getCpuFreqMHz);
    callEspSpecificFunction(getCycleCount);
    callEspSpecificFunction(getFlashChipId);
    callEspSpecificFunction(getFlashChipMode);
    callEspSpecificFunction(getFlashChipSize);
    callEspSpecificFunction(getFlashChipRealSize);
    callEspSpecificFunction(getFlashChipSizeByChipId);
    callEspSpecificFunction(getFlashChipSpeed);
    callEspSpecificFunction(getFreeHeap);
    callEspSpecificFunction(getFreeSketchSpace);
    callEspSpecificFunction(getResetInfo);
    callEspSpecificFunction(getSdkVersion);
    callEspSpecificFunction(getSketchSize);
    callEspSpecificFunction(getVcc);
}

void loop()
{
    toggleLed();
    measureRateOfADC();
    sampleADC();
    espSpecificFunctions();
    delay(500);
}
