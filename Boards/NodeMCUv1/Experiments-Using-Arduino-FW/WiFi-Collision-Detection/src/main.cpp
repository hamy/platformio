/*
 * WiFi-Voroblobinoids
  */
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <FS.h>

void ledOff()
{
  digitalWrite(LED_BUILTIN, HIGH);
}

void ledOn()
{
  digitalWrite(LED_BUILTIN, LOW);
}

#define SERIAL_BAUD_RATE 115200

void setupSerial()
{
  Serial.begin(SERIAL_BAUD_RATE);
  Serial.println(F("Serial started."));
}

void setupSPIFFS()
{
  SPIFFS.begin();
  Serial.println(F("SPIFFS started."));
}

void setupLED()
{
  Serial.println(F("setting up LED..."));
  pinMode(LED_BUILTIN, OUTPUT);
  ledOn();
  delay(100);
  ledOff();
}

#ifndef AP_SSID
#define AP_SSID "Hamy-NodeMCU-via-Arduino-FW"
#endif

#ifndef AP_PASS
#define AP_PASS "topsecret"
#endif

const char *ssid = AP_SSID;
const char *password = AP_PASS;
IPAddress myIP;

void setupWiFiAccessPoint()
{
  Serial.println(F("setting up WiFi..."));
  WiFi.softAP(ssid, password);
  myIP = WiFi.softAPIP();
  Serial.print(F("my IP: "));
  Serial.println(myIP);
  Serial.print(F("SSID: "));
  Serial.print(ssid);
  Serial.print(F(" / "));
  Serial.println(password);
}

ESP8266WebServer server(80);

void htmlGzip(char *compressed)
{
  ledOn();
  File file = SPIFFS.open(compressed, "r");
  server.streamFile(file, "text/html");
  file.close();
  ledOff();
}

void htmlGzipIndex()
{
  htmlGzip("/index.html.gz");
}

void jsGzip(char *compressed)
{
  ledOn();
  File file = SPIFFS.open(compressed, "r");
  server.streamFile(file, "application/javascript");
  file.close();
  ledOff();
}

void jsGzipD3()
{
  jsGzip("/d3.v3.min.js.gz");
}
 
void jsGzipMain()
{
  jsGzip("/main.js.gz");
}

void setupWebServerRouting()
{
  Serial.println(F("setting up web server routing..."));
  server.on("/", htmlGzipIndex);
  server.on("/index.html", htmlGzipIndex);
  server.on("/d3.v3.min.js", jsGzipD3);
   server.on("/main.js", jsGzipMain);
  server.begin();
}

void setup()
{
  delay(1000);
  setupSerial();
  setupSPIFFS();
  setupLED();
  setupWiFiAccessPoint();
  setupWebServerRouting();
  Serial.println(F("setup done."));
}

void loop()
{
  server.handleClient();
}
