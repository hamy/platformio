#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <FS.h>

void ledOff()
{
  digitalWrite(LED_BUILTIN, HIGH);
}

void ledOn()
{
  digitalWrite(LED_BUILTIN, LOW);
}

#define SERIAL_BAUD_RATE 115200

void setupSerial()
{
  Serial.begin(SERIAL_BAUD_RATE);
  Serial.println(F("Serial started."));
}

void setupSPIFFS()
{
  SPIFFS.begin();
  Serial.println(F("SPIFFS started."));
}

void setupLED()
{
  Serial.println(F("setting up LED..."));
  pinMode(LED_BUILTIN, OUTPUT);
  ledOn();
  delay(100);
  ledOff();
}

#ifndef AP_SSID
#define AP_SSID "Hamy-NodeMCU-via-Arduino-FW"
#endif

#ifndef AP_PASS
#define AP_PASS "topsecret"
#endif

const char *ssid = AP_SSID;
const char *password = AP_PASS;
IPAddress myIP;

void setupWiFiAccessPoint()
{
  Serial.println(F("setting up WiFi..."));
  WiFi.softAP(ssid, password);
  myIP = WiFi.softAPIP();
  Serial.print(F("my IP: "));
  Serial.println(myIP);
}

ESP8266WebServer server(80);

void streamBigJavascriptBlobWithGzipEncoding()
{
  Serial.println(F("streamBigJavascriptBlobWithGzipEncoding: entering"));
  ledOn();
  File file = SPIFFS.open("/d3-v5-min-js.gz", "r");
  size_t len = server.streamFile(file, "application/javascript");
  file.close();
  Serial.print(F("streamed "));
  Serial.print(len);
  Serial.println(F(" bytes (gzipped)"));
  ledOff();
  Serial.println(F("streamBigJavascriptBlobWithGzipEncoding: leaving"));
}

void setupWebServerRouting()
{
  Serial.println(F("setting up web server routing..."));
  server.serveStatic("/", SPIFFS, "/index.html");
  server.serveStatic("/index.html", SPIFFS, "/index.html");
  server.serveStatic("/testit.html", SPIFFS, "/testit.html");
  server.serveStatic("/testit.js", SPIFFS, "/testit.js");
  server.serveStatic("/booktitle.jpg", SPIFFS, "/booktitle.jpg");
  server.serveStatic("/favicon.ico", SPIFFS, "/favicon.ico");
  server.serveStatic("/avatar-24.png", SPIFFS, "/avatar-24.png");
  server.serveStatic("/d3-v5-min-js.gz", SPIFFS, "/d3-v5-min-js.gz");
  server.on("/d3-v5-min.js", streamBigJavascriptBlobWithGzipEncoding);
  server.begin();
}

void setup()
{
  delay(1000);
  setupSerial();
  setupSPIFFS();
  setupLED();
  setupWiFiAccessPoint();
  setupWebServerRouting();
  Serial.println(F("setup done."));
}

void loop()
{
  server.handleClient();
}
