const array = [100, 200, 300, 350, 375, 400, 500];

d3.select("#chart")
        .selectAll("circle")
        .data(array)
        .enter()
        .append("circle")
        .attr("r", "10")
        .attr("cy", 100)
        .attr("cx", d => d);

setTimeout(function () {
        d3.select("#chart").selectAll("circle")
                .data([50, 75, 125, 225, 325, 425, 450])
                .transition().duration(1000)
                .attr("r", 20)
                .attr("cx", d => d)
                .style("fill", "red")
}, 2000);

setTimeout(function () {
        d3.select("#chart").selectAll("circle")
                .data([150, 275, 325, 425, 455, 225, 50])
                .transition().duration(4000)
                .attr("r", 10)
                .attr("cx", d => d)
                .style("fill", "blue")
}, 5000);
