#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <FS.h>

void ledOff()
{
  digitalWrite(LED_BUILTIN, HIGH);
}

void ledOn()
{
  digitalWrite(LED_BUILTIN, LOW);
}

#define SERIAL_BAUD_RATE 115200

void setupSerial()
{
  // while (!Serial);
  Serial.begin(SERIAL_BAUD_RATE);
  Serial.println(F("Serial started."));
}

void setupFS()
{
  SPIFFS.begin();
  Serial.println(F("SPIFFS started."));
}

void setupLED()
{
  Serial.println(F("setting up LED..."));
  pinMode(LED_BUILTIN, OUTPUT);
  ledOn();
  delay(100);
  ledOff();
}

#ifndef AP_SSID
#define AP_SSID "Hamy-NodeMCU-via-Arduino-FW"
#endif

#ifndef AP_PASS
#define AP_PASS "credential"
#endif

const char *ssid = AP_SSID;
const char *password = AP_PASS;
IPAddress myIP = IPAddress(10,0,82,66);
IPAddress myMask = IPAddress(255,255,255,0);

void setupWiFi()
{
  Serial.println(F("setting up WiFi..."));
  Serial.print(ssid);
  Serial.print(F(" / "));
  Serial.println(password);
  WiFi.softAPConfig(myIP,myIP,myMask);
  WiFi.softAP(ssid, password);
  myIP = WiFi.softAPIP();
  Serial.print(F("my IP: "));
  Serial.println(myIP);
}

ESP8266WebServer server(80);
char buffer1[4096];
char buffer2[4096];

int loadBuffer1(char *path)
{
  Serial.print(F("loading "));
  Serial.print(path);
  Serial.println(F("..."));
  File file = SPIFFS.open(path, "r");
  memset(buffer1, 0, sizeof buffer1);
  int len = file.readBytes(buffer1, sizeof buffer1);
  file.close();
  Serial.print("loaded ");
  Serial.print(len);
  Serial.println(F(" bytes."));
  return len;
}

void handleIndex()
{
  ledOn();
  loadBuffer1("/index.html");
  sprintf(buffer2, buffer1, ESP.getChipId(), ESP.getFlashChipId(), ESP.getFlashChipSize(), ESP.getFlashChipSpeed(), ESP.getFreeHeap(), ESP.getCycleCount());
  server.send(200, "text/html", buffer2);
  ledOff();
}

void handleIndexRaw()
{
  ledOn();
  loadBuffer1("/index.html");
  server.send(200, "text/plain", buffer1);
  ledOff();
}

void setupWebServer()
{
  Serial.println(F("setting up WebServer..."));
  server.on("/", handleIndex);
  server.on("/index.html", handleIndex);
  server.on("/index.raw", handleIndexRaw);
  server.serveStatic("/favicon.ico", SPIFFS, "/favicon.ico");
  server.serveStatic("/avatar-100.png", SPIFFS, "/avatar-100.png");
  server.begin();
}

void setup()
{
  delay(1000);
  setupSerial();
  setupFS();
  setupLED();
  setupWiFi();
  setupWebServer();
  Serial.println(F("setup done."));
}

void loop()
{
  server.handleClient();
}
