#include "Arduino.h"

void setup()
{
  Serial.begin(9600);
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(9600);
  Serial.println(F("Sketch \"Getting-Started\" starts..."));
}

void hilo(int delayHigh, int delayLow, int repeat)
{
  for (int r = 0; r < repeat; r++)
  {
    Serial.print(F("HIGH for "));
    Serial.print(delayHigh);
    Serial.println(F(" milliseconds"));
    digitalWrite(LED_BUILTIN, HIGH);
    delay(delayHigh);
    Serial.print(F("LOW for "));
    Serial.print(delayLow);
    Serial.println(F(" milliseconds"));
    digitalWrite(LED_BUILTIN, LOW);
    delay(delayLow);
  }
}

void loop()
{
  hilo(600, 300, 3);
  hilo(50, 100, 17);
}
