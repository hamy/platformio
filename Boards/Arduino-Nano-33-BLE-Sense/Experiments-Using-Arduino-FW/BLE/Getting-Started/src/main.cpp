// The Arduino "HelloWorld" example.

#include "Arduino.h"
#include "LoopUtil.h"
#include "LEDUtil.h"
#include "SourceInfo.h"
#include "ArduinoBLE.h"

#define BAUD_RATE 115200

LoopUtil loopUtil(true,2,1000);
RGBLED rgbLED(22,23,24,false,false);

void setupSerial() {

  Serial.begin(BAUD_RATE);
  while (!Serial)
    ;
  delay(500);
  SOURCE_INFO_PRINT
}
 
void setupLEDs() {
  pinMode(LED_BUILTIN,OUTPUT);
  digitalWrite(LED_BUILTIN,LOW);
}

void setup() {
  setupSerial();
  setupLEDs();
  pinMode(3,INPUT);
  digitalWrite(3,HIGH);
}
 
void loop() {loopUtil.loopStart();
  rgbLED.off();
  int value = digitalRead(3);
  if (value) {
    rgbLED.blueOn();
  }
  loopUtil.loopEnd();
}
