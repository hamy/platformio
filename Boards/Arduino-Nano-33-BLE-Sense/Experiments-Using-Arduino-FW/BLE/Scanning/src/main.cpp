

#include "Arduino.h"
#include "LoopUtil.h"
#include "LEDUtil.h"
#include "SourceInfo.h"
#include "ArduinoBLE.h"

#define BAUD_RATE 115200

LoopUtil loopUtil(true, 2, 1000);
RGBLED rgbLED(22, 23, 24, false, false);

void setupSerial()
{

  Serial.begin(BAUD_RATE);
  while (!Serial)
    ;
  delay(500);
  SOURCE_INFO_PRINT
}

void setupLEDs()
{
}

void setupBLE()
{
  Serial.println(F("setupBLE: entering..."));
  if (BLE.begin())
  {
    Serial.println(F("BLE was successfully initialized"));
    Serial.println(F("Now, I work in BLE central mode and start scanning for peripherals..."));
    BLE.scan();
  }
  else
  {
    Serial.println(F("BLE inilialization failed."));
    while (true)
      ;
  }
  Serial.println(F("setupBLE: leaving..."));
}

void setup()
{
  setupSerial();
  setupLEDs();
  setupBLE();
}

void loop()
{
  loopUtil.loopStart();
  BLEDevice peripheral = BLE.available();

  if (peripheral)
  {
    // discovered a peripheral
    Serial.println("Discovered a peripheral");
    Serial.println("-----------------------");

    // print address
    Serial.print("Address: ");
    Serial.println(peripheral.address());

    // print the local name, if present
    if (peripheral.hasLocalName())
    {
      Serial.print("Local Name: ");
      Serial.println(peripheral.localName());
    }

    // print the advertised service UUIDs, if present
    if (peripheral.hasAdvertisedServiceUuid())
    {
      Serial.print("Service UUIDs: ");
      for (int i = 0; i < peripheral.advertisedServiceUuidCount(); i++)
      {
        Serial.print(peripheral.advertisedServiceUuid(i));
        Serial.print(" ");
      }
      Serial.println();
    }

    // print the RSSI
    Serial.print("RSSI: ");
    Serial.println(peripheral.rssi());

    Serial.println();
  }
  loopUtil.loopEnd();
}
