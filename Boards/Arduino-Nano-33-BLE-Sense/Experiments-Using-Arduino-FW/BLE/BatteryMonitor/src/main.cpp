

#include "Arduino.h"
#include "LoopUtil.h"
#include "LEDUtil.h"
#include "SourceInfo.h"
#include "ArduinoBLE.h"

#define BAUD_RATE 115200

LoopUtil loopUtil(true, 2, 1000);
RGBLED rgbLED(22, 23, 24, false, false);

// BLE Battery Service
BLEService batteryService("180F");

// BLE Battery Level Characteristic
BLEUnsignedCharCharacteristic batteryLevelChar("2A19",               // standard 16-bit characteristic UUID
                                               BLERead | BLENotify); // remote clients will be able to get notifications if this characteristic changes

int oldBatteryLevel = 0; // last battery level reading from analog input
long previousMillis = 0; // last time the battery level was checked, in ms

void updateBatteryLevel()
{
  /* Read the current voltage level on the A0 analog input pin.
     This is used here to simulate the charge level of a battery.
  */
  int battery = analogRead(A0);
  int batteryLevel = map(battery, 0, 1023, 0, 100);

  if (batteryLevel != oldBatteryLevel)
  {                                           // if the battery level has changed
    Serial.print("Battery Level % is now: "); // print it
    Serial.println(batteryLevel);
    batteryLevelChar.writeValue(batteryLevel); // and update the battery level characteristic
    oldBatteryLevel = batteryLevel;            // save the level for next comparison
  }
}
void setupSerial()
{

  Serial.begin(BAUD_RATE);
  while (!Serial)
    ;
  delay(500);
  SOURCE_INFO_PRINT
}

void setupLEDs()
{
  rgbLED.off();
}

void setupBLE()
{
  Serial.println(F("setupBLE: entering..."));
  if (BLE.begin())
  {
    Serial.println(F("BLE was successfully initialized"));
    Serial.println(F("Now, I work in BLE peripheral mode..."));
  }
  else
  {
    Serial.println(F("BLE inilialization failed."));
    while (true)
      ;
  }
  Serial.println(F("setupBLE: leaving..."));
}

void setup()
{
  setupSerial();
  setupLEDs();
  setupBLE();
}

void loop()
{
  loopUtil.loopStart();

  // wait for a BLE central
  BLEDevice central = BLE.central();

  // if a central is connected to the peripheral:
  if (central)
  {
    Serial.print("Connected to central: ");
    // print the central's BT address:
    Serial.println(central.address());
    // turn on the LED to indicate the connection:
    rgbLED.on();

    // check the battery level every 200ms
    // while the central is connected:
    while (central.connected())
    {
      long currentMillis = millis();
      // if 200ms have passed, check the battery level:
      if (currentMillis - previousMillis >= 200)
      {
        previousMillis = currentMillis;
        updateBatteryLevel();
      }
    }
    // when the central disconnects, turn off the LED:
    rgbLED.off();
    Serial.print("Disconnected from central: ");
    Serial.println(central.address());
  }

  loopUtil.loopEnd();
}
