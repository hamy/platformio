#include <Arduino.h>
#include <Arduino_LSM9DS1.h>
#include <LEDUtil.h>
#include <LoopUtil.h>
#include <SourceInfo.h>

#define BAUD_RATE 115200

/**
 * This sketch may be run in two modes selected by the debug flag:
 * - debugFlag = true: Slow operation, many debug messages are printed to the serial connection.
 * - debugFlag = false: Fast operation, no debug messages are printed; instead, a magnetometer value time series is printed to the
 *   serial connection. Visualize this time series in the Arduion serial plotter.
 */
boolean debugFlag = false;

// The debug flag also affects the loop rate.
uint32_t loopDuration = debugFlag ? 1000 : 100;

RGBLED rgbLED(22, 23, 24, false, false);
LoopUtil loopUtil(debugFlag, -1, loopDuration);

/**
 * Initializes the serial connection to the host.
 */
void setupSerial() {
  Serial.begin(BAUD_RATE);
  while (!Serial) {
    ;
  }
  if (debugFlag) {
    SOURCE_INFO_NL
    SOURCE_INFO_ENTERING("setupSerial")
    SOURCE_INFO_PRINT
    SOURCE_INFO_LEAVING("setupSerial")
    SOURCE_INFO_NL
  }
}

/**
 * Initializes the color LED.
 */
void setupLED() {
  if (debugFlag) {
    SOURCE_INFO_ENTERING("setupLED")
  }
  rgbLED.off();
  if (debugFlag) {
    SOURCE_INFO_LEAVING("setupLED")
    SOURCE_INFO_NL
  }
}

/**
 * Initializes the  magnetometer.
 */
void setupMagnetometer() {
  if (debugFlag) {
    SOURCE_INFO_ENTERING("setupMagnetometer")
  }
  if (IMU.begin()) {
    if (debugFlag) {
      Serial.println(F("IMU initialized successfully"));
    }
  } else {
    SOURCE_INFO_BAILING_OUT("failed to initialize IMU")
  }
  if (debugFlag) {
    SOURCE_INFO_LEAVING("setupMagnetometer")
  }
}

/**
 * Top-level initialization function.
 */
void setup() {
  setupSerial();
  setupLED();
  setupMagnetometer();
  if (debugFlag) {
    loopUtil.setNewLineAfterLoopEnd(true);
  }
}

/**
 * The Arduino main loop.
 */
void loop() {
  loopUtil.loopStart();
  rgbLED.off();

  if (debugFlag) {
    Serial.print(F("Magnetic field sampling rate: "));
    Serial.print(IMU.magneticFieldSampleRate());
    Serial.println(F(" Hz"));
    Serial.print(F("Magnetic field available ? "));
  }

  boolean flag = IMU.magneticFieldAvailable();

  if (debugFlag) {
    Serial.println(flag);
  }

  if (flag) {
    float mx, my, mz, ma;
    rgbLED.blueOn(); // indicate that measurement is pending
    IMU.readMagneticField(mx, my, mz);
    rgbLED.blueOff();
    ma = sqrt(mx * mx + my * my + mz * mz);
    if (debugFlag) {
      Serial.print(F("Magnetometer values (mx,my,mz,|m|): "));
    }
    Serial.print(mx);
    Serial.print(F(" "));
    Serial.print(my);
    Serial.print(F(" "));
    Serial.print(mz);
    Serial.print(F(" "));
    Serial.println(ma);
  }

  loopUtil.loopEnd();
}
