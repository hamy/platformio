#include <Arduino.h>
#include <Arduino_LPS22HB.h>
#include <LEDUtil.h>
#include <LoopUtil.h>
#include <SourceInfo.h>

#define BAUD_RATE 115200
#define TRIGGER_PIN 2

// Create the instances.
LoopUtil loopUtil(true, TRIGGER_PIN, 1000);
RGBLED rgbLED(22, 23, 24, false, false);

/**
 * Initializes the serial interface.
 */
void setupSerial() {
  Serial.begin(BAUD_RATE);
  while (!Serial) {
    ;
  }
  delay(1000);
  SOURCE_INFO_NL
  SOURCE_INFO_ENTERING("setupSerial")
  SOURCE_INFO_PRINT
  SOURCE_INFO_LEAVING("setupSerial")
  SOURCE_INFO_NL
}

/**
 * LED initialization.
 */
void setupLED() {
  SOURCE_INFO_ENTERING("setupLED")
  rgbLED.off();
  SOURCE_INFO_LEAVING("setupLED")
  SOURCE_INFO_NL
}

/**
 * Intialize the barometer.
 */
void setupBarometer() {
  SOURCE_INFO_ENTERING("setupBarometer")
  if (BARO.begin()) {
    Serial.println(F("barometer initialized successfully"));
  } else {
    rgbLED.redOn(); // indicate error
    SOURCE_INFO_BAILING_OUT("failed to initialize barometer")
  }
  SOURCE_INFO_LEAVING("setupBarometer")
  SOURCE_INFO_NL
}

/**
 * The top-level initialization method.
 */
void setup() {
  setupSerial();
  setupLED();
  setupBarometer();
  loopUtil.setNewLineAfterLoopEnd(true);
}

/**
 * The Arduino main loop.
 */
void loop() {
  loopUtil.loopStart();
  rgbLED.off();

  rgbLED.blueOn(); // indicate measurement start
  float psi = BARO.readPressure(PSI);
  float mbar = BARO.readPressure(MILLIBAR);
  float kpa = BARO.readPressure(KILOPASCAL);
  rgbLED.off(); // indicate measurement end

  Serial.print(F("Pressure: "));
  Serial.print(psi);
  Serial.print(F(" PSI = "));
  Serial.print(mbar);
  Serial.print(F(" mbar = "));
  Serial.print(kpa);
  Serial.println(F(" kPa"));

  loopUtil.loopEnd();
}
