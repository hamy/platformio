#include <Arduino.h>
#include <Arduino_LSM9DS1.h>
#include <LEDUtil.h>
#include <LoopUtil.h>
#include <SourceInfo.h>
#include <V3DComponentComparator.h>
 
#define BAUD_RATE 115200
boolean debugFlag = false;
uint32_t loopDuration = debugFlag ? 1000 : 10;

LoopUtil loopUtil(debugFlag, 2, loopDuration);
RGBLED rgbLED(22, 23, 24, false, false);
V3DComponentComparator comparator(25.0);

/**
 * Initialize the serial connection to the host.
 */
void setupSerial() {
  Serial.begin(BAUD_RATE);
  while (!Serial) {
    ;
  }
  if (debugFlag) {
    SOURCE_INFO_NL
    SOURCE_INFO_ENTERING("setupSerial");
    SOURCE_INFO_PRINT
    SOURCE_INFO_LEAVING("setupSerial");
    SOURCE_INFO_NL
  }
}

/**
 * Initialize the color LED.
 */
void setupLED() {
  if (debugFlag) {
    SOURCE_INFO_ENTERING("setupLED");
  }
  rgbLED.off();
  if (debugFlag) {
    SOURCE_INFO_LEAVING("setupLED");
    SOURCE_INFO_NL
  }
}

/**
 * Initialize the gyroscope.
 */
void setupGyroscope() {
  if (debugFlag) {
    SOURCE_INFO_ENTERING("setupGyroscope");
  }
  if (IMU.begin()) {
    if (debugFlag) {
      Serial.println(F("IMU initialized successfully"));
    }
  } else {
    SOURCE_INFO_BAILING_OUT("failed to initialize IMU")
  }
  if (debugFlag) {
    SOURCE_INFO_LEAVING("setupGyroscope");
    SOURCE_INFO_NL
  }
}

/**
 * Initialize the comparator.
 */
void setupComparator() {
  if (debugFlag) {
    SOURCE_INFO_ENTERING("setupComparator")
    Serial.print(F("usesThreshold ? "));
    Serial.println(comparator.usesThreshold());
    Serial.print(F("threshold: "));
    Serial.println(comparator.getThreshold());
    Serial.print(F("thresholdSquare: "));
    Serial.println(comparator.getThresholdSquare());
    SOURCE_INFO_LEAVING("setupComparator")
    SOURCE_INFO_NL
  }
}

/**
 * The top-level initialization function.
 */
void setup() {
  setupSerial();
  setupLED();
  setupGyroscope();
  setupComparator();
  if (debugFlag) {
    loopUtil.setNewLineAfterLoopEnd(true);
  }
}
/**
 * The Arduino main function.
 */
void loop() {
  loopUtil.loopStart();
  if (debugFlag) {
    Serial.print(F("Gyroscope sampling rate: "));
    Serial.print(IMU.gyroscopeSampleRate());
    Serial.println(F(" Hz"));
    Serial.print(F("Gyroscope available ? "));
  }
  boolean flag = IMU.gyroscopeAvailable();
  if (debugFlag) {
    Serial.println(flag);
  }

  if (flag) {
    float gx, gy, gz, ga;
    IMU.readGyroscope(gx, gy, gz);
    ga = sqrt(gx * gx + gy * gy + gz * gz);
    rgbLED.off();
    switch (comparator.compareByMagnitude(gx, gy, gz)) {
    case V3D_XYZ:
      rgbLED.redOn();
      break;
    case V3D_XZY:
      rgbLED.redOn();
      rgbLED.greenOn();
      break;
    case V3D_YXZ:
      rgbLED.greenOn();
      break;
    case V3D_YZX:
      rgbLED.greenOn();
      rgbLED.blueOn();
      break;
    case V3D_ZXY:
      rgbLED.blueOn();
      break;
    case V3D_ZYX:
      rgbLED.redOn();
      rgbLED.blueOn();
      break;
    }
    if (debugFlag) {
      Serial.print(F("Gyroscope values: "));
    }
    Serial.print(gx);
    Serial.print(F(" "));
    Serial.print(gy);
    Serial.print(F(" "));
    Serial.print(gz);
    Serial.print(F(" "));
    Serial.println(ga);
  }

  loopUtil.loopEnd();
}
