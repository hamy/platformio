#include <Arduino.h>
#include <Arduino_APDS9960.h>
#include <LEDUtil.h>
#include <LoopUtil.h>
#include <RetryUtil.h>
#include <SourceInfo.h>

#define BAUD_RATE 115200
#define TRIGGER_PIN 2

LoopUtil loopUtil(true, TRIGGER_PIN, 1000);
RGBLED rgbLED(22, 23, 24, false, false);
RetryUtil retryUtil(true, 50, 5);

/**
 * Initialize the serial connection to the host.
 */
void setupSerial() {
  Serial.begin(BAUD_RATE);
  while (!Serial)
    ;
  delay(1000);
  SOURCE_INFO_NL
  SOURCE_INFO_ENTERING("setupSerial")
  SOURCE_INFO_PRINT
  SOURCE_INFO_LEAVING("setupSerial")
  SOURCE_INFO_NL
}

/**
 * Initalize the color led.
 */
void setupLED() {
  SOURCE_INFO_ENTERING("setupLED")
  rgbLED.off();
  SOURCE_INFO_LEAVING("setupLED")
  SOURCE_INFO_NL
}

/**
 * Initalize the proximity sensor.
 */
void setupSensor() {
  SOURCE_INFO_ENTERING("setupSensor")
  if (APDS.begin()) {
    Serial.println(F("proximity sensor initialized successfully"));
  } else {
    rgbLED.redOn();
    SOURCE_INFO_BAILING_OUT("failed to initialize proximity sensor")
  }
  SOURCE_INFO_LEAVING("setupSensor")
  SOURCE_INFO_NL
}

/**
 * The request callback function for RetryUtil.
 */
boolean requestCB(int attempt) { return APDS.proximityAvailable(); }

/**
 * The success handler callback for RetryUtil.
 */
void successCB(int attempts) {
  rgbLED.workerSuccess();
  Serial.print(F("Proximity value is available after "));
  Serial.print(attempts);
  Serial.println(F(" attempts."));
}

/**
 * The failure handler callback for RetryUtil.
 */
void failureCB(int attempts) {
  rgbLED.workerFailure();
  Serial.print(F("No proximity value is available after "));
  Serial.print(attempts);
  Serial.println(F(" attempts."));
}

/**
 * Initialize the RetryUtil callback set.
 */
void setupCallbacks() {
  SOURCE_INFO_ENTERING("setupCallbacks")
  retryUtil.setRequestCallback(requestCB);
  retryUtil.setSuccessCallback(successCB);
  retryUtil.setFailureCallback(failureCB);
  SOURCE_INFO_LEAVING("setupCallbacks")
  SOURCE_INFO_NL
}

void reportCB() { retryUtil.printStatistics(); }

/**
 * The top-level initialization function.
 */
void setup() {
  setupSerial();
  setupLED();
  setupSensor();
  setupCallbacks();
  loopUtil.setNewLineAfterLoopEnd(true);
  loopUtil.registerLoopEndIntermittentCallback(reportCB, 20);
}

void loop() {
  loopUtil.loopStart();

  rgbLED.workerRunning();
  if (retryUtil.retry() > 0) {
    int proxy = APDS.readProximity();
    Serial.print(F("Proximity value:"));
    Serial.println(proxy);
  }

  loopUtil.loopEnd();
}
