#include <Arduino.h>
#include <Arduino_HTS221.h>
#include <LEDUtil.h>
#include <LoopUtil.h>
#include <SourceInfo.h>

#define BAUD_RATE 115200
#define TRIGGER_PIN 2

LoopUtil loopUtil(true, TRIGGER_PIN, 1000);
RGBLED rgbLED(22, 23, 24, false, false);

/**
 * Initialize the serial connection to the host.
 */
void setupSerial() {
  Serial.begin(BAUD_RATE);
  while (!Serial) {
    ;
  }
  delay(1000);
  SOURCE_INFO_NL
  SOURCE_INFO_ENTERING("setupSerial")
  SOURCE_INFO_PRINT
  SOURCE_INFO_LEAVING("setupSerial")
  SOURCE_INFO_NL
}

/**
 * Initialize the color LED.
 */
void setupLED() {
  SOURCE_INFO_ENTERING("setupLED")
  rgbLED.off();
  SOURCE_INFO_LEAVING("setupLED")
  SOURCE_INFO_NL
}

/**
 * Initialize the hygrometer sensor.
 */
void setupHygrometer() {
  SOURCE_INFO_ENTERING("setupHygrometer")
  if (HTS.begin()) {
    Serial.println(F("hygrometer initialized successfully"));
  } else {
    rgbLED.redOn(); // indicate failure
    SOURCE_INFO_BAILING_OUT("failed to initialize hygrometer")
  }
  SOURCE_INFO_LEAVING("setupHygrometer")
  SOURCE_INFO_NL
}

/**
 * The top-level initialization function.
 */
void setup() {
  setupSerial();
  setupLED();
  setupHygrometer();
  loopUtil.setNewLineAfterLoopEnd(true);
}

/**
 * The Arduino main loop.
 */
void loop() {
  loopUtil.loopStart();
  rgbLED.off();

  rgbLED.blueOn(); // indicate measurement is pending
  float rh = HTS.readHumidity();
  float c = HTS.readTemperature(CELSIUS);
  rgbLED.blueOff();

  Serial.print(F("Relative humidity: "));
  Serial.print(rh);
  Serial.print(F(" % read at "));
  Serial.print(c);
  Serial.println(F(" °C"));

  loopUtil.loopEnd();
}
