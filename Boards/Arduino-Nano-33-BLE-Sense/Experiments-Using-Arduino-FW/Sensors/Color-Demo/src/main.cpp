#include <Arduino.h>
#include <Arduino_APDS9960.h>
#include <LEDUtil.h>
#include <LoopUtil.h>
#include <RetryUtil.h>
#include <SourceInfo.h>

#define BAUD_RATE 115200
#define TRIGGER_PIN 2

LoopUtil loopUtil(true, TRIGGER_PIN, 1000);
RGBLED rgbLED(22, 23, 24, false, false);
RetryUtil retryUtil(true, 50, 1);

/**
 * Initializes the serial connection to the host.
 */
void setupSerial() {
  Serial.begin(BAUD_RATE);
  while (!Serial) {
    ;
  }
  delay(1000);
  SOURCE_INFO_NL
  SOURCE_INFO_ENTERING("setupSerial")
  SOURCE_INFO_PRINT
  SOURCE_INFO_LEAVING("setupSerial")
  SOURCE_INFO_NL
}

/**
 * Initializes the RGB LED.
 */
void setupLED() {
  SOURCE_INFO_ENTERING("setupLED")
  rgbLED.off();
  SOURCE_INFO_LEAVING("setupLED")
  SOURCE_INFO_NL
}

/**
 * Initializes the color sensor.
 */
void setupSensor() {
  SOURCE_INFO_ENTERING("setupSensor")
  if (APDS.begin()) {
    Serial.println(F("color sensor initialized successfully"));
  } else {
    rgbLED.redOn(); // indicate error
    SOURCE_INFO_BAILING_OUT("failed to initialize color sensor")
  }
  SOURCE_INFO_LEAVING("setupSensor")
  SOURCE_INFO_NL
}

/**
 * The request callback function for RetryUtil.
 */
boolean requestCB(int attempt) { return APDS.colorAvailable(); }

/**
 * The success indicator callback function for RetryUtil.
 */
void successCB(int attempts) {
  Serial.print(F("Color is available after "));
  Serial.print(attempts);
  Serial.println(F(" attempts."));
  rgbLED.workerSuccess();
}

/**
 * The success indicator callback function for RetryUtil.
 */
void failureCB(int attempts) {
  Serial.print(F("No color is available after "));
  Serial.print(attempts);
  Serial.println(F(" attempts."));
  rgbLED.workerFailure();
}

void reportCB() { retryUtil.printStatistics(); }

/**
 * Initializes the RetryUtil callback set.
 */
void setupCallbacks() {
  SOURCE_INFO_ENTERING("setupCallbacks")
  retryUtil.setRequestCallback(requestCB);
  retryUtil.setSuccessCallback(successCB);
  retryUtil.setFailureCallback(failureCB);
  retryUtil.printInfo();
  SOURCE_INFO_LEAVING("setupCallbacks")
  SOURCE_INFO_NL
}

/**
 * The top-level initialization function.
 */
void setup() {
  setupSerial();
  setupLED();
  setupSensor();
  setupCallbacks();
  loopUtil.setNewLineAfterLoopEnd(true);
  loopUtil.registerLoopEndIntermittentCallback(reportCB, 20);
}

/**
 * The Arduino main loop.
 */
void loop() {
  loopUtil.loopStart();

  rgbLED.off();
  rgbLED.workerRunning();

  if (retryUtil.retry() > 0) {
    rgbLED.workerSuccess();
    int r, g, b, a;
    APDS.readColor(r, g, b, a);
    Serial.print(F("RGB:"));
    Serial.print(r);
    Serial.print(F("/"));
    Serial.print(g);
    Serial.print(F("/"));
    Serial.print(b);
    Serial.print(F(", ambient: "));
    Serial.println(b);
  } else {
    rgbLED.workerFailure();
  }

  loopUtil.loopEnd();
}
