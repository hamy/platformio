#include <Arduino.h>
#include <Arduino_LSM9DS1.h>
#include <LEDUtil.h>
#include <LoopUtil.h>
#include <SourceInfo.h>
#include <V3DComponentComparator.h>

#define BAUD_RATE 115200

/**
 * This sketch may be run in two modes depending on the value of debugFlag:
 * - with debugging: a lot of debug output messages are printed during the run.
 * - only a time series of the accelerometer data is printed via serial
 * connection. Use Arduino's serial plotter to visualize this series.
 */
boolean debugFlag = false;

// The debug flag also controls the loop rate.
uint32_t loopDuration = debugFlag ? 1000 : 20;

// Create objects on the heap.
RGBLED rgbLED(22, 23, 24, debugFlag, false);
SimpleLED simpleLED(LED_BUILTIN, debugFlag, true);
LoopUtil loopUtil(debugFlag, 2, loopDuration);
V3DComponentComparator comparator(0.2);

/**
 * Initialize the serial interface.
 */
void setupSerial() {
  Serial.begin(BAUD_RATE);
  while (!Serial) {
    ;
  }
  delay(1000);
  if (debugFlag) {
    SOURCE_INFO_NL
    SOURCE_INFO_ENTERING("setupSerial")
    Serial.print(F("baud rate: "));
    Serial.println(BAUD_RATE);
    SOURCE_INFO_PRINT
    SOURCE_INFO_LEAVING("setupSerial")
    SOURCE_INFO_NL
  }
}

/**
 * Initialize the IMU sensor components.
 */
void setupIMU() {
  if (debugFlag) {
    SOURCE_INFO_ENTERING("setupIMU")
  }
  if (IMU.begin()) {
    if (debugFlag) {
      Serial.println(F("IMU initialized successfully"));
    }
  } else {
    SOURCE_INFO_BAILING_OUT("failed to initialize IMU")
  }
  if (debugFlag) {
    SOURCE_INFO_LEAVING("setupIMU")
    SOURCE_INFO_NL
  }
}

/**
 * Initialize the 3D vector comparator instance.
 */
void setupComparator() {
  if (debugFlag) {
    SOURCE_INFO_ENTERING("setupComparator")
    Serial.print(F("usesThreshold ? "));
    Serial.println(comparator.usesThreshold());
    Serial.print(F("threshold: "));
    Serial.println(comparator.getThreshold());
    Serial.print(F("thresholdSquare: "));
    Serial.println(comparator.getThresholdSquare());
    SOURCE_INFO_LEAVING("setupComparator")
    SOURCE_INFO_NL
  }
}

/**
 * The top-level initialization function.
 */
void setup() {
  setupSerial();
  setupIMU();
  setupComparator();
  if (debugFlag) {
    loopUtil.setNewLineAfterLoopEnd(true);
  }
}

/**
 * The Arduino main loop.
 */
void loop() {
  loopUtil.loopStart();
  simpleLED.on();
  if (debugFlag) {
    Serial.print(F("Acceleration sampling rate: "));
    Serial.print(IMU.accelerationSampleRate());
    Serial.println(F(" Hz"));
    Serial.print(F("Acceleration available ? "));
  }
  boolean flag = IMU.accelerationAvailable();
  if (debugFlag) {
    Serial.println(flag);
  }
  float ax, ay, az, aa;
  if (flag) {
    IMU.readAcceleration(ax, ay, az);
    aa = sqrt(ax * ax + ay * ay + az * az);
    rgbLED.off();
    int comp = comparator.compareByMagnitude(ax, ay, az);
    switch (comp) {
    case V3D_XYZ:
      rgbLED.redOn();
      break;
    case V3D_XZY:
      rgbLED.redOn();
      rgbLED.greenOn();
      break;
    case V3D_YXZ:
      rgbLED.greenOn();
      break;
    case V3D_YZX:
      rgbLED.greenOn();
      rgbLED.blueOn();
      break;
    case V3D_ZXY:
      rgbLED.blueOn();
      break;
    case V3D_ZYX:
      rgbLED.redOn();
      rgbLED.blueOn();
      break;
    }
    if (debugFlag) {
      Serial.print(F("Acceleration values (ax,ay,az,|a|,comp): "));
    }
    Serial.print(ax);
    Serial.print(F(" "));
    Serial.print(ay);
    Serial.print(F(" "));
    Serial.print(az);
    Serial.print(F(" "));
    Serial.print(aa);
    Serial.print(F(" "));
    Serial.println(comp);
  }
  simpleLED.off();
  loopUtil.loopEnd();
}
