#include <Arduino.h>
#include <Arduino_LSM9DS1.h>
#include <LEDUtil.h>
#include <LoopUtil.h>
#include <SourceInfo.h>
#include <Wire.h>

#define DEBUG_FLAG (false)

#define BAUD_RATE 115200

/**
 Initializes the serial interface.
 */
void setupSerial()
{
  Serial.begin(BAUD_RATE);
  while (!Serial)
  {
    ;
  }
  delay(500);
  if (DEBUG_FLAG)
  {
    SOURCE_INFO_ENTERING("setupSerial")
    Serial.print(F("baud rate: "));
    Serial.println(BAUD_RATE);
    SOURCE_INFO_PRINT
    SOURCE_INFO_LEAVING("setupSerial")
  }
}

#define BUTTON_A_PIN (2)
#define BUTTON_B_PIN (3)

/**
 Button initialization. For both buttons, pull-up resistors are activated.
 */
void setupButtons()
{
  if (DEBUG_FLAG)
  {
    SOURCE_INFO_ENTERING("setupButtons")
  }
  pinMode(BUTTON_A_PIN, OUTPUT);
  digitalWrite(BUTTON_A_PIN, HIGH);
  pinMode(BUTTON_B_PIN, OUTPUT);
  digitalWrite(BUTTON_B_PIN, HIGH);
  if (DEBUG_FLAG)
  {
    SOURCE_INFO_LEAVING("setupButtons")
  }
}

/**
 The RGB LED that is built-in in the board.
 */
RGBLED rgbLED(22, 23, 24, false, false);

/**
 LED initialization.
 */
void setupLEDs()
{
  if (DEBUG_FLAG)
  {
    SOURCE_INFO_ENTERING("setupLEDs")
  }
  rgbLED.off();
  rgbLED.greenOn();
  if (DEBUG_FLAG)
  {
    SOURCE_INFO_LEAVING("setupLEDs")
  }
}

#define GYRO_CALIBRATION_DURATION (5000)

enum gyroMode_t
{
  GYRO_RAW,
  GYRO_DIRECT,
  GYRO_AGGREGATION,
  GYRO_CALIBRATION
};
gyroMode_t gyroMode;

float gxRaw, gyRaw, gzRaw;
float gxOffset, gyOffset, gzOffset;
float gxDirect, gyDirect, gzDirect;
float gxAggregated, gyAggregated, gzAggregated;
int gyroCalibrationSampleCount;
uint32_t gyroCalibrationEndTime;

void startCalibration()
{
  if (DEBUG_FLAG)
  {
    SOURCE_INFO_ENTERING("startCalibration");
  }
  gyroMode = GYRO_CALIBRATION;
  gyroCalibrationSampleCount = 0;
  gyroCalibrationEndTime = millis();
  gyroCalibrationEndTime += GYRO_CALIBRATION_DURATION;
  rgbLED.off();
  rgbLED.redOn();
  if (DEBUG_FLAG)
  {
    SOURCE_INFO_LEAVING("startCalibration");
  }
}

boolean readRawGyroValues()
{
  boolean result = IMU.gyroscopeAvailable();
  if (result)
  {
    IMU.readGyroscope(gxRaw, gyRaw, gzRaw);
  }
  return result;
}

void doCalibration()
{
  if (readRawGyroValues())
  {
    gyroCalibrationSampleCount++;
    gxOffset += gxRaw;
    gyOffset += gyRaw;
    gzOffset += gzRaw;
  }
}

void printXYZ(float gx, float gy, float gz)
{
  Serial.print(gx);
  Serial.print(F(" "));
  Serial.print(gy);
  Serial.print(F(" "));
  Serial.print(gz);
  Serial.println();
}

void doReport()
{
  if (readRawGyroValues())
  {
    switch (gyroMode)
    {
    case GYRO_RAW:
      printXYZ(gxRaw, gyRaw, gzRaw);
      break;
    case GYRO_DIRECT:
      gxDirect = gxRaw - gxOffset;
      gyDirect = gyRaw - gyOffset;
      gzDirect = gzRaw - gzOffset;
      printXYZ(gxDirect, gyDirect, gzDirect);
      break;
    case GYRO_AGGREGATION:
      gxAggregated += gxRaw - gxOffset;
      gyAggregated += gyRaw - gyOffset;
      gzAggregated += gzRaw - gzOffset;
      printXYZ(gxAggregated, gyAggregated, gzAggregated);
      break;
    default:
      break;
    }
  }
}

void endCalibration()
{
  if (DEBUG_FLAG)
  {
    SOURCE_INFO_ENTERING("endCalibration");
    Serial.print(F("No. of samples: "));
    Serial.println(gyroCalibrationSampleCount);
  }
  gyroMode = GYRO_RAW;
  gxOffset /= gyroCalibrationSampleCount;
  gyOffset /= gyroCalibrationSampleCount;
  gzOffset /= gyroCalibrationSampleCount;
  rgbLED.off();
  rgbLED.greenOn();
  if (DEBUG_FLAG)
  {
    Serial.print(F("Gyro offset X: "));
    Serial.println(gxOffset);
    Serial.print(F("Gyro offset Y: "));
    Serial.println(gyOffset);
    Serial.print(F("Gyro offset Z: "));
    Serial.println(gzOffset);
    SOURCE_INFO_LEAVING("endCalibration");
  }
}

void setupIMU()
{
  if (DEBUG_FLAG)
  {
    SOURCE_INFO_ENTERING("setupIMU")
  }
  if (IMU.begin())
  {
    if (DEBUG_FLAG)
    {
      Serial.println(F("IMU initialized successfully"));
    }
  }
  else
  {
    SOURCE_INFO_BAILING_OUT("Failed to initialize IMU");
  }
  gyroMode = GYRO_RAW;
  gxRaw = gyRaw = gzRaw = 0;
  gxDirect = gyDirect = gzDirect = 0;
  gxOffset = gyOffset = gzOffset = 0;
  if (DEBUG_FLAG)
  {
    SOURCE_INFO_LEAVING("setupIMU")
  }
}

void setup()
{
  setupSerial();
  setupButtons();
  setupLEDs();
  setupIMU();
}

LoopUtil loopUtil(DEBUG_FLAG, -1, 2);

void loop()
{
  loopUtil.loopStart();
  boolean buttonApressed = !digitalRead(BUTTON_A_PIN);
  boolean buttonBpressed = !digitalRead(BUTTON_B_PIN);
  if (buttonApressed && buttonBpressed)
  {
    switch (gyroMode)
    {
    case GYRO_RAW:
    case GYRO_DIRECT:
    case GYRO_AGGREGATION:
      startCalibration();
      break;
    default:
      break;
    }
  }
  else
  {
    switch (gyroMode)
    {
    case GYRO_RAW:
      if (buttonApressed)
      {
        gyroMode = GYRO_DIRECT;
      }
      if (buttonApressed)
      {
        gyroMode = GYRO_AGGREGATION;
      }
      doReport();
      break;
    case GYRO_DIRECT:
      if (buttonApressed)
      {
        gyroMode = GYRO_AGGREGATION;
      }
      doReport();
      break;
    case GYRO_AGGREGATION:
      if (buttonApressed)
      {
        gyroMode = GYRO_DIRECT;
      }
      doReport();
      // TBD
      break;
    case GYRO_CALIBRATION:
      if (millis() < gyroCalibrationEndTime)
      {
        doCalibration();
      }
      else
      {
        endCalibration();
      }
      break;
    }
  }
  loopUtil.loopEnd();
}
