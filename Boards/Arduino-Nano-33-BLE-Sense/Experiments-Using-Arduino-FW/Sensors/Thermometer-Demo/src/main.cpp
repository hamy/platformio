#include <Arduino.h>
#include <Arduino_HTS221.h>
#include <LEDUtil.h>
#include <LoopUtil.h>
#include <SourceInfo.h>

#define BAUD_RATE 115200
#define TRIGGER_PIN 2

LoopUtil loopUtil(true, TRIGGER_PIN, 1000);
RGBLED rgbLED(22, 23, 24, false, false);

/**
 * Initialize the serial connection to the desktop host.
 */
void setupSerial() {
  Serial.begin(BAUD_RATE);
  while (!Serial) {
    ;
  }
  delay(1000);
  SOURCE_INFO_NL
  SOURCE_INFO_ENTERING("setupSerial")
  SOURCE_INFO_PRINT
  SOURCE_INFO_LEAVING("setupSerial")
  SOURCE_INFO_NL
}

/**
 * Initialize the color LED.
 */
void setupLED() {
  SOURCE_INFO_ENTERING("setupLED")
  rgbLED.off();
  SOURCE_INFO_LEAVING("setupLED")
  SOURCE_INFO_NL
}

/**
 * Initialize the thermometer.
 */
void setupSensor() {
  SOURCE_INFO_ENTERING("setupSensor")
  if (HTS.begin()) {
    Serial.println(F("thermometer initialized successfully"));
  } else {
    rgbLED.redOn(); // indicate failure
    SOURCE_INFO_BAILING_OUT("failed to initialize thermometer")
  }
  SOURCE_INFO_LEAVING("setupSensor")
  SOURCE_INFO_NL
}

/**
 * The top-level initialization function.
 */
void setup() {
  setupSerial();
  setupLED();
  setupSensor();
  loopUtil.setNewLineAfterLoopEnd(true);
}

/**
 * The Arduino main loop.
 */
void loop() {
  loopUtil.loopStart();
  rgbLED.off();

  rgbLED.blueOn();
  float c = HTS.readTemperature();
  rgbLED.blueOff();

  Serial.print(F("Temperature: "));
  Serial.print(c);
  Serial.println(F(" C = "));

  loopUtil.loopEnd();
}
