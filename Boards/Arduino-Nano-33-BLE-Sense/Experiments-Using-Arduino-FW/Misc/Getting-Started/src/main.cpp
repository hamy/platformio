// The Arduino "HelloWorld" example.

#include "Arduino.h"
#include "LEDUtil.h"
#include "LoopUtil.h"
#include "SourceInfo.h"

#define BAUD_RATE 115200
#define RGB_LED_PIN_R 22
#define RGB_LED_PIN_G 23
#define RGB_LED_PIN_B 24
#define TRIGGER_PIN 2

int count = 0;
LoopUtil loopUtil(true, TRIGGER_PIN, 1000);
SimpleLED simpleLED(LED_BUILTIN);
RGBLED rgbLED(22, 23, 24, false, false);

void setupSerial()
{
  Serial.begin(BAUD_RATE);
  while (!Serial)
    ;
  delay(1000);
  SOURCE_INFO_ENTERING("setupSerial")
  SOURCE_INFO_PRINT
  Serial.print(F("Baud rate: "));
  Serial.println(BAUD_RATE);
  SOURCE_INFO_LEAVING("setupSerial")
  SOURCE_INFO_NL
}

void setup() { setupSerial(); }

void loop()
{
  loopUtil.loopStart();
  simpleLED.off();
  rgbLED.off();
  Serial.println(count, BIN);
  if (count & 1)
  {
    simpleLED.on();
  }
  else
  {
    simpleLED.off();
  }
  if (count & 2)
  {
    rgbLED.redOn();
  }
  else
  {
    rgbLED.redOff();
  }
  if (count & 4)
  {
    rgbLED.greenOn();
  }
  else
  {
    rgbLED.greenOff();
  }
  if (count & 8)
  {
    rgbLED.blueOn();
  }
  else
  {
    rgbLED.blueOff();
  }
  count++;
  if (count > 15)
  {
    count = 0;
    simpleLED.off();
    rgbLED.off();
  }
  loopUtil.loopEnd();
}
