// The Arduino "HelloWorld" example.

#include "Arduino.h"

#define BAUD_RATE 9600

#define RGB_LED_PIN_R 22
#define RGB_LED_PIN_G 23
#define RGB_LED_PIN_B 24

void setup()
{
  Serial.begin(BAUD_RATE);
  while (!Serial)
    ;
  delay(1000);
  Serial.print(F("Standard Arduino LED_BUILTIN on pin "));
  Serial.println(LED_BUILTIN);
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.print(F("R component of the RGB LED on pin "));
  Serial.println(RGB_LED_PIN_R);
  pinMode(RGB_LED_PIN_R, OUTPUT);
  Serial.print(F("G component of the RGB LED on pin "));
  Serial.println(RGB_LED_PIN_G);
  pinMode(RGB_LED_PIN_G, OUTPUT);
  Serial.print(F("B component of the RGB LED on pin "));
  Serial.println(RGB_LED_PIN_B);
  pinMode(RGB_LED_PIN_B, OUTPUT);
}

void allOff()
{
  digitalWrite(LED_BUILTIN, LOW);
  digitalWrite(RGB_LED_PIN_R, HIGH);
  digitalWrite(RGB_LED_PIN_G, HIGH);
  digitalWrite(RGB_LED_PIN_B, HIGH);
}

void loop()
{
  Serial.println(F("All four LEDs are off."));
  allOff();
  delay(3000);

  Serial.println(F("LED_BUILTIN is HIGH=on"));
  allOff();
  digitalWrite(LED_BUILTIN, HIGH);
  delay(3000);

  Serial.println(F("RGB_LED_PIN_R is LOW=on"));
  allOff();
  digitalWrite(RGB_LED_PIN_R, LOW);
  delay(3000);

  Serial.println(F("RGB_LED_PIN_G is LOW=on"));
  allOff();
  digitalWrite(RGB_LED_PIN_G, LOW);
  delay(3000);

  Serial.println(F("RGB_LED_PIN_B is LOW=on"));
  allOff();
  digitalWrite(RGB_LED_PIN_B, LOW);
  delay(3000);

  Serial.println(F("Cycling through R->G->B"));
  for (int i = 0; i < 6; i++)
  {
    allOff();
    digitalWrite(RGB_LED_PIN_R, LOW);
    delay(300);
    allOff();
    digitalWrite(RGB_LED_PIN_G, LOW);
    delay(300);
    allOff();
    digitalWrite(RGB_LED_PIN_B, LOW);
    delay(300);
  }
}
