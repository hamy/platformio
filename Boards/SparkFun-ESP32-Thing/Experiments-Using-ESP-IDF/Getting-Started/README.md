# Getting Started with Sparkfun ESP32 Thing and the ESP-IDF Framework

* As indicated by [platformio.ini](platformio.ini), this experiment uses the tool chain
  provided by the ESPIDF framework. The fine-tuning of ESPIDF
  is done by PlatformIO (I don't know how it works, but it **does** ;-).
  Perhaps I will understand more after using the --verbose option during `pio run`...
* [main.c](src/main.c) is the main program.
* [sdkconfig.h](src/sdkconfig.h) was shamelessly stolen from other ESPIDF examples.