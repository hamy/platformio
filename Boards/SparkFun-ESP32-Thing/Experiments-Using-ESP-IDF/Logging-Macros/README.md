# Using the Log Macros

* As indicated by [platformio.ini](platformio.ini), this experiment uses the tool chain
  provided by the ESPIDF framework.  
* The header file [esp_log.h](https://github.com/espressif/esp-idf/blob/master/components/log/include/esp_log.h) 
  declares the constants and functions of the logging API. 
  [esp_log.c](https://github.com/espressif/esp-idf/blob/master/components/log/log.c) 
  is its implementation.   
* [main.c](src/main.c) is the main program.
* [sdkconfig.h](src/sdkconfig.h) was shamelessly stolen from other ESPIDF examples.
