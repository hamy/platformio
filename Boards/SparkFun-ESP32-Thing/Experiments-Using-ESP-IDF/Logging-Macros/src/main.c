/* Hello World Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#define TAG0 "TAG0"
#define TAG1 "TAG1"
#define TAG2 "TAG2"

static esp_log_level_t thresholdForTag1 = ESP_LOG_DEBUG;
static esp_log_level_t thresholdForTag2 = ESP_LOG_NONE;


void loggingWithTag(char *tag) {
    ESP_LOGE(tag,"logging with %s and level ERROR=%d",tag,ESP_LOG_ERROR);
    ESP_LOGW(tag,"logging with %s and level WARN=%d",tag,ESP_LOG_WARN);
    ESP_LOGI(tag,"logging with %s and level INFO=%d",tag,ESP_LOG_INFO);
    ESP_LOGD(tag,"logging with %s and level DEBUG=%d",tag,ESP_LOG_DEBUG);
    ESP_LOGV(tag,"logging with %s and level VERBOSE=%d",tag,ESP_LOG_VERBOSE);
}

esp_log_level_t computeNextThresholdForTag2() {
    switch (thresholdForTag2) {
        case ESP_LOG_NONE: return ESP_LOG_ERROR;
        case ESP_LOG_ERROR: return ESP_LOG_WARN;
        case ESP_LOG_WARN: return ESP_LOG_INFO;
        case ESP_LOG_INFO: return ESP_LOG_DEBUG;
        case ESP_LOG_DEBUG: return ESP_LOG_VERBOSE;
        case ESP_LOG_VERBOSE: return ESP_LOG_NONE;
    }
    return ESP_LOG_NONE;
}


void app_main()
{
    ESP_LOGI(TAG0,"logging with TAG0 and level INFO in app_main");
    while (1) {
        ESP_LOGI(TAG0,"waiting for 2 seconds...");
        vTaskDelay(1000 / portTICK_PERIOD_MS);
        ESP_LOGI(TAG0,"threshold for %s=%d, threshold for %s=%d",TAG1,thresholdForTag1,TAG2,thresholdForTag2);
        esp_log_level_set(TAG1,thresholdForTag1);
        esp_log_level_set(TAG2,thresholdForTag2);
        loggingWithTag(TAG1);
        loggingWithTag(TAG2);
        thresholdForTag2 = computeNextThresholdForTag2();
    }
}
