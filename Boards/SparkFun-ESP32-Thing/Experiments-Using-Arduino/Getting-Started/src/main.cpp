#include <Arduino.h>

void setup()
{
    pinMode(LED_BUILTIN, OUTPUT);
    Serial.begin(9600);
    Serial.println(F("Sketch \"Getting-Started\" starts..."));
}

boolean ledFlag = false;

void loop()
{
    ledFlag = !ledFlag;
    Serial.print(F("ledFlag: "));
    Serial.println(ledFlag);
    digitalWrite(LED_BUILTIN, ledFlag);
    delay(500);
}