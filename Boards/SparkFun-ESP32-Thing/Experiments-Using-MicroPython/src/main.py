import network
import time

ap = network.WLAN(network.AP_IF) # create access-point interface
ap.config(essid='Hamy-ESP32-Thing') # set the ESSID of the access point
ap.active(True)         # activate the interface
ap.config('mac')
ap.ifconfig()
 