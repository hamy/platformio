#!/bin/bash
rm -rf /tmp/mws-work
mkdir /tmp/mws-work
cd /tmp/mws-work
curl https://raw.githubusercontent.com/jczic/MicroWebSrv/master/microWebSrv.py -o microWebSrv.py 
curl https://raw.githubusercontent.com/jczic/MicroWebSrv/master/microWebSocket.py -o microWebSocket.py 
curl https://raw.githubusercontent.com/jczic/MicroWebSrv/master/microWebTemplate.py -o microWebTemplate.py 
mkdir www

rshell --buffer-size=30 -a -p /dev/ttyUSB0
rsync . /pyboard
exit

