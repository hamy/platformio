


import network
import time
from machine import Pin

myLED = Pin(5,Pin.OUT)

def myBlink(count):
    for i in range(count):
        myLED.value(1)
        time.sleep(0.2)
        myLED.value(0)
        time.sleep(0.2)

MY_IP_ADDRESS ='10.0.0.1'
MY_NETWORK_MASK='255.255.255.0'
MY_ESSID = 'Hamy-ESP32-Thing'
MY_PASSWD = 'semmelrogge'

myStation = network.WLAN(network.STA_IF)  
myStation.active(False)          
myAccessPoint = network.WLAN(network.AP_IF)  
myAccessPoint.active(True)          
myAccessPoint.config(essid=MY_ESSID, authmode=network.AUTH_WPA_WPA2_PSK, password=MY_PASSWD) 
myAccessPoint.ifconfig((MY_IP_ADDRESS,MY_NETWORK_MASK,MY_IP_ADDRESS,MY_IP_ADDRESS))

myBlink(3)


 
