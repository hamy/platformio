#include <Arduino.h>

void setup() { pinMode(LED_BUILTIN, OUTPUT); }

int loopCounter = 0;

/**
 * Drive LED in PWM mode.
 * The argument value (between 0 and 127) is scaled to use the full PWM range
 * 0...255.
 */
void driveLEDviaPWM(int valueBetween0and127) {
  int scaledValue = 2 * valueBetween0and127;
  analogWrite(LED_BUILTIN, scaledValue);
}

/**
 * Drive LED as on/off indicator.
 * From argument value (between 128 and 255), one bit is tested as on/off
 * criterion. If this bit is not the LSB, this also works as frequency divider.
 */
void driveLEDviaOnOffSwitch(int valueBetween128and255) {
  int onOffSwitch = 2 * valueBetween128and255 & 0x10;
  digitalWrite(LED_BUILTIN, onOffSwitch);
}

void loop() {

  // rollover at 255
  int valueBetween0and255 = loopCounter & 0xff;

  if (valueBetween0and255 < 128) {
    // half of the time, use PWM with an appropriately scaled value
    driveLEDviaPWM(valueBetween0and255);
  } else {
    // for the remaining time, create square waves at reduced frequency
    driveLEDviaOnOffSwitch(valueBetween0and255);
  }

  // wait for 20 milliseconds and prepare next loop
  delay(20);
  loopCounter++;
}
