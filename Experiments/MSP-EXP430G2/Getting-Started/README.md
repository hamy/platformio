# MSP-EXP430G2: Getting Started

This is the "Hello World" sample of the MSP-EXP430G2 LaunchPad development kit.

## PlatformIO Commands to Initialize this Subproject

Hint: all `platformio` / `pio` commands should be run from the top-level directory
of the subproject-specific file tree (i.e., the directory that contains
the `platformio.ini` leaf for that subproject).

`pio init --board=lpmsp430g2553`  is used to create the skeleton file tree for the
new subproject. This command should only be run at the beginning of the
subproject. It will create (among others)
* ```platformio.ini``` The PlatformIO project descriptor
* [src](src/) an empty source tree that will be populated with the appropriate source
    files.
  
You might delete the unused directories
* `include`
* `lib`
* `test`

## Creating Support Files for a Specific IDE 

The `pio init` command may also be used to create support files for an integrated
development environment like Eclipse, VS Code or even Emacs or Vim. For this purpose,
use the `--ide` option with an identifier that denotes the specific IDE. For example,
support files for Visual Studio Codee IDE are created by the command  
`pio init --board=lpmsp430g2553 --ide=vscode` 

## Compiling and Linking the Sources, Flashing the Microcontroller

* `pio run`   compiles the sources of the subproject and creates the
  hex file that might be uploaded to the microcontroller.

* `pio run --target=upload` flashes the microcontroller.

## Clean-up Operations

* `pio run --target=clean` removes all artefacts

## Creating Detailed Log Output

Use the option `--verbose` for a detailed description of the operations, e.g.  
`pio run --verbose target=clean`

## Sample Output

```
Sketch "Getting-Started" starting...

at loop start
toggling LEDs, toggleFlag=1
waiting for loop end...
at loop end

at loop start
toggling LEDs, toggleFlag=0
waiting for loop end...
at loop end

at loop start
toggling LEDs, toggleFlag=1
waiting for loop end...
at loop end

```
  