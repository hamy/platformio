/*
 * main.cpp
 * Once per second, the built-in LEDs (both the red and the green LED) are
 * toggled.
 */

#include <Energia.h>

void setup() {
  Serial.begin(9600);
  pinMode(RED_LED, OUTPUT);
  pinMode(GREEN_LED, OUTPUT);
  Serial.println(F("Sketch \"Getting-Started\" starting..."));
}

boolean toggleFlag = false;

void loop() {
  Serial.println();
  Serial.println(F("at loop start"));
  toggleFlag = !toggleFlag;
  Serial.print(F("toggling LEDs, toggleFlag="));
  Serial.println(toggleFlag);
  digitalWrite(RED_LED, toggleFlag);
  digitalWrite(GREEN_LED, !toggleFlag);
  Serial.println(F("waiting for loop end..."));
  delay(1000);
  Serial.println(F("at loop end"));
}
