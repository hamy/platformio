/*
 * main.cpp
 * Once per second, the built-in LED is toggled.
 */

#include <Arduino.h>

void setup()
{
  Serial.begin(9600);
  Serial.print(F("The built-in LED uses pin "));
  Serial.print(LED_BUILTIN);
  Serial.println(F("."));
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop()
{
  Serial.println(F("on"));
  digitalWrite(LED_BUILTIN, HIGH);
  delay(1000);
  Serial.println(F("off"));
  digitalWrite(LED_BUILTIN, LOW);
  delay(1000);
}
