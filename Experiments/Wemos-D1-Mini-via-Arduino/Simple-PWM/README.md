# Simple PWM with Wemos D1 Mini using the Arduino Framework

## The C Sketch

Simple PWM, no rocket science. But some remarks:
* The PWM input range is 0..1023 (since the MCU uses a 10 bit granularity).
* If the voltage at the pin of the built-in LED is HIGH, the LED is dark.
* Because of the Weber-Fechner law, the duty cycle is not varied linearly, but
  uses a geometric sequence (at each step, the duty cycle is doubled), with
  the exception of the value for complete darkness.
  Otherwise, it would be difficult to recognize changes near the maximum brightness.

[main.cpp](src/main.cpp) 

## Project Configuration

[platformio.ini](platformio.ini)

I had to add the three options 

```
upload_speed = 115200
board_build.f_flash = 20000000L
board_build.flash_mode = dio
```

since otherwise, I run into flashing problems.
