#include <Arduino.h>

void setup() {
    Serial.begin(9600);
    pinMode(LED_BUILTIN,OUTPUT);
}

// Caveat 1: PWM input value: 0..1023 (10 bit)
// Caveat 2: duty cycle is inverted 
#define PWM_MIN 0
#define PWM_MAX 1024 
#define PWM_MULT 2
int pwmValue=PWM_MIN;

int normalizePwmValue(int value) {
    value = PWM_MAX - value;
    if (value<0) value=0;
    if (value>1023) value=1023;
    return value;
}

void loop() {
    Serial.print(F("PWM value: "));
    Serial.println(pwmValue);
    analogWrite(LED_BUILTIN, normalizePwmValue(pwmValue));
    delay(200);
    if (pwmValue<1) {
        pwmValue = 1;
    } else {
        pwmValue *= PWM_MULT;
    }
    if (pwmValue>PWM_MAX) pwmValue=PWM_MIN;
}
