/*
 *  This sketch demonstrates how to scan WiFi networks. 
 *  The API is almost the same as with the WiFi Shield library, 
 *  the most obvious difference being the different file you need to include:
 */
#include <Arduino.h>
#include "ESP8266WiFi.h"

void setup() {
  Serial.begin(115200);

  // Set WiFi to station mode and disconnect from an AP if it was previously connected
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  delay(100);

  Serial.println(F("Setup done"));
}

void loop() {
  Serial.println(F("scan start"));

  // WiFi.scanNetworks will return the number of networks found
  int n = WiFi.scanNetworks();
  Serial.println(F("scan done"));
  if (n == 0)
    Serial.println(F("no networks found"));
  else
  {
    Serial.print(n);
    Serial.println(F(" networks found"));
    for (int i = 0; i < n; ++i)
    {
      Serial.println();
      Serial.print(F("network #"));
      Serial.println(i + 1);
      Serial.print(F("SSID: "));   
      Serial.println(WiFi.SSID(i));
      Serial.print(F("BSSIDstr: "));   
      Serial.println(WiFi.BSSIDstr(i));
      Serial.print(F("RSSI: "));
      Serial.println(WiFi.RSSI(i));
      Serial.print(F("encryption type: "));
      int encryptionType = WiFi.encryptionType(i);
      Serial.print(encryptionType);
      Serial.print(F(" ("));
      switch (encryptionType) {
      case ENC_TYPE_NONE: Serial.print(F("ENC_TYPE_NONE")); break;
      case ENC_TYPE_AUTO: Serial.print(F("ENC_TYPE_AUTO")); break;
      case ENC_TYPE_WEP: Serial.print(F("ENC_TYPE_WEP")); break;
      case ENC_TYPE_TKIP: Serial.print(F("ENC_TYPE_TKIP")); break;
      case ENC_TYPE_CCMP: Serial.print(F("ENC_TYPE_CCMP")); break;
      default: Serial.print(F("Oops: unknown")); break;
      }
      Serial.println(F(")"));
      Serial.print(F("channel: "));
      Serial.println(WiFi.channel(i));
      Serial.print(F("hidden? "));
      Serial.println(WiFi.isHidden(i));
      delay(10);
    }
  }
  Serial.println("");

  // Wait a bit before scanning again
  delay(5000);
}
