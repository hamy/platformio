# Wemos D1 Mini: Wifi Scan

## Arduino Sketch

* Original sketch: https://github.com/platformio/platform-espressif8266/blob/master/examples/arduino-wifiscan/src/WiFiScan.ino
* C,CPP library header files: https://github.com/esp8266/Arduino/tree/master/libraries/ESP8266WiFi/src
* [main.cpp](src/main.cpp)

## Flashing Problems

Sometimes, flashing via `pio run target=upload` failed.
Therefore, several options were  added at the end of 
[platformio.ini](platformio.ini).
