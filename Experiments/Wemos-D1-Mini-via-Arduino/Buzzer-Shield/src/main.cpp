#include <Arduino.h>

// The default pin for the buzzer (may be changed by resoldering)
#define BUZZER_PIN D5

// The low and high frequencies in Hertz
#define TONE_FREQ_LOW 1400
#define TONE_FREQ_HIGH 1700

// The tone duration in milliseconds
#define TONE_DURATION 90

// The toggle interval in milliseconds (must be at least TONE_DURATION)
#define TOGGLE_DURATION 100

void setup() {
  Serial.begin(9600);
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(BUZZER_PIN, OUTPUT);
}

void loop() {

  // low frequency phase
  digitalWrite(LED_BUILTIN, LOW);
  tone(BUZZER_PIN, TONE_FREQ_LOW, TONE_DURATION);
  delay(TOGGLE_DURATION);
  
  // high frequency phase
  digitalWrite(LED_BUILTIN, HIGH);
  tone(BUZZER_PIN, TONE_FREQ_HIGH, TONE_DURATION);
  delay(TOGGLE_DURATION);
}
