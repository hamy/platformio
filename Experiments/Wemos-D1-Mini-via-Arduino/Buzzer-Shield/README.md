# Using Wemos D1 Mini with the Buzzer Shield

![shield.jpg](images/shield.jpg)

(image source: [buzzer shield product page](https://wiki.wemos.cc/products:d1_mini_shields:buzzer_shield))


## The C Sketch

 
[main.cpp](src/main.cpp)

## Project Configuration

[platformio.ini](platformio.ini)

I had to add the three options 

```
upload_speed = 115200
board_build.f_flash = 20000000L
board_build.flash_mode = dio
```

since otherwise, I run into flashing problems.
