# Wemos D1 Mini : Dump the Contents of the Flash Memory

The ESP class provides a function  that allows to access the flash.
 
The header file `ESP.h` can be found under the address
https://github.com/esp8266/Arduino/blob/master/cores/esp8266/Esp.h


## The C Sketch
 
[main.cpp](src/main.cpp)

## Project Configuration

[platformio.ini](platformio.ini)

I had to add the three options 

```
upload_speed = 115200
board_build.f_flash = 20000000L
board_build.flash_mode = dio
```

since otherwise, I run into flashing problems.

