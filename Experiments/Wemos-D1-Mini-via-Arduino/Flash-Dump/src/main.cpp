
#include <Arduino.h>
#include <c_types.h>
#include <stdio.h>

#define BUFFER_LENGTH_IN_WORDS 64
#define BUFFER_LENGTH_IN_BYTES ((BUFFER_LENGTH_IN_WORDS)*4)

static uint32_t buffer[BUFFER_LENGTH_IN_WORDS];
uint32_t bufferWordOffset = 0;
uint32_t bufferByteOffset = 0;
static char text[32];

void clearBuffer() { memset(buffer, 0, BUFFER_LENGTH_IN_BYTES); }

void setup() { Serial.begin(115200); }

void loop() {
  bufferByteOffset = bufferWordOffset * 4;
  sprintf(text, "%08x", bufferByteOffset);
  Serial.print(text);
  Serial.print(": ");
  clearBuffer();
  int rc = ESP.flashRead(bufferWordOffset, buffer, BUFFER_LENGTH_IN_BYTES);
  sprintf(text, "%04x", rc);
  Serial.print(text);
  Serial.print(": ");
  uint8_t *ptr = (uint8_t *)buffer;
  for (int i = 0; i < BUFFER_LENGTH_IN_BYTES; i++) {
    int ch = *ptr++;
    sprintf(text, "%02x", ch);
    Serial.print(text);
  }
  Serial.println();
  bufferWordOffset += BUFFER_LENGTH_IN_WORDS;
}
