# Wemos D1 Mini: Wifi Web Server Within KA-WLAN

## Arduino Sketch

* Original sketch: https://raw.githubusercontent.com/platformio/platform-espressif8266/master/examples/arduino-webserver/src/HelloServer.ino
* C,CPP library header files: https://github.com/esp8266/Arduino/tree/master/libraries/ESP8266WiFi/src
* [main.cpp](src/main.cpp)
* Connection is possible from a tablet that is also connected through KA-WLAN. 

## Flashing Problems

Sometimes, flashing via `pio run target=upload` failed.
Therefore, several options were  added at the end of 
[platformio.ini](platformio.ini).
