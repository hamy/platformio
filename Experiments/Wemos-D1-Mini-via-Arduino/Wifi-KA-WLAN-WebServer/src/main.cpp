#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

const char* ssid = "KA-WLAN";
const char* password = NULL;

ESP8266WebServer server(80);

int messageCounter = 0;
String message;

void ledOn() {
  digitalWrite(LED_BUILTIN,false);
}

void ledOff() {
    digitalWrite(LED_BUILTIN,true);
}


void prologue() {
  ledOn();
  messageCounter++;
  message = "\nRequest#";
  message += messageCounter;
  message += "\nURI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET)?"GET":"POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i=0; i<server.args(); i++){
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
}

void epilogue() {
    delay(10);
    ledOff();
}

void handleRoot() {
  prologue();
  server.send(200, "text/plain", message + "hello from esp8266!");
  epilogue();
}

void handleNotFound(){
  prologue();
  server.send(404, "text/plain", message + "File Not Found\n");
  epilogue();
}

void setup(void){
  pinMode(LED_BUILTIN, OUTPUT);
  ledOff();
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.println("");

  // Wait for connection
  ledOn();
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  ledOff();
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  if (MDNS.begin("esp8266")) {
    Serial.println("MDNS responder started");
  }

  server.on("/", handleRoot);

  server.on("/inline", [](){
    prologue();
    server.send(200, "text/plain", message + "this works as well");
    epilogue();
  });

  server.onNotFound(handleNotFound);

  server.begin();
  Serial.println("HTTP server started");
}

void loop(void){
  server.handleClient();
}
