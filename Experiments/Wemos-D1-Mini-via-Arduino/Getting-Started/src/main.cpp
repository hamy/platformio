#include <Arduino.h>

void setup() {
    Serial.begin(9600);
    pinMode(LED_BUILTIN,OUTPUT);
}

boolean ledFlag = false;

void loop() {
    ledFlag = !ledFlag;
    int adcValue = analogRead(0);
    Serial.print(F("ledFlag: "));
    Serial.print(ledFlag);
    Serial.print(F(", ADC value: "));
    Serial.println(adcValue);
    digitalWrite(LED_BUILTIN, ledFlag);
    delay(500);
}