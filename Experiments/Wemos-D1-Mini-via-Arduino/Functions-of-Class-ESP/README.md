# Wemos D1 Mini : Display the Results of Certain Functions of the ESP Class

The ESP class provides several static functions that describe properties
of the current MCU instance; e.g., the chip id or the size of the
current sketch that was flashed onto the MCU. Hint: check the results returned
by those functions against the values returned by the command-line tool
`esptool`.

The header file `ESP.h` can be found under the address
https://github.com/esp8266/Arduino/blob/master/cores/esp8266/Esp.h


## The C Sketch
 
[main.cpp](src/main.cpp)

## Project Configuration

[platformio.ini](platformio.ini)

I had to add the three options 

```
upload_speed = 115200
board_build.f_flash = 20000000L
board_build.flash_mode = dio
```

since otherwise, I run into flashing problems.

## Results

The following snippet was captured by the device monitor:

```
getVcc(): 65535
getChipId(): 0x8A86E7
getFreeHeap(): 52032
getSdkVersion(): 2.2.1(cfd48f3)
getCoreVersion(): 2_4_2
getFullVersion(): SDK:2.2.1(cfd48f3)/Core:2.4.2/lwIP:2.0.3(STABLE-2_0_3_RELEASE/glue:arduino-2.4.1-13-g163bb82)/BearSSL:6d1cefc
getBootVersion(): 31
getBootMode(): 1
getCpuFreqMHz(): 80
getFlashChipId(): 0x1640EF
getFlashChipSize(): 4194304
getFlashChipSpeed(): 20000000
getFlashChipMode(): 2
getFlashChipSizeByChipId(): 4194304
getSketchSize(): 259248
getSketchMD5(): a36bf5a2f204ab42ee313397f6a5cc49
getFreeSketchSpace(): 2883584
getResetReason(): External System
getResetInfo(): Fatal exception:0 flag:6 (EXT_SYS_RST) epc1:0x00000000 epc2:0x00000000 epc3:0x00000000 excvaddr:0x00000000 depc:0x00000000
getCycleCount(): 3510680098
```