#include <Arduino.h>

#define show(what) \
    Serial.print(F("" #what ": ")); \
    Serial.println(ESP.what);

#define showx(what) \
    Serial.print(F("" #what ": 0x")); \
    Serial.println(ESP.what,HEX);

void showAll() {
    show(getVcc())
    showx(getChipId())
    show(getFreeHeap())
    show(getSdkVersion())
    show(getCoreVersion())
    show(getFullVersion())
    show(getBootVersion())
    show(getBootMode())
    show(getCpuFreqMHz())
    showx(getFlashChipId())
    show(getFlashChipSize())
    show(getFlashChipSpeed())
    show(getFlashChipMode())
    show(getFlashChipSizeByChipId())
    show(getSketchSize())
    show(getSketchMD5())
    show(getFreeSketchSpace())
    show(getResetReason())
    show(getResetInfo())
    show(getCycleCount())
}        

void setup() {
    Serial.begin(9600);
}

void loop() {
    showAll();
    delay(1000);
}
