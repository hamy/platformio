#include <Arduino.h>

// The default pin for the buzzer (may be changed by resoldering)
#define BUZZER_PIN D5

// The low and high frequencies in Hertz
#define TONE_FREQ_LOW 100
#define TONE_FREQ_HIGH 4000
long freq=TONE_FREQ_LOW;

// The tone duration in milliseconds
#define TONE_DURATION 90

// The toggle interval in milliseconds (must be at least TONE_DURATION)
#define TOGGLE_DURATION 100

boolean ledFlag=false;

void setup() {
  Serial.begin(9600);
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(BUZZER_PIN, OUTPUT);
}

void loop() {

  ledFlag = !ledFlag;    
  digitalWrite(LED_BUILTIN, ledFlag);
  tone(BUZZER_PIN, (int)freq, TONE_DURATION);
  delay(TOGGLE_DURATION);
 
  // increment the frequency by a coarse approximation to a semitone
  freq *= 1059;
  freq /= 1000; 
  if (freq>TONE_FREQ_HIGH) freq = TONE_FREQ_LOW;
}
