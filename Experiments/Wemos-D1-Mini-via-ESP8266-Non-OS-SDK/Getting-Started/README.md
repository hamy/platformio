# Getting Started with Wemos D1 Mini using the Arduino Framework


## The C Sketch

This is a simple *Hello World* sketch. It
* makes the built-in blue LED blink with a frequency of 1 Hz and a duty cycle of 50%, and
* converts an analog voltage read from Pin A0 to a digital value and prints that value.

[main.cpp](src/main.cpp)

## Project Configuration

[platformio.ini](platformio.ini)

I had to add the three options 

```
upload_speed = 115200
board_build.f_flash = 20000000L
board_build.flash_mode = dio
```

since otherwise, I run into flashing problems.
