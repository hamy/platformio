# Thermal Imaging Camera using MLX90640

This experiment uses the 
[Pimoroni breakout board for the Melexis MLX90640](../../Sensors/Pimoroni-MLX90640-Breakout-Board/) 
32x24 thermopile matrix to create a thermal image. The image data 
are written als CSV values to the serial interface. The MCU does not perform any image rendering, this
is done on the desktop or a remote computer. Remember that it is very easy to publish the temperature data
via MQTT, e.g. using the following command:

```
pio device monitor | mosquitto_pub  -t thermal-imaging-camera -l
```

This commands continuously publishes the thermal imaging data to the topic ```thermal-imaging-camera```.
Each MQTT message contains a single text line of 32x24 temperature values as CSV. Rendering those
data can be performed by MQTT subscribers of the topic ```thermal-imaging-camera```.


## Choosing the Appropriate MLX90640 Library

Using Google to search for MLX90640 libraries, I got several hits for different MCU
platforms. IMHO, they were all more or less derived from  Melexis' original 
[GitHub library](https://github.com/melexis/mlx90640-library) provided by the
Melexis Open Source Term (just look at the copyright statements at the 
beginning of the C sources of the derived versions...). 
However, when compiling the Melexis original version  within the
PlatformIO / Teensy 3.5 environment, I run into difficulties; 
e.g. it tries to include ```mbed.h``` which is inappropriate for the Arduino framework.

So, it is necessary to look at the fine print within those derived versions.
Code a little, test a little, SNAFU. Finally,
I found [Sparkfun's  MLX90640 Arduino Example](https://github.com/sparkfun/SparkFun_MLX90640_Arduino_Example)
of the MLX90640 library, which worked after moving the C sources and header files
to the appropriate locations within the ```lib``` branch of the PlatformIO 
project tree. The steps for installing that library are:

* Download and unzip Sparkfun's library into an auxiliary directory (e.g., into ```/tmp```)
* There, go to the subdirectory ```Example1_BasicReadings```
* Copy the ```MLX90640_...```  files 
  to the [```lib```](lib/) branch of your PlatformIO project **strictly** following the rules described 
  in the file [README](lib/README) that was created  in the ```lib``` branch by the command
  ```pio init --board=teensy35``` that you executed in the salad days of your project. You should end up
  with the following tree:

```
lib
├── MLX90640_API
│   └── src
│       ├── MLX90640_API.cpp
│       └── MLX90640_API.h
├── MLX90640_I2C_Driver
│   └── src
│       ├── MLX90640_I2C_Driver.cpp
│       └── MLX90640_I2C_Driver.h
└── README
```

* Copy the main Arduino sketch ```Example1_BasicReadings.ino``` to 
  [```src/main.cpp```](src/main.cpp).
* In ```src/main.cpp```, add a forward declaration ```boolean isConnected;``` before the ```setup()``` function.

Note that the library is provided by Sparkfun for their [own MLX90640 breakout board](https://www.sparkfun.com/products/14844), 
but it also works for the Pimoroni board.




## The Main Program

[main.cpp](src/main.cpp) invokes the functions of the [Melexis API](lib/MLX90640_API/src/MLX90640_API.h)
and of the underlying [I2C Driver](lib/MLX90640_I2C_Driver/src/MLX90640_I2C_Driver.h). Each loop provides
the data of the thermal image as a single text line via the serial interface.
The line contains the 32x24=768 temperature values (in °C) as comma-separated floating point numbers in decimal representation. See the following snippet
for an example of the data line:

```
27.66,26.84,27.73,28.06,27.99,27.85,27.42,27.52,27.38,26.79,26.78,27.04,27.55,27.81,27.30,26.50,26.95,26.20,26.99,26.90,27.45,26.64,27.04,27.15,27.42,27.07,27.42,26.99,27.60,26.81,28.38,27.39,
26.14,27.01,27.65,27.67,26.98,27.48,26.89,27.62,26.45,27.35,27.33,26.93,26.87,26.66,26.63,26.81,27.06,26.69,26.46,27.42,27.12,27.15,26.93,27.24,26.83,27.41,26.84,27.59,27.19,27.75,27.93,29.20,
27.61,26.19,26.72,27.35,27.15,27.14,26.98,27.21,26.96,26.32,26.41,26.30,27.15,26.58,26.15,26.76,26.58,26.05,26.28,26.25,26.44,26.52,27.11,27.16,27.22,26.57,27.15,27.46,27.07,27.10,27.94,27.11,
26.71,26.61,27.33,27.67,27.03,26.62,26.51,27.10,26.23,26.64,26.51,25.99,26.25,25.90,26.29,26.83,26.28,26.72,25.79,27.18,26.33,26.63,26.79,27.23,27.10,26.90,26.82,27.13,27.45,27.69,27.02,28.01,
26.29,26.36,26.56,27.07,26.82,26.42,26.12,26.21,26.52,26.22,26.29,26.23,25.57,26.67,26.19,26.16,26.52,25.99,26.02,26.09,26.34,25.52,26.22,26.50,26.80,26.95,27.41,26.75,27.26,26.82,27.11,27.11,
26.43,26.29,26.28,26.96,26.32,26.35,26.27,26.09,26.22,26.09,26.23,26.50,26.04,26.20,26.30,26.22,25.69,26.43,26.14,25.98,26.22,25.98,26.53,26.58,26.69,26.86,27.09,26.79,27.13,26.90,27.28,27.94,
26.78,26.55,26.93,26.80,25.93,26.23,26.43,26.90,26.29,25.85,25.89,26.27,26.17,25.79,25.73,26.12,26.68,26.56,25.79,26.09,26.38,25.34,26.25,26.44,27.12,26.65,26.65,27.15,26.73,26.53,27.31,27.18,
26.24,26.44,26.20,26.92,26.47,26.16,25.75,26.79,26.57,26.35,26.20,25.96,26.09,26.09,26.38,26.01,25.84,26.45,25.89,26.36,26.04,26.38,26.17,26.72,26.42,26.77,26.96,27.40,27.23,27.26,26.95,27.76,
26.46,26.46,26.03,26.42,26.67,26.15,26.18,26.44,26.40,26.25,26.02,26.29,26.67,25.67,25.98,26.56,26.23,26.11,26.18,26.02,25.95,25.69,25.69,26.24,27.24,27.06,26.75,27.45,27.09,27.11,27.11,27.02,
25.92,27.28,26.35,26.12,26.17,26.24,26.89,26.49,25.93,26.19,26.10,26.39,26.44,26.13,26.46,26.27,25.96,25.86,25.75,25.91,26.21,25.64,26.17,26.65,26.56,26.97,26.29,26.87,26.08,26.97,27.00,27.55,
26.68,26.49,26.26,26.25,26.28,26.08,26.01,26.28,26.49,26.44,25.71,26.60,26.03,25.83,26.17,26.22,26.06,25.73,26.38,26.36,26.32,25.86,25.78,26.05,26.91,26.40,26.90,26.92,26.94,26.45,27.22,27.12,
26.17,26.89,26.22,26.81,26.02,26.43,26.36,25.99,25.65,26.19,26.01,26.48,25.79,26.15,26.11,26.14,25.78,25.86,25.93,26.43,25.87,25.98,26.26,26.28,26.61,26.49,26.83,27.58,27.45,26.80,27.12,27.22,
26.61,25.98,26.19,26.41,26.64,25.82,26.13,26.40,26.36,26.17,26.24,26.65,26.07,26.10,26.28,25.64,25.93,26.10,26.23,25.88,25.91,26.03,26.26,26.36,26.72,26.68,26.91,27.18,27.63,27.19,26.62,27.63,
26.31,26.37,26.12,26.90,26.57,26.57,26.28,26.48,26.27,26.08,26.18,26.01,25.65,26.06,25.86,26.41,25.85,26.42,25.80,26.70,25.80,26.30,26.18,26.40,26.44,26.79,26.62,27.01,26.67,26.60,27.25,28.14,
26.87,26.43,26.61,26.34,26.60,26.25,26.34,26.56,26.35,25.99,26.74,26.40,26.26,26.03,26.04,26.17,25.81,26.00,26.53,26.05,26.28,25.98,26.08,26.14,26.54,26.26,26.70,26.83,27.17,26.38,27.27,27.45,
26.35,26.39,26.13,26.50,26.15,26.18,26.09,26.46,26.25,25.77,25.98,25.93,25.47,26.18,25.99,26.41,25.89,26.10,25.92,26.12,26.36,26.29,26.18,25.82,26.44,26.91,26.79,27.27,26.19,27.11,26.91,27.51,
27.04,26.24,26.73,26.44,26.59,26.03,26.42,26.88,26.08,25.92,25.73,26.18,26.11,26.04,26.08,26.12,26.11,26.31,26.71,26.07,26.38,25.99,25.94,26.62,26.32,26.40,27.11,27.34,27.57,26.96,27.31,27.42,
26.09,26.18,26.25,27.18,25.96,26.39,26.19,26.00,25.98,26.06,25.71,26.46,26.05,26.20,25.65,26.58,25.85,26.09,26.27,26.35,26.09,26.48,25.86,26.12,27.01,26.90,26.57,27.39,26.79,27.04,27.00,27.49,
26.64,27.04,26.63,26.34,26.65,25.76,26.02,26.47,26.50,26.07,26.14,25.87,26.32,26.27,26.45,26.35,26.57,25.95,26.20,26.43,26.49,26.22,26.59,26.62,26.07,25.92,27.25,27.52,27.37,26.65,27.46,27.56,
26.29,26.79,26.61,26.66,26.39,26.54,26.41,26.58,26.03,26.40,26.27,26.24,25.85,26.74,25.82,26.42,25.41,27.00,26.31,26.49,26.41,26.51,25.75,26.69,26.17,26.62,26.09,27.57,27.02,27.44,27.61,27.43,
27.28,26.72,26.71,26.92,26.37,26.33,26.44,26.62,26.03,26.03,26.42,26.44,26.35,25.70,26.54,26.66,26.38,26.37,26.37,27.11,26.67,26.16,26.56,26.35,25.97,26.36,26.99,27.21,27.72,26.73,27.92,28.20,
26.06,27.12,26.50,27.11,26.31,26.50,25.81,26.79,26.18,26.20,26.79,26.79,26.30,26.63,26.11,26.79,26.10,26.89,26.65,26.62,25.62,26.47,26.47,26.63,26.65,26.66,26.65,27.06,27.13,27.13,27.29,28.09,
26.43,27.37,26.58,27.26,27.03,26.17,26.46,26.49,26.08,26.29,26.37,26.34,26.63,27.01,26.58,26.57,26.47,26.05,26.94,26.98,26.58,25.78,26.39,26.62,26.42,25.91,27.27,27.37,27.43,26.87,27.56,27.40,
26.00,26.79,26.86,26.92,26.21,26.71,26.55,27.16,26.28,26.40,26.78,27.16,25.87,26.53,26.64,27.02,25.91,26.52,26.75,26.98,26.36,26.83,26.60,26.80,26.42,26.64,26.81,27.75,27.40,27.90,27.79,27.99,
```

Note: in the text above, the long line was artificially split into parts (each containing 24 values) for better readability.


