# Getting Started with ArduinoJson: Serialization

This is the starter project for ArduinoJson. The main program
[main.cpp](src/main.cpp) is derived from
the Arduino example sketch ```JsonGeneratorExample``` that comes with
the ArduinoJson library. 