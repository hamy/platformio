# Getting Started with ArduinoJson: Deserialization

This is a starter project for ArduinoJson. The main program
[main.cpp](src/main.cpp) is derived from
the Arduino example sketch ```JsonParserExample``` that comes with
the ArduinoJson library. 