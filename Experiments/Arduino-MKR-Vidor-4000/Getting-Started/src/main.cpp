/*
 * main.cpp
 */

#include <Arduino.h>

void setup()
{
  Serial.begin(9600);
  Serial.print(F("The built-in LED uses pin "));
  Serial.print(LED_BUILTIN);
  Serial.println(F("."));
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop()
{
  Serial.print(F("pin "));
  Serial.print(LED_BUILTIN);
  Serial.println(F(" on"));
  digitalWrite(LED_BUILTIN, HIGH);
  delay(980);
  Serial.print(F("pin "));
  Serial.print(LED_BUILTIN);
  Serial.println(F(" off"));
  digitalWrite(LED_BUILTIN, LOW);
  delay(20);
}
