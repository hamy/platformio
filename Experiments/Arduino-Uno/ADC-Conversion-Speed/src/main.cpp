#include <Arduino.h>

// macros for setting and clearing register bits
#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif

#define BAUD_RATE 9600
#define NUMBER_OF_CONVERSIONS 100000
#define ANALOG_INPUT_PIN_MIN 0
#define ANALOG_INPUT_PIN_MAX 5

int allDone = 0;
int blink = LOW;

void setup()
{
  Serial.begin(BAUD_RATE);
  pinMode(LED_BUILTIN, OUTPUT);
}

void setADCPrescaleBit(int flag, int bitPos)
{
  if (flag)
  {
    sbi(ADCSRA, bitPos);
  }
  else
  {
    cbi(ADCSRA, bitPos);
  }
}

void setADCPrescaleBits(int adcpsBit2, int adcpsBit1, int adcpsBit0)
{
  setADCPrescaleBit(adcpsBit2, ADPS2);
  setADCPrescaleBit(adcpsBit1, ADPS1);
  setADCPrescaleBit(adcpsBit0, ADPS0);
}

unsigned long measureConversionRate(int analogInputPin)
{
  blink = !blink;
  digitalWrite(LED_BUILTIN, blink);
  unsigned long usecBeforeMeasurement = micros();
  for (long i = 0; i < NUMBER_OF_CONVERSIONS; i++)
  {
    analogRead(analogInputPin);
  }
  unsigned long usecAfterMeasurement = micros();
  unsigned long result = usecAfterMeasurement - usecBeforeMeasurement;
  return result;
}

void printComma()
{
  Serial.print(",");
}

void printMeasurementTitle()
{
  Serial.println(F("adcpsBit2,adcpsBit1,adcpsBit0,loopCount,usecA0,usecA1,usecA2,usecA3,usecA4,usecA5"));
  for (int bit = 2; bit >= 0; bit--)
  {
    Serial.print(F("adcpsBit"));
    Serial.print(bit);
    printComma();
  }
  Serial.print(F("loopCount"));
  for (int analogInputPin = ANALOG_INPUT_PIN_MIN; analogInputPin <= ANALOG_INPUT_PIN_MAX; analogInputPin++)
  {
    printComma();
    Serial.print(F("usecA"));
    Serial.print(analogInputPin);
  }
  Serial.println();
}

void printMeasurementBegin(int adcpsBit2, int adcpsBit1, int adcpsBit0)
{
  Serial.print(adcpsBit2);
  printComma();
  Serial.print(adcpsBit1);
  printComma();
  Serial.print(adcpsBit0);
  printComma();
  Serial.print(NUMBER_OF_CONVERSIONS);
}

void printMeasurementResults(unsigned long usec)
{
  printComma();
  Serial.print(usec);
}

void printMeasurementEnd()
{
  Serial.println();
}

void loop()
{
  if (allDone)
  {
    return;
  }
  printMeasurementTitle();

  for (int adcpsBit2 = 0; adcpsBit2 < 2; adcpsBit2++)
  {
    for (int adcpsBit1 = 0; adcpsBit1 < 2; adcpsBit1++)
    {
      for (int adcpsBit0 = 0; adcpsBit0 < 2; adcpsBit0++)
      {
        setADCPrescaleBits(adcpsBit2, adcpsBit1, adcpsBit0);
        printMeasurementBegin(adcpsBit2, adcpsBit1, adcpsBit0);
        for (int analogInputPin = ANALOG_INPUT_PIN_MIN; analogInputPin <= ANALOG_INPUT_PIN_MAX; analogInputPin++)
        {
          unsigned long usec = measureConversionRate(analogInputPin);
          printMeasurementResults(usec);
        }
        printMeasurementEnd();
      }
    }
  }

  allDone = 1;
  digitalWrite(LED_BUILTIN, LOW);
}
