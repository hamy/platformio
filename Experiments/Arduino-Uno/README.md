# Arduino Uno Experiments

* [Getting Started](Getting-Started/) - A trivial PlatformIO "Hello World" starter project 
  using the Arduino Uno board and the Arduino framework.
* [Processing RX/TX Data on the Desktop](Processing-RX-TX-Data-on-the-Desktop/) - Another kindergarten project 
  that demonstrates different methods for displaying or capturing serial data sent by the microcontroller.
* [ADC Conversion Speed](ADC-Conversion-Speed/) - Controlling the speed of analog-digital conversion by selecting
  appropriate clock prescaling factors.
* [Pololu VL53L0X Time-of-Flight Distance Sensor Carrier](Pololu-VL53L0X-Time-of-Flight-Distance-Sensor-Carrier/)
