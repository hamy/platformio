/*
 * main.cpp
 * Performs ADC operations for the analog input ports A0 through A5
 * and writes the conversion results and the minimum and maximum possible
 * values as line of 8 comma separated tokens to the serial connector.
 */

#include <Arduino.h>

#define BAUD_RATE 9600
#define DELAY_MSEC 100

void setup()
{
  Serial.begin(BAUD_RATE);
}

void loop()
{
  // ADC for port 0..5
  for (int i = 0; i < 6; i++)
  {
    int value = analogRead(i);
    Serial.print(value);
    Serial.print(",");
  }
  // add minimum and maximum possible value
  Serial.println("0,1023");
  // sleep until next loop
  delay(DELAY_MSEC);
}
