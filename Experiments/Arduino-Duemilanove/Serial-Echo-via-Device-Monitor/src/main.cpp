#include <Arduino.h>

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(9600);
  while (!Serial)
    continue;
  Serial.println(F("setup done"));
}

void loop() {
  while (Serial.available() > 0) {
    int ch = Serial.read();
    digitalWrite(LED_BUILTIN, ch & 1);
    Serial.print(F("read character: "));
    Serial.write(ch);
    Serial.flush();
    Serial.println();
  }
}
