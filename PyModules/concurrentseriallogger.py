# Concurrent Serial Logger

import sys
import time
import threading

class ConcurrentSerialLogger:

    def __init__(self, serial, idleDelay=0.001, logPath=None):
        self.serial = serial
        self.idleDelay = idleDelay
        self.logPath = logPath
        self.running = False
        self.runner = threading.Thread(target=self._runner,daemon=True)
        self.logFile = None
        print('ConcurrentSerialLogger uses the following properties:')
        print('serial:', self.serial)
        print('idleDelay:', self.idleDelay,'seconds')
        print('logPath:', self.logPath)

    def isRunning(self):
        return self.running

    def start(self):
        print('logger starts...')
        while self.serial.in_waiting>0:
            omitted = self.serial.read()
        if self.logPath:
            self.logFile = open(self.logPath,'w')
        self.running = True
        self.runner.start()

    def stop(self):
        print('logger stops...')
        self.running = False
        self.runner.join()
        if self.logPath:
            self.logFile.close()
            self.logFile = None
        
    def _runner(self):
        while self.running:
            while self.serial.in_waiting>0 and self.running:
                by = None
                by= self.serial.read()
                try:
                    by = by.decode(encoding="utf-8")
                    if self.logPath:
                        self.logFile.write(by)
                    else:
                        print(by,end='')
                except:
                    if self.logPath:
                        self.logFile.write('?')
                    else:
                        print('?',end='')
                      
            time.sleep(self.idleDelay)
                