# Extending the Search Path for Python Modules
import sys

print(sys.path)

__hamy_pymodules_path = '/home/hamy/Projects/PlatformIO/PyModules' 
if __hamy_pymodules_path in sys.path:
    print('Hint: sys.path already contains',__hamy_pymodules_path)
else:
    sys.path.append(__hamy_pymodules_path)
    print('Hint: sys.path was extended by',__hamy_pymodules_path)

# print('sys.path:')
print(sys.path)
